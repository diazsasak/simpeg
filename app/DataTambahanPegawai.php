<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class DataTambahanPegawai extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['no_surat_ket_sehat_dokter', 'tanggal_surat_ket_sehat_dokter', 'no_surat_ket_bebas_narkoba', 'tanggal_surat_ket_bebas_narkoba', 'no_surat_ket_catatan_polisi', 'tanggal_surat_ket_catatan_polisi', 'akte_kelahiran', 'is_hidup', 'akte_meninggal', 'tanggal_meninggal', 'no_npwp', 'tanggal_npwp', 'id_pegawai', 'no_taspen', 'no_bpjs', 'no_karis'];
      protected $fillable = ['no_surat_ket_sehat_dokter', 'tanggal_surat_ket_sehat_dokter', 'no_surat_ket_bebas_narkoba', 'tanggal_surat_ket_bebas_narkoba', 'no_surat_ket_catatan_polisi', 'tanggal_surat_ket_catatan_polisi', 'akte_kelahiran', 'is_hidup', 'akte_meninggal', 'tanggal_meninggal', 'no_npwp', 'tanggal_npwp', 'id_pegawai', 'no_taspen', 'no_bpjs', 'no_karis'];
      protected $table = 'data_tambahan_pegawai';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai');
      }

}
