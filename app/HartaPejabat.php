<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class HartaPejabat extends Model
{

      use SoftDeletes;
      use LogsActivity;

      protected static $logAttributes = ['id_pegawai', 'tanggal_laporan', 'keterangan', 'dokumen', 'is_approved'];
      protected $fillable = ['id_pegawai', 'tanggal_laporan', 'keterangan', 'dokumen', 'is_approved'];
      protected $dates = ['deleted_at'];
      protected $table = 'harta_pejabat';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai');
      }

}
