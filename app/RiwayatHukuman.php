<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatHukuman extends Model
{
      use SoftDeletes;
      use LogsActivity;

      protected static $logAttributes = ['id_pegawai', 'id_jenis_hukuman', 'sk_hukuman', 'tanggal_sk_hukuman', 'masa_hukuman_berlaku_tahun', 'masa_hukuman_berlaku_bulan', 'tanggal_hukuman_berakhir', 'no_pp', 'alasan_hukuman', 'keterangan', 'is_approved'];
      protected $fillable = ['id_pegawai', 'id_jenis_hukuman', 'sk_hukuman', 'tanggal_sk_hukuman', 'masa_hukuman_berlaku_tahun', 'masa_hukuman_berlaku_bulan', 'tanggal_hukuman_berakhir', 'no_pp', 'alasan_hukuman', 'keterangan', 'is_approved'];
      protected $dates = ['deleted_at'];
      protected $table = 'riwayat_hukuman';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function masterHukuman()
      {
            return $this->belongsTo('App\MasterHukuman', 'id_jenis_hukuman', 'id');
      }

      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }
}
