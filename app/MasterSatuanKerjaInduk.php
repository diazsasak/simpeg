<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterSatuanKerjaInduk extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama', 'id_instansi_induk', 'kode_satuan_kerja_induk', 'id_jenis_unit'];
      protected $fillable = ['nama', 'id_instansi_induk', 'kode_satuan_kerja_induk', 'id_jenis_unit'];
      protected $table = 'master_satuan_kerja_induk';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function masterInstansiInduk()
      {
            return $this->belongsTo('App\MasterInstansiInduk', 'id_instansi_induk','id');
      }

      public function masterUnitSkpd()
      {
            return $this->hasMany('App\MasterUnitSkpd', 'id_satuan_kerja_induk', 'id');
      }

      public function masterJenisUnit()
      {
            return $this->belongsTo('App\MasterJenisUnit', 'id_jenis_unit', 'id');
      }

      public static function getPegawai($id_satuan_kerja_induk){
            $pegawai = [];
            //mski
            $mski = Pegawai::whereHas('riwayatJabatan', function($q) use ($id_satuan_kerja_induk){
                  $q->where('jenis_instansi', 'MasterSatuanKerjaInduk');
                  $q->where('id_instansi', $id_satuan_kerja_induk);
                  $q->where('id_status_kepegawaian',1);
            })
            ->doesnthave('user')
            ->get(['id', 'nama']);
            foreach ($mski as $key) {
                        array_push($pegawai, $key);
            };
            //mus
            $mus = Pegawai::whereHas('riwayatJabatan', function($q) use ($id_satuan_kerja_induk){
                  $q->where('jenis_instansi', "MasterUnitSkpd");
                  $q->where('id_status_kepegawaian',1);
                  $q->join('master_unit_skpd', function($join){
                        $join->on('riwayat_jabatan.id_instansi', '=', 'master_unit_skpd.id');
                  })
                  ->where('master_unit_skpd.id_satuan_kerja_induk', $id_satuan_kerja_induk);
            })
            ->doesnthave('user')
            ->get(['id', 'nama']);
            foreach ($mus as $key) {
                        $pendidikan = 
                        array_push($pegawai, $key);
            };
            //msuo
            $mus = Pegawai::whereHas('riwayatJabatan', function($q) use ($id_satuan_kerja_induk){
                  $q->where('jenis_instansi', "MasterSubUnitOrganisasi");
                  $q->where('id_status_kepegawaian',1);
                  $q->join('master_sub_unit_organisasi', function($join){
                        $join->on('riwayat_jabatan.id_instansi', '=', 'master_sub_unit_organisasi.id');
                  })
                  ->join('master_unit_skpd', function($join){
                        $join->on('master_sub_unit_organisasi.id_unit_skpd', '=', 'master_unit_skpd.id');
                  })
                  ->where('master_unit_skpd.id_satuan_kerja_induk', $id_satuan_kerja_induk);
            })
            ->doesnthave('user')
            ->get(['id', 'nama']);
            foreach ($mus as $key) {
                        array_push($pegawai, $key);
            };
            return $pegawai;
      }
}
