<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterEselon extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['jenis'];
      protected $fillable = ['nama'];
      protected $table = 'master_eselon';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function riwayatJabatan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_eselon', 'id');
      }

}
