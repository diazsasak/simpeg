<?php

//diaz

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class Dokumen extends Model
{

      use SoftDeletes;
      use LogsActivity;

      protected $fillable = ['judul', 'document_path', 'id_kategori_artikel', 'id_penulis', 'published'];
      protected static $logAttributes = ['judul', 'document_path', 'id_kategori_artikel', 'id_penulis', 'published'];
      protected $dates = ['deleted_at'];
      protected $table = 'dokumen';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function masterKategoriArtikel()
      {
            return $this->belongsTo('App\MasterKategoriArtikel', 'id_kategori_artikel');
      }

      public function user()
      {
            return $this->belongsTo('App\User', 'id_penulis');
      }

}
