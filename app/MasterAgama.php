<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterAgama extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama'];
      protected $fillable = ['nama'];
      protected $table = 'master_agama';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->hasMany('App\MasterAgama', 'id_agama', 'id');
      }

}
