<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterPangkat extends Model
{
	use LogsActivity;
	protected static $logAttributes = ['nama_golongan','nama_pangkat'];
	protected $fillable = ['nama_golongan','nama_pangkat'];
	protected $table = 'master_pangkat';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}
}
