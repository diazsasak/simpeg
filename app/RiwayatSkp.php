<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatSkp extends Model
{
    use SoftDeletes;
	use LogsActivity;
	protected static $logAttributes = ['id_pegawai', 'tahun_skp', 'nilai_skp', 'orientasi_pelayanan', 'komitmen', 'kerjasama', 'nilai_perilaku_kerja', 'nilai_prestasi_kerja', 'integritas', 'disiplin', 'id_pegawai_penilai', 'id_atasan_pegawai_penilai', 'is_approved'];
	protected $fillable = ['id_pegawai', 'tahun_skp', 'nilai_skp', 'orientasi_pelayanan', 'komitmen', 'kerjasama', 'nilai_perilaku_kerja', 'nilai_prestasi_kerja', 'integritas', 'disiplin', 'id_pegawai_penilai', 'id_atasan_pegawai_penilai', 'is_approved'];
	protected $dates = ['deleted_at'];
	protected $table = 'riwayat_skp';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

  	//relasi
	public function pegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai');
	}

	public function pegawaiPenilai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai_penilai');
	}

	public function atasanPegawaiPenilai()
	{
		return $this->belongsTo('App\Pegawai', 'id_atasan_pegawai_penilai');
	}
}
