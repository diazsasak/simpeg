<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterKategoriJabatan extends Model
{
	use LogsActivity;

	protected static $logAttributes = ['nama'];
	protected $fillable = ['nama'];
	protected $table = 'master_kategori_jabatan';

      //log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

      //relasi
	public function masterJabatan()
	{
		return $this->hasMany('App\MasterJabatan', 'id_kategori_jabatan', 'id');
	}
}
