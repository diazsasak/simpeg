<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterJabatan extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama', 'id_kategori_jabatan'];
      protected $fillable = ['nama', 'id_kategori_jabatan'];
      protected $table = 'master_jabatan';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function riwayatJabatan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_jabatan', 'id');
      }

      public function kategoriJabatan()
      {
            return $this->belongsTo('App\MasterKategoriJabatan', 'id_kategori_jabatan', 'id');
      }

}
