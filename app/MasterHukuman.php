<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterHukuman extends Model
{
	use LogsActivity;
	protected static $logAttributes = ['jenis_hukuman'];
	protected $fillable = ['jenis_hukuman'];
	protected $table = 'master_hukuman';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

	public function riwayatHukuman()
	{
		return $this->hasMany('App\RiwayatHukuman', 'id_jenis_hukuman');
	}
}
