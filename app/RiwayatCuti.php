<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatCuti extends Model
{
	use SoftDeletes;
	use LogsActivity;

	protected static $logAttributes = ['id_pegawai', 'sk_cuti', 'jenis_cuti', 'alasan_cuti', 'tanggal_sk_cuti', 'tanggal_mulai_cuti', 'tanggal_selesai_cuti', 'is_approved'];
	protected $fillable = ['id_pegawai', 'sk_cuti', 'jenis_cuti', 'alasan_cuti', 'tanggal_sk_cuti', 'tanggal_mulai_cuti', 'tanggal_selesai_cuti', 'is_approved'];
	protected $dates = ['deleted_at'];
	protected $table = 'riwayat_cuti';

      //log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

      //relasi

	public function pegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
	}
}
