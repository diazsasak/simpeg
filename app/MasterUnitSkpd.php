<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterUnitSkpd extends Model
{

      use LogsActivity;

//      protected static $logAttributes = ['nama', 'nama_jabatan', 'id_jenis_unit', 'id_eselon', 'kode_unit_skpd'];
//      protected $fillable = ['nama', 'nama_jabatan', 'id_jenis_unit', 'id_eselon', 'kode_unit_skpd'];
//      protected $table = 'master_unit_skpd';
      
      protected static $logAttributes = ['nama', 'id_jenis_unit', 'id_satuan_kerja_induk', 'kode_unit_skpd'];
      protected $fillable = ['nama', 'id_jenis_unit', 'id_satuan_kerja_induk', 'kode_unit_skpd'];
      protected $table = 'master_unit_skpd';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi

      public function masterJenisUnit()
      {
            return $this->belongsTo('App\MasterJenisUnit', 'id_jenis_unit', 'id');
      }
      
      public function masterSatuanKerjaInduk()
      {
            return $this->belongsTo('App\MasterSatuanKerjaInduk', 'id_satuan_kerja_induk', 'id');
      }

//      public function masterEselon()
//      {
//            return $this->belongsTo('App\MasterEselon', 'id_eselon', 'id');
//      }

      public function masterSubUnitOrganisasi()
      {
            return $this->hasMany('App\MasterSubUnitOrganisasi', 'id_unit_skpd', 'id');
      }

      public function user()
      {
            return $this->hasMany('App\User', 'user_group', 'id');
      }

}
