<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'id_provinsi';

    public function pegawai()
      {
            return $this->hasMany('App\Pegawai', 'id_provinsi', 'id_provinsi');
      }
}
