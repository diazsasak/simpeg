<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterJenisUnit extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama'];
      protected $fillable = ['nama'];
      protected $table = 'master_jenis_unit';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function unitSkpd()
      {
            return $this->hasMany('App\MasterUnitSkpd', 'id_jenis_unit', 'id');
      }
      
      public function masterInstansiInduk()
      {
            return $this->hasMany('App\MasterInstansiInduk', 'id_jenis_unit', 'id');
      }

}
