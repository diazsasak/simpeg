<?php

//diaz

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class AnggotaKeluarga extends Model
{

      use SoftDeletes;
      use LogsActivity;

      protected $fillable = ['nama', 'jk', 'id_status', 'id_pegawai', 'tanggal_lahir', 'tempat_lahir', 'id_pendidikan', 'id_pekerjaan', 'status_perkawinan'];
      protected static $logAttributes = ['nama', 'jk', 'id_status', 'id_pegawai', 'tanggal_lahir', 'tempat_lahir', 'id_pendidikan', 'id_pekerjaan', 'status_perkawinan'];
      protected $dates = ['deleted_at'];
      protected $table = 'anggota_keluarga';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }

      public function masterKeluarga()
      {
            return $this->belongsTo('App\MasterKeluarga', 'id_status', 'id');
      }

      public function masterPendidikan()
      {
            return $this->belongsTo('App\MasterPendidikan', 'id_pendidikan', 'id');
      }

      public function masterPekerjaan()
      {
            return $this->belongsTo('App\MasterPekerjaan', 'id_pekerjaan', 'id');
      }

}
