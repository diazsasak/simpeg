<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;
use DB;

class Pegawai extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nip', 'nip_lama', 'poto', 'nama', 'gelar_depan', 'gelar_belakang', 'tempat_lahir', 'tgl_lahir', 'id_agama', 'alamat_sekarang', 'jns_kelamin', 'id_provinsi', 'id_kabupaten', 'id_kecamatan', 'asal_alamat_lengkap', 'email', 'nomor_telepon', 'no_hp', 'jenis_dokumen', 'no_dokumen', 'jenis_pegawai', 'status_pegawai', 'tanggal_pns', 'tanggal_cpns', 'tahun_pengangkatan', 'status_perkawinan', 'is_aktif', 'is_atasan', 'is_approved', 'sk_pns', 'sk_cpns', 'nuptk', 'is_aktif_false_ket', 'id_status_kepegawaian'];
      protected $fillable = ['nip', 'nip_lama', 'poto', 'nama', 'gelar_depan', 'gelar_belakang', 'tempat_lahir', 'tgl_lahir', 'id_agama', 'alamat_sekarang', 'jns_kelamin', 'id_provinsi', 'id_kabupaten', 'id_kecamatan', 'asal_alamat_lengkap', 'email', 'nomor_telepon', 'nomor_hp', 'jenis_dokumen', 'no_dokumen', 'jenis_pegawai', 'status_pegawai', 'tanggal_pns', 'tanggal_cpns', 'tahun_pengangkatan', 'status_perkawinan', 'is_aktif', 'is_atasan', 'is_approved', 'sk_pns', 'sk_cpns', 'nuptk', 'is_aktif_false_ket', 'id_status_kepegawaian'];
      // protected $dates = ['deleted_at'];
      protected $table = 'pegawai';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function user()
      {
            return $this->hasOne('App\User', 'id_pegawai', 'id');
      }

      public function hasOneStrukturOrganisasi()
      {
            return $this->hasOne('App\StrukturOrganisasi', 'id_pegawai', 'id');
      }

      public function masterAgama()
      {
            return $this->belongsTo('App\MasterAgama', 'id_agama', 'id');
      }

      public function dataTambahanPegawai()
      {
            return $this->hasOne('App\DataTambahanPegawai', 'id_pegawai');
      }

      public function riwayatJabatan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_pegawai', 'id');
      }

      public function pensiun()
      {
            return $this->hasMany('App\Pensiun', 'id_pegawai', 'id');
      }

      public function riwayatPendidikan()
      {
            return $this->belongsToMany('App\MasterPendidikan', 'riwayat_pendidikan', 'id_pegawai', 'id_pendidikan')->withPivot('id','id_jurusan', 'nama_institusi', 'tahun_lulus', 'no_ijazah', 'gelar_depan', 'gelar_belakang')->withTimestamps();
      }

      public function hasManyRiwayatPendidikan()
      {
            return $this->hasMany('App\RiwayatPendidikan', 'id_pegawai', 'id');
      }

      public function anggotaKeluarga()
      {
            return $this->hasMany('App\AnggotaKeluarga', 'id_pegawai', 'id');
      }

      public function riwayatDiklat()
      {
            return $this->hasMany('App\RiwayatDiklat', 'id_pegawai', 'id');
      }

      public function riwayatSkp()
      {
            return $this->hasMany('App\RiwayatSkp', 'id_pegawai');
      }

      public function riwayatPangkat()
      {
            return $this->hasMany('App\RiwayatPangkat', 'id_pegawai');
      }

      public function riwayatHukuman()
      {
            return $this->hasMany('App\RiwayatHukuman', 'id_pegawai');
      }

      public function riwayatCuti()
      {
            return $this->hasMany('App\RiwayatCuti', 'id_pegawai');
      }

      public function riwayatAbsensi()
      {
            return $this->hasMany('App\RiwayatAbsensi', 'id_pegawai');
      }

      public function hartaPejabat()
      {
            return $this->hasMany('App\HartaPejabat', 'id_pegawai');
      }

      public function provinsi()
      {
            return $this->belongsTo('App\Provinsi', 'id_provinsi', 'id_provinsi');
      }

      public function kabupaten()
      {
            return $this->belongsTo('App\Kabupaten', 'id_kabupaten', 'id_kabupaten');
      }

      public function belongsToStatusKepegawaian()
      {
            return $this->belongsTo('App\MasterStatusKepegawaian', 'id_status_kepegawaian', 'id');
      }

      public function kecamatan()
      {
            return $this->belongsTo('App\Kecamatan', 'id_kecamatan', 'id_kecamatan');
      }

      public function bawahan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_pegawai_atasan');
      }

      public function lastPendidikan()
      {
            if(sizeof($this->riwayatPendidikan())>0) 
                  return $this->riwayatPendidikan()->orderBy('tahun_lulus', 'DESC')->first();
            return false;
      }

      public function lastJabatan()
      {
            if(null !== $this->riwayatJabatan()->orderBy('tanggal_jabatan', 'DESC')->where('is_approved', 1)->first())
                  return $this->riwayatJabatan()->with('masterJabatan')->orderBy('tanggal_jabatan', 'DESC')->where('is_approved', 1)->first();
            return false;
      }

      public function lastPangkat()
      {
            if(null !== $this->riwayatPangkat()->orderBy('tmt_pangkat', 'DESC')->where('is_approved', 1)->first())
                  return $this->riwayatPangkat()->with('masterPangkat')->orderBy('tmt_pangkat', 'DESC')->where('is_approved', 1)->first();
            return false;
      }

      public function jabatanTerakhir(){
            return $this->riwayatJabatan()->orderBy('tanggal_jabatan', 'DESC')->where('is_approved', true)->first();
      }

      public function jabatanTerakhirName(){
            if($this->jabatanTerakhir() != null){
                  return $this->jabatanTerakhir()->masterJabatan->nama;
            }
            return '-';
      }

      public function getKategoriJabatanName(){
            if($this->jabatanTerakhir() != null){
                  return $this->jabatanTerakhir()->masterJabatan->kategoriJabatan->nama;
            }
            return '-';
      }

      public function getEselonTerakhirName(){
            if($this->jabatanTerakhir() != null){
                  if($this->jabatanTerakhir()->masterEselon != null){
                        return $this->jabatanTerakhir()->masterEselon->nama;
                  }
            }
            return '-';
      }

      public function getPangkatTerbaru()
      {
            return $this->hasMany('App\RiwayatPangkat', 'id_pegawai')->where('is_approved', true)->orderBy('tmt_pangkat', 'DESC')->first();
      }

      public function getPangkatTerbaruName(){
            if($this->getPangkatTerbaru() != null){
                  return $this->getPangkatTerbaru()->masterPangkat->nama_golongan;
            }
            return '-';
      }

}
