<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatDiklat extends Model
{

      use SoftDeletes;
      use LogsActivity;

      protected static $logAttributes = ['nama', 'id_diklat', 'tempat', 'tanggal_pelaksanaan', 'pelaksana', 'id_pegawai', 'is_approved', 'sk_diklat', 'no_sertifikat', 'sertifikat'];
      protected $fillable = ['nama', 'id_diklat', 'tempat', 'tanggal_pelaksanaan', 'pelaksana', 'id_pegawai', 'is_approved', 'sk_diklat', 'no_sertifikat', 'sertifikat'];
      protected $dates = ['deleted_at'];
      protected $table = 'riwayat_diklat';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }

      public function masterDiklat()
      {
            return $this->belongsTo('App\MasterDiklat', 'id_diklat', 'id');
      }

}
