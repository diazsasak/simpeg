<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterKategoriArtikel extends Model
{
    use LogsActivity;
	protected static $logAttributes = ['nama'];
	protected $fillable = ['nama'];
	protected $table = 'master_kategori_artikel';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

	public function hasManyArtikel(){
		return $this->hasMany('App\Artikel', 'id_kategori_artikel', 'id');
	}

	public function getCategoryName(){
          $types = array(
            'layanan-sekretariat-bkpsdm'  => 'Sekretariat BKPSDM',
            'layanan-bidang-mutasi'        => 'Bidang Mutasi',
            'layanan-bidang-disiplin'     => 'Bidang Disiplin',
            'layanan-bidang-data-dan-formasi'     => 'Bidang Data dan Formasi',
            'layanan-bidang-pengembangan-sdm'        => 'Bidang Pengembangan SDM'
            );

          $type = $this->nama;

          return (isset($types[$type]) ? $types[$type] : 'Unknown');
      } 
}
