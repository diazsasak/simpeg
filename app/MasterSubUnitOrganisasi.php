<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterSubUnitOrganisasi extends Model
{

      use LogsActivity;
            
      protected static $logAttributes = ['nama', 'id_unit_skpd', 'kode_sub_unit_organisasi',  'id_jenis_unit'];
      protected $fillable = ['nama', 'id_unit_skpd', 'kode_sub_unit_organisasi', 'id_jenis_unit'];
      protected $table = 'master_sub_unit_organisasi';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      public function MasterJenisUnit()
      {
            return $this->belongsTo('App\MasterJenisUnit', 'id_jenis_unit', 'id');
      }

//      public function masterEselon()
//      {
//            return $this->belongsTo('App\MasterEselon', 'id_eselon', 'id');
//      }

      public function riwayatJabatan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_sub_unit_organisasi');
      }

      public function masterUnitSkpd()
      {
            return $this->belongsTo('App\MasterUnitSkpd', 'id_unit_skpd', 'id');
      }

}
