<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatAbsensi extends Model
{
    use SoftDeletes;
	use LogsActivity;

	protected static $logAttributes = ['id_pegawai', 'hadir', 'bolos', 'izin', 'telat', 'cuti', 'cuti_eksternal', 'sakit', 'file_izin_dokter', 'tanggal', 'bulan', 'tahun', 'is_approved'];
	protected $fillable = ['id_pegawai', 'hadir', 'bolos', 'izin', 'telat', 'cuti', 'cuti_eksternal', 'sakit', 'file_izin_dokter', 'tanggal', 'bulan', 'tahun', 'is_approved'];
	protected $dates = ['deleted_at'];
	protected $table = 'riwayat_absensi';

      //log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

      //relasi

	public function pegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
	}
}
