<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterPendidikan extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama'];
      protected $fillable = ['nama'];
      protected $table = 'master_pendidikan';

      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      public function anggotaKeluarga(){
      	return $this->hasMany('App\AnggotaKeluarga', 'id_pendidikan', 'id');
      }

      public function belongsToManyPegawai(){
            return $this->belongsToMany('App\Pegawai', 'riwayat_pendidikan', 'id_pendidikan', 'id_pegawai')->withPivot('id','id_jurusan', 'nama_institusi', 'tahun_lulus', 'no_ijazah', 'gelar_depan', 'gelar_belakang')->withTimestamps();
      }

}
