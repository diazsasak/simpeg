<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StrukturOrganisasi extends Model
{
	protected $fillable = [
		'id_pegawai',
		'nama_jabatan'
	];
	protected $table = 'struktur_organisasi';

	public function belongsToPegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
	}
}
