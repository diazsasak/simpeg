<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterInstansiInduk extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama', 'kode_instansi_induk', 'id_jenis_unit'];
      protected $fillable = ['nama', 'kode_instansi_induk', 'id_jenis_unit'];
      protected $table = 'master_instansi_induk';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi

      public function masterSatuanKerjaInduk()
      {
            return $this->hasMany('App\MasterSatuanKerjaInduk', 'id_instansi_induk');
      }

//      public function masterUnitSkpd()
//      {
//            return $this->hasMany('App\MasterUnitSkpd', 'id_instansi_induk', 'id');
//      }
      
      public function masterJenisUnit()
      {
            return $this->belongsTo('App\MasterJenisUnit', 'id_jenis_unit', 'id');
      }

}