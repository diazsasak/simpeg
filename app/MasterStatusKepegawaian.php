<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterStatusKepegawaian extends Model
{
	use LogsActivity;

	protected static $logAttributes = ['nama'];
	protected $fillable = ['nama'];
	protected $table = 'master_status_kepegawaian';

	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

	public function hasManyPegawai(){
		return $this->hasMany('App\Pegawai', 'id_status_kepegawaian', 'id');
	}
}
