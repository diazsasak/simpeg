<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class Pensiun extends Model
{
	use SoftDeletes;
	use LogsActivity;

	protected static $logAttributes = ['id_pegawai', 'jenis_pensiun', 'sk_pensiun', 'tmt_pensiun', 'tanggal_sk_pensiun', 'is_approved'];
	protected $fillable = ['id_pegawai', 'jenis_pensiun', 'sk_pensiun', 'tmt_pensiun', 'tanggal_sk_pensiun', 'is_approved'];
	protected $dates = ['deleted_at'];
	protected $table = 'pensiun';

      //log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

	//relasi
	public function pegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
	}
}
