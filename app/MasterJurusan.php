<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterJurusan extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama'];
      protected $fillable = ['nama'];
      protected $table = 'master_jurusan';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function riwayatPendidikan()
      {
            return $this->hasMany('App\RiwayatPendidikan', 'id_jurusan', 'id');
      }

}
