<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class User extends Authenticatable
{

      use Notifiable;
      // use SoftDeletes;
      use LogsActivity;

      /**
       * The attributes that are mass assignable.
       *
       * @var array
       */
      protected $fillable = [
          'name', 'password', 'username', 'id_pegawai', 'user_level'
      ];
      protected static $logAttributes = [
          'name', 'password', 'username', 'id_pegawai', 'user_level'
      ];

      /**
       * The attributes that should be hidden for arrays.
       *
       * @var array
       */
      protected $hidden = [
          'password', 'remember_token',
      ];

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }

      public function getInstansiName(){
            if($this->pegawai != null){
                  if($this->pegawai->jabatanTerakhirName() != null){
                        return $this->pegawai->jabatanTerakhir()->getInstansiName();
                  }
            }
            return "-";
      }

      public function isBKD(){
            if($this->pegawai != null){
                  if($this->pegawai->jabatanTerakhir() != null){
                        return $this->pegawai->jabatanTerakhir()->isBKD();
                  }
            }
            return false;
      }

}
