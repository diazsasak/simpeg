<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Kasubid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->user_level != 'kasubid'){
            session(['fail' => 'Tidak memiliki izin']);
            return redirect('/');
        }
        return $next($request);
    }
}
