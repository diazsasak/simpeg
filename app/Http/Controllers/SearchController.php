<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use Auth;
use DB;

class SearchController extends Controller
{

  public function index()
  {

  }

  public function show($query)
  {
    $pegawai = Pegawai::where(function($q) use ($query){
      $q->where('nama', 'LIKE', '%' . $query . '%');
      $q->orWhere('nip', 'LIKE', '%' . $query . '%');
      $q->orWhere('tempat_lahir', 'LIKE', '%' . $query . '%');
      $q->orWhere('alamat_sekarang', 'LIKE', '%' . $query . '%');
      $q->orWhere('asal_alamat_lengkap', 'LIKE', '%' . $query . '%');
      $q->orWhere('tahun_pengangkatan', 'LIKE', '%' . $query . '%');
    });
    if(Auth::check()){
      if(!Auth::user()->isBKD() && (Auth::user()->user_level != 'super_admin')){
        $idSatuanKerjaInduk = Auth::user()->pegawai->jabatanTerakhir()->getSatuanKerjaInduk()->id;
        $pegawai->join(DB::raw('(select id, id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left')
        ->where(function($q) use ($idSatuanKerjaInduk){
          $q->whereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan
            where jenis_instansi = "MasterSatuanKerjaInduk"
            and id_instansi = '.$idSatuanKerjaInduk))), true));
          $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan join master_unit_skpd
            on riwayat_jabatan.id_instansi = master_unit_skpd.id
            where riwayat_jabatan.jenis_instansi = "MasterUnitSkpd" and master_unit_skpd.id_satuan_kerja_induk =  '.$idSatuanKerjaInduk))), true));
          $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan 
            join master_sub_unit_organisasi on riwayat_jabatan.id_instansi = master_sub_unit_organisasi.id
            join master_unit_skpd on master_unit_skpd.id = master_sub_unit_organisasi.id_unit_skpd
            where riwayat_jabatan.jenis_instansi = "MasterSubUnitOrganisasi" and master_unit_skpd.id_satuan_kerja_induk = '.$idSatuanKerjaInduk))), true));
        });
      }    
    }
    $pegawai = $pegawai->select('pegawai.*')->limit(5)->get();
    return $pegawai;
  }

}
