<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnggotaKeluarga;
use App\Pegawai;

class AnggotaKeluargaController extends Controller
{

      public function store(Request $r)
      {
            $r = $r->all();
            Pegawai::find($r['id_pegawai'])->anggotaKeluarga()->create($r);
      }

      public function update($id, Request $r)
      {
            AnggotaKeluarga::find($id)->update($r->all());
      }

      public function destroy($id)
      {
            AnggotaKeluarga::find($id)->delete();
      }

}
