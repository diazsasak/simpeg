<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\RiwayatHukuman;

class RiwayatHukumanController extends Controller
{
      public function store(Request $r)
      {
            $r = $r->all();
            Pegawai::find($r['id_pegawai'])->riwayatHukuman()->create($r);
      }

      public function update($id, Request $r)
      {
            RiwayatHukuman::find($id)->update($r->all());
      }

      public function destroy($id)
      {
            RiwayatHukuman::find($id)->delete();
      }

      public function approve($id, Request $a)
      {
            $req['is_approved'] = true;
            RiwayatHukuman::find($id)->update($req);
      }

      public function unapprove($id, Request $a)
      {
            $req['is_approved'] = false;
            RiwayatHukuman::find($id)->update($req);
      }
}
