<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use App\Dokumen;
use App\Pegawai;
use App\StrukturOrganisasi;

class BerandaController extends Controller
{

      public function index()
      {
            $so = StrukturOrganisasi::all();
            $dokumen = Dokumen::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();


            $berita = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 1)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();

            $pengumuman = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 2)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();
            $running_text = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 9)
                    ->limit(3)
                    ->orderBy('id', 'desc')
                    ->get();
            $slider = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 8)
                    ->limit(3)
                    ->orderBy('id', 'desc')
                    ->get();
            $visi = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 10)
                    ->first();

            $menu = 'beranda';
            return view('beranda.index', compact('berita', 'menu', 'dokumen', 'pengumuman', 'slider', 'so','running_text','visi'));
      }

      public function getDetailArtikel($url)
      {
            
            $dokumen = Dokumen::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();


            $berita = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 1)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();

            $pengumuman = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 2)
                    ->limit(5)
                    ->orderBy('id', 'desc')
                    ->get();
    
            $detail_artikel = Artikel::where('url', $url)->first();
            $menu = 'detail_artikel';
            return view('beranda.detail_artikel', compact('detail_artikel', 'dokumen', 'berita', 'pengumuman', 'menu'));
      }
      
      public function getAllArtikel()
      {
            $berita = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 1)
                    ->paginate(8);
            
            $pengumuman = Artikel::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->where('id_kategori_artikel', 2)
                    ->paginate(8);
            
            $dokumen = Dokumen::with('masterKategoriArtikel', 'user')
                    ->where('published', 1)
                    ->paginate(8);
            $menu = 'all_artikel';
            return view('beranda.all_artikel', compact( 'berita', 'menu','dokumen','pengumuman'));
      }

      public function kontak()
      {
            return view('beranda.kontak');
      }

      //
}
