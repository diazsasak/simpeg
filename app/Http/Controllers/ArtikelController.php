<?php

//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use App\Pegawai;
use Image;
use Auth;
use File;

class ArtikelController extends Controller
{

      public function index(Request $r)
      {
            $artikel = Artikel::withTrashed()
            ->with('masterKategoriArtikel', 'user')
            ->paginate(10);
            if ($r->ajax())
                  return $artikel;
            else {
                  $menu = 'artikel';
                  return view('artikel.index', compact('artikel', 'menu'));
            }
      }

      public function store(Request $a)
      {
            $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
            $request = $a->all();
            $file = $a->file('gambar_path');
            $url = str_replace($car, "", $a->input('judul'));
            if ($file != null) {
                  File::makeDirectory('upload/artikel', $mode = 0777, true, true);
                  $photo = 'upload/artikel/artikel_' . str_random(30) . '.' . $file->getClientOriginalExtension();
                  Image::make($file)->resize(null, 200, function ($constraint) {
                        $constraint->aspectRatio();
                  })->save($photo);
            } else {
                  $photo = 'images/default_gambar_artikel.png';
            }
            $request['url'] = rtrim(strtolower(str_replace(" ", "-", $url)));
            $request['gambar_path'] = $photo;
            $request['id_penulis'] = Auth::id();
            if(Auth::user()->pegawai != null){
                  if(Auth::user()->pegawai->jabatanTerakhir() != null){
                        $request['instansi_on_created'] = Auth::user()->pegawai->jabatanTerakhir()->getInstansiName();
                  }
            }
            else{
                  $request['instansi_on_created'] = "-";
            }
            Artikel::create($request);
      }

      public function destroy($id)
      {
            Artikel::find($id)->delete();
      }

      public function restore($id)
      {
            Artikel::onlyTrashed()->where('id', $id)->restore();
      }

      public function update($id, Request $r)
      {
            $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
            $data = Artikel::find($id);
            $new = $r->all();
            $foto = $r->file('gambar_path');
            $url = str_replace($car, "", $r->input('judul'));
            if ($foto != null) {
                  File::makeDirectory('upload/artikel', $mode = 0777, true, true);
                  if ($data->gambar_path != 'images/default_gambar_artikel.png') {
                        File::delete(public_path($data->gambar_path));
                  }
                  $this->validate($r, [
                      'picture' => 'image'
                ]);
                  $nama = 'upload/artikel/artikel_' . str_random(30) . '.' . $foto->getClientOriginalExtension();
                  Image::make($foto)->resize(null, 200, function ($constraint) {
                        $constraint->aspectRatio();
                  })->save($nama);
                  // Image::make($foto)->save($nama);
                  $new['gambar_path'] = $nama;
            } else {
                  if ($new['hapus_foto'] == "true") {
                        if ($data->poto != 'images/default_gambar_artikel.png') {
                              File::delete(public_path($data->gambar_path));
                              $new['gambar_path'] = 'images/default_gambar_artikel.png';
                        }
                  }
            }
            $new['url'] = rtrim(strtolower(str_replace(" ", "-", $url)));
            $data->update($new);
      }

      public function publish($id, Request $a)
      {
            $req = $a->all();
            $req['published'] = true;
            Artikel::find($id)->update($req);
      }

      public function unpublish($id, Request $a)
      {
            $req = $a->all();
            $req['published'] = false;
            Artikel::find($id)->update($req);
      }

}
