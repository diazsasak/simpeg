<?php
//ridwan

namespace App\Http\Controllers;
use App\RiwayatSkp;
use App\Pegawai;

use Illuminate\Http\Request;

class RiwayatSkpController extends Controller
{
    public function store(Request $r)
	{
		$r = $r->all();
		Pegawai::find($r['id_pegawai'])->riwayatSkp()->create($r);
	}

	public function update($id, Request $r)
	{
		RiwayatSkp::find($id)->update($r->all());
	}

	public function destroy($id)
	{
		RiwayatSkp::find($id)->delete();
	}

	public function approve($id, Request $a)
	{
		$req['is_approved'] = true;
		RiwayatSkp::find($id)->update($req);
	}

	public function unapprove($id, Request $a)
	{
		$req['is_approved'] = false;
		RiwayatSkp::find($id)->update($req);
	}
}
