<?php
//ridwan

namespace App\Http\Controllers;
use App\RiwayatPangkat;
use App\Pegawai;

use Illuminate\Http\Request;

class RiwayatPangkatController extends Controller
{
    public function store(Request $r)
	{
		$r = $r->all();
		Pegawai::find($r['id_pegawai'])->riwayatPangkat()->create($r);
	}

	public function update($id, Request $r)
	{
		RiwayatPangkat::find($id)->update($r->all());
	}

	public function destroy($id)
	{
		RiwayatPangkat::find($id)->delete();
	}

	public function approve($id, Request $a)
	{
		$req['is_approved'] = true;
		RiwayatPangkat::find($id)->update($req);
	}

	public function unapprove($id, Request $a)
	{
		$req['is_approved'] = false;
		RiwayatPangkat::find($id)->update($req);
	}
}
