<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterDiklat;

class MasterDiklatController extends Controller
{
      public function index(Request $r)
      {
            $data = MasterDiklat::all();
            if ($r->ajax())
                  return $data;
            $menu = 'master';
            $submenu = 'diklat';
            return view('master_diklat.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterDiklat::create($r->all());
            return redirect('master_diklat');
      }

      public function edit($id)
      {
            $data = MasterDiklat::find($id);
            return view('master_diklat.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterDiklat::find($id)->update($r->all());
            return redirect('master_diklat');
      }

      public function destroy($id)
      {
            MasterDiklat::find($id)->delete();
            return redirect('master_diklat');
      }

}
