<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterPendidikan;

class MasterPendidikanController extends Controller
{

      public function index(Request $request)
      {
            $data = MasterPendidikan::all();
            if ($request->ajax())
                  return $data;
            else {
                  $menu = 'master';
                  $submenu = 'pendidikan';
                  return view('master_pendidikan.index', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterPendidikan::create($r->all());
            return redirect('master_pendidikan');
      }

      public function edit($id)
      {
            $mp = MasterPendidikan::find($id);
            return view('master_pendidikan.edit', compact('mp'));
      }

      public function update($id, Request $r)
      {
            MasterPendidikan::find($id)->update($r->all());
            return redirect('master_pendidikan');
      }

      public function destroy($id)
      {
            MasterPendidikan::find($id)->delete();
            return redirect('master_pendidikan');
      }

}
