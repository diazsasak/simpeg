<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\RiwayatAbsensi;
use File;

class RiwayatAbsensiController extends Controller
{
	public function store(Request $r)
	{
		$a = $r->all();
		$file = $r->file('file_izin_dokter');
		if ($file != null) {
			$fileName = $file->getClientOriginalName(); // renameing image
			$r->file('file_izin_dokter')->move('upload/file_izin_dokter', $fileName); // uploading file to given path
			$a['file_izin_dokter'] = 'upload/file_izin_dokter/'.$fileName;
		} else {
			$a['file_izin_dokter'] = '';
		}
		Pegawai::find($a['id_pegawai'])->riwayatAbsensi()->create($a);
	}

	public function update($id, Request $r)
	{
		$data = RiwayatAbsensi::find($id);
		$a = $r->all();
		$file = $r->file('file_izin_dokter');
		if ($file != null) {
			if ($data->file_izin_dokter != '') {
				File::delete(public_path($data->file_izin_dokter));
			}
			$fileName = $file->getClientOriginalName(); // renameing image
			$r->file('file_izin_dokter')->move('upload/file_izin_dokter', $fileName); // uploading file to given path
			$a['file_izin_dokter'] = 'upload/file_izin_dokter/'.$fileName;
		}
		RiwayatAbsensi::find($id)->update($a);
	}

	public function destroy($id)
	{
		RiwayatAbsensi::find($id)->delete();
	}

	public function approve($id, Request $a)
	{
		$req['is_approved'] = true;
		RiwayatAbsensi::find($id)->update($req);
	}

	public function unapprove($id, Request $a)
	{
		$req['is_approved'] = false;
		RiwayatAbsensi::find($id)->update($req);
	}
}
