<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HartaPejabat;
use App\Pegawai;

class HartaPejabatController extends Controller
{
	public function store(Request $a)
	{
		// $r = $r->all();
		$request = $a->all();
		$file = $a->file('dokumen');
		if ($file != null) {
			$fileName = $file->getClientOriginalName(); // renameing image
			$a->file('dokumen')->move('upload/harta_pejabat', $fileName); // uploading file to given path
			$request['dokumen'] = 'upload/harta_pejabat/' . $fileName;
		} else {
			$request['dokumen'] = '';
		}
		Pegawai::find($a['id_pegawai'])->hartaPejabat()->create($request);
	}

	public function update($id, Request $r)
	{
		$data = HartaPejabat::find($id);
		$request = $r->all();
		$file = $r->file('dokumen');
		if ($file != null) {
			if ($data->dokumen != '') {
				File::delete(public_path($data->dokumen));
			}
			$fileName = $file->getClientOriginalName(); // renameing image
			$r->file('dokumen')->move('upload/harta_pejabat', $fileName); // uploading file to given path
			$request['dokumen'] = 'upload/harta_pejabat/' . $fileName;
		}
		// $data->update($request);
		HartaPejabat::find($id)->update($request);
	}

	public function destroy($id)
	{
		HartaPejabat::find($id)->delete();
	}

	public function approve($id, Request $a)
	{
		$req['is_approved'] = true;
		HartaPejabat::find($id)->update($req);
	}

	public function unapprove($id, Request $a)
	{
		$req['is_approved'] = false;
		HartaPejabat::find($id)->update($req);
	}
}
