<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterUnitSkpd;
use App\MasterJenisUnit;
//use App\MasterEselon;
use App\MasterSatuanKerjaInduk;

class MasterUnitSkpdController extends Controller
{

      public function index(Request $request)
      {
            if ($request->ajax())
                  return MasterUnitSkpd::all();
            else {
                  $data = MasterUnitSkpd::paginate(30);
                  $ju = MasterJenisUnit::all(['id', 'nama']);
                  $e = MasterSatuanKerjaInduk::all(['id', 'nama']);
                  $menu = 'master';
                  $submenu = 'unit_skpd';
                  return view('master_unit_skpd.index', compact('data', 'ju', 'menu', 'submenu', 'e'));
            }
      }

      public function store(Request $r)
      {
            MasterUnitSkpd::create($r->all());
            return redirect('master_unit_skpd');
      }

      public function edit($id)
      {
            $data = MasterUnitSkpd::find($id);
            return view('master_unit_skpd.edit', compact('data'));
      }

      public function show($id)
      {
            return MasterUnitSkpd::find($id);
      }

      public function update($id, Request $r)
      {
            MasterUnitSkpd::find($id)->update($r->all());
            return redirect('master_unit_skpd');
      }

      public function destroy($id)
      {
            MasterUnitSkpd::find($id)->delete();
            return redirect('master_unit_skpd');
      }

      public function getMasterSubUnitOrganisasi($id){
            return MasterUnitSkpd::find($id)->masterSubUnitOrganisasi()->get(['id', 'nama']);
      }

}
