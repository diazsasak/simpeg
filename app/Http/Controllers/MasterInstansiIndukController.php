<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterInstansiInduk;
use App\MasterKantorRegionalInduk;
use App\MasterJenisUnit;

class MasterInstansiIndukController extends Controller
{

      public function index(Request $request)
      {
            $data = MasterInstansiInduk::all();
            if ($request->ajax())
                  return $data;
            else {
                  $ju = MasterJenisUnit::all(['id', 'nama']);
                  $menu = 'master';
                  $submenu = 'instansi_induk';
                  return view('master_instansi_induk.index', compact('data', 'menu', 'submenu', 'ju'));
            }
      }

      public function store(Request $r)
      {
            MasterInstansiInduk::create($r->all());
            return redirect('master_instansi_induk');
      }

      public function update($id, Request $r)
      {
            MasterInstansiInduk::find($id)->update($r->all());
            return redirect('master_instansi_induk');
      }

      public function destroy($id)
      {
            MasterInstansiInduk::find($id)->delete();
            return redirect('master_instansi_induk');
      }

      public function getMasterSatuanKerjaInduk($id){
            return MasterInstansiInduk::find($id)->masterSatuanKerjaInduk()->get(['id', 'kode_satuan_kerja_induk','nama']);
      }

      public function getMasterUnitSkpd($id){
            return MasterInstansiInduk::find($id)->masterUnitSkpd()->get(['id', 'kode_unit_skpd', 'nama']);
      }

      public function getSelectedInstansiInduk($id){
            return MasterKantorRegionalInduk::find($id)->masterSatuanKerjaInduk->masterInstansiInduk->id;
      }

}