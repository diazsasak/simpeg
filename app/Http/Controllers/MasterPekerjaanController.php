<?php
//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterPekerjaan;

class MasterPekerjaanController extends Controller
{
    public function index(Request $r)
      {
            $data = MasterPekerjaan::all();
            if($r->ajax()) return $data;
            $menu = 'master';
            $submenu = 'pekerjaan';
            return view('master_pekerjaan.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterPekerjaan::create($r->all());
            return redirect('master_pekerjaan');
      }

      public function edit($id)
      {
            $data = MasterPekerjaan::find($id);
            return view('master_pekerjaan.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterPekerjaan::find($id)->update($r->all());
            return redirect('master_pekerjaan');
      }

      public function destroy($id)
      {
            MasterPekerjaan::find($id)->delete();
            return redirect('master_pekerjaan');
      }

}
