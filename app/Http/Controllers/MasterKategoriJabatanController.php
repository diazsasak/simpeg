<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterKategoriJabatan;

class MasterKategoriJabatanController extends Controller
{
    public function index(Request $r)
      {
            $data = MasterKategoriJabatan::all(['id', 'nama']);
            if ($r->ajax())
                  return $data;
            $menu = 'master';
            $submenu = 'kategori_jabatan';
            return view('master_kategori_jabatan.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterKategoriJabatan::create($r->all());
            return redirect('master_kategori_jabatan');
      }

      public function edit($id)
      {
            $data = MasterKategoriJabatan::find($id);
            return view('master_kategori_jabatan.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterKategoriJabatan::find($id)->update($r->all());
            return redirect('master_kategori_jabatan');
      }

      public function destroy($id)
      {
            MasterKategoriJabatan::find($id)->delete();
            return redirect('master_kategori_jabatan');
      }

      public function getMasterJabatan($id){
            return MasterKategoriJabatan::find($id)->masterJabatan()->get(['id', 'nama']);
      }
}
