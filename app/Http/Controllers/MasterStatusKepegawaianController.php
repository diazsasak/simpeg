<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterStatusKepegawaian;

class MasterStatusKepegawaianController extends Controller
{
    public function index(Request $request)
      {
            $data = MasterStatusKepegawaian::all();
            if ($request->ajax())
                  return $data;
            else {
                  $menu = 'master';
                  $submenu = 'status_kepegawaian';
                  return view('master_status_kepegawaian.index', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterStatusKepegawaian::create($r->all());
            return redirect('master_status_kepegawaian');
      }

      public function edit($id)
      {
            $data = MasterStatusKepegawaian::find($id);
            return view('master_status_kepegawaian.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterStatusKepegawaian::find($id)->update($r->all());
            return redirect('master_status_kepegawaian');
      }

      public function destroy($id)
      {
            MasterStatusKepegawaian::find($id)->delete();
            return redirect('master_status_kepegawaian');
      }
}
