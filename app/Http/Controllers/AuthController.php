<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\BuatAkun;
use App\Http\Requests\EditAkun;
use App\User;
use App\Pegawai;
use App\MasterSatuanKerjaInduk;
use Auth;
use Hash;

class AuthController extends Controller
{

      public function index()
      {
            if(Auth::user()->user_level == 'super_admin'){
                  $akun = User::all();
            }
            else{
                  $akun = User::where('id',Auth::user()->id)->get();
            }
            $menu = 'akun';
            $masterSatuanKerjaInduk = MasterSatuanKerjaInduk::all(['id', 'nama']);
            return view('auth.index', compact('akun', 'menu', 'masterSatuanKerjaInduk'));
      }

      public function store(BuatAkun $r)
      {
            $a = $r->all();
            $a['password'] = Hash::make($a['password']);
            if (isset($a['id_pegawai']) && ($a['id_pegawai'] == '-')) {
                  $a['id_pegawai'] = null;
            } 
            User::create($a);
            return redirect('akun');
      }

      public function edit($id)
      {
            $akun = User::find($id);
            $pegawai = Pegawai::all(['id', 'nama']);
            return view('auth.edit', compact('akun', 'pegawai'));
      }

      public function update($id, EditAkun $r)
      {
            $a = $r->all();
            if ($a['password'] != null) {
                  $a['password'] = bcrypt($a['password']);
            } else {
                  unset($a['password']);
            }
            User::find($id)->update($a);
            return redirect('akun');
      }

      public function destroy($id)
      {
            User::find($id)->delete();
            return redirect('akun');
      }

      public function restore($id)
      {
            User::onlyTrashed()->where('id', $id)->restore();
            return redirect('akun');
      }

      public function login()
      {
            return view('auth.login');
      }

      public function postLogin(Request $re)
      {
            $r = $re->all();
            if (isset($r['remember_me'])) {
                  $remember = true;
            } else {
                  $remember = false;
            }
            if (Auth::attempt(['username' => $r['username'], 'password' => $r['password']])) {
                  activity()
                          ->by(Auth::user())
                          ->log('Login from IP :' . $re->ip());
                  return redirect()->intended('pegawai');
            } else {
                  return back()->with('fail', 'Username atau password salah');
                  
            }
      }

      public function logout(Request $r)
      {
            activity()
                    ->by(Auth::user())
                    ->log('Logout from IP :' . $r->ip());
            Auth::logout();
            return redirect('login');
      }

}
