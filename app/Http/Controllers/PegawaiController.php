<?php

//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\MasterAgama;
use App\MasterUnitSkpd;
use App\MasterJabatan;
use App\MasterPendidikan;
use App\MasterEselon;
use App\MasterJenisUnit;
use App\MasterKeluarga;
use App\MasterDiklat;
use App\MasterJurusan;
use App\AnggotaKeluarga;
use App\RiwayatDiklat;
use App\RiwayatJabatan;
use App\RiwayatPendidikan;
use App\Pensiun;
use App\RiwayatSkp;
use App\RiwayatPangkat;
use App\RiwayatHukuman;
use App\RiwayatCuti;
use App\RiwayatAbsensi;
use App\HartaPejabat;
use App\MasterSatuanKerjaInduk;
use Image;
use DB;
use File;
use Excel;
use Auth;

class PegawaiController extends Controller
{

    public function index(Request $request)
    {
        if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin')){
            $pegawai = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left');
        }
        else{
            $idSatuanKerjaInduk = Auth::user()->pegawai->jabatanTerakhir()->getSatuanKerjaInduk()->id;
            $pegawai = Pegawai::join(DB::raw('(select id, id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left')
            ->where(function($q) use ($idSatuanKerjaInduk){
                $q->whereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan
                    where jenis_instansi = "MasterSatuanKerjaInduk"
                    and id_instansi = '.$idSatuanKerjaInduk))), true));
                $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan join master_unit_skpd
                    on riwayat_jabatan.id_instansi = master_unit_skpd.id
                    where riwayat_jabatan.jenis_instansi = "MasterUnitSkpd" and master_unit_skpd.id_satuan_kerja_induk =  '.$idSatuanKerjaInduk))), true));
                $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan 
                    join master_sub_unit_organisasi on riwayat_jabatan.id_instansi = master_sub_unit_organisasi.id
                    join master_unit_skpd on master_unit_skpd.id = master_sub_unit_organisasi.id_unit_skpd
                    where riwayat_jabatan.jenis_instansi = "MasterSubUnitOrganisasi" and master_unit_skpd.id_satuan_kerja_induk = '.$idSatuanKerjaInduk))), true));
            });            
        }
        //eselon
        $pegawai->join('master_eselon', 'rjab.id_eselon', '=', 'master_eselon.id', 'left');
        //pendidikan
        $pegawai->with(['riwayatPendidikan' => function($query) {
            $query->orderBy('tahun_lulus', 'desc');
        }])
        ->select('pegawai.*', 'master_eselon.nama as nama_eselon');
        $pegawai = $pegawai->paginate(30);
        if ($request->ajax())
            return $pegawai;
        else {
            $menu = 'pegawai';
            return view('pegawai.index', compact('pegawai', 'menu'));
        }
    }

    public function getSimpleList()
    {
        return Pegawai::all(['id', 'nama']);
    }

    public function filter($statusKepegawaian, $eselon, $jk, $tahun_angkatan, $pendidikan)
    {
        if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin')){
            $pegawai = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left');
        }
        else{
            $idSatuanKerjaInduk = Auth::user()->pegawai->jabatanTerakhir()->getSatuanKerjaInduk()->id;
            $pegawai = Pegawai::join(DB::raw('(select id, id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left')
            ->where(function($q) use ($idSatuanKerjaInduk){
                $q->whereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan
                    where jenis_instansi = "MasterSatuanKerjaInduk"
                    and id_instansi = '.$idSatuanKerjaInduk))), true));
                $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan join master_unit_skpd
                    on riwayat_jabatan.id_instansi = master_unit_skpd.id
                    where riwayat_jabatan.jenis_instansi = "MasterUnitSkpd" and master_unit_skpd.id_satuan_kerja_induk =  '.$idSatuanKerjaInduk))), true));
                $q->orWhereIn('rjab.id', json_decode(json_encode(DB::select(DB::raw('select riwayat_jabatan.id from riwayat_jabatan 
                    join master_sub_unit_organisasi on riwayat_jabatan.id_instansi = master_sub_unit_organisasi.id
                    join master_unit_skpd on master_unit_skpd.id = master_sub_unit_organisasi.id_unit_skpd
                    where riwayat_jabatan.jenis_instansi = "MasterSubUnitOrganisasi" and master_unit_skpd.id_satuan_kerja_induk = '.$idSatuanKerjaInduk))), true));
            });            
        }
        //eselon
        $pegawai->join('master_eselon', 'rjab.id_eselon', '=', 'master_eselon.id', 'left');
        //pendidikan
        $pegawai->with(['riwayatPendidikan' => function($query) {
            $query->orderBy('tahun_lulus', 'desc');
        }]);

        if ($statusKepegawaian != "all") $pegawai->where('id_status_kepegawaian', $statusKepegawaian);
        if ($jk != "all") $pegawai->where('jns_kelamin', 'like', '%' . $jk . '%');
        if ($tahun_angkatan != "all") $pegawai->where('tahun_pengangkatan', 'like', '%' . $tahun_angkatan . '%');
        if ($eselon != "all") {
            $pegawai->where('rjab.id_eselon', $eselon);
        }
        if ($pendidikan != "all") {
            $pegawai->join(
                DB::raw('(select id_pegawai, max(id_pendidikan) as terakhir from riwayat_pendidikan group by id_pegawai) AS pendidikan_terakhir'),
                'pegawai.id', '=', 'pendidikan_terakhir.id_pegawai'
            )->where('pendidikan_terakhir.terakhir', '=', $pendidikan);
        }
        return $pegawai->select('pegawai.*', 'master_eselon.nama as nama_eselon')->paginate(30);
    }

    public function doExport($statusKepegawaian, $eselon, $jk, $tahun_angkatan, $pendidikan)
    {
        $query = Pegawai::
        join(DB::raw('(select id_pegawai, jenis_instansi, id_instansi, max(tanggal_jabatan) as last_jab, id_eselon from riwayat_jabatan group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai', 'left')
        ->with(['riwayatPendidikan' => function($query) {
            $query->orderBy('tahun_lulus', 'desc');
        }]);
        if ($statusKepegawaian != "all") $query->where('id_status_kepegawaian', $statusKepegawaian);
        if ($jk != "all") $query->where('jns_kelamin', 'like', '%' . $jk . '%');
        if ($tahun_angkatan != "all") $query->where('tahun_pengangkatan', 'like', '%' . $tahun_angkatan . '%');
        if ($eselon != "all") {
            $query->where('rjab.id_eselon', $eselon);
        }
        if ($pendidikan != "all") {
            $query->whereHas('riwayatPendidikan', function($q) use($pendidikan) {
                $q->where('id_pendidikan', $pendidikan);
            });
        }
        //export only approved record
        $query->where('is_approved', true);
        $query = $query->get();
        Excel::create("Data Pegawai", function($excel) use ($query){
            $excel->setTitle("Data Pegawai");
            $excel->sheet('Sheet1', function($sheet) use ($query) {
                $arr =array();
                foreach($query as $p) {
                    $jk = "";
                    if($p->jns_kelamin == 1){ $jk = "Laki - laki"; }
                    else{ $jk = "Perempuan"; }
                    $jp = "";
                    if($p->jenis_pegawai == 1){ $jp = "PNS Daerah Kab. Lombok Timur"; }
                    else{ $jp = 'CPNS Daerah Kab. Lombok Timur'; }

                    $lastJabatan = $p->lastJabatan();
                    if($lastJabatan){
                        if(RiwayatJabatan::find($lastJabatan->id)->getInstansiInduk()){
                            $instansiInduk = RiwayatJabatan::find($lastJabatan->id)->getInstansiInduk()->nama;
                        }else{ $instansiInduk = '-'; }
                        if(RiwayatJabatan::find($lastJabatan->id)->getsatuanKerjaInduk()){
                            $satuanKerjaInduk = RiwayatJabatan::find($lastJabatan->id)->getsatuanKerjaInduk()->nama;
                        }else{ $satuanKerjaInduk = '-'; }
                        if(RiwayatJabatan::find($lastJabatan->id)->getUnitOrganisasi()){
                            $unitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getUnitOrganisasi()->nama;
                        }else{ $unitOrganisasi = '-'; }
                        if(RiwayatJabatan::find($lastJabatan->id)->getSubUnitOrganisasi()){
                            $subUnitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getSubUnitOrganisasi()->nama;
                        }else{ $subUnitOrganisasi = '-'; }
                    }
                    else $instansiInduk = $satuanKerjaInduk = $unitOrganisasi = $subUnitOrganisasi = '-';

                    $data = [$p->nip, $p->nama, $p->masterAgama->nama, $jk, $p->email, $p->nomor_hp, $jp, $p->belongsToStatusKepegawaian->nama, $p->sk_pns, $p->jabatanTerakhirName(), ($p->jabatanTerakhir() ? $p->jabatanTerakhir()->tanggal_jabatan : "-"), $p->getKategoriJabatanName(), $p->getEselonTerakhirName(), $p->getPangkatTerbaruName(), ($p->getPangkatTerbaru() ? $p->getPangkatTerbaru()->tmt_pangkat : '-' ), $subUnitOrganisasi, $unitOrganisasi, $satuanKerjaInduk, $instansiInduk];
                    array_push($arr, $data);
                }
                  //set the titles
                $sheet->fromArray($arr,null,'A1',false,false)->prependRow(array(
                  'NIP', 'Nama', 'Agama', 'Kelamin', 'Email', 'Telpon', 'Jenis Pegawai', 'Status Pegawai', 'SK PNS', 'Jabatan Terbaru', 'TMT Jabatan', 'Kategori Jabatan Terbaru', 'Eselon Terbaru', 'Pangkat Terbaru', 'TMT Pangkat', 'Unit Kerja', 'Sub Organisasi', 'Unit Organisasi', 'Instansi Induk'
              )
            );
            });
        })->download('xls');
    }

    public function loadAllPegawai()
    {
        return Pegawai::all(['id', 'nip', 'nama']);
    }

    public function show(Request $r, $id)
    {
        if ($r->ajax()) {
            $p = Pegawai::where('id', $id)->with(['riwayatPendidikan', 'hasManyRiwayatPendidikan', 'hasManyRiwayatPendidikan.masterJurusan', 'masterAgama', 'provinsi', 'kabupaten', 'kecamatan', 'dataTambahanPegawai'])->first();
            $lastJabatan = Pegawai::find($id)->lastJabatan();
            if($lastJabatan){
                $instansiInduk = RiwayatJabatan::find($lastJabatan->id)->getInstansiInduk();
                $satuanKerjaInduk = RiwayatJabatan::find($lastJabatan->id)->getsatuanKerjaInduk();
                $unitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getUnitOrganisasi();
                $subUnitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getSubUnitOrganisasi();
            }
            else $instansiInduk = $satuanKerjaInduk = $unitOrganisasi = $subUnitOrganisasi = null;
            return [
                'pegawai' => $p,
                'masterPendidikan' => MasterPendidikan::all(),
                'masterJurusan' => MasterJurusan::all(),
                'jumlahAnak' => AnggotaKeluarga::where('id_pegawai', $id)->leftJoin('master_keluarga', 'master_keluarga.id', '=', 'anggota_keluarga.id_status')->where('master_keluarga.status', 'like', 'Anak%')->count(),
                'jumlahIstri' => AnggotaKeluarga::where('id_pegawai', $id)->leftJoin('master_keluarga', 'master_keluarga.id', '=', 'anggota_keluarga.id_status')->where('master_keluarga.status', 'like', 'Istri')->count(),
                'pendidikan' => Pegawai::find($id)->lastPendidikan(), 
                'jabatan' => $lastJabatan,
                'pangkat' => Pegawai::find($id)->lastPangkat(),
                'instansiInduk' => $instansiInduk,
                'satuanKerjaInduk' => $satuanKerjaInduk,
                'unitOrganisasi' => $unitOrganisasi,
                'subUnitOrganisasi' => $subUnitOrganisasi
            ];
        } else {
            $menu = 'pegawai';
            return view('pegawai.detail', compact('id', 'menu'));
        }
    }

    public function print(Request $r, $id)
    {
        if ($r->ajax()) {
            $p = Pegawai::where('id', $id)->with(['riwayatPendidikan', 'masterAgama', 'provinsi', 'kabupaten', 'kecamatan', 'dataTambahanPegawai'])
            ->with(['riwayatJabatan' => function($query) {
                $query->with('masterJabatan')->where('is_approved', 1);
            }])
            ->with(['riwayatPangkat' => function($query) {
                $query->with('masterPangkat')->where('is_approved', true);
            }])
            ->with(['riwayatDiklat' => function($query) {
                $query->where('is_approved', true);
            }])->first();
            $lastJabatan = Pegawai::find($id)->lastJabatan();
            if($lastJabatan){
                $instansiInduk = RiwayatJabatan::find($lastJabatan->id)->getInstansiInduk();
                $satuanKerjaInduk = RiwayatJabatan::find($lastJabatan->id)->getsatuanKerjaInduk();
                $unitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getUnitOrganisasi();
                $subUnitOrganisasi = RiwayatJabatan::find($lastJabatan->id)->getSubUnitOrganisasi();
            }
            return [
                'pegawai' => $p,
                'masterPendidikan' => MasterPendidikan::all(),
                'masterJurusan' => MasterJurusan::all(),
                'jumlahAnak' => AnggotaKeluarga::where('id_pegawai', $id)->leftJoin('master_keluarga', 'master_keluarga.id', '=', 'anggota_keluarga.id_status')->where('master_keluarga.status', 'like', 'Anak%')->count(),
                'jumlahIstri' => AnggotaKeluarga::where('id_pegawai', $id)->leftJoin('master_keluarga', 'master_keluarga.id', '=', 'anggota_keluarga.id_status')->where('master_keluarga.status', 'like', 'Istri')->count(),
                'instansiInduk' => $instansiInduk,
                'satuanKerjaInduk' => $satuanKerjaInduk,
                'unitOrganisasi' => $unitOrganisasi,
                'subUnitOrganisasi' => $subUnitOrganisasi
            ];
        } else {
            $menu = 'print';
            return view('pegawai.print', compact('id', 'menu'));
        }
    }

    public function store(Request $a)
    {
        $request = $a->all();
        if(Pegawai::where('nip', $request['nip'])->exists()){
            return response()->json([
                'error' => 'NIP sudah terdaftar'
            ]);
        }
        $file = $a->file('poto');
        if ($file != null) {
            $photo = 'images/pegawai/pegawai_' . str_random(30) . '.' . $file->getClientOriginalExtension();
            Image::make($file)->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($photo);
        } else {
            $photo = 'images/default_poto_pegawai.png';
        }
        $request['poto'] = $photo;
        $p = Pegawai::create($request);
    }

    public function approvePegawai($id, Request $a)
    {
        $req = $a->all();
        $req['is_approved'] = true;
        $pegawai = Pegawai::find($id)->update($req);
    }

    public function unapprovePegawai($id, Request $a)
    {
        $req = $a->all();
        $req['is_approved'] = false;
        $pegawai = Pegawai::find($id)->update($req);
    }

    public function edit($id)
    {
        $akun = User::find($id);
        return view('auth.edit', compact('akun'));
    }

    public function update($id, Request $r)
    {
        $data = Pegawai::find($id);
        $new = $r->all();
        $foto = $r->file('poto');
        if ($foto != null) {
            if ($data->poto != 'images/default_poto_pegawai.png') {
                File::delete(public_path($data->poto));
            }
            $this->validate($r, [
                'picture' => 'image'
            ]);
            $nama = 'images/pegawai/pegawai_' . str_random(30) . '.' . $foto->getClientOriginalExtension();
            Image::make($foto)->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($nama);
            $new['poto'] = $nama;
        } else {
            if ($new['hapus_foto']) {
                if ($data->poto != 'images/default_poto_pegawai.png') {
                    File::delete(public_path($data->poto));
                    $new['poto'] = 'images/default_poto_pegawai.png';
                }
            }
        }
        $data->update($new);
    }

    public function destroy($id)
    {
        Pegawai::find($id)->delete();
    }

    public function restore($id)
    {
        Pegawai::onlyTrashed()->where('id', $id)->restore();
// return redirect('akun');
    }

    public function getRiwayatJabatan($id)
    {
        return RiwayatJabatan::where('id_pegawai', $id)->with([
            'masterJabatan',
            'masterEselon',
            'golonganAwal',
            'golonganTerbaru',
            'atasan'
        ])
        ->orderBy('tanggal_jabatan', 'DESC')
        ->get();
    }

    public function getRiwayatPendidikan($id)
    {
        return Pegawai::find($id)->riwayatPendidikan()->orderBy('tahun_lulus','DESC')->get();
    }

    public function getAnggotaKeluarga($id)
    {
        return AnggotaKeluarga::where('id_pegawai', $id)->with(['masterKeluarga', 'masterPendidikan', 'masterPekerjaan'])->get();
    }

    public function getRiwayatDiklat($id)
    {
        return RiwayatDiklat::where('id_pegawai', $id)->orderBy('tanggal_pelaksanaan', 'DESC')->with('masterDiklat')->get();
    }

    public function getProvinsi()
    {
        return DB::table('provinsi')->get();
    }

    public function getKabupaten($id)
    {
        return DB::table('kabupaten')->where('id_provinsi', $id)->get();
    }

    public function getKecamatan($id)
    {
        return DB::table('kecamatan')->where('id_kabupaten', $id)->get();
    }

    public function searchByNip($nip)
    {
        return Pegawai::where('nip', 'LIKE', '%' . $nip . '%')->get();
    }

    public function getPensiun($id)
    {
        return Pensiun::where('id_pegawai', $id)->orderBy('tanggal_sk_pensiun', 'DESC')->get();
    }

    public function getRiwayatSkp($id)
    {
        return RiwayatSkp::where('id_pegawai', $id)
        ->orderBy('tahun_skp', 'DESC')
        ->with('pegawai', 'pegawaiPenilai', 'atasanPegawaiPenilai')
        ->get();
    }

    public function getRiwayatPangkat($id)
    {
        return RiwayatPangkat::where('id_pegawai', $id)
        ->orderBy('tmt_pangkat', 'DESC')
        ->with('pegawai', 'masterPangkat')
        ->get();
    }

    public function getRiwayatHukuman($id)
    {
        return RiwayatHukuman::where('id_pegawai', $id)
        ->orderBy('tanggal_sk_hukuman', 'DESC')
        ->with('masterHukuman')
        ->get();
    }

    public function getRiwayatCuti($id)
    {
        return RiwayatCuti::where('id_pegawai', $id)->orderBy('tanggal_sk_cuti', 'DESC')->get();
    }

    public function getRiwayatAbsensi($id)
    {
        return RiwayatAbsensi::where('id_pegawai', $id)->orderBy('created_at', 'DESC')->get();
    }

    public function getHartaPejabat($id)
    {
        return HartaPejabat::where('id_pegawai', $id)->orderBy('tanggal_laporan', 'DESC')->get();
    }

    public function getGolonganAwal($id)
    {
        return RiwayatPangkat::where('id_pegawai', $id)->with('masterPangkat')->orderBy('tanggal_sk_pangkat', 'asc')->first();
    }

    public function getGolonganTerbaru($id)
    {
        return RiwayatPangkat::where('id_pegawai', $id)->with('masterPangkat')->orderBy('tanggal_sk_pangkat', 'desc')->first();
    }

    public function getMasterUnitSkpd($id)
    {
        $data = Pegawai::whereHas('riwayatJabatan', function($query) use($id){
            $query->whereHas('masterUnitSkpd', function($query) use($id){
                $query->where('id', $id);
            });
        })
        //harus pegawai aktif
        ->where('id_status_kepegawaian', 1)
        ->get();
        return $data;
    }

    public function publicDetail($id){
        $pegawai = Pegawai::find($id);
        $pangkat = $pegawai->lastPangkat();
        $jabatan = $pegawai->lastJabatan();
        $pendidikan = $pegawai->lastPendidikan();
        if($jabatan){
            $instansiInduk = RiwayatJabatan::find($jabatan->id)->getInstansiInduk();
            $satuanKerjaInduk = RiwayatJabatan::find($jabatan->id)->getsatuanKerjaInduk();
            $unitOrganisasi = RiwayatJabatan::find($jabatan->id)->getUnitOrganisasi();
            $subUnitOrganisasi = RiwayatJabatan::find($jabatan->id)->getSubUnitOrganisasi();
        }
        else $instansiInduk = $satuanKerjaInduk = $unitOrganisasi = $subUnitOrganisasi = null;
        return view('pegawai.public_detail', compact('pegawai', 'pangkat', 'jabatan', 'pendidikan', 'instansiInduk', 'unitOrganisasi', 'subUnitOrganisasi','satuanKerjaInduk'));
    }

    public function byInstansi(Request $r){
        $pegawai = DB::select(DB::raw("select * from `pegawai` inner join (select id_pegawai, jenis_instansi, id_instansi, max(tanggal_jabatan) as last_jab from riwayat_jabatan group by id_pegawai) AS rjab on `pegawai`.`id` = `rjab`.`id_pegawai` where `rjab`.`jenis_instansi` = \"".$r->jenis_instansi."\" and `rjab`.`id_instansi` = ".$r->id_instansi));
        return $pegawai;
    }

}
