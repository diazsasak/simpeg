<?php
//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterPangkat;

class MasterPangkatController extends Controller
{
    public function index(Request $r)
      {
            $data = MasterPangkat::all();
            if($r->ajax()) return $data;
            else {
                  $menu = 'master';
                  $submenu = 'pangkat';
                  return view('master_pangkat.index', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterPangkat::create($r->all());
            return redirect('master_pangkat');
      }

      public function edit($id)
      {
            $data = MasterPangkat::find($id);
            return view('master_pangkat.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterPangkat::find($id)->update($r->all());
            return redirect('master_pangkat');
      }

      public function destroy($id)
      {
            MasterPangkat::find($id)->delete();
            return redirect('master_pangkat');
      }

}
