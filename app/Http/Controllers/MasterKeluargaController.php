<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterKeluarga;

class MasterKeluargaController extends Controller
{

      public function index(Request $r)
      {
            $data = MasterKeluarga::all();
            if($r->ajax()) return $data;
            $menu = 'master';
            $submenu = 'keluarga';
            return view('master_keluarga.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterKeluarga::create($r->all());
            return redirect('master_keluarga');
      }

      public function edit($id)
      {
            $data = MasterKeluarga::find($id);
            return view('master_keluarga.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterKeluarga::find($id)->update($r->all());
            return redirect('master_keluarga');
      }

      public function destroy($id)
      {
            MasterKeluarga::find($id)->delete();
            return redirect('master_keluarga');
      }

}
