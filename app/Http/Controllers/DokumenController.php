<?php

//diaz

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dokumen;
use Auth;
use File;

class DokumenController extends Controller
{

      public function index(Request $r)
      {
            $data = Dokumen::withTrashed()
                    ->with('masterKategoriArtikel', 'user')
                    ->paginate(10);
            if ($r->ajax())
                  return $data;
            else {
                  $menu = 'dokumen';
                  return view('dokumen.index', compact('data', 'menu'));
            }
      }

      public function store(Request $a)
      {
            $request = $a->all();
            $file = $a->file('dokumen');
            if ($file != null) {
                  $fileName = $file->getClientOriginalName(); // renameing image
                  $a->file('dokumen')->move('upload/dokumen', $fileName); // uploading file to given path
                  $request['document_path'] = 'upload/dokumen/' . $fileName;
            } else {
                  $request['document_path'] = '';
            }
            $request['id_penulis'] = Auth::id();
            Dokumen::create($request);
      }

      public function destroy($id)
      {
            Dokumen::find($id)->delete();
      }

      public function restore($id)
      {
            Dokumen::onlyTrashed()->where('id', $id)->restore();
      }

      public function update($id, Request $a)
      {
            $data = Dokumen::find($id);
            $request = $a->all();
            $file = $a->file('dokumen');
            if ($file != null) {
                  if ($data->document_path != '') {
                        File::delete(public_path($data->document_path));
                  }
                  $fileName = $file->getClientOriginalName(); // renameing image
                  $a->file('dokumen')->move('upload/dokumen', $fileName); // uploading file to given path
                  $request['document_path'] = 'upload/dokumen/' . $fileName;
            }
            $data->update($request);
      }

      public function publish($id, Request $a)
      {
            $req = $a->all();
            $req['published'] = true;
            Dokumen::find($id)->update($req);
      }

      public function unpublish($id, Request $a)
      {
            $req = $a->all();
            $req['published'] = false;
            Dokumen::find($id)->update($req);
      }

}
