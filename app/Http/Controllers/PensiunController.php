<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pensiun;
use App\Pegawai;

class PensiunController extends Controller
{
	public function store(Request $r)
	{
		$r = $r->all();
		Pegawai::find($r['id_pegawai'])->pensiun()->create($r);
	}

	public function update($id, Request $r)
	{
		Pensiun::find($id)->update($r->all());
	}

	public function destroy($id)
	{
		Pensiun::find($id)->delete();
	}

	public function approvePensiun($id, Request $a)
	{
		$req['is_approved'] = true;
		Pensiun::find($id)->update($req);
	}

	public function unapprovePensiun($id, Request $a)
	{
		$req['is_approved'] = false;
		Pensiun::find($id)->update($req);
	}
}
