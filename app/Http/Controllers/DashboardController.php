<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use DB;

class DashboardController extends Controller
{

      public function index(Request $r)
      {
            $menu = 'dashboard';
            return view('dashboard.index', compact('menu'));
      }

      public function getJkDiagram()
      {
            $data = DB::select(DB::raw("SELECT COUNT(CASE WHEN jns_kelamin = 1 THEN 1 END) as l_count,
			COUNT(CASE WHEN jns_kelamin = 0 THEN 1 END) as p_count, 
			tahun_pengangkatan FROM pegawai group by tahun_pengangkatan"));
            return $data;
      }

}
