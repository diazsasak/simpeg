<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterKategoriArtikel;

class MasterKategoriArtikelController extends Controller
{
    public function index(Request $r)
      {
            $data = MasterKategoriArtikel::all();
            if ($r->ajax())
                  return $data;
            $menu = 'master';
            $submenu = 'kategori_artikel';
            return view('master_kategori_artikel.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterKategoriArtikel::create($r->all());
            return redirect('master_kategori_artikel');
      }

      public function edit($id)
      {
            $data = MasterKategoriArtikel::find($id);
            return view('master_kategori_artikel.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterKategoriArtikel::find($id)->update($r->all());
            return redirect('master_kategori_artikel');
      }

      public function destroy($id)
      {
            MasterKategoriArtikel::find($id)->delete();
            return redirect('master_kategori_artikel');
      }
}
