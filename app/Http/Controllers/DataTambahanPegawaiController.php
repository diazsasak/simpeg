<?php
//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTambahanPegawai;
use App\Pegawai;

class DataTambahanPegawaiController extends Controller
{

      public function index(Request $request)
      {
            $data = DataTambahanPegawai::all();
            if ($request->ajax())
                  return $data;
            else {
                  $menu = 'pegawai';
                  $submenu = 'semua';
                  return view('pegawai.data_tambahan', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            DataTambahanPegawai::create($r->all());
            // return redirect('pegawai');
      }

      public function update($id, Request $r)
      {
            if (Pegawai::find($id)->has('dataTambahanPegawai')->count() == 0) {
                  DataTambahanPegawai::create($r->all());
            }
            else DataTambahanPegawai::find($id)->update($r->all());
            // return redirect('pegawai');
      }

      public function destroy($id)
      {
            DataTambahanPegawai::find($id)->delete();
            // return redirect('pegawai');
      }

}
