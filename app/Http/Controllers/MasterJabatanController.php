<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterJabatan;
use App\MasterKategoriJabatan;

class MasterJabatanController extends Controller
{

      public function index()
      {
            $data = MasterJabatan::orderBy('nama', 'asc')->get();
            $kj = MasterKategoriJabatan::all(['id', 'nama']);
            $menu = 'master';
            $submenu = 'jabatan';
            return view('master_jabatan.index', compact('data', 'menu', 'submenu', 'kj'));
      }

      public function store(Request $r)
      {
            MasterJabatan::create($r->all());
            return redirect('master_jabatan');
      }

      public function edit($id)
      {
            $data = MasterJabatan::find($id);
            return view('master_jabatan.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterJabatan::find($id)->update($r->all());
            return redirect('master_jabatan');
      }

      public function destroy($id)
      {
            MasterJabatan::find($id)->delete();
            return redirect('master_jabatan');
      }

}
