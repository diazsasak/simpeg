<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterJenisUnit;

class MasterJenisUnitController extends Controller
{

      public function index(Request $r)
      {
            $data = MasterJenisUnit::all();
            if ($r->ajax())
                  return $data;
            $menu = 'master';
            $submenu = 'jenis_unit';
            return view('master_jenis_unit.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterJenisUnit::create($r->all());
            return redirect('master_jenis_unit');
      }

      public function edit($id)
      {
            $data = MasterJenisUnit::find($id);
            return view('master_jenis_unit.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterJenisUnit::find($id)->update($r->all());
            return redirect('master_jenis_unit');
      }

      public function destroy($id)
      {
            MasterJenisUnit::find($id)->delete();
            return redirect('master_jenis_unit');
      }

}
