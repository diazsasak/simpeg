<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterSatuanKerjaInduk;
use App\MasterInstansiInduk;
use App\MasterJenisUnit;

class MasterSatuanKerjaIndukController extends Controller
{

      public function index(Request $request)
      {
            $data = MasterSatuanKerjaInduk::all();
            if ($request->ajax())
                  return $data;
            else {
                  $ju = MasterJenisUnit::all(['id', 'nama']);
                  $mii = MasterInstansiInduk::all(['id', 'kode_instansi_induk', 'nama']);
                  $menu = 'master';
                  $submenu = 'satuan_kerja_induk';
                  return view('master_satuan_kerja_induk.index', compact('data', 'mii', 'menu', 'submenu', 'ju'));
            }
      }

      public function store(Request $r)
      {
            MasterSatuanKerjaInduk::create($r->all());
            return redirect('master_satuan_kerja_induk');
      }

      public function update($id, Request $r)
      {
            MasterSatuanKerjaInduk::find($id)->update($r->all());
            return redirect('master_satuan_kerja_induk');
      }

      public function show($id)
      {
            return MasterSatuanKerjaInduk::find($id);
      }

      public function destroy($id)
      {
            MasterSatuanKerjaInduk::find($id)->delete();
            return redirect('master_satuan_kerja_induk');
      }

      public function getMasterUnitSkpd($id){
            return MasterSatuanKerjaInduk::find($id)->masterUnitSkpd()->get(['id', 'nama']);
      }

      public function getPegawaiNotUser($id){
            return MasterSatuanKerjaInduk::getPegawai($id);
      }

}
