<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterAgama;

class MasterAgamaController extends Controller
{
      public function index(Request $request)
      {
            $data = MasterAgama::all();
            if ($request->ajax())
                  return $data;
            else {
                  $menu = 'master';
                  $submenu = 'agama';
                  return view('master_agama.index', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterAgama::create($r->all());
            return redirect('master_agama');
      }

      public function edit($id)
      {
            $data = MasterAgama::find($id);
            return view('master_agama.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterAgama::find($id)->update($r->all());
            return redirect('master_agama');
      }

      public function destroy($id)
      {
            MasterAgama::find($id)->delete();
            return redirect('master_agama');
      }

}
