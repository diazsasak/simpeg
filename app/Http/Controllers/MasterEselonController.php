<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterEselon;

class MasterEselonController extends Controller
{

      public function index(Request $request)
      {
            $data = MasterEselon::all(['id', 'nama']);
            if ($request->ajax())
                  return $data;
            else {
                  $menu = 'master';
                  $submenu = 'eselon';
                  return view('master_eselon.index', compact('data', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterEselon::create($r->all());
            return redirect('master_eselon');
      }

      public function edit($id)
      {
            $data = MasterEselon::find($id);
            return view('master_eselon.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterEselon::find($id)->update($r->all());
            return redirect('master_eselon');
      }

      public function destroy($id)
      {
            MasterEselon::find($id)->delete();
            return redirect('master_eselon');
      }

}
