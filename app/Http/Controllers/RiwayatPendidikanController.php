<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPendidikan;
use App\Pegawai;

class RiwayatPendidikanController extends Controller
{

      public function store(Request $r)
      {
            $r = $r->all();
            Pegawai::find($r['id_pegawai'])->riwayatPendidikan()->attach($r['id_pendidikan'], $r);
      }

      public function update($id, Request $r)
      {
            RiwayatPendidikan::find($id)->update($r->all());
      }

      public function destroy($id)
      {
            RiwayatPendidikan::find($id)->delete();
      }

}
