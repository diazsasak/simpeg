<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterSatuanKerja;
use App\MasterInstansiKerja;

class MasterSatuanKerjaController extends Controller
{

      public function index(Request $request)
      {
            $data = MasterSatuanKerja::all();
            if ($request->ajax())
                  return $data;
            else {
                  $master_instansi_kerja = MasterInstansiKerja::all(['id', 'nama']);
                  $menu = 'master';
                  $submenu = 'satuan_kerja';
                  return view('master_satuan_kerja.index', compact('data', 'master_instansi_kerja', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterSatuanKerja::create($r->all());
            return redirect('master_satuan_kerja');
      }

      public function update($id, Request $r)
      {
            MasterSatuanKerja::find($id)->update($r->all());
            return redirect('master_satuan_kerja');
      }

      public function destroy($id)
      {
            MasterSatuanKerja::find($id)->delete();
            return redirect('master_satuan_kerja');
      }

}
