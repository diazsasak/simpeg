<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatJabatan;
use App\Pegawai;
use App\User;
use App\MasterSubUnitOrganisasi;
use DB;

class RiwayatJabatanController extends Controller
{
      public function store(Request $r)
      {
            $r = $r->all();
            Pegawai::find($r['id_pegawai'])->riwayatJabatan()->create($r);
      }

      public function update($id, Request $r)
      {
            RiwayatJabatan::find($id)->update($r->all());
      }

      public function destroy($id)
      {
            $riwayatJabatan = RiwayatJabatan::find($id);
            $idPegawai = $riwayatJabatan->id_pegawai;
            $riwayatJabatan->delete();
      }

      public function approveRiwayatJabatan($id, Request $a)
      {
            $req['is_approved'] = true;
            $riwayatJabatan = RiwayatJabatan::find($id);
            $idPegawai = $riwayatJabatan->id_pegawai;
            $riwayatJabatan->update($req);
      }

      public function unapproveRiwayatJabatan($id, Request $a)
      {
            $req['is_approved'] = false;
            $riwayatJabatan = RiwayatJabatan::find($id);
            $idPegawai = $riwayatJabatan->id_pegawai;
            $riwayatJabatan->update($req);
      }
}
