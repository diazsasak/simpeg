<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Artikel;

class LayananController extends Controller
{
	public function getLayanan($jenis){

		$data = Artikel::whereHas('masterKategoriArtikel', function($query) use ($jenis) {
				$query->where('nama', $jenis);
			})->where('published', 1)->paginate(10);

		$types = array(
            'layanan-sekretariat-bkpsdm'  => 'Sekretariat BKPSDM',
            'layanan-bidang-mutasi'        => 'Bidang Mutasi',
            'layanan-bidang-disiplin'     => 'Bidang Disiplin',
            'layanan-bidang-data-dan-formasi'     => 'Bidang Data dan Formasi',
            'layanan-bidang-pengembangan-sdm'        => 'Bidang Pengembangan SDM'
            );

		$title = $types[$jenis];
		return view('layanan_kepegawaian.index', compact('data', 'title'));
	}
}
