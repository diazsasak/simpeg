<?php
//ridwan

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterHukuman;

class MasterHukumanController extends Controller
{
	public function index(Request $request)
	{
		$data = MasterHukuman::all();
		if ($request->ajax())
			return $data;
		else {
			$menu = 'master';
			$submenu = 'hukuman';
			return view('master_hukuman.index', compact('data', 'menu', 'submenu'));
		}
	}

	public function store(Request $r)
	{
		MasterHukuman::create($r->all());
		return redirect('master_hukuman');
	}

	public function edit($id)
	{
		$data = MasterHukuman::find($id);
		return view('master_hukuman.edit', compact('data'));
	}

	public function update($id, Request $r)
	{
		MasterHukuman::find($id)->update($r->all());
		return redirect('master_hukuman');
	}

	public function destroy($id)
	{
		MasterHukuman::find($id)->delete();
		return redirect('master_hukuman');
	}
}
