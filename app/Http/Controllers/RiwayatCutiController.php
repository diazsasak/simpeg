<?php
//diaz
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
use App\RiwayatCuti;

class RiwayatCutiController extends Controller
{
    public function store(Request $r)
      {
            $r = $r->all();
            Pegawai::find($r['id_pegawai'])->riwayatCuti()->create($r);
      }

      public function update($id, Request $r)
      {
            RiwayatCuti::find($id)->update($r->all());
      }

      public function destroy($id)
      {
            RiwayatCuti::find($id)->delete();
      }

      public function approve($id, Request $a)
      {
            $req['is_approved'] = true;
            RiwayatCuti::find($id)->update($req);
      }

      public function unapprove($id, Request $a)
      {
            $req['is_approved'] = false;
            RiwayatCuti::find($id)->update($req);
      }
}
