<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterJurusan;

class MasterJurusanController extends Controller
{

      public function index()
      {
            $data = MasterJurusan::all();
            $menu = 'master';
            $submenu = 'jurusan';
            return view('master_jurusan.index', compact('data', 'menu', 'submenu'));
      }

      public function store(Request $r)
      {
            MasterJurusan::create($r->all());
            return redirect('master_jurusan');
      }

      public function edit($id)
      {
            $data = MasterJurusan::find($id);
            return view('master_jurusan.edit', compact('data'));
      }

      public function update($id, Request $r)
      {
            MasterJurusan::find($id)->update($r->all());
            return redirect('master_jurusan');
      }

      public function destroy($id)
      {
            MasterJurusan::find($id)->delete();
            return redirect('master_jurusan');
      }

}
