<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;
// use App\RiwayatPangkat;
use App\MasterPangkat;
use App\MasterAgama;
use App\MasterPendidikan;
use App\MasterEselon;
use App\MasterJabatan;
use DB;

class RekapitulasiPegawaiController extends Controller
{

	public function index(Request $r)
	{
		// pangkat
		$masterPangkat = MasterPangkat::all();
		$pangkat = array();
		foreach ($masterPangkat as $value) {
			$jmlL = DB::select(DB::raw('select count(*) as jml, riwayat_pangkat.id_pangkat from riwayat_pangkat, pegawai, (
				select id, max(tmt_pangkat) as max_tmt_pangkat, id_pegawai from riwayat_pangkat group by riwayat_pangkat.id_pegawai
			) as pangkat_terakhir
			where riwayat_pangkat.id_pegawai = pegawai.id
			and riwayat_pangkat.tmt_pangkat = pangkat_terakhir.max_tmt_pangkat
			and pegawai.id = pangkat_terakhir.id_pegawai
			and pegawai.jns_kelamin = 1
			and pegawai.is_approved = 1
			and pegawai.id_status_kepegawaian = 1
			and riwayat_pangkat.id_pangkat = '.$value->id.'
			group by riwayat_pangkat.id_pangkat'
		));
			if($jmlL) $jmlL = $jmlL[0]->jml;
			else $jmlL = 0;
			$jmlP = DB::select(DB::raw('select count(*) as jml, riwayat_pangkat.id_pangkat from riwayat_pangkat, pegawai, (
				select id, max(tmt_pangkat) as max_tmt_pangkat, id_pegawai from riwayat_pangkat group by riwayat_pangkat.id_pegawai
			) as pangkat_terakhir
			where riwayat_pangkat.id_pegawai = pegawai.id
			and riwayat_pangkat.tmt_pangkat = pangkat_terakhir.max_tmt_pangkat
			and pegawai.id = pangkat_terakhir.id_pegawai
			and pegawai.jns_kelamin = 0
			and pegawai.is_approved = 1
			and pegawai.id_status_kepegawaian = 1
			and riwayat_pangkat.id_pangkat = '.$value->id.'
			group by riwayat_pangkat.id_pangkat'
		));
			if($jmlP) $jmlP = $jmlP[0]->jml;
			else $jmlP = 0;
			array_push($pangkat, array('nama_golongan' => $value->nama_golongan, 'jmlL' => $jmlL, 'jmlP' => $jmlP));
		}

		// jenis kelamin
		$jenisKelamin = array();
		array_push($jenisKelamin, array('nama' => 'Laki-laki', 'jml' => Pegawai::where('id_status_kepegawaian', 1)->where('jns_kelamin', true)->where('is_approved', 1)->get()->count()));
		array_push($jenisKelamin, array('nama' => 'Perempuan', 'jml' => Pegawai::where('id_status_kepegawaian', 1)->where('jns_kelamin', false)->where('is_approved', 1)->get()->count()));

		// agama
		$agama = Pegawai::where('id_status_kepegawaian', 1)->groupBy('id_agama')->get(['id_agama', DB::raw('count(*) as jml')]);
		$masterAgama = MasterAgama::all();
		$agama = array();
		foreach ($masterAgama as $value) {
			$tmp = Pegawai::where('id_status_kepegawaian', 1)->where('id_agama', $value->id)->where('is_approved', 1)->get()->count();
			array_push($agama, array('nama' => $value->nama, 'jml' => $tmp));
		}

		// pendidikan
		$masterPendidikan = MasterPendidikan::all();
		$pendidikan = array();
		foreach ($masterPendidikan as $key => $value) {
			$jml = Pegawai::where('id_status_kepegawaian', 1)->with(['riwayatPendidikan' => function($query) { $query->orderBy('tahun_lulus', 'desc'); }])
			->join(DB::raw('(select id_pegawai, max(id_pendidikan) as terakhir from riwayat_pendidikan group by id_pegawai) AS pendidikan_terakhir'),'pegawai.id', '=', 'pendidikan_terakhir.id_pegawai')->where('pendidikan_terakhir.terakhir', '=', $value->id)
			->where('is_approved', 1)->get()->count();
			array_push($pendidikan, array('nama' => $value->nama, 'jml' => $jml));
		}

		// kategori jabatan - struktural
		$masterEselon = MasterEselon::all();
		$eselon = array();
		foreach ($masterEselon as $key => $value) {
			$tmpL = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai')
			->where('pegawai.id_status_kepegawaian', 1)
			->where('id_eselon', $value->id)
			->where('jns_kelamin', 1)
			->where('is_approved', 1)
			->get()->count();
			$tmpP = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved=true group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai')
			->where('pegawai.id_status_kepegawaian', 1)
			->where('id_eselon', $value->id)
			->where('jns_kelamin', 0)
			->where('is_approved', 1)
			->get()->count();
			array_push($eselon, array('nama' => $value->nama, 'jmlL' => $tmpL, 'jmlP' => $tmpP));
		}

		// kategori jabatan - fungsional tertentu
		$masterFungsionalTertentu = MasterJabatan::whereHas('kategoriJabatan', function($query) {
			$query->where('nama', 'Fungsional Tertentu');
		})->get();
		$fungsionalTertentu = array();
		foreach ($masterFungsionalTertentu as $value) {
			$jml = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved = true and id_jabatan = '.$value->id.' group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai')
			->where('pegawai.id_status_kepegawaian', 1)
			->where('is_approved', 1)
			->get()->count();
			array_push($fungsionalTertentu, array('nama' => $value->nama, 'jml' => $jml));
		}

		// kategori jabatan - fungsional umum/staff
		$masterFungsionalUmum = MasterJabatan::whereHas('kategoriJabatan', function($query) {
			$query->where('nama', 'Fungsional Umum');
		})->get();
		$fungsionalUmum = array();
		foreach ($masterFungsionalUmum as $value) {
			$jml = Pegawai::join(DB::raw('(select id_pegawai, jenis_instansi, id_eselon, id_instansi, max(tanggal_jabatan) as last_jab, id_sub_unit_organisasi from riwayat_jabatan where is_approved = true and id_jabatan = '.$value->id.' group by id_pegawai) AS rjab'), 'pegawai.id', '=', 'rjab.id_pegawai')
			->where('pegawai.id_status_kepegawaian', 1)
			->where('is_approved', 1)
			->get()->count();
			array_push($fungsionalUmum, array('nama' => $value->nama, 'jml' => $jml));
		}
		
		$menu = 'rekapitulasi-pegawai';
		if($r->ajax()){
			return [
				'agama' => $agama, 'jenisKelamin' => $jenisKelamin, 'pangkat' => $pangkat, 'pendidikan' => $pendidikan, 'eselon' => $eselon, 'fungsionalTertentu' => $fungsionalTertentu, 'fungsionalUmum' => $fungsionalUmum 
			];
		}
		else{
			return view('rekapitulasi_pegawai.index', compact('menu', 'agama', 'jenisKelamin', 'pangkat', 'pendidikan', 'eselon', 'fungsionalTertentu', 'fungsionalUmum'));
		}
	}
}
