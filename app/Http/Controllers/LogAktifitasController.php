<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use Auth;

class LogAktifitasController extends Controller
{

      public function index()
      {
            if ((Auth::user()->user_level != 'super_admin') && (Auth::user()->user_level != 'kaban') && (Auth::user()->user_level != 'kabin')) {
                  return redirect('pegawai');
            } else {
                  $data = Activity::orderBy('created_at', 'DESC')->paginate(30);
                  $menu = 'log';
                  // foreach ($data as $a) {
                  // 	var_dump($a->description);
                  // }

                  return view('log_aktifitas.index', compact('data', 'menu'));
            }
      }

}
