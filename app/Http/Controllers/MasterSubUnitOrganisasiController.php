<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MasterUnitSkpd;
use App\MasterSubUnitOrganisasi;
use App\MasterEselon;
use App\MasterJenisUnit;

class MasterSubUnitOrganisasiController extends Controller
{
    public function index(Request $request)
      {
            if ($request->ajax())
                  return MasterSubUnitOrganisasi::all();
            else {
                  $data = MasterSubUnitOrganisasi::paginate(30);
                  $us = MasterUnitSkpd::all(['id', 'nama']);
                  $e = MasterEselon::all(['id', 'nama']);
                  $ju = MasterJenisUnit::all(['id', 'nama']);
                  $menu = 'master';
                  $submenu = 'sub_unit_organisasi';
                  return view('master_sub_unit_organisasi.index', compact('data', 'us', 'e', 'ju', 'menu', 'submenu'));
            }
      }

      public function store(Request $r)
      {
            MasterSubUnitOrganisasi::create($r->all());
            return redirect('master_sub_unit_organisasi');
      }

      public function edit($id)
      {
            $data = MasterSubUnitOrganisasi::find($id);
            return view('master_sub_unit_organisasi.edit', compact('data'));
      }
      
      public function show($id)
      {
            return MasterSubUnitOrganisasi::find($id);
      }

      public function update($id, Request $r)
      {
            MasterSubUnitOrganisasi::find($id)->update($r->all());
            return redirect('master_sub_unit_organisasi');
      }

      public function destroy($id)
      {
            MasterSubUnitOrganisasi::find($id)->delete();
            return redirect('master_sub_unit_organisasi');
      }
}
