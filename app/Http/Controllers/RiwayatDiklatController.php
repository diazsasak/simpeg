<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatDiklat;
use App\Pegawai;
use Image;
use File;

class RiwayatDiklatController extends Controller
{

      public function store(Request $rr)
      {
            $r = $rr->all();
            $file = $rr->file('sertifikat');
            if ($file != null) {
                  $photo = 'images/pegawai/sertifikat_diklat/sertifikat_diklat_' . str_random(30) . '.' . $file->getClientOriginalExtension();
                  Image::make($file)->save($photo);
            } else {
                  $photo = 'images/default_poto_pegawai.png';
            }
            $r['sertifikat'] = $photo;
            Pegawai::find($r['id_pegawai'])->riwayatDiklat()->create($r);
      }

      public function update($id, Request $rr)
      {
            $r = $rr->all();
            $file = $rr->file('sertifikat');
            $data = RiwayatDiklat::find($id);
            if ($file != null) {
                  if ($data->sertifikat != 'images/default_poto_pegawai.png') {
                        File::delete(public_path($data->sertifikat));
                  }
                  $photo = 'images/pegawai/sertifikat_diklat/sertifikat_diklat_' . str_random(30) . '.' . $file->getClientOriginalExtension();
                  Image::make($file)->save($photo);
                  $r['sertifikat'] = $photo;
            }
            if(isset($r['hapus_foto']) && $r['hapus_foto']){
                  if ($data->sertifikat != 'images/default_poto_pegawai.png') {
                        File::delete(public_path($data->sertifikat));
                        $r['sertifikat'] = 'images/default_poto_pegawai.png';
                  }
            }
            RiwayatDiklat::find($id)->update($r);
      }

      public function destroy($id)
      {
            RiwayatDiklat::find($id)->delete();
      }

      public function approveRiwayatDiklat($id, Request $a)
      {
            $req['is_approved'] = true;
            $pegawai = RiwayatDiklat::find($id)->update($req);
      }

      public function unapproveRiwayatDiklat($id, Request $a)
      {
            $req['is_approved'] = false;
            $pegawai = RiwayatDiklat::find($id)->update($req);
      }

      public function diklatPegawai()
      {
            $submenu = "diklat_pegawai";
            return view('diklat_pegawai.index', compact('submenu'));
      }

}
