<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StrukturOrganisasi;
use App\Pegawai;
use Session;

class StrukturOrganisasiController extends Controller
{
	public function index(){
		$data = StrukturOrganisasi::all();
		$menu = 'struktur_organisasi';
		return view('struktur_organisasi.index', compact('data', 'menu'));
	}

	public function store(Request $r){
		$rr = $r->all();
		for($i = 0; $i < sizeOf($rr['id']); $i++){
			$so = StrukturOrganisasi::find($rr['id'][$i]);
			$so->update([
				'nama_jabatan' => $rr['nama_jabatan'][$i]
			]);
			if($rr['nip'][$i] != ''){
				$pegawai = Pegawai::where('nip', $rr['nip'][$i])->first();
				if($pegawai != null){
					$so->update([
						'id_pegawai' => $pegawai->id 
					]);
				}
				else{
					Session::put('fail', 'NIP : '.$rr['nip'][$i].' tidak terdaftar');
					return back();
				}
			}
		}
		return back();
	}

}
