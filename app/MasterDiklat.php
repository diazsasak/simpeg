<?php

//diaz

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterDiklat extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['jenis', 'nama'];
      protected $fillable = ['jenis', 'nama'];
      protected $table = 'master_diklat';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function riwayatDiklat()
      {
            return $this->hasMany('App\RiwayatDiklat', 'id_diklat', 'id');
      }

}
