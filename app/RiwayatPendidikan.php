<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatPendidikan extends Model
{
      use LogsActivity;

      protected static $logAttributes = ['id_pegawai', 'id_pendidikan', 'id_jurusan', 'nama_institusi', 'tahun_lulus', 'no_ijazah', 'gelar_depan', 'gelar_belakang'];
      protected $fillable = ['id_pegawai', 'id_pendidikan', 'id_jurusan', 'nama_institusi', 'tahun_lulus', 'no_ijazah', 'gelar_depan', 'gelar_belakang'];
      protected $table = 'riwayat_pendidikan';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }

      public function masterPendidikan()
      {
            return $this->belongsTo('App\MasterPendidikan', 'id_pendidikan', 'id');
      }

      public function masterJurusan()
      {
            return $this->belongsTo('App\MasterJurusan', 'id_jurusan', 'id');
      }

      public function pendidikanTerakhir(){
            return $this->orderBy('tahun_lulus', 'DESC')->first();
      }

}
