<?php
//diaz
namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatJabatan extends Model
{

      // use SoftDeletes;
      use LogsActivity;

      protected static $logAttributes = ['id_pegawai', 'id_jabatan', 'id_eselon', 'bobot_jabatan', 'sk_jabatan', 'tanggal_sk', 'is_jabatan_sekarang', 'id_sub_unit_organisasi', 'golongan_awal', 'golongan_terbaru', 'id_pegawai_atasan', 'is_keluar_daerah', 'keterangan', 'tanggal_jabatan', 'is_approved', 'jenis_instansi', 'id_instansi'];
      protected $fillable = ['id_pegawai', 'id_jabatan', 'id_eselon', 'bobot_jabatan', 'sk_jabatan', 'tanggal_sk', 'is_jabatan_sekarang', 'id_sub_unit_organisasi', 'golongan_awal', 'golongan_terbaru', 'id_pegawai_atasan', 'is_keluar_daerah', 'keterangan', 'tanggal_jabatan', 'is_approved', 'jenis_instansi', 'id_instansi'];
      protected $dates = ['deleted_at'];
      protected $table = 'riwayat_jabatan';
      protected $appends = ['struktur_instansi'];

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function pegawai()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai', 'id');
      }

      public function masterJabatan()
      {
            return $this->belongsTo('App\MasterJabatan', 'id_jabatan', 'id');
      }

      public function masterEselon()
      {
            return $this->belongsTo('App\MasterEselon', 'id_eselon', 'id');
      }

      public function masterInstansiInduk()
      {
            return $this->belongsTo('App\MasterInstansiInduk', 'id_instansi', 'id');
      }

      public function masterSatuanKerjaInduk()
      {
            return $this->belongsTo('App\MasterSatuanKerjaInduk', 'id_instansi', 'id');
      }

      public function masterUnitSkpd()
      {
            return $this->belongsTo('App\MasterUnitSkpd', 'id_instansi', 'id');
      }

      public function masterSubUnitOrganisasi()
      {
            return $this->belongsTo('App\MasterSubUnitOrganisasi', 'id_instansi', 'id');
      }

      public function golonganAwal()
      {
            return $this->belongsTo('App\MasterPangkat', 'golongan_awal', 'id');
      }

      public function golonganTerbaru()
      {
            return $this->belongsTo('App\MasterPangkat', 'golongan_terbaru', 'id');
      }

      public function atasan()
      {
            return $this->belongsTo('App\Pegawai', 'id_pegawai_atasan');
      }

      public function getStrukturInstansiAttribute(){
            $strukturInstansi = "";
            try {
                $strukturInstansi .= $this->getInstansiInduk()->nama.", ";
            } catch (\Exception $e) {}
            try {
                $strukturInstansi .= $this->getSatuanKerjaInduk()->nama.", ";
            } catch (\Exception $e) {}
            try {
                $strukturInstansi .= $this->getUnitOrganisasi()->nama.", ";
            } catch (\Exception $e) {}
            try {
                $strukturInstansi .= $this->getSubUnitOrganisasi()->nama;
            } catch (\Exception $e) {}

            return $strukturInstansi;
      }

      public function getInstansiName(){
            if($this->jenis_instansi == "MasterInstansiInduk"){
                  return $this->masterInstansiInduk->nama;
            }
            else if($this->jenis_instansi == "MasterSatuanKerjaInduk"){
                  return $this->masterSatuanKerjaInduk->nama;
            }
            else if($this->jenis_instansi == "MasterUnitSkpd"){
                  return $this->belongsTo('App\MasterUnitSkpd', 'id_instansi', 'id')->first()->nama;
            }
            else if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  return $this->masterSubUnitOrganisasi->nama;
            }
      }

      public function getInstansiInduk(){
            if($this->jenis_instansi == "MasterInstansiInduk"){
                  return $this->masterInstansiInduk;
            }
            if($this->jenis_instansi == "MasterSatuanKerjaInduk"){
                  return $this->masterSatuanKerjaInduk->masterInstansiInduk;
            }
            else if($this->jenis_instansi == "MasterUnitSkpd"){
                  return $this->masterUnitSkpd->masterSatuanKerjaInduk->masterInstansiInduk;
            }
            else if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  return $this->masterSubUnitOrganisasi->masterUnitSkpd->masterSatuanKerjaInduk->masterInstansiInduk;
            }
            else{
                  return null;
            }
      }

      public function getSatuanKerjaInduk(){
            if($this->jenis_instansi == "MasterSatuanKerjaInduk"){
                  return $this->masterSatuanKerjaInduk;
            }
            else if($this->jenis_instansi == "MasterUnitSkpd"){
                  return $this->masterUnitSkpd->masterSatuanKerjaInduk;
            }
            else if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  return $this->masterSubUnitOrganisasi->masterUnitSkpd->masterSatuanKerjaInduk;
            }
            else{
                  return null;
            }
      }

      public function getUnitOrganisasi(){
            if($this->jenis_instansi == "MasterUnitSkpd"){
                  return $this->masterUnitSkpd;
            }
            else if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  return $this->masterSubUnitOrganisasi->masterUnitSkpd;
            }
            else{
                  return null;
            }
      }

      public function getSubUnitOrganisasi(){
            if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  return $this->masterSubUnitOrganisasi;
            }
            else{
                  return null;
            }
      }

      public function isBKD(){
            if($this->is_approved == false){
                  return false;
            }
            if($this->jenis_instansi == "MasterSubUnitOrganisasi"){
                  if($this->masterSubUnitOrganisasi->masterUnitSkpd->masterSatuanKerjaInduk->id == 1){
                        return true;
                  }
            }
            else if($this->jenis_instansi == "MasterUnitSkpd"){
                  if($this->masterUnitSkpd->masterSatuanKerjaInduk->id == 1){
                        return true;
                  }
            }
            else if($this->jenis_instansi == "MasterSatuanKerjaInduk"){
                  if($this->id_instansi == 1){
                        return true;
                  }
            }
            return false;
      }
}
