<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class Artikel extends Model
{

      use SoftDeletes;
      use LogsActivity;

      protected $fillable = ['judul', 'url', 'deskripsi', 'gambar_path', 'id_kategori_artikel', 'id_penulis', 'instansi_on_created', 'published'];
      protected static $logAttributes = ['judul', 'deskripsi', 'gambar_path', 'id_kategori_artikel', 'id_penulis', 'instansi_on_created', 'published'];
      protected $dates = ['deleted_at'];
      protected $table = 'artikel';
      
      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function masterKategoriArtikel()
      {
            return $this->belongsTo('App\MasterKategoriArtikel', 'id_kategori_artikel', 'id');
      }

      public function user()
      {
            return $this->belongsTo('App\User', 'id_penulis', 'id');
      }
}
