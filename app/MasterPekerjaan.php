<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterPekerjaan extends Model
{
	use LogsActivity;
	protected static $logAttributes = ['nama'];
	protected $fillable = ['nama'];
	protected $table = 'master_pekerjaan';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

	public function anggotaKeluarga(){
		return $this->hasMany('App\AnggotaKeluarga', 'id_pekerjaan', 'id');
	}
}
