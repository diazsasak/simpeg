<?php

namespace App\Observers;

use Spatie\Activitylog\Models\Activity;
use Request;

class ActivityObserver
{
    public function creating(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function created(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function updating(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function updated(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function saving(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function saved(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function deleting(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function deleted(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function restoring(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
    public function restored(Activity $a)
    {
        $a->properties->put('ip', Request::ip());
    }
}