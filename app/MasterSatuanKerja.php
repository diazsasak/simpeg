<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterSatuanKerja extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['nama', 'id_instansi_kerja'];
      protected $fillable = ['nama', 'id_instansi_kerja'];
      protected $table = 'master_satuan_kerja';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function riwayatJabatan()
      {
            return $this->hasMany('App\RiwayatJabatan', 'id_satuan_kerja');
      }

      public function masterInstansiKerja()
      {
            return $this->belongsTo('App\MasterInstansiKerja', 'id_instansi_kerja');
      }

}
