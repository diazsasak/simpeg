<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class MasterKeluarga extends Model
{

      use LogsActivity;

      protected static $logAttributes = ['status'];
      protected $fillable = ['status'];
      protected $table = 'master_keluarga';

      //log IP
      public function getDescriptionForEvent(string $eventName): string
      {
            return "{$eventName} from IP : " . Request::ip();
      }

      //relasi
      public function anggotaKeluarga()
      {
            return $this->hasMany('App\AnggotaKeluarga', 'id_status', 'id');
      }

}
