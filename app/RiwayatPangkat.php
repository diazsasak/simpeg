<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Request;

class RiwayatPangkat extends Model
{
    use SoftDeletes;
	use LogsActivity;
	protected static $logAttributes = ['id_pegawai', 'id_pangkat', 'tmt_pangkat', 'sk_pangkat', 'tanggal_sk_pangkat', 'gaji_pokok', 'is_pangkat_awal', 'is_pangkat_terbaru', 'is_approved', 'masa_kerja_tahun', 'masa_kerja_bulan'];
	protected $fillable = ['id_pegawai', 'id_pangkat', 'tmt_pangkat', 'sk_pangkat', 'tanggal_sk_pangkat', 'gaji_pokok', 'is_pangkat_awal', 'is_pangkat_terbaru', 'is_approved', 'masa_kerja_tahun', 'masa_kerja_bulan'];
	protected $dates = ['deleted_at'];
	protected $table = 'riwayat_pangkat';

  	//log IP
	public function getDescriptionForEvent(string $eventName): string
	{
		return "{$eventName} from IP : " . Request::ip();
	}

  	//relasi
	public function pegawai()
	{
		return $this->belongsTo('App\Pegawai', 'id_pegawai');
	}
	public function masterPangkat()
	{
		return $this->belongsTo('App\MasterPangkat', 'id_pangkat');
	}
	public function riwayatJabatanGolonganAwal()
	{
		return $this->hasMany('App\RiwayatJabatan', 'golongan_awal');
	}

	public function riwayatJabatanGolonganTerbaru()
	{
		return $this->hasMany('App\RiwayatJabatan', 'golongan_terbaru');
	}

	public function pangkatTerbaru(){
		return $this->orderBy('tmt_pangkat', 'DESC')->limit(1);
	}

}
