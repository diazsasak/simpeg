<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterSatuanKerjaInduk extends Migration
{

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('master_satuan_kerja_induk', function (Blueprint $table) {
                  $table->increments('id');
                  $table->string('nama');
                  $table->string('kode_satuan_kerja_induk', 15);
                  $table->integer('id_instansi_induk')->unsigned();
                  $table->foreign('id_instansi_induk')
                          ->references('id')->on('master_instansi_induk')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->integer('id_jenis_unit')->unsigned();
                  $table->foreign('id_jenis_unit')
                          ->references('id')->on('master_jenis_unit')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            // Schema::dropIfExists('master_satuan_kerja_induk');
      }

}
