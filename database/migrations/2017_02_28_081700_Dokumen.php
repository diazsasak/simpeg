<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dokumen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('document_path');
            $table->integer('id_kategori_artikel')->unsigned();
            $table->foreign('id_kategori_artikel')
            ->references('id')->on('master_kategori_artikel')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_penulis')->unsigned();
            $table->foreign('id_penulis')
            ->references('id')->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->boolean('published')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen');
    }
}
