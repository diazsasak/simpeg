<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatCuti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_cuti', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('sk_cuti');
            $table->string('jenis_cuti');
            $table->longText('alasan_cuti');
            $table->date('tanggal_sk_cuti');
            $table->date('tanggal_mulai_cuti');
            $table->date('tanggal_selesai_cuti');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_cuti');
    }
}
