<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatPendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pendidikan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_pendidikan')->unsigned();
            $table->foreign('id_pendidikan')
            ->references('id')->on('master_pendidikan')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_jurusan')->unsigned()->nullable();
            $table->foreign('id_jurusan')
                ->references('id')->on('master_jurusan')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('nama_institusi');
            $table->integer('tahun_lulus');
            $table->string('no_ijazah')->nullable();
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pendidikan');
    }
}
