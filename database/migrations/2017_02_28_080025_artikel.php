<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Artikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul');
            $table->string('url');
            $table->longText('deskripsi');
            $table->string('gambar_path');
            $table->integer('id_kategori_artikel')->unsigned();
            $table->foreign('id_kategori_artikel')
            ->references('id')->on('master_kategori_artikel')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_penulis')->unsigned();
            $table->foreign('id_penulis')
            ->references('id')->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('instansi_on_created')->nullable();
            $table->boolean('published')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel');
    }
}
