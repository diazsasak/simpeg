<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterUnitSkpd extends Migration
{

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('master_unit_skpd', function (Blueprint $table) {
                  $table->increments('id');
                  $table->string('nama');
                  $table->string('kode_unit_skpd', 15);
//            $table->string('nama_jabatan');
                  $table->timestamps();
//            $table->integer('id_eselon')->unsigned();
//            $table->foreign('id_eselon')
//            ->references('id')->on('master_eselon')
//            ->onUpdate('cascade')
//            ->onDelete('cascade');
                  $table->integer('id_satuan_kerja_induk')->unsigned();
                  $table->foreign('id_satuan_kerja_induk')
                          ->references('id')->on('master_satuan_kerja_induk')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->integer('id_jenis_unit')->unsigned();
                  $table->foreign('id_jenis_unit')
                          ->references('id')->on('master_jenis_unit')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  // $table->integer('id_instansi_induk')->unsigned();
                  // $table->foreign('id_instansi_induk')
                  // ->references('id')->on('master_instansi_induk')
                  // ->onUpdate('cascade')
                  // ->onDelete('cascade');
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::dropIfExists('master_unit_skpd');
      }

}
