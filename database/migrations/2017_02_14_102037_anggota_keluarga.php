<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnggotaKeluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anggota_keluarga', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('nama');
            $table->string('jk');
            $table->integer('id_status')->unsigned();
            $table->foreign('id_status')
            ->references('id')->on('master_keluarga')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->date('tanggal_lahir');
            $table->integer('id_pendidikan')->unsigned();
            $table->foreign('id_pendidikan')
            ->references('id')->on('master_pendidikan')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('status_perkawinan');
            $table->string('tempat_lahir');
            $table->integer('id_pekerjaan')->unsigned();
            $table->foreign('id_pekerjaan')
            ->references('id')->on('master_pekerjaan')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anggota_keluarga');
    }
}
