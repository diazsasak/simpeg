<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HartaPejabat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('harta_pejabat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->date('tanggal_laporan');
            $table->longText('keterangan');
            $table->string('dokumen'); //file upload
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('harta_pejabat');
    }
}
