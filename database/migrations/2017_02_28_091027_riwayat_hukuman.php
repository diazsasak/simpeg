<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatHukuman extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_hukuman', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_jenis_hukuman')->unsigned();
            $table->foreign('id_jenis_hukuman')
            ->references('id')->on('master_hukuman')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('sk_hukuman');
            $table->date('tanggal_sk_hukuman');
            $table->smallInteger('masa_hukuman_berlaku_tahun');
            $table->smallInteger('masa_hukuman_berlaku_bulan');
            $table->date('tanggal_hukuman_berakhir');
            $table->string('no_pp');
            $table->string('alasan_hukuman');
            $table->longText('keterangan');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_hukuman');
    }
}
