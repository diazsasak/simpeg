<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatDiklat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_diklat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('nama');
            $table->integer('id_diklat')->unsigned();
            $table->foreign('id_diklat')
            ->references('id')->on('master_diklat')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('tempat');
            $table->date('tanggal_pelaksanaan');
            $table->string('pelaksana');
            $table->string('sk_diklat');
            $table->string('sertifikat'); //file upload
            $table->string('no_sertifikat');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_diklat');
    }
}
