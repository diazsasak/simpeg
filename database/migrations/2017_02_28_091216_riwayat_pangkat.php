<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatPangkat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pangkat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            // $table->string('nama'); ini kan ga ada di tabel excel
            $table->integer('id_pangkat')->unsigned();
            $table->foreign('id_pangkat')
            ->references('id')->on('master_pangkat')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->date('tmt_pangkat');
            $table->string('sk_pangkat');
            $table->date('tanggal_sk_pangkat');
            $table->string('gaji_pokok');
            $table->boolean('is_pangkat_awal')->default(false);
            $table->boolean('is_pangkat_terbaru')->default(false);
            $table->boolean('is_approved')->default(false);
            $table->tinyInteger('masa_kerja_tahun');
            $table->tinyInteger('masa_kerja_bulan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pangkat');
    }
}
