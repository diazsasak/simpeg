<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatAbsensi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_absensi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->smallInteger('hadir')->nullable();
            $table->smallInteger('bolos')->nullable();
            $table->smallInteger('izin')->nullable();
            $table->smallInteger('telat')->nullable();
            $table->smallInteger('cuti')->nullable();
            $table->smallInteger('cuti_eksternal')->nullable();
            $table->smallInteger('sakit')->nullable();
            $table->string('file_izin_dokter')->nullable(); // file upload
            $table->smallInteger('tanggal')->nullable();
            $table->smallInteger('bulan')->nullable();
            $table->smallInteger('tahun')->nullable();
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_absensi');
    }
}
