<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nip', 50)->unique();
            $table->string('nip_lama')->nullable();
            $table->string('poto')->nullable();
            $table->string('nama');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->integer('id_agama')->unsigned();
            $table->foreign('id_agama')
                ->references('id')->on('master_agama')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string('alamat_sekarang');
            $table->boolean('jns_kelamin');
            $table->char('id_provinsi', 2);
            $table->char('id_kabupaten', 4);
            $table->char('id_kecamatan', 6);
            $table->string('asal_alamat_lengkap');
            $table->string('email')->nullable();
            $table->string('nomor_telepon');
            $table->string('nomor_hp');
            $table->tinyInteger('jenis_dokumen');
            $table->string('no_dokumen');
            $table->tinyInteger('jenis_pegawai');
            $table->tinyInteger('status_pegawai')->nullable();
            $table->integer('id_status_kepegawaian')->unsigned();
            $table->foreign('id_status_kepegawaian')
                ->references('id')->on('master_status_kepegawaian')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->string("sk_pns")->nullable();
            $table->date('tanggal_pns')->nullable();
            $table->string("sk_cpns")->nullable();
            $table->date('tanggal_cpns')->nullable();
            $table->string('tahun_pengangkatan');
            $table->string('status_perkawinan')->nullable();
            $table->boolean('is_aktif')->nullable();
            // $table->tinyInteger('is_aktif_false_ket')->nullable();
            $table->boolean('is_atasan');
            $table->boolean('is_approved')->default(false);
            $table->string("nuptk")->nullable();
            $table->timestamps();
            $table->softDeletes();
            //FK
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
