<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_jabatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_sub_unit_organisasi')->unsigned()->nullable();
            $table->foreign('id_sub_unit_organisasi')
            ->references('id')->on('master_sub_unit_organisasi')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_jabatan')->unsigned();
            $table->foreign('id_jabatan')
            ->references('id')->on('master_jabatan')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('id_eselon')->unsigned()->nullable();
            $table->foreign('id_eselon')
            ->references('id')->on('master_eselon')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->date('tanggal_jabatan');
            $table->integer('golongan_awal')->unsigned()->nullable();
             $table->foreign('golongan_awal')
            ->references('id')->on('master_pangkat')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('golongan_terbaru')->unsigned()->nullable();
             $table->foreign('golongan_terbaru')
            ->references('id')->on('master_pangkat')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('bobot_jabatan')->nullable();
            $table->string('sk_jabatan');
            $table->date('tanggal_sk');
            $table->boolean('is_jabatan_sekarang')->nullable();
            $table->bigInteger('id_pegawai_atasan')->unsigned()->nullable();
            $table->foreign('id_pegawai_atasan')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('keterangan')->nullable();
            $table->boolean('is_keluar_daerah')->default(false);
            $table->boolean('is_approved')->default(false);
            $table->string('jenis_instansi')->nullable();
            $table->integer('id_instansi')->unsigned()->nullable();
            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_jabatan');
    }
}
