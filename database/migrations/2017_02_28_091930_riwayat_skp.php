<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RiwayatSkp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_skp', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('tahun_skp');
            $table->integer('nilai_skp');
            $table->integer('orientasi_pelayanan');
            $table->integer('komitmen');
            $table->integer('kerjasama');
            $table->integer('nilai_perilaku_kerja');
            $table->integer('nilai_prestasi_kerja');
            $table->integer('integritas');
            $table->integer('disiplin');
            $table->bigInteger('id_pegawai_penilai')->unsigned();
            $table->foreign('id_pegawai_penilai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->bigInteger('id_atasan_pegawai_penilai')->unsigned();
            $table->foreign('id_atasan_pegawai_penilai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_skp');
    }
}
