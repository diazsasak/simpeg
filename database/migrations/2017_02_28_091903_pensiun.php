<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pensiun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pensiun', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
            ->references('id')->on('pegawai')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->string('jenis_pensiun');
            $table->string('sk_pensiun');
            $table->date('tmt_pensiun');
            $table->date('tanggal_sk_pensiun');
            $table->boolean('is_approved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pensiun');
    }
}
