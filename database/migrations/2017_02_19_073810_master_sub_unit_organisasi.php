<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterSubUnitOrganisasi extends Migration
{

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('master_sub_unit_organisasi', function (Blueprint $table) {
                  $table->increments('id');
                  $table->string('nama');
                  $table->string('kode_sub_unit_organisasi', 15);
//            $table->string('kode_atasan_langsung');
//            $table->string('nama_jabatan');
//            $table->integer('id_eselon')->unsigned();
//            $table->foreign('id_eselon')
//            ->references('id')->on('master_eselon')
//            ->onUpdate('cascade')
//            ->onDelete('cascade');
                  $table->integer('id_jenis_unit')->unsigned();
                  $table->foreign('id_jenis_unit')
                          ->references('id')->on('master_jenis_unit')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->integer('id_unit_skpd')->unsigned();
                  $table->foreign('id_unit_skpd')
                          ->references('id')->on('master_unit_skpd')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::dropIfExists('master_sub_unit_organisasi');
      }

}
