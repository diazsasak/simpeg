<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MasterInstansiInduk extends Migration
{

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('master_instansi_induk', function (Blueprint $table) {
                  $table->increments('id');
                  $table->string('kode_instansi_induk', 15);
                  $table->string('nama');
                  $table->integer('id_jenis_unit')->unsigned();
                  $table->foreign('id_jenis_unit')
                          ->references('id')->on('master_jenis_unit')
                          ->onUpdate('cascade')
                          ->onDelete('cascade');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::dropIfExists('master_instansi_induk');
      }

}
