<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DataTambahanPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_tambahan_pegawai', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_surat_ket_sehat_dokter')->nullable();
            $table->date('tanggal_surat_ket_sehat_dokter')->nullable();
            $table->string('no_surat_ket_bebas_narkoba')->nullable();
            $table->date('tanggal_surat_ket_bebas_narkoba')->nullable();
            $table->string('no_surat_ket_catatan_polisi')->nullable();
            $table->date('tanggal_surat_ket_catatan_polisi')->nullable();
            $table->string('no_taspen')->nullable();
            $table->string('no_bpjs')->nullable();
            $table->string('no_karis')->nullable();
            $table->string('akte_kelahiran')->nullable();
            $table->boolean('is_hidup')->default(true);
            $table->string('akte_meninggal')->nullable();
            $table->date('tanggal_meninggal')->nullable();
            $table->string('no_npwp')->nullable();
            $table->date('tanggal_npwp')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //FK
            $table->bigInteger('id_pegawai')->unsigned();
            $table->foreign('id_pegawai')
                ->references('id')->on('pegawai')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_tambahan_pegawai');
    }
}
