INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (1, 'Aktif', '2017-11-16 18:09:35', '2017-11-16 18:09:38');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (2, 'Tugas Belajar', '2017-11-16 18:09:30', '2017-11-16 18:09:32');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (3, 'Pensiun BUP', '2017-11-16 18:09:25', '2017-11-16 18:09:28');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (4, 'Pensiun APS', '2017-11-16 18:09:19', '2017-11-16 18:09:22');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (5, 'Diberhentikan', '2017-11-16 18:09:15', '2017-11-16 18:09:17');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (6, 'Meninggal Dunia', '2017-11-16 18:09:09', '2017-11-16 18:09:12');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (7, 'Mutasi Luar Daerah', '2017-11-16 18:09:00', '2017-11-16 18:09:05');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (8, 'Berhenti', '2017-11-16 18:09:59', '2017-11-16 18:10:01');
INSERT INTO `master_status_kepegawaian` (`id`, `nama`, `created_at`, `updated_at`) VALUES (9, 'CPNSD', '2017-11-20 03:12:09', '2017-11-20 03:12:09');
