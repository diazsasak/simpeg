<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
          'name' => "Super Admin",
          'username' => 'superadmin',
          'user_level' => 'super_admin',
//          'user_group' => null,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);

       DB::table('users')->insert([
          'name' => "kabid_data",
          'username' => 'kabid_data',
          'user_level' => 'kabid',
//          'user_group' => 1,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "kasubbid_data",
          'username' => 'kasubbid_data',
          'user_level' => 'kasubbid',
//          'user_group' => 1,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "staff_data",
          'username' => 'staff_data',
          'user_level' => 'staff',
//          'user_group' => 1,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);

       DB::table('users')->insert([
          'name' => "kabid_mutasi",
          'username' => 'kabid_mutasi',
          'user_level' => 'kabid',
//          'user_group' => 2,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "kasubbid_mutasi",
          'username' => 'kasubbid_mutasi',
          'user_level' => 'kasubbid',
//          'user_group' => 2,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "staff_mutasi",
          'username' => 'staff_mutasi',
          'user_level' => 'staff',
//          'user_group' => 2,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);

       DB::table('users')->insert([
          'name' => "kabid_disiplin",
          'username' => 'kabid_disiplin',
          'user_level' => 'kabid',
//          'user_group' => 3,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "kasubbid_disiplin",
          'username' => 'kasubbid_disiplin',
          'user_level' => 'kasubbid',
//          'user_group' => 3,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "staff_disiplin",
          'username' => 'staff_disiplin',
          'user_level' => 'staff',
//          'user_group' => 3,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);

       DB::table('users')->insert([
          'name' => "kabid_pengembangan",
          'username' => 'kabid_pengembangan',
          'user_level' => 'kabid',
//          'user_group' => 4,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "kasubbid_pengembangan",
          'username' => 'kasubbid_pengembangan',
          'user_level' => 'kasubbid',
//          'user_group' => 4,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
       DB::table('users')->insert([
          'name' => "staff_pengembangan",
          'username' => 'staff_pengembangan',
          'user_level' => 'staff',
//          'user_group' => 4,
          'id_pegawai' => null,
          'password' => Hash::make('testing')
          ]);
    }
}
