<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     //hapus semua data supaya ndk conflik
      DB::statement("SET foreign_key_checks=0");
      DB::table('master_agama')->truncate();
      DB::table('master_keluarga')->truncate();
      DB::table('master_diklat')->truncate();
      DB::table('master_hukuman')->truncate();
      DB::table('master_jurusan')->truncate();
      DB::table('master_pendidikan')->truncate();
      DB::table('master_pekerjaan')->truncate();
      DB::table('master_kategori_artikel')->truncate();
      DB::table('master_jenis_unit')->truncate();
      DB::table('master_instansi_induk')->truncate();
      DB::table('master_satuan_kerja_induk')->truncate();
      DB::table('master_unit_skpd')->truncate();
      DB::table('master_eselon')->truncate();
      DB::table('master_jabatan')->truncate();
      DB::table('master_kategori_jabatan')->truncate();
      DB::table('master_unit_skpd')->truncate();
      DB::table('master_status_kepegawaian')->truncate();
      DB::table('pegawai')->truncate();
      DB::table('users')->truncate();
      DB::table('riwayat_jabatan')->truncate();
      DB::table('riwayat_pendidikan')->truncate();
      DB::table('riwayat_diklat')->truncate();
      DB::table('anggota_keluarga')->truncate();
      DB::table('activity_log')->truncate();
      DB::table('provinsi')->truncate();
      DB::table('kabupaten')->truncate();
      DB::table('kecamatan')->truncate();
      DB::table('struktur_organisasi')->truncate();
      DB::statement("SET foreign_key_checks=1");


//      $this->call(MasterTableSeeder::class);
      $this->call(UserSeeder::class);

      DB::table('struktur_organisasi')->insert([
        'nama_jabatan' => 'Kepala Badan'
      ]);
      DB::table('struktur_organisasi')->insert([
        'nama_jabatan' => 'Kepala Bidang Mutasi'
      ]);
      DB::table('struktur_organisasi')->insert([
        'nama_jabatan' => 'Kepala Bidang Data Dan Formasi'
      ]);
      DB::table('struktur_organisasi')->insert([
        'nama_jabatan' => 'Kepala Bidang Disiplin'
      ]);
      DB::table('struktur_organisasi')->insert([
        'nama_jabatan' => 'Kepala Bidang Pemberdayaan Dan Sumberdaya Manusia'
      ]);
      
      //1 master daerah ok
      $a = 'database/db_daerah.sql';
      DB::unprepared(file_get_contents($a));
      $this->command->info('prov, kab, kec done !');
      
      //2 tabel master agama ok
      $b = 'database/master_agama.sql';
      DB::unprepared(file_get_contents($b));
      $this->command->info('insert master agama done !');
      
      //27 tabel master_status_kepegawaian
      $bb = 'database/master_status_kepegawaian.sql';
      DB::unprepared(file_get_contents($bb));
      $this->command->info('insert master status kepegawaian done !');
      
      //3 tabel master diklat ok
      $c = 'database/master_diklat.sql';
      DB::unprepared(file_get_contents($c));
      $this->command->info('insert master diklat done !');
      
      //4 tabel master eselon ok
      $d = 'database/master_eselon.sql';
      DB::unprepared(file_get_contents($d));
      $this->command->info('insert master eselon done !');
      
      //5 tabel master_kategori_jabatan ok
      $e = 'database/master_kategori_jabatan.sql';
      DB::unprepared(file_get_contents($e));
      $this->command->info('insert master kategori jabatan done !');
      
      //6 tabel master jabatan ok
      $f = 'database/master_jabatan.sql';
      DB::unprepared(file_get_contents($f));
      $this->command->info('insert master jabatan done !');
      
      //7 tabel master_jurusan ok
      $g = 'database/master_jurusan.sql';
      DB::unprepared(file_get_contents($g));
      $this->command->info('insert master jurusan done !');
      
      //8 tabel master_jenis_unit ok
      $h = 'database/master_jenis_unit.sql';
      DB::unprepared(file_get_contents($h));
      $this->command->info('insert master jenis unit done !');
      
      //9 tabel master_kategori_artikel
      $i = 'database/master_kategori_artikel.sql';
      DB::unprepared(file_get_contents($i));
      $this->command->info('insert master kategori artikel done !');
      
      //10 tabel master_keluarga
      $j = 'database/master_keluarga.sql';
      DB::unprepared(file_get_contents($j));
      $this->command->info('insert master keluarga done !');
      
      //11 tabel master_pangkat
      $k = 'database/master_pangkat.sql';
      DB::unprepared(file_get_contents($k));
      $this->command->info('insert master pangkat done !');
      
      //12 tabel master_pekerjaan
      $l = 'database/master_pekerjaan.sql';
      DB::unprepared(file_get_contents($l));
      $this->command->info('insert master pekerjaan done !');
      
      //13 tabel master_pendidikan
      $m = 'database/master_pendidikan.sql';
      DB::unprepared(file_get_contents($m));
      $this->command->info('insert master pendidikan done !');
      
      //14 tabel master_instansi_induk
      $n = 'database/master_instansi_induk.sql';
      DB::unprepared(file_get_contents($n));
      $this->command->info('insert master instansi induk done !');
      
      //15 tabel master_satuan_kerja_induk
      $o = 'database/master_satuan_kerja_induk.sql';
      DB::unprepared(file_get_contents($o));
      $this->command->info('insert master satuan kerja induk done !');
      
      //16 tabel master_unit_skpd
      $p = 'database/master_unit_skpd.sql';
      DB::unprepared(file_get_contents($p));
      $this->command->info('insert master unit skpd done !');
      
      //17 tabel master_sub_unit_organisasi
      $q = 'database/master_sub_unit_organisasi.sql';
      DB::unprepared(file_get_contents($q));
      $this->command->info('insert master sub unit organisasi done !');
      
      //18 tabel pegawai
      $r = 'database/pegawai.sql';
      DB::unprepared(file_get_contents($r));
      $this->command->info('insert pegawai done !');
      
      //19 tabel pensiun
      $s = 'database/pensiun.sql';
      DB::unprepared(file_get_contents($s));
      $this->command->info('insert pensiun done !');
      
      //tabel artikel
//      $t = 'database/artikel.sql';
//      DB::unprepared(file_get_contents($t));
//      $this->command->info('insert artikel done !');
      
      //20 tabel data_tambahan_pegawai
      $u = 'database/data_tambahan_pegawai.sql';
      DB::unprepared(file_get_contents($u));
      $this->command->info('insert data tambahan pegawai done !');
      
      //21 tabel riwayat_diklat
      $v = 'database/riwayat_diklat.sql';
      DB::unprepared(file_get_contents($v));
      $this->command->info('insert riwayat diklat done !');
      
      //22 tabel riwayat_jabatan
      $w = 'database/riwayat_jabatan.sql';
      DB::unprepared(file_get_contents($w));
      $this->command->info('insert riwayat jabatan done !');
      
      //23 tabel riwayat_pangkat
      $x = 'database/riwayat_pangkat.sql';
      DB::unprepared(file_get_contents($x));
      $this->command->info('insert riwayat pangkat done !');
      
      //24 tabel riwayat_pendidikan
      $y = 'database/riwayat_pendidikan.sql';
      DB::unprepared(file_get_contents($y));
      $this->command->info('insert riwayat pendidikan done !');
      
      //25 tabel struktur_organisasi
      $z = 'database/struktur_organisasi.sql';
      DB::unprepared(file_get_contents($z));
      $this->command->info('insert struktur organisasi done !');
      
      //26 tabel anggota keluarga
      $aa = 'database/anggota_keluarga.sql';
      DB::unprepared(file_get_contents($aa));
      $this->command->info('insert anggota keluarga done !');
    }
  }
