<?php

use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('master_jurusan')->insert([
//            'nama' => "Jurusan A"
//        ]);
//        DB::table('master_jurusan')->insert([
//            'nama' => "Jurusan B"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '1',
//            'nama' => "Islam"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '2',
//            'nama' => "Kristen Protestan"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '3',
//            'nama' => "Kristen Katolik"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '4',
//            'nama' => "Hindu"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '5',
//            'nama' => "Buddha"
//        ]);
//        DB::table('master_agama')->insert([
//        	'id' => '6',
//            'nama' => "Khonghucu"
//        ]);
//
//        DB::table('master_eselon')->insert([
//        	'id' => '1',
//            'nama' => "Ia"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '2',
//            'nama' => "Ib"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '3',
//            'nama' => "IIa"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '4',
//            'nama' => "IIb"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '5',
//            'nama' => "IIIa"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '6',
//            'nama' => "IIIb"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '7',
//            'nama' => "IVa"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '8',
//            'nama' => "IVb"
//        ]);
//        DB::table('master_eselon')->insert([
//        	'id' => '9',
//            'nama' => "Va"
//        ]);

//        DB::table('master_pekerjaan')->insert([
//            'id' => '1',
//            'nama' => "Pekerjaan A"
//        ]);
//        DB::table('master_pekerjaan')->insert([
//            'id' => '2',
//            'nama' => "Pekerjaan B"
//        ]);

//        DB::table('master_hukuman')->insert([
//            'id' => '1',
//            'jenis_hukuman' => "Hukuman A"
//        ]);
//        DB::table('master_hukuman')->insert([
//            'id' => '2',
//            'jenis_hukuman' => "Hukuman B"
//        ]);
//
//        DB::table('master_pendidikan')->insert([
//        	'id' => '1',
//            'nama' => "SD"
//        ]);
//        DB::table('master_pendidikan')->insert([
//        	'id' => '2',
//            'nama' => "SLTP"
//        ]);
//        DB::table('master_pendidikan')->insert([
//        	'id' => '3',
//            'nama' => "SLTA"
//        ]);
//        DB::table('master_pendidikan')->insert([
//        	'id' => '4',
//            'nama' => "S-1"
//        ]);
//        DB::table('master_pendidikan')->insert([
//        	'id' => '5',
//            'nama' => "S-2"
//        ]);
//        DB::table('master_pendidikan')->insert([
//        	'id' => '6',
//            'nama' => "S-3"
//        ]);

//        DB::table('master_keluarga')->insert([
//        	'id' => '1',
//            'status' => "Suami"
//        ]);
//        DB::table('master_keluarga')->insert([
//        	'id' => '2',
//            'status' => "Istri"
//        ]);
//        DB::table('master_keluarga')->insert([
//        	'id' => '3',
//            'status' => "Ayah"
//        ]);
//        DB::table('master_keluarga')->insert([
//        	'id' => '4',
//            'status' => "Ibu"
//        ]);
//        DB::table('master_keluarga')->insert([
//        	'id' => '5',
//            'status' => "Anak"
//        ]);

//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SDN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SMAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SMPN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'MTSN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'KECAMATAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'KELURAHAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'BIDANG'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SUB BIDANG'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'BADAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SEKRETARIAT BADAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'BAGIAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SUB BAGIAN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SEKRETARIAT DAERAH'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'STAFF AHLI'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'ASISTEN'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'INSPEKTORAT'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'UPTB'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'UPTD'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'DINAS'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'RUMAH SAKIT'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SEKSI'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'SEKRETARIAT CAMAT'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'PENYELENGGARA'
//        ]);
//        DB::table('master_jenis_unit')->insert([
//            'nama' => 'TATA USAHA'
//        ]);

//        DB::table('master_diklat')->insert([
//            'jenis' => 'Fungsional',
//            'nama' => 'a'
//        ]);
//        DB::table('master_diklat')->insert([
//            'jenis' => 'Struktural',
//            'nama' => 'b'
//        ]);

//        DB::table('master_kategori_jabatan')->insert([
//            'nama' => 'Non Struktural'
//        ]);
//
//        DB::table('master_kategori_jabatan')->insert([
//            'nama' => 'Struktural'
//        ]);
//
//        DB::table('master_kategori_jabatan')->insert([
//            'nama' => 'Fungsional Umum'
//        ]);
//
//        DB::table('master_kategori_jabatan')->insert([
//            'nama' => 'Fungsional Tertentu'
//        ]);
//
//        DB::table('master_kategori_jabatan')->insert([
//            'nama' => 'Rangkap'
//        ]);

//        DB::table('master_jabatan')->insert([
//            'id_kategori_jabatan' => 2,
//            'nama' => 'Sekretaris Daerah'
//        ]);
//
//        DB::table('master_jabatan')->insert([
//            'id_kategori_jabatan' => 2,
//            'nama' => 'Staf Ahli Bupati Lombok Timur Bidang Hukum Politik dan Pemerintahan'
//        ]);

//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'berita'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'pengumuman'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'layanan-sekretariat-bkpsdm'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'layanan-bidang-mutasi'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'layanan-bidang-disiplin'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'layanan-bidang-data-dan-formasi'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'layanan-bidang-pengembangan-sdm'
//        ]);
//        DB::table('master_kategori_artikel')->insert([
//            'nama' => 'slider'
//        ]);

//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Aktif'
//        ]);
//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Tugas Belajar'
//        ]);
//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Pensiun BUP'
//        ]);
//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Pensiun APS'
//        ]);
//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Diberhentikan'
//        ]);
//        DB::table('master_status_kepegawaian')->insert([
//            'nama' => 'Meninggal Dunia'
//        ]);

//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'I/a',
//            'nama_pangkat' => 'Juru Muda',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'I/b',
//            'nama_pangkat' => 'Juru Muda Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'I/c',
//            'nama_pangkat' => 'Juru',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'I/d',
//            'nama_pangkat' => 'Juru Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'II/a',
//            'nama_pangkat' => 'Pengatur Muda',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'II/b',
//            'nama_pangkat' => 'Pengatur Muda Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'II/c',
//            'nama_pangkat' => 'Pengatur',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'II/d',
//            'nama_pangkat' => 'Pengatur Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'III/a',
//            'nama_pangkat' => 'Penata Muda',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'III/b',
//            'nama_pangkat' => 'Penata Muda Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'III/c',
//            'nama_pangkat' => 'Penata',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'III/d',
//            'nama_pangkat' => 'Penata Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'IV/a',
//            'nama_pangkat' => 'Pembina',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'IV/b',
//            'nama_pangkat' => 'Pembina Tingkat I',
//        ]);
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'IV/c',
//            'nama_pangkat' => 'Pembina Utama Muda',
//        ]);
//        
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'IV/d',
//            'nama_pangkat' => 'Pembina Utama Madya',
//        ]);
//        
//        DB::table('master_pangkat')->insert([
//            'nama_golongan' => 'IV/e',
//            'nama_pangkat' => 'Pembina Utama',
//        ]);
        
//        DB::table('master_instansi_induk')->insert([
//            'nama' => 'Sekretariat Daerah',
//            'kode_instansi_induk' => '0001000000',
//            'id_jenis_unit' => 8
//        ]);
//        
//        DB::table('master_instansi_induk')->insert([
//            'nama' => 'Pemerintah Kabupaten Lombok Timur',
//            'kode_instansi_induk' => '0000000000',
//            'id_jenis_unit' => 1
//        ]);
//        
//        DB::table('master_satuan_kerja_induk')->insert([
//            'nama' => 'Badan Kepegawaian Dan Diklat',
//            'kode_satuan_kerja_induk' => '0010000000',
//            'id_instansi_induk'=>2,
//            'id_jenis_unit' => 9
//        ]);
//        
//        DB::table('master_unit_skpd')->insert([
//            'nama' => 'Bidang Mutasi',
//            'kode_unit_skpd' => '0010020000',
//            'id_satuan_kerja_induk'=>1,
//            'id_jenis_unit' => 8
//        ]);
//
//        DB::table('master_sub_unit_organisasi')->insert([
//            'nama' => 'Sub Bidang Mutasi Kepangkatan dan Penggajian',
//            'kode_sub_unit_organisasi' => '0010020100',
//            'id_jenis_unit' => 8,
//            'id_unit_skpd' => 1
//        ]);
        
    }
}
