INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (1, 'Suami', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (2, 'Istri', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (3, 'Ayah Kandung', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (4, 'Ibu Kandung', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (5, 'Anak Kandung', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (6, 'Anak Tiri', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (7, 'Ayah Tiri', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (8, 'Ibu Tiri', NULL, NULL);
INSERT INTO `master_keluarga` (`id`, `status`, `created_at`, `updated_at`) VALUES (9, 'Pasangan (Suami / Istri)', NULL, NULL);
