FROM php:7.0.4-fpm

RUN apt-get update && apt-get install -y \
	libmcrypt-dev \
    mysql-client libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && docker-php-ext-install mcrypt pdo_mysql \
    && apt-get install -y --no-install-recommends git zip

RUN docker-php-ext-install mbstring \
    && apt-get install zip unzip -y

COPY composer.lock composer.json /var/www/

WORKDIR /var/www

COPY . /var/www

RUN ./docker/composer.phar install --no-dev --no-scripts

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN php artisan optimize \
    && php artisan key:generate
