<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

  Route::group(['middleware' => 'auth'], function() {
      Route::resource('master_pendidikan', 'MasterPendidikanController');
      Route::resource('master_jenis_unit', 'MasterJenisUnitController');
      Route::resource('master_status_kepegawaian', 'MasterStatusKepegawaianController');
      Route::resource('pensiun', 'PensiunController');
      Route::get('pensiun/{id}/approve', 'PensiunController@approvePensiun');
      Route::get('pensiun/{id}/unapprove', 'PensiunController@unapprovePensiun');
      Route::resource('riwayat_skp', 'RiwayatSkpController');
      Route::get('riwayat_skp/{id}/approve', 'RiwayatSkpController@approve');
      Route::get('riwayat_skp/{id}/unapprove', 'RiwayatSkpController@unapprove');
      Route::resource('riwayat_pangkat', 'RiwayatPangkatController');
      Route::get('riwayat_pangkat/{id}/approve', 'RiwayatPangkatController@approve');
      Route::get('riwayat_pangkat/{id}/unapprove', 'RiwayatPangkatController@unapprove');
      Route::resource('master_jurusan', 'MasterJurusanController');
      Route::resource('master_diklat', 'MasterDiklatController');
      Route::resource('master_kategori_jabatan', 'MasterKategoriJabatanController');
      Route::get('master_kategori_jabatan/{id}/master_jabatan', 'MasterKategoriJabatanController@getMasterJabatan');
      Route::resource('master_keluarga', 'MasterKeluargaController');
      Route::resource('master_unit_skpd', 'MasterUnitSkpdController');
      Route::get('master_unit_skpd/{id}/master_sub_unit_organisasi', 'MasterUnitSkpdController@getMasterSubUnitOrganisasi');
      Route::resource('master_agama', 'MasterAgamaController');
      Route::resource('master_eselon', 'MasterEselonController');
      Route::resource('master_jabatan', 'MasterJabatanController');
      Route::resource('master_instansi_induk', 'MasterInstansiIndukController');
      Route::get('master_instansi_induk/{id}/master_satuan_kerja_induk', 'MasterInstansiIndukController@getMasterSatuanKerjaInduk');
      Route::get('master_instansi_induk/{id}/master_unit_skpd', 'MasterInstansiIndukController@getMasterUnitSkpd');
      Route::get('selected_instansi_induk/{id}', 'MasterInstansiIndukController@getSelectedInstansiInduk');
      Route::resource('master_instansi_kerja', 'MasterInstansiKerjaController');
      Route::resource('master_kantor_regional_induk', 'MasterKantorRegionalIndukController');
      Route::resource('master_kantor_regional', 'MasterKantorRegionalController');
      Route::resource('master_satuan_kerja_induk', 'MasterSatuanKerjaIndukController');
      Route::get('master_satuan_kerja_induk/{id}/master_unit_skpd', 'MasterSatuanKerjaIndukController@getMasterUnitSkpd');
      Route::get('master_satuan_kerja_induk/{id}/pegawai/not_user', 'MasterSatuanKerjaIndukController@getPegawaiNotUser');
      Route::resource('master_satuan_kerja', 'MasterSatuanKerjaController');
      Route::resource('master_sub_unit_organisasi', 'MasterSubUnitOrganisasiController');
      Route::resource('master_pangkat', 'MasterPangkatController');
      Route::resource('master_pekerjaan', 'MasterPekerjaanController');
      Route::resource('master_kategori_artikel', 'MasterKategoriArtikelController');
      Route::resource('master_hukuman', 'MasterHukumanController');
      Route::resource('anggota_keluarga', 'AnggotaKeluargaController');
      Route::resource('riwayat_diklat', 'RiwayatDiklatController');
      Route::get('diklat_pegawai', 'RiwayatDiklatController@diklatPegawai');
      Route::resource('riwayat_jabatan', 'RiwayatJabatanController');
      Route::resource('riwayat_hukuman', 'RiwayatHukumanController');
      Route::get('riwayat_hukuman/{id}/approve', 'RiwayatHukumanController@approve');
      Route::get('riwayat_hukuman/{id}/unapprove', 'RiwayatHukumanController@unapprove');
      Route::resource('riwayat_cuti', 'RiwayatCutiController');
      Route::get('riwayat_cuti/{id}/approve', 'RiwayatCutiController@approve');
      Route::get('riwayat_cuti/{id}/unapprove', 'RiwayatCutiController@unapprove');
      Route::resource('riwayat_absensi', 'RiwayatAbsensiController');
      Route::get('riwayat_absensi/{id}/approve', 'RiwayatAbsensiController@approve');
      Route::get('riwayat_absensi/{id}/unapprove', 'RiwayatAbsensiController@unapprove');
      Route::resource('harta_pejabat', 'HartaPejabatController');
      Route::get('harta_pejabat/{id}/approve', 'HartaPejabatController@approve');
      Route::get('harta_pejabat/{id}/unapprove', 'HartaPejabatController@unapprove');
      Route::resource('riwayat_pendidikan', 'RiwayatPendidikanController');
      Route::get('riwayat_diklat/{id}/approve', 'RiwayatDiklatController@approveRiwayatDiklat');
      Route::get('riwayat_diklat/{id}/unapprove', 'RiwayatDiklatController@unapproveRiwayatDiklat');
      Route::get('riwayat_jabatan/{id}/approve', 'RiwayatJabatanController@approveRiwayatJabatan');
      Route::get('riwayat_jabatan/{id}/unapprove', 'RiwayatJabatanController@unapproveRiwayatJabatan');
      Route::post('pegawai/by_instansi', 'PegawaiController@byInstansi');
      Route::get('pegawai/filter/{statusKepegawaian}/{eselon}/{jk}/{tahun_angkatan}/{pendidikan}', 'PegawaiController@filter');
      Route::get('pegawai/export/{statusKepegawaian}/{eselon}/{jk}/{tahun_angkatan}/{pendidikan}', 'PegawaiController@doExport');
      Route::get('pegawai/loadAllPegawai', 'PegawaiController@loadAllPegawai');
      Route::get('pegawai/{id}/anggota_keluarga', 'PegawaiController@getAnggotaKeluarga');
      Route::get('pegawai/{id}/riwayat_diklat', 'PegawaiController@getRiwayatDiklat');
      Route::get('pegawai/{id}/riwayat_jabatan', 'PegawaiController@getRiwayatJabatan');
      Route::get('pegawai/{id}/anggota_keluarga', 'PegawaiController@getAnggotaKeluarga');
      Route::get('pegawai/{id}/riwayat_pendidikan', 'PegawaiController@getRiwayatPendidikan');
      Route::get('pegawai/{id}/pensiun', 'PegawaiController@getPensiun');
      Route::get('pegawai/{id}/riwayat_skp', 'PegawaiController@getRiwayatSkp');
      Route::get('pegawai/{id}/riwayat_pangkat', 'PegawaiController@getRiwayatPangkat');
      Route::get('pegawai/{id}/riwayat_hukuman', 'PegawaiController@getRiwayatHukuman');
      Route::get('pegawai/{id}/riwayat_cuti', 'PegawaiController@getRiwayatCuti');
      Route::get('pegawai/{id}/riwayat_absensi', 'PegawaiController@getRiwayatAbsensi');
      Route::get('pegawai/{id}/harta_pejabat', 'PegawaiController@getHartaPejabat');
      Route::get('pegawai/{id}/golongan_awal', 'PegawaiController@getGolonganAwal');
      Route::get('pegawai/{id}/golongan_terbaru', 'PegawaiController@getGolonganTerbaru');
      Route::get('pegawai/{id}/master_unit_skpd', 'PegawaiController@getMasterUnitSkpd');
      Route::get('pegawai/{id}/print', 'PegawaiController@print');
      Route::get('pegawai/search_by_nip/{nip}', 'PegawaiController@searchByNip');
      Route::get('log_aktifitas', 'LogAktifitasController@index');
      Route::get('akun/{id}/restore', 'AuthController@restore');
      Route::get('provinsi', 'PegawaiController@getProvinsi');
      Route::get('kabupaten/{id}', 'PegawaiController@getKabupaten');
      Route::get('kecamatan/{id}', 'PegawaiController@getKecamatan');
      Route::resource('artikel', 'ArtikelController');
      Route::get('artikel/{id}/restore', 'ArtikelController@restore');
      Route::put('artikel/publish/{id}/edit', 'ArtikelController@publish');
      Route::put('artikel/unpublish/{id}/edit', 'ArtikelController@unpublish');
      Route::resource('dokumen', 'DokumenController');
      Route::get('dokumen/{id}/restore', 'DokumenController@restore');
      Route::put('dokumen/publish/{id}/edit', 'DokumenController@publish');
      Route::put('dokumen/unpublish/{id}/edit', 'DokumenController@unpublish');
      Route::get('get_jk_diagram', 'DashboardController@getJkDiagram');
      Route::get('dashboard', 'DashboardController@index');
      Route::resource('akun', 'AuthController');
      Route::get('pegawai/simple_list', 'PegawaiController@getSimpleList');
      Route::resource('pegawai', 'PegawaiController');
      Route::resource('data_tambahan_pegawai', 'DataTambahanPegawaiController');
      Route::put('pegawai/approve/{id}/edit', 'PegawaiController@approvePegawai');
      Route::put('pegawai/unapprove/{id}/edit', 'PegawaiController@unapprovePegawai');
      Route::get('pegawai/{id}/restore', 'PegawaiController@restore');
      Route::get('logout', 'AuthController@logout');
      Route::group(['middleware' => 'super_admin'], function() {
            Route::get('struktur_organisasi', 'StrukturOrganisasiController@index');
            Route::post('struktur_organisasi', 'StrukturOrganisasiController@store');
            Route::get('tes_user_level', functioN() {
                  echo "hai";
            });
      });
});

Route::get('search/{query}', 'SearchController@show');
Route::get('pegawai/public-detail/{id}', 'PegawaiController@publicDetail');
Route::get('login', 'AuthController@login');
Route::post('login', 'AuthController@postLogin');
Route::get('/', 'BerandaController@index');
// Route::get('/', function(){
//       return App\Pegawai::find(1)->getPangkatTerbaru()->masterPangkat;
// });
Route::get('kontak', 'BerandaController@kontak');
Route::get('public-artikel', [ 'uses' =>  'BerandaController@getAllArtikel', 'as'=>'public-artikel']);
Route::get('public-artikel-detail/{url}', [ 'uses' =>  'BerandaController@getDetailArtikel', 'as'=>'public-artikel-detail']);
Route::get('rekapitulasi-pegawai', [ 'uses' =>  'RekapitulasiPegawaiController@index', 'as'=>'rekapitulasi-pegawai']);

Route::get('layanan/{jenis}', 'LayananController@getLayanan');