angular.module('simpegApp.controllers')
.controller('RiwayatCutiCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.jenisCuti = [
			{jenis_cuti: 'Cuti Tahunan'},
			{jenis_cuti: 'Cuti Alasan Penting'},
			{jenis_cuti: 'Cuti Luar Tanggungan Negara'}
		];
		$scope.getRiwayatCuti = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_cuti'
			}).then(function(data){
				$scope.riwayat_cuti = data.data;
			});
		}
		$scope.getRiwayatCuti($scope.idPegawai);
		$scope.saveRiwayatCuti = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			$http({
				method: 'POST',
				url: '/riwayat_cuti',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatCuti($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createRiwayatCuti = function(){
			$('.create-riwayat-cuti-modal').modal('show');
		}
		$scope.editRiwayatCuti = function(a){
			$scope.editData = angular.copy(a);
			$('.edit-riwayat-cuti-modal').modal('show');
		}
		$scope.updateRiwayatCuti = function(data){
			$http({
				method: 'PUT',
				url: '/riwayat_cuti/'+data.id,
				data: data
			}).then(function(){
				$scope.getRiwayatCuti($scope.idPegawai);
			});
		};
		$scope.delRiwayatCuti = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_cuti/'+id
				}).then(function(){
					$scope.getRiwayatCuti($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatCuti = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/riwayat_cuti/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getRiwayatCuti($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatCuti = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/riwayat_cuti/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatCuti($scope.idPegawai);
		};
		$scope.approvedRiwayatCuti = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/riwayat_cuti/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getRiwayatCuti($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedRiwayatCuti = function(id) {
	    	$http({
	    		url: '/riwayat_cuti/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatCuti($scope.idPegawai);
	    	});
		};
	}]);