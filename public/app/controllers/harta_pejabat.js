angular.module('simpegApp.controllers')
.controller('HartaPejabatCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.getHartaPejabat = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/harta_pejabat'
			}).then(function(data){
				console.log(data);
				$scope.harta_pejabat = data.data;
			});
		}
		$scope.getHartaPejabat($scope.idPegawai);
		$scope.saveHartaPejabat = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			var fd = new FormData();
			for (var key in $scope.newData) {
				if ($scope.newData.hasOwnProperty(key)) {
					fd.append(key, $scope.newData[key]);
				}
			}
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/harta_pejabat',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(){
				$scope.getHartaPejabat($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createHartaPejabat = function(){
			$scope.getMasterHukuman();
			$('.create-harta-pejabat-modal').modal('show');
		}
		$scope.getMasterHukuman = function(){
			$http({
				method: 'GET',
				url: '/master_hukuman/'
			}).then(function(data){
				$scope.masterHukuman = data.data;
			});
		}
		$scope.editHartaPejabat = function(a){
			$scope.getMasterHukuman();
			$scope.editData = angular.copy(a);
			$('.edit-harta-pejabat-modal').modal('show');
		}
		$scope.updateHartaPejabat = function(data){
			var fd = new FormData();
			data._method = 'PUT';
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					fd.append(key, data[key]);
				}
			}
			$http({
				method: 'POST',
				url: '/harta_pejabat/'+data.id,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(){
				$scope.getHartaPejabat($scope.idPegawai);
			});
		};
		$scope.delHartaPejabat = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/harta_pejabat/'+id
				}).then(function(){
					$scope.getHartaPejabat($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveHartaPejabat = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/harta_pejabat/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getHartaPejabat($scope.idPegawai);
			});
		};
		$scope.unapproveHartaPejabat = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/harta_pejabat/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getHartaPejabat($scope.idPegawai);
		};
		$scope.approvedHartaPejabat = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/harta_pejabat/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getHartaPejabat($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedHartaPejabat = function(id) {
	    	$http({
	    		url: '/harta_pejabat/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getHartaPejabat($scope.idPegawai);
	    	});
		};
	}]);