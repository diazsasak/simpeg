angular.module('simpegApp.controllers')
.controller('RiwayatSkpCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.pegawai = {};
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.getRiwayatSkp = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_skp'
			}).then(function(data){
				console.log(data);
				$scope.riwayat_skp = data.data;
			});
		}
		$scope.getRiwayatSkp($scope.idPegawai);
		$scope.saveRiwayatSkp = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/riwayat_skp',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatSkp($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createRiwayatSkp = function(){
			$http({
				method: 'GET',
				url: '/pegawai/loadAllPegawai/'
			}).then(function(data){
				$scope.pegawaiPenilai = data.data;
				// $scope.atasanPegawaiPenilai = data.data;
			});
			$('.create-riwayat-skp-modal').modal('show');
		}
		$scope.editRiwayatSkp = function(a){
			$http({
				method: 'GET',
				url: '/pegawai/loadAllPegawai/'
			}).then(function(data){
				$scope.pegawaiPenilai = data.data;
				// $scope.atasanPegawaiPenilai = data.data;
			});
			$scope.editData = angular.copy(a);
			$('.edit-riwayat-skp-modal').modal('show');
		}
		$scope.updateRiwayatSkp = function(data){
			$http({
				method: 'PUT',
				url: '/riwayat_skp/'+data.id,
				data: data
			}).then(function(){
				$scope.getRiwayatSkp($scope.idPegawai);
			});
		};
		$scope.delRiwayatSkp = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_skp/'+id
				}).then(function(){
					$scope.getRiwayatSkp($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatSkp = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/riwayat_skp/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getRiwayatSkp($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatSkp = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/riwayat_skp/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatSkp($scope.idPegawai);
		};
		$scope.approvedRiwayatSkp = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/riwayat_skp/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getRiwayatSkp($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedRiwayatSkp = function(id) {
	    	$http({
	    		url: '/riwayat_skp/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatSkp($scope.idPegawai);
	    	});
		};
	}]);