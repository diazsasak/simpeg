//LOKASI
angular.module('goFutsalApp.controllers')
.controller('StadiumCtrl', ['$http', '$scope', '$window', '$confirm',
	function($http, $scope, $window, $confirm){
		$scope.newStadium = false;
		$scope.saveClicked = 0;

		$scope.countSc = function(id) {
			$http({
				url: id+'/stadium/count',
				method: 'GET'
			})
			.success(function(data){
				return data;
			})
			.error(function() {
				return 0;
			});
		}
		$scope.types = [
		{
			type: 'grass',
			name: 'Rumput Sintetis'
		},
		{
			type: 'interlock',
			name: 'Interlock'
		}
		];
		$scope.getPlace = function(){
			$http({
				method: 'GET',
				url: '/place'
			}).success(function(data){
				$scope.places = data;
			});
		}
		$scope.getStadium = function(){
			$http({
				url: '/'+$scope.inputData.place_id+'/stadium',
				method: 'GET'
			})
			.success(function(data){
				$scope.stadiums = data;
			});
		};
		$scope.saveStadium = function(){
			$scope.saveClicked++;
			if($scope.saveClicked == 1){
				$scope.forms.stadiumForm.name.$dirty = true;
				$scope.forms.stadiumForm.type.$dirty = true;
				if($scope.forms.stadiumForm.$valid){
					var fd = new FormData();
					for (var key in $scope.inputData) {
						if ($scope.inputData.hasOwnProperty(key)) {
							fd.append(key, $scope.inputData[key]);
						}
					}
					$http({
						url: '/stadium',
						method: 'POST',
						data: fd,
						transformRequest: angular.identity,
						headers: {'Content-Type': undefined}
					})
					.success(function(data){
						$scope.resetForm();
						$scope.getStadium();
						$scope.saveClicked = 0;
					}).error(function(data){
						alert(data);
						$scope.saveClicked = 0;
					});
				}
				else{
					$scope.saveClicked=0;
				}
			}
		}
		$scope.resetForm = function(){
			$scope.forms.stadiumForm.$setPristine();
			$scope.newStadium = false;
			$scope.inputData.name = '';
			$scope.inputData.type = '';
		}
		$scope.saveEditStadium = function(){
			$scope.forms.stadiumForm.name.$dirty = true;
			$scope.forms.stadiumForm.type.$dirty = true;
			if($scope.forms.stadiumForm.$valid){
				document.getElementById('stadiumForm').submit();
			}
		}
		$scope.delStadium = function(id){
			$confirm({text: 'Hapus lapangan ?', title: 'Konfirmasi', ok: 'Ya', cancel: 'Tidak'})
			.then(function() {
				$http({
					url: '/stadium/'+id,
					method: 'DELETE'
				})
				.success(function(){
					$scope.getStadium();
				});
			});
		}
		$scope.getPlace();

		var pathArray = window.location.pathname.split( '/' );
		if(pathArray[3] == 'edit'){
			$scope.inputData = $window.data;
		}
	}]);