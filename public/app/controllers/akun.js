angular.module('simpegApp.controllers')
.controller('AkunCtrl', ['$http', '$scope', '$confirm',
	function($http, $scope, $confirm){
		$scope.inputAkun = {
			'id_unor' : ''
		};
		$scope.getMasterSatuanKerjaInduk = function() {
			$http({
				url: '/master_satuan_kerja_induk',
				method: 'GET'
			}).then( function (response) {
				$scope.masterSatuanKerjaInduk = response.data;
			});
		};
		$scope.getMasterSatuanKerjaInduk();

		$scope.loadSelectedPegawai = function(){
			//load selected pegawai
			var idPegawai = document.getElementById('id-pegawai').value;
			var idSkpd = document.getElementById('id-unit-skpd').value;
			console.log(idPegawai);
			console.log(idSkpd);
		}

		$('.master-satuan-kerja-induk-select').on('change', function() {
			console.log(this.value);
			$.ajax({
				url: '/master_satuan_kerja_induk/'+this.value+'/pegawai/not_user',
				error: function(err) {
					console.log(err);
				},
				dataType: 'json',
				success: function(data) {
					$scope.$apply(function () {
						$scope.pegawai = data;
					});
				},
				type: 'GET'
			});
		});
	}]);