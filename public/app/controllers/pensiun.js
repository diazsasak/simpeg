angular.module('simpegApp.controllers')
.controller('PensiunCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.pegawai = {};
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.jenisPensiun = [
			{jenis_pensiun: 'PENSIUN BUP'},
			{jenis_pensiun: 'PENSIUN APS'},
			{jenis_pensiun: 'MENINGGAL DUNIA'}
		];
		$scope.getPensiun = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/pensiun'
			}).then(function(data){
				console.log(data);
				$scope.pensiun = data.data;
			});
		}
		$scope.getPensiun($scope.idPegawai);
		$scope.savePensiun = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/pensiun',
				data: $scope.newData
			}).then(function(){
				$scope.getPensiun($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.editPensiun = function(a){
			$scope.editData = angular.copy(a);
			$('.edit-pensiun-modal').modal('show');
		}
		$scope.updatePensiun = function(data){
			$http({
				method: 'PUT',
				url: '/pensiun/'+data.id,
				data: data
			}).then(function(){
				$scope.getPensiun($scope.idPegawai);
			});
		};
		$scope.delPensiun = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/pensiun/'+id
				}).then(function(){
					$scope.getPensiun($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approvePensiun = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/pensiun/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getPensiun($scope.idPegawai);
			});
		};
		$scope.unapprovePensiun = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/pensiun/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getPensiun($scope.idPegawai);
		};
		$scope.approvedPensiun = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/pensiun/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getPensiun($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedPensiun = function(id) {
	    	$http({
	    		url: '/pensiun/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getPensiun($scope.idPegawai);
	    	});
		};
	}]);