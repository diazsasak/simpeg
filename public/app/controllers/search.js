angular.module('simpegApp.controllers')
.controller('SearchCtrl', ['$http', '$scope', 
	function($http, $scope){
		$scope.globalSearch = function(query) {
			if(query!='') {
				$http({
					url: '/search/'+query,
					method: 'GET'
				}).then(function successCallback(response) {
					console.log(response);
					$scope.searchPegawai = response.data;
				});
			}
			else $scope.searchPegawai = {};
		}
	}]);