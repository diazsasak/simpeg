angular.module('simpegApp.controllers')
.controller('PegawaiCtrl', ['$http', '$scope', '$confirm', '$window',
	function($http, $scope, $confirm, $window){
		$scope.inputPegawai = {
			'jns_kelamin' : '1',
			'gelar_depan' : 'Ir',
			'nama' : 'Ridwan',
			'gelar_belakang' : 'M. Pd',
			'tempat_lahir' : 'Mataram',
			'tgl_lahir' : '1999-12-12',
			'id_master_agama' : '1',
			'alamat_sekarang' : 'Jl Energi',
			'asal_alamat_lengkap' : 'Jl Energi',
			'email' : 'kocedt@gmail.com',
			'nomor_telepon' : '03709384998',
			'nomor_hp' : '082339144512',
			'jenis_dokumen' : 1,
			'no_dokumen' : '123',
			'nuptk': '3423423',
			'jenis_pegawai' : 1,
			'status_pegawai' : 1,
			'sk_pns' : 123,
			'sk_cpns' : 123,
			'tanggal_pns' : '2016-12-12',
			'tanggal_cpns' : '2016-12-12',
			'id_status_kepegawaian': 1,
			'tahun_pengangkatan' : '2011',
			'is_atasan' : false,
			'status_perkawinan': 'Belum Kawin'
		};
		var isTrue = function (obj) {
			for (var i in obj) if (obj.hasOwnProperty(i)) return false;
				return true;
		};
		$scope.validViewSelect = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		$scope.validView = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$dirty||a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		//varibable for pre
		// $scope.status_perkawinan = [
		// { id: 1, nama: 'Belum Kawin' },{ id: 2, nama: 'Sudah Kawin' },{ id: 3, nama: 'Janda' },{ id: 4, nama: 'Duda' }
		// ];
		$scope.status_perkawinan = ['Belum Kawin', 'Sudah Kawin', 'Janda', 'Duda'];
		$scope.isError = function(a) {
			return (a.$error && (a.$dirty || a.$touched));
		}
		$scope.is_approved = false;
		$scope.selectedPegawai = {};
		$scope.jk = [
		{ id: 1, nama: 'Laki-laki' },{ id: 0, nama: 'Perempuan' }
		];
		// $scope.pegawaiFilter = [
		// 	{ id: 1, nama: 'Semua Pegawai' },
		// 	{ id: 2, nama: 'Pegawai Mutasi' },
		// 	{ id: 3, nama: 'Pegawai Pensiun' },
		// 	{ id: 4, nama: 'Pegawai Meninggal' },
		// ];
		$scope.getPegawai = function(id) {
			$http({
				url: '/pegawai',
				method: 'GET'
			}).then( function (response) {
				console.log(response.data);
				$scope.pegawai = response.data;
			});
			$http({
				url: '/master_eselon',
				method: 'GET'
			}).then( function (response) {
				$scope.eselon = response.data;
			});
			$http({
				url: '/master_pendidikan',
				method: 'GET'
			}).then( function (response) {
				$scope.pendidikan = response.data;
			});
			//for filter
			$http({
				url: '/master_status_kepegawaian',
				method: 'GET'
			}).then( function (response) {
				$scope.status_kepegawaian = response.data;
			});
		};
		$scope.getPegawai();

		$scope.doFilter = function() {
			var f = {
				'pegawai' : angular.element('#filter-pegawai').val(),
				'eselon' : angular.element('#filter-eselon').val(),
				'jk' : angular.element('#filter-jk').val(),
				'tahun_angkatan' : angular.element('#filter-tahun-angkatan').val(),
				'pendidikan' : angular.element('#filter-pendidikan').val(),
			};
			if (angular.element('#filter-tahun-angkatan').val()=='') f.tahun_angkatan = 'all'; 
			console.log(f);
			$http({
				url: '/pegawai/filter/'+f.pegawai+'/'+f.eselon+'/'+f.jk+'/'+f.tahun_angkatan+'/'+f.pendidikan,
				method: 'GET',
				// data: f
			}).then(function successCallback(response) {
				console.log(response);
				$scope.pegawai = response.data;
			}, function errorCallback(response) {
			});
		};

		$scope.doExport = function() {
			var f = {
				'pegawai' : angular.element('#filter-pegawai').val(),
				'eselon' : angular.element('#filter-eselon').val(),
				'jk' : angular.element('#filter-jk').val(),
				'tahun_angkatan' : angular.element('#filter-tahun-angkatan').val(),
				'pendidikan' : angular.element('#filter-pendidikan').val(),
			};
			if (angular.element('#filter-tahun-angkatan').val()=='') f.tahun_angkatan = 'all'; 
			$window.open($window.baseUrl+'/pegawai/export/'+f.pegawai+'/'+f.eselon+'/'+f.jk+'/'+f.tahun_angkatan+'/'+f.pendidikan, '_blank');
			// $http({
			// 	url: '/pegawai/export/'+f.pegawai+'/'+f.eselon+'/'+f.jk+'/'+f.tahun_angkatan+'/'+f.pendidikan,
			// 	method: 'GET',
			// }).then(function successCallback(response) {
			// 	console.log(response);
			// 	$scope.pegawai = response.data;
			// }, function errorCallback(response) {
			// });
		};
		$scope.loadAll = function() {
			$scope.loadAgama();
			$scope.loadProvinsi();
			$scope.loadStatusKepegawaian();
		}
		$scope.loadAgama = function() {
			$http({
				url: '/master_agama',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.agama = response.data;
			});
		};
		$scope.loadStatusKepegawaian = function() {
			$http({
				url: '/master_status_kepegawaian',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.masterStatusKepegawaian = response.data;
				// $scope.inputPegawai.id_status_kepegawaian = response.data[0].id;
			});
		};
		$scope.loadProvinsi = function() {
			$http({
				url: '/provinsi',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.provinsi = response.data;
			});
		};
		$scope.loadKabupaten = function(id) {
			$http({
				url: '/kabupaten/'+id,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.kabupaten = response.data;
			});
		};
		$scope.loadKecamatan = function(id) {
			$http({
				url: '/kecamatan/'+id,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.kecamatan = response.data;
			});
		};
		$scope.savePegawai = function(a){
			var fd = new FormData();
			$scope.inputPegawai.is_aktif ? $scope.inputPegawai.is_aktif = '1' : $scope.inputPegawai.is_aktif = '0';
			$scope.inputPegawai.is_atasan ? $scope.inputPegawai.is_atasan = '1' : $scope.inputPegawai.is_atasan = '0';
			for (var key in $scope.inputPegawai) {
				if ($scope.inputPegawai.hasOwnProperty(key)) {
					fd.append(key, $scope.inputPegawai[key]);
				}
			}
			// console.log(fd);
			$http({
				method: 'POST',
				url: '/pegawai',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function successCallback(response) {
				if(response.data.hasOwnProperty('error')){
					if(response.data.error == 'NIP sudah terdaftar'){
						$scope.nipError = response.data.error;
					}
				}
				else{
					$scope.reloadPaginate($scope.pegawai.current_page);
					$scope.inputPegawai = {};
					a.$setPristine();
					a.$setUntouched();
					a.$submitted = false;
					$('#create-modal').modal('hide');
				}
			}, function errorCallback(response) {
				console.log(response);
			});
		};
		$scope.delPegawai = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/pegawai/'+id
				}).then(function(){
					$scope.reloadPaginate($scope.pegawai.current_page);
					$scope.getPegawai();
				});
			});
		};
		$scope.resPegawai = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/restore'
			}).then(function(){
				$scope.reloadPaginate($scope.pegawai.current_page);
				$scope.getPegawai();
			});
		};
		$scope.approve = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedPegawai);
				var filtered = keys.filter(function(key) {
					if($scope.selectedPegawai[key]){
						$http({
							url: '/pegawai/approve/'+key+'/edit',
							method: 'PUT'
						}).then(function successCallback(response){
							console.log(response);
							$scope.reloadPaginate($scope.pegawai.current_page);
						});
					}
				});
			});
		};
		$scope.unapprove = function() {
			var keys = Object.keys($scope.selectedPegawai);
			var filtered = keys.filter(function(key) {
				if($scope.selectedPegawai[key]){
					$http({
						url: '/pegawai/unapprove/'+key+'/edit',
						method: 'PUT'
					}).then(function successCallback(response){
						$scope.reloadPaginate($scope.pegawai.current_page);
					});
				}
			});
		};
		$scope.approved = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				$http({
					url: '/pegawai/approve/'+id+'/edit',
					method: 'PUT'
				}).then(function successCallback(response){
					$scope.reloadPaginate($scope.pegawai.current_page);
				});
			});
		};
		$scope.unapproved = function(id) {
			$http({
				url: '/pegawai/unapprove/'+id+'/edit',
				method: 'PUT'
			}).then(function successCallback(response){
				$scope.reloadPaginate($scope.pegawai.current_page);
			});
		};
		$scope.paginate = function(myUrl) {
			$http({
				url: myUrl,
				method: 'GET'
			}).then(function successCallback(response) {
				console.log(response);
				$scope.pegawai = response.data;
			});
		};
		var filterPaginate = function(current) {
			var f = {
				'pegawai' : angular.element('#filter-pegawai').val(),
				'eselon' : angular.element('#filter-eselon').val(),
				'jk' : angular.element('#filter-jk').val(),
				'tahun_angkatan' : angular.element('#filter-tahun-angkatan').val(),
				'pendidikan' : angular.element('#filter-pendidikan').val(),
			};
			if (angular.element('#filter-tahun-angkatan').val()=='') f.tahun_angkatan = 'all'; 
			$http({
				url: '/pegawai/filter/'+f.pegawai+'/'+f.eselon+'/'+f.jk+'/'+f.tahun_angkatan+'/'+f.pendidikan+'?page='+current,
				method: 'GET'
			}).then(function successCallback(response) {
				console.log(response);
				$scope.pegawai = response.data;
			});
		};
		var normalPaginate = function(current) {
			$http({
				url: '/pegawai?page='+current,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.pegawai = response.data;
			});
		}
		$scope.reloadPaginate = function(current) {
			if($scope.pegawai.next_page_url!=undefined) { 
				if($scope.pegawai.next_page_url.match("filter")) filterPaginate(current);
				else normalPaginate(current);
			}
			else if($scope.pegawai.prev_page_url!=undefined) { 
				if($scope.pegawai.prev_page_url.match("filter")) filterPaginate(current);
				else normalPaginate(current);
			}
			$scope.getPegawai();
		};
		$scope.globalSearch = function(query) {
			if(query!='') {
				$http({
					url: '/search/'+query,
					method: 'GET'
				}).then(function successCallback(response) {
					$scope.searchPegawai = response.data;
				});
			}
			else $scope.searchPegawai = {};
		}
	}]);