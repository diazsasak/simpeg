angular.module('simpegApp.controllers')
.controller('RiwayatHukumanCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.getRiwayatHukuman = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_hukuman'
			}).then(function(data){
				console.log(data);
				$scope.riwayat_hukuman = data.data;
			});
		}
		$scope.getRiwayatHukuman($scope.idPegawai);
		$scope.saveRiwayatHukuman = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/riwayat_hukuman',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatHukuman($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createRiwayatHukuman = function(){
			$scope.getMasterHukuman();
			$('.create-riwayat-hukuman-modal').modal('show');
		}
		$scope.getMasterHukuman = function(){
			$http({
				method: 'GET',
				url: '/master_hukuman/'
			}).then(function(data){
				$scope.masterHukuman = data.data;
			});
		}
		$scope.editRiwayatHukuman = function(a){
			$scope.getMasterHukuman();
			$scope.editData = angular.copy(a);
			$('.edit-riwayat-hukuman-modal').modal('show');
		}
		$scope.updateRiwayatHukuman = function(data){
			$http({
				method: 'PUT',
				url: '/riwayat_hukuman/'+data.id,
				data: data
			}).then(function(){
				$scope.getRiwayatHukuman($scope.idPegawai);
			});
		};
		$scope.delRiwayatHukuman = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_hukuman/'+id
				}).then(function(){
					$scope.getRiwayatHukuman($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatHukuman = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/riwayat_hukuman/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getRiwayatHukuman($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatHukuman = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/riwayat_hukuman/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatHukuman($scope.idPegawai);
		};
		$scope.approvedRiwayatHukuman = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/riwayat_hukuman/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getRiwayatHukuman($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedRiwayatHukuman = function(id) {
	    	$http({
	    		url: '/riwayat_hukuman/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatHukuman($scope.idPegawai);
	    	});
		};
	}]);