angular.module('simpegApp.controllers')
.controller('DiklatCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.editData = {
			hapus_foto: false
		};
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.getMasterDiklat = function(id){
			$http({
				method: 'GET',
				url: '/master_diklat'
			}).then(function(data){
				console.log(data);
				$scope.masterDiklat = data.data;
			});
		}
		$scope.saveRiwayatDiklat = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			var fd = new FormData();
			for (var key in $scope.newData) {
				if ($scope.newData.hasOwnProperty(key)) {
					fd.append(key, $scope.newData[key]);
				}
			}
			// Display the key/value pairs
			// for (var pair of fd.entries()) {
			// 	console.log(pair[0]+ ', ' + pair[1]); 
			// }
			$http({
				method: 'POST',
				url: '/riwayat_diklat',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function successCallback(response) {
				$scope.getRiwayatDiklat($scope.idPegawai);
				$scope.resetForm();
			}, function errorCallback(response) {
			});
		};
		$scope.getRiwayatDiklat = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_diklat'
			}).then(function(data){
				console.log(data.data);
				$scope.riwayatDiklat = data.data;
			});
		};
		$scope.getRiwayatDiklat($scope.idPegawai);
		$scope.updateRiwayatDiklat = function(data){
			data._method = 'PUT';
			var fd = new FormData();
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					fd.append(key, data[key]);
				}
			}
			// Display the key/value pairs
			for (var pair of fd.entries()) {
				console.log(pair[0]+ ', ' + pair[1]); 
			}
			$http({
				method: 'POST',
				url: '/riwayat_diklat/'+data.id,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(data){
				console.log(data);
				$scope.getRiwayatDiklat($scope.idPegawai);
			});
		};
		$scope.delRiwayatDiklat = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_diklat/'+id
				}).then(function(){
					$scope.getRiwayatDiklat($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		//APPROVING DATA RIWAYAT
		$scope.approveRiwayatDiklat = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
					if($scope.selectedData[key]){
						$http({
							url: '/riwayat_diklat/'+key+'/approve',
							method: 'GET'
						});
					}
				});
				$scope.getRiwayatDiklat($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatDiklat = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
				if($scope.selectedData[key]){
					$http({
						url: '/riwayat_diklat/'+key+'/unapprove',
						method: 'GET'
					});
				}
			});
			$scope.getRiwayatDiklat($scope.idPegawai);
		};
		$scope.approvedRiwayatDiklat = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				$http({
					url: '/riwayat_diklat/'+id+'/approve',
					method: 'GET'
				}).then(function successCallback(response){
					$scope.getRiwayatDiklat($scope.idPegawai);
				});
			});
		};
		$scope.unapprovedRiwayatDiklat = function(id) {
			$http({
				url: '/riwayat_diklat/'+id+'/unapprove',
				method: 'GET'
			}).then(function successCallback(response){
				$scope.getRiwayatDiklat($scope.idPegawai);
			});
		};
		$scope.createRiwayatDiklat = function(){
			$scope.getMasterDiklat($scope.idPegawai);
			$('.create-diklat-modal').modal('show');
		}
		$scope.editRiwayatDiklat = function(a){
			$scope.editData =  angular.copy(a);
			console.log($scope.editData);
			$scope.getMasterDiklat($scope.idPegawai);
			$('.edit-diklat-modal').modal('show');
		}
	}]);