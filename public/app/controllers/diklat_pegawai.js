angular.module('simpegApp.controllers')
.controller('DiklatPegawaiCtrl', ['$http', '$scope', '$filter', '$confirm',
	function($http, $scope, $filter, $confirm){
		$scope.pegawai = {};
		$scope.selectedRiwayatDiklat = {};
		$scope.getMasterDiklat = function(){
			$http({
				method: 'GET',
				url: '/master_diklat'
			}).then(function(data){
				$scope.master_diklat = data.data;
			});
		}
		$scope.getMasterDiklat();
		$scope.globalSearch = function(query) {
			if(query!='') {
				$http({
					url: '/pegawai/search_by_nip/'+query,
					method: 'GET'
				}).then(function successCallback(response) {
					$scope.searchPegawai = response.data;
				});
			}
			else $scope.searchPegawai = {};
		}
		$scope.setPegawai = function(a){
			$scope.pegawai = a;
			$scope.searchPegawai = {};
			$scope.getRiwayatDiklat();
		}
		$scope.saveRiwayatDiklat = function(){
			$scope.newData.id_pegawai = $scope.pegawai.id;
			$scope.newData.tanggal_pelaksanaan = $filter('date')($scope.dateFilter, 'yyyy-MM-dd');
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/riwayat_diklat',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatDiklat();
				$scope.resetForm();
			});
		};
		$scope.getRiwayatDiklat = function(){
			$http({
				method: 'GET',
				url: '/pegawai/'+$scope.pegawai.id+'/riwayat_diklat'
			}).then(function(data){
				$scope.riwayat_diklat = data.data;
			});
		};
		$scope.updateRiwayatDiklat = function(data){
			var c = $filter('date')(data.tanggal_pelaksanaan, 'yyyy-MM-dd');
			data.tanggal_pelaksanaan = c;
			$http({
				method: 'PUT',
				url: '/riwayat_diklat/'+data.id,
				data: data
			}).then(function(){
				$scope.getRiwayatDiklat($scope.idPegawai);
			});
		};
		$scope.delRiwayatDiklat = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_diklat/'+id
				}).then(function(){
					$scope.getRiwayatDiklat($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatDiklat = function() {
			var keys = Object.keys($scope.selectedRiwayatDiklat);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedRiwayatDiklat[key]){
			    	$http({
			    		url: '/riwayat_diklat/'+key+'/approve',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatDiklat($scope.idPegawai);
		};
		$scope.unapproveRiwayatDiklat = function() {
			var keys = Object.keys($scope.selectedRiwayatDiklat);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedRiwayatDiklat[key]){
			    	$http({
			    		url: '/riwayat_diklat/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatDiklat($scope.idPegawai);
		};
		$scope.approvedRiwayatDiklat = function(id) {
	    	$http({
	    		url: '/riwayat_diklat/'+id+'/approve',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatDiklat($scope.idPegawai);
	    	});
		};
		$scope.unapprovedRiwayatDiklat = function(id) {
	    	$http({
	    		url: '/riwayat_diklat/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatDiklat($scope.idPegawai);
	    	});
		};
	}]);