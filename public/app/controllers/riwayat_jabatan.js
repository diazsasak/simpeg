angular.module('simpegApp.controllers')
.controller('RiwayatJabatanCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.idPegawai = $window.id;
		$scope.user_level = $window.user_level;
		$scope.user_group = $window.user_group;
		$scope.editData = $scope.selectedData = {};
		$scope.newData = {
			jenis_instansi: null, id_instansi: null
		};
		$scope.statusPerkawinan = [
		{ id: 1, nama: 'Belum Kawin' },{ id: 2, nama: 'Sudah Kawin' },{ id: 3, nama: 'Janda' },{ id: 4, nama: 'Duda' }
		];
		$scope.jk = [
		{ id: 1, nama: 'Laki-laki' },{ id: 0, nama: 'Perempuan' }
		];
		// $scope.jabatan_sekarang = [
		// {text: 'Jabatan Sekarang'},
		// {text: 'Jabatan yang Lalu'}
		// ];		
		$scope.selectedInstansi = {
			id_instansi_induk: null, id_satuan_kerja_induk: null, id_master_unit_skpd: null, id_sub_unit_organisasi: null
		}
		$scope.showJk = function() {
			var selected = $filter('filter')($scope.jk, {id: $scope.pegawai.jns_kelamin});
			return ($scope.pegawai.jns_kelamin !== undefined && selected.length) ? selected[0].nama : '';
		};
		//APPROVING RIWAYAT JABATAN
		$scope.approveRiwayatJabatan = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
					if($scope.selectedData[key]){
						$http({
							url: '/riwayat_jabatan/'+key+'/approve',
							method: 'GET'
						});
					}
				});
				$scope.getRiwayatJabatan($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatJabatan = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
				if($scope.selectedData[key]){
					$http({
						url: '/riwayat_jabatan/'+key+'/unapprove',
						method: 'GET'
					});
				}
			});
			$scope.getRiwayatJabatan($scope.idPegawai);
		};
		$scope.approvedRiwayatJabatan = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				$http({
					url: '/riwayat_jabatan/'+id+'/approve',
					method: 'GET'
				}).then(function successCallback(response){
					$scope.getRiwayatJabatan($scope.idPegawai);
				});
			});
		};
		$scope.unapprovedRiwayatJabatan = function(id) {
			$http({
				url: '/riwayat_jabatan/'+id+'/unapprove',
				method: 'GET'
			}).then(function successCallback(response){
				$scope.getRiwayatJabatan($scope.idPegawai);
			});
		};
		$scope.getAtasan = function(){
			$scope.setInstansi();
			$http({
				method: 'POST',
				url: '/pegawai/by_instansi',
				data: {
					jenis_instansi: $scope.newData.jenis_instansi, id_instansi: $scope.newData.id_instansi
				}
			}).then(function(data){
				$scope.atasan = data.data;
			});
		};
		$scope.setInstansi = function(){
			if($scope.selectedInstansi.id_sub_unit_organisasi != null){ 
				$scope.newData.jenis_instansi = "MasterSubUnitOrganisasi"; $scope.newData.id_instansi = $scope.selectedInstansi.id_sub_unit_organisasi; 
			}
			else if($scope.selectedInstansi.id_master_unit_skpd != null){
				$scope.newData.jenis_instansi = "MasterUnitSkpd"; $scope.newData.id_instansi = $scope.selectedInstansi.id_master_unit_skpd; 
			}
			else if($scope.selectedInstansi.id_satuan_kerja_induk != null){ 
				$scope.newData.jenis_instansi = "MasterSatuanKerjaInduk"; $scope.newData.id_instansi = $scope.selectedInstansi.id_satuan_kerja_induk; 
			}
			else{ 
				$scope.newData.jenis_instansi = "MasterInstansiInduk"; $scope.newData.id_instansi = $scope.selectedInstansi.id_instansi_induk; 
			}
		}
		$scope.getMasterEselon = function(){
			$http({
				method: 'GET',
				url: '/master_eselon'
			}).then(function(data){
				$scope.masterEselon = data.data;
			});
		};
		$scope.getMasterKategoriJabatan = function(){
			$http({
				method: 'GET',
				url: '/master_kategori_jabatan'
			}).then(function(data){
				$scope.masterKategoriJabatan = data.data;
			});
		};
		$scope.getMasterInstansiInduk = function(){
			$scope.selectedInstansi.id_satuan_kerja_induk = null;
			$scope.selectedInstansi.id_master_unit_skpd = null; 
			$scope.selectedInstansi.id_sub_unit_organisasi = null;
			$http({
				method: 'GET',
				url: '/master_instansi_induk'
			}).then(function(data){
				$scope.masterInstansiInduk = data.data;
			});
		};
		$scope.getMasterInstansiInduk();
		$scope.getMasterSatuanKerjaInduk = function(id){
			$scope.selectedInstansi.id_master_unit_skpd = null; 
			$scope.selectedInstansi.id_sub_unit_organisasi = null;
			$http({
				method: 'GET',
				url: '/master_instansi_induk/'+id+'/master_satuan_kerja_induk'
			}).then(function(data){
				$scope.masterSatuanKerjaInduk = data.data;
			});
		};
		$scope.getMasterUnitSkpd = function(id){
			$scope.selectedInstansi.id_sub_unit_organisasi = null;
			$http({
				method: 'GET',
				url: '/master_satuan_kerja_induk/'+id+'/master_unit_skpd'
			}).then(function(data){
				$scope.masterUnitSkpd = data.data;
			});
		};
		$scope.getMasterSubUnitOrganisasiDetail = function(id){
			$http({
				method: 'GET',
				url: '/master_sub_unit_organisasi/'+id
			}).then(function(data){
				$scope.masterSubUnitOrganisasi = [data.data];
			});
		};
		$scope.getMasterUnitSkpdDetail = function(id){
			$http({
				method: 'GET',
				url: '/master_unit_skpd/'+id
			}).then(function(data){
				$scope.masterUnitSkpd = [data.data];
			});
		};
		$scope.getMasterSatuanKerjaIndukDetail = function(id){
			$http({
				method: 'GET',
				url: '/master_satuan_kerja_induk/'+id
			}).then(function(data){
				$scope.masterSatuanKerjaInduk = [data.data];
			});
		};
		$scope.getMasterSubUnitOrganisasi = function(id){
			$http({
				method: 'GET',
				url: '/master_unit_skpd/'+id+'/master_sub_unit_organisasi'
			}).then(function(data){
				$scope.masterSubUnitOrganisasi = data.data;
			});
		};

		//CRUD RIWAYAT JABATAN
		$scope.getRiwayatJabatan = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_jabatan'
			}).then(function(data){
				console.log(data.data);
				$scope.riwayatJabatan = data.data;
			});
		};
		$scope.getRiwayatJabatan($scope.idPegawai);
		$scope.saveRiwayatJabatan = function(){
			console.log($scope.newData);
			$scope.setInstansi();
			$scope.newData.id_pegawai = $scope.idPegawai;
			$http({
				method: 'POST',
				url: '/riwayat_jabatan',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatJabatan($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.updateRiwayatJabatan = function(){
			$scope.newData = $scope.editData;
			$scope.setInstansi();
			$http({
				method: 'PUT',
				url: '/riwayat_jabatan/'+$scope.newData.id,
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatJabatan($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.delRiwayatJabatan = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_jabatan/'+id
				}).then(function(){
					$scope.getRiwayatJabatan($scope.idPegawai);
				});
			});
		};
		$scope.getMasterJabatan = function(id){
			$http({
				method: 'GET',
				url: '/master_kategori_jabatan/'+id+'/master_jabatan'
			}).then(function(data){
				$scope.masterJabatan = data.data;
			});
		};
		$scope.getGolonganAwal = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/golongan_awal'
			}).then(function(data){
				console.log(data);
				$scope.golonganAwal = data.data;
				if($scope.golonganAwal.master_pangkat) {
					$scope.newData.golongan_awal = $scope.golonganAwal.master_pangkat.id;
					$scope.editData.golongan_awal = $scope.golonganAwal.master_pangkat.id;
				}
			});
		};
		$scope.getGolonganTerbaru = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/golongan_terbaru'
			}).then(function(data){
				$scope.golonganTerbaru = data.data;
				if($scope.golonganAwal.master_pangkat) {
					$scope.newData.golongan_awal = $scope.golonganAwal.master_pangkat.id;
					$scope.editData.golongan_awal = $scope.golonganAwal.master_pangkat.id;
				}
			});
		};
		$scope.editRiwayatJabatan = function(a){
			console.log(a);
			$scope.editData = angular.copy(a);
			$scope.editData.id_kategori_jabatan = a.master_jabatan.id_kategori_jabatan;
			$scope.masterJabatan = [{id: a.master_jabatan.id, nama: a.master_jabatan.nama}];
			$scope.selectedInstansi = {
				id_instansi_induk: null, id_satuan_kerja_induk: null, id_master_unit_skpd: null, id_sub_unit_organisasi: null
			}
			if($scope.editData.jenis_instansi == "MasterSubUnitOrganisasi"){
				$scope.getMasterSubUnitOrganisasiDetail($scope.editData.id_instansi);
				$scope.selectedInstansi.id_sub_unit_organisasi = $scope.editData.id_instansi;
			}
			else if($scope.editData.jenis_instansi == "MasterUnitSkpd"){
				$scope.getMasterUnitSkpdDetail($scope.editData.id_instansi);
				$scope.selectedInstansi.id_master_unit_skpd = $scope.editData.id_instansi;
			}
			else if($scope.editData.jenis_instansi == "MasterSatuanKerjaInduk"){
				$scope.getMasterSatuanKerjaIndukDetail($scope.editData.id_instansi);
				$scope.selectedInstansi.id_satuan_kerja_induk = $scope.editData.id_instansi;
			}
			else if($scope.editData.jenis_instansi == "MasterInstansiInduk"){
				$scope.selectedInstansi.id_instansi_induk = $scope.editData.id_instansi;
			}
			if(a.golongan_awal != null){
				$scope.editData.golongan_awal = a.golongan_awal.id;
			}
			if(a.golongan_terbaru != null){
				$scope.editData.golongan_terbaru = a.golongan_terbaru.id;
			}
			if(a.atasan != null){
				$scope.editData.id_pegawai_atasan = a.atasan.id;
			}
			$scope.getMasterEselon();
			$scope.getMasterKategoriJabatan();
			$scope.getGolonganAwal($scope.idPegawai);
			$scope.getGolonganTerbaru($scope.idPegawai);
			$('.edit-riwayat-jabatan-modal').modal('show');
		}
		$scope.resetForm = function(){
			$scope.newData = {};
			$scope.selectedInstansi = {
				id_instansi_induk: null, id_satuan_kerja_induk: null, id_master_unit_skpd: null, id_sub_unit_organisasi: null
			}
		}
		$scope.createRiwayatJabatan = function(){
			$scope.getMasterEselon();
			$scope.getGolonganAwal($scope.idPegawai);
			$scope.getGolonganTerbaru($scope.idPegawai);
			$scope.getMasterKategoriJabatan();
			$('.create-riwayat-jabatan-modal').modal('show');
		}

		$scope.validViewSelect = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		$scope.validView = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$dirty||a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		$scope.isError = function(a) {
			return (a.$error && (a.$dirty || a.$touched));
		};
		var isTrue = function (obj) {
			for (var i in obj) if (obj.hasOwnProperty(i)) return false;
				return true;
		};
	}]);