angular.module('simpegApp.controllers')
.controller('DashboardCtrl', ['$http', '$scope',
	function($http, $scope){
		$scope.getData = function() {
			$http({
				url: '/get_jk_diagram',
				method: 'GET'
			}).then(function successCallback(response) {
				console.log(response.data);
				$scope.setData(response.data);
			});
		}
		$scope.getData();
		var ctx = document.getElementById("myChart");
		var chartData = {
			labels: [],
			datasets: [
			{
				label: 'Laki - laki',
				data: [],
				backgroundColor: [
				'rgba(255, 99, 132, 0.2)'
				],
				borderColor: [
				'rgba(255,99,132,1)'
				],
				borderWidth: 1
			},
			{
				label: 'Perempuan',
				data: [],
				backgroundColor: [
				'rgba(54, 162, 235, 0.2)'
				],
				borderColor: [
				'rgba(54, 162, 235, 1)'
				],
				borderWidth: 1
			}
			]
		};
		$scope.setData = function(data){
			for(var i = 0; i < data.length; i++){
				chartData.datasets[0].data.push(data[i].l_count);
				chartData.datasets[0].backgroundColor.push('rgba(255, 99, 132, 0.2)');
				chartData.datasets[0].borderColor.push('rgba(255,99,132,1)');
				chartData.datasets[1].data.push(data[i].p_count);
				chartData.datasets[1].backgroundColor.push('rgba(54, 162, 235, 0.2)');
				chartData.datasets[1].borderColor.push('rgba(54, 162, 235, 1)');
				chartData.labels.push(data[i].tahun_pengangkatan);
			}
			var myBarChart = new Chart(ctx, {
				type: 'bar',
				data: chartData,
				options: {
					scales: {
						yAxes: [{
							ticks: {
								beginAtZero:true
							}
						}]
					},
					title: {
						display: true,
						text: 'Jumlah Pegawai Aktif berdasarkan Tahun Pengangkatan dan Jenis Kelamin'
					}
				}
			});
		};
	}]);
