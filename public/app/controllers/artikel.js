angular.module('simpegApp.controllers')
.controller('ArtikelCtrl', ['$http', '$scope','$confirm', 
	function($http, $scope, $confirm){
		$scope.tinymceOptions = {
			plugins: 'link image code',
			toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
		};
		$scope.published = false;
		$scope.selected = {};
		$scope.getData = function() {
			$http({
				url: '/master_kategori_artikel/',
				method: 'GET'
			}).then( function (data) {
				$scope.master_kategori_artikel = data.data;
			});
			$http({
				url: '/artikel/',
				method: 'GET'
			}).then( function (data) {
				$scope.artikel = data.data;
			});
		}
		$scope.getData();
		$scope.paginate = function(myUrl) {
			$http({
				url: myUrl,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.pegawai = response.data;
			});
		};
		$scope.reloadPaginate = function(current) {
			$http({
				url: '/artikel?page='+current,
				method: 'GET'
			}).then( function (data) {
				$scope.artikel = data.data;
			});
		};
		$scope.insertArtikel = function(){
			var fd = new FormData();
			for (var key in $scope.newData) {
				if ($scope.newData.hasOwnProperty(key)) {
					fd.append(key, $scope.newData[key]);
				}
			}
			$http({
				method: 'POST',
				url: '/artikel',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then( function () {
				$scope.reloadPaginate($scope.artikel.current_page);
				$scope.newData = {};
			});
		};
		$scope.delArtikel = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/artikel/'+id
				}).then(function(){
					$scope.reloadPaginate($scope.artikel.current_page);
				});
			});
		};
		$scope.resArtikel = function(id){
			$http({
				method: 'GET',
				url: '/artikel/'+id+'/restore'
			}).then(function(){
				$scope.reloadPaginate($scope.artikel.current_page);
			});
		};
		$scope.editModal = function(a){
			$scope.editData = angular.copy(a);
			$scope.editData.hapus_foto = false;
			$('.edit-modal').modal('show');
		};
		$scope.updateArtikel = function(a){
			var fd = new FormData();
			$scope.editData._method = 'PUT';
			for (var key in $scope.editData) {
				if ($scope.editData.hasOwnProperty(key)) {
					fd.append(key, $scope.editData[key]);
				}
			}
			// Display the key/value pairs
			// for (var pair of fd.entries()) {
			// 	console.log(pair[0]+ ', ' + pair[1]); 
			// }
			$http({
				method: 'POST',
				url: '/artikel/'+a.id,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then( function (data) {
				// console.log(data);
				$scope.reloadPaginate($scope.artikel.current_page);
			});
		};
		$scope.publish = function(id) {
			$http({
				url: '/artikel/publish/'+id+'/edit',
				method: 'PUT'
			}).then(function successCallback(response){
				$scope.reloadPaginate($scope.artikel.current_page);
			});
		};
		$scope.unpublish = function(id) {
			$http({
				url: '/artikel/unpublish/'+id+'/edit',
				method: 'PUT'
			}).then(function successCallback(response){
				$scope.reloadPaginate($scope.artikel.current_page);
			});
		};

	}]);