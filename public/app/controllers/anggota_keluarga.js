angular.module('simpegApp.controllers')
.controller('AnggotaKeluargaCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.idPegawai = $window.id;
		$scope.statusPerkawinan = [
		{ id: 1, nama: 'Belum Kawin' },{ id: 2, nama: 'Sudah Kawin' },{ id: 3, nama: 'Janda' },{ id: 4, nama: 'Duda' }
		];
		$scope.jk = [
		{ id: 1, nama: 'Laki-laki' },{ id: 0, nama: 'Perempuan' }
		];		
		$scope.showJk = function() {
			var selected = $filter('filter')($scope.jk, {id: $scope.pegawai.jns_kelamin});
			return ($scope.pegawai.jns_kelamin !== undefined && selected.length) ? selected[0].nama : '';
		};
		$scope.saveAnggotaKeluarga = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			$http({
				method: 'POST',
				url: '/anggota_keluarga',
				data: $scope.newData
			}).then(function(){
				$scope.getAnggotaKeluarga($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.updateAnggotaKeluarga = function(data){
			$http({
				method: 'PUT',
				url: '/anggota_keluarga/'+data.id,
				data: data
			}).then(function(){
				$scope.getAnggotaKeluarga($scope.idPegawai);
			});
		};
		$scope.delAnggotaKeluarga = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/anggota_keluarga/'+id
				}).then(function(){
					$scope.getAnggotaKeluarga($scope.idPegawai);
				});
			});
		};
		$scope.getAnggotaKeluarga = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/anggota_keluarga'
			}).then(function(data){
				$scope.anggotaKeluarga = data.data;
			});
		};
		$scope.getAnggotaKeluarga($scope.idPegawai);
		$scope.getMasterKeluarga = function(){
			$http({
				method: 'GET',
				url: '/master_keluarga'
			}).then(function(data){
				$scope.masterKeluarga = data.data;
			});
		};
		$scope.getMasterPendidikan = function(){
			$http({
				method: 'GET',
				url: '/master_pendidikan'
			}).then(function(data){
				$scope.masterPendidikan = data.data;
			});
		};
		
		$scope.getMasterPekerjaan = function(){
			$http({
				method: 'GET',
				url: '/master_pekerjaan'
			}).then(function(data){
				$scope.masterPekerjaan = data.data;
			});
		};
		
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.createAnggotaKeluarga = function(){
			$scope.getMasterKeluarga();
			$scope.getMasterPendidikan();
			$scope.getMasterPekerjaan();
			$('.create-anggota-keluarga-modal').modal('show');
		}
		$scope.editAnggotaKeluarga = function(a){
			$scope.editData = angular.copy(a);
			$scope.getMasterKeluarga();
			$scope.getMasterPendidikan();
			$scope.getMasterPekerjaan();
			$('.edit-anggota-keluarga-modal').modal('show');
		}
	}]);