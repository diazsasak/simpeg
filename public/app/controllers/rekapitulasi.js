angular.module('simpegApp.controllers')
.controller('RekapitulasiCtrl', ['$http', '$scope',
	function($http, $scope){
		$scope.getData = function() {
			$http({
				url: '/rekapitulasi-pegawai',
				method: 'GET'
			}).then(function successCallback(response) {
				console.log(response);
				$scope.setPieChart(response.data.agama, "agamaChart");
				$scope.setPieChart(response.data.pendidikan, "pendidikanChart");
				$scope.setPieChart(response.data.jenisKelamin, "jkChart");
				$scope.setFungsionalTertentuChart(response.data.fungsionalTertentu, "fungsionalTertentuChart");
				$scope.setFungsionalUmumChart(response.data.fungsionalUmum, "fungsionalUmumChart");
				$scope.setPangkatChart(response.data.pangkat, "pangkatChart");
				$scope.setEselonChart(response.data.eselon, "eselonChart");
			});
		}
		$scope.getData();
		$scope.setPieChart = function(d, chartName){
			var data = {
				datasets: [{
					data: [],
					backgroundColor: []
				}],

				labels: []
			};
			for(var i = 0; i < d.length; i++){
				data.datasets[0].data.push(d[i].jml);
				data.datasets[0].backgroundColor.push($scope.dynamicColors());
				data.labels.push(d[i].nama);
			}
			var ctx = document.getElementById(chartName);
			var myPieChart = new Chart(ctx,{
				type: 'pie',
				data: data
			});
		}

		$scope.setPangkatChart = function(d, chartName){
			var data = {
				labels: [],
				datasets: [{
					label: "Laki - laki",
					backgroundColor: $scope.dynamicColors(),
					data: []
				},
				{
					label: "Perempuan",
					backgroundColor: $scope.dynamicColors(),
					data: []
				}]
			};
			for(var i = 0; i < d.length; i++){
				data.datasets[0].data.push(d[i].jmlL);
				data.datasets[1].data.push(d[i].jmlP);
				data.labels.push(d[i].nama_golongan);
			}
			var ctx = document.getElementById(chartName);
			var myBarChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: data,
				option: {
					scales: {
						xAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(value) {if (value % 1 === 0) {return value;}}
							}
						}]
					}
				}
			});
		}
                              
                                                                  $scope.setFungsionalUmumChart = function(d, chartName){
			var data = {
				labels: [],
				datasets: [
				{
					label: "Fungsional Umum",
					backgroundColor: [],
					data: []
				}]
			};
			for(var i = 0; i < d.length; i++){
				data.datasets[0].data.push(d[i].jml);
				data.labels.push(d[i].nama);
				data.datasets[0].backgroundColor.push($scope.dynamicColors());
			}
			var ctx = document.getElementById(chartName);
			var myBarChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: data,
				option: {
					scales: {
						xAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(value) {if (value % 1 === 0) {return value;}}
							}
						}]
					}
				}
			});
		}

		$scope.setFungsionalTertentuChart = function(d, chartName){
			var data = {
				labels: [],
				datasets: [
				{
					label: "Fungsional Tertentu",
					backgroundColor: [],
					data: []
				}]
			};
			for(var i = 0; i < d.length; i++){
				data.datasets[0].data.push(d[i].jml);
				data.labels.push(d[i].nama);
				data.datasets[0].backgroundColor.push($scope.dynamicColors());
			}
			var ctx = document.getElementById(chartName);
			var myBarChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: data,
				option: {
					scales: {
						xAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(value) {if (value % 1 === 0) {return value;}}
							}
						}]
					}
				}
			});
		}

		$scope.setEselonChart = function(d, chartName){
			var data = {
				labels: [],
				datasets: [{
					label: "Laki - laki",
					backgroundColor: $scope.dynamicColors(),
					data: []
				},
				{
					label: "Perempuan",
					backgroundColor: $scope.dynamicColors(),
					data: []
				}]
			};
			for(var i = 0; i < d.length; i++){
				data.datasets[0].data.push(d[i].jmlL);
				data.datasets[1].data.push(d[i].jmlP);
				data.labels.push(d[i].nama);
			}
			var ctx = document.getElementById(chartName);
			var myBarChart = new Chart(ctx, {
				type: 'horizontalBar',
				data: data,
				option: {
					scales: {
						xAxes: [{
							ticks: {
								beginAtZero: true,
								callback: function(value) {if (value % 1 === 0) {return value;}}
							}
						}]
					}
				}
			});
		}

		$scope.dynamicColors = function() {
			var r = Math.floor(Math.random() * 255);
			var g = Math.floor(Math.random() * 255);
			var b = Math.floor(Math.random() * 255);
			return "rgb(" + r + "," + g + "," + b + ")";
		}
	}]);
