angular.module('simpegApp.controllers')
.controller('RiwayatPangkatCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.pegawai = {};
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.getRiwayatPangkat = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_pangkat'
			}).then(function(data){
				console.log(data);
				$scope.riwayat_pangkat = data.data;
			});
		}
		$scope.getRiwayatPangkat($scope.idPegawai);
		$scope.saveRiwayatPangkat = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			console.log($scope.newData);
			$http({
				method: 'POST',
				url: '/riwayat_pangkat',
				data: $scope.newData
			}).then(function(){
				$scope.getRiwayatPangkat($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createRiwayatPangkat = function(){
			$http({
				method: 'GET',
				url: '/master_pangkat/'
			}).then(function(data){
				$scope.pangkat = data.data;
			});
			$('.create-riwayat-pangkat-modal').modal('show');
		}
		$scope.editRiwayatPangkat = function(a){
			$http({
				method: 'GET',
				url: '/master_pangkat/'
			}).then(function(data){
				$scope.pangkat = data.data;
			});
			$scope.editData = angular.copy(a);
			$('.edit-riwayat-pangkat-modal').modal('show');
		}
		$scope.updateRiwayatPangkat = function(data){
			$http({
				method: 'PUT',
				url: '/riwayat_pangkat/'+data.id,
				data: data
			}).then(function(){
				$scope.getRiwayatPangkat($scope.idPegawai);
			});
		};
		$scope.delRiwayatPangkat = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_pangkat/'+id
				}).then(function(){
					$scope.getRiwayatPangkat($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatPangkat = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/riwayat_pangkat/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getRiwayatPangkat($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatPangkat = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/riwayat_pangkat/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatPangkat($scope.idPegawai);
		};
		$scope.approvedRiwayatPangkat = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/riwayat_pangkat/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getRiwayatPangkat($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedRiwayatPangkat = function(id) {
	    	$http({
	    		url: '/riwayat_pangkat/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatPangkat($scope.idPegawai);
	    	});
		};
	}]);