//LOKASI
angular.module('simpegApp.controllers')
.controller('DetailPegawaiCtrl', ['$http', '$scope', '$window', '$confirm', '$filter',
	function($http, $scope, $window, $confirm, $filter){
		$scope.kabupaten = $scope.kecamatan = $scope.showPangkat = $scope.showJabatan = $scope.showPendidikan = [];
		$scope.inputPegawai = {};
		var isTrue = function (obj) {
			for (var i in obj) if (obj.hasOwnProperty(i)) return false;
				return true;
		};
		$scope.validViewSelect = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		$scope.validView = function(a) {
			return a.$invalid ? ((!isTrue(a.$error)&& (a.$dirty||a.$touched)) ? 'has-error' : '') : 'has-success';
		};
		$scope.isError = function(a) {
			return (a.$error && (a.$dirty || a.$touched));
		}
		$scope.editPegawai = function(a) {
			console.log(a);
			$scope.inputPegawai = a;
			$scope.kabupaten.push($scope.inputPegawai.kabupaten);
			$scope.kecamatan.push($scope.inputPegawai.kecamatan);
			$('.create-modal').modal('show');
		};
		$scope.loadAgama = function() {
			$http({
				url: '/master_agama',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.masterAgama = response.data;
  			});
		};
		$scope.loadAgama();
		$scope.loadStatusKepegawaian = function() {
			$http({
				url: '/master_status_kepegawaian',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.masterStatusKepegawaian = response.data;
  			});
		};
		$scope.loadStatusKepegawaian();
		$scope.loadProvinsi = function() {
			$http({
				url: '/provinsi',
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.provinsi = response.data;
  			});
		};
		$scope.loadProvinsi();
		$scope.loadKabupaten = function(id) {
			console.log('load');
			$http({
				url: '/kabupaten/'+id,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.kabupaten = response.data;
				console.log($scope.kabupaten);
  			});
		};
		$scope.loadKecamatan = function(id) {
			$http({
				url: '/kecamatan/'+id,
				method: 'GET'
			}).then(function successCallback(response) {
				$scope.kecamatan = response.data;
  			});
		};
		$scope.loadDetail = {
			'id' : false
		};
		$scope.is_approved = false;
		$scope.idPegawai = $window.id;
		$scope.selectedRiwayatJabatan = {};
		$scope.selectedRiwayatDiklat = {};
		$scope.buttonEdit = {
			'pkk' : false
		};
		$scope.loadMore = function() {
			if($scope.loadDetail.id) {

				$scope.loadDetail = {
					'id' : false
				};
			}
			else {
				$scope.loadDetail = {
					'id' : true
				};
			}
		};
		//varibable for pre
		// $scope.status_perkawinan = [
		// 	{ id: 1, nama: 'Belum Kawin' },{ id: 2, nama: 'Sudah Kawin' },{ id: 3, nama: 'Janda' },{ id: 4, nama: 'Duda' }
		// ];
		$scope.status_perkawinan = ['Belum Kawin', 'Sudah Kawin', 'Janda', 'Duda'];
		$scope.jenis_dokumen = [
			{ id: 1, nama: 'KTP' },{ id: 2, nama: 'SIM' },{ id: 3, nama: 'Passport' }
		];
		$scope.jenis_pegawai = [
			{ id: 1, nama: 'PNS Daerah Kab. Lombok Timur' }
		];
		// $scope.status_pegawai = [
		// 	{ id: 1, nama: 'PNS' }, { id: 2, nama: 'PNSD' }, { id: 3, nama: 'NON PNS' }
		// ];
		$scope.pegawai = {
			'jns_kelamin' : undefined
		};
		$scope.jk = [
			{ id: 1, nama: 'Laki-laki' },{ id: 0, nama: 'Perempuan' }
		];
		$scope.showJk = function() {
			var selected = $filter('filter')($scope.jk, {id: $scope.pegawai.jns_kelamin});
			return ($scope.pegawai.jns_kelamin !== undefined && selected.length) ? selected[0].nama : '';
		};
		$scope.golongan = [
			{ nama: 'I/A' },{ nama: 'I/B' },{ nama: 'I/C' },{ nama: 'I/D' },{ nama: 'II/A' },{ nama: 'II/B' },{ nama: 'II/C' },{ nama: 'II/D' },{ nama: 'III/A' },{ nama: 'III/B' },{ nama: 'III/C' },{ nama: 'III/D' },{ nama: 'IV/A' },{ nama: 'IV/B' },{ nama: 'IV/C' },{ nama: 'IV/D' }
		]
		
		$scope.jenis = [
			{text: 'Formal'},
			{text: 'Non Formal'}
		];
		$scope.getDataPegawai = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id
			}).then(function(data){
				console.log(data);
				$scope.pegawai = data.data.pegawai;
				$scope.showPendidikan = data.data.pendidikan;
				$scope.showJabatan = data.data.jabatan;
				$scope.showPangkat = data.data.pangkat;
				$scope.showInstansiInduk = data.data.instansiInduk;
				$scope.showSatuanKerjaInduk = data.data.satuanKerjaInduk;
				$scope.showUnitOrganisasi = data.data.unitOrganisasi;
				$scope.showSubUnitOrganisasi = data.data.subUnitOrganisasi;

				$scope.masterJurusan = data.data.masterJurusan;
				$scope.masterPendidikan = data.data.masterPendidikan;
				$scope.jumlahAnak = data.data.jumlahAnak;
				$scope.jumlahIstri = data.data.jumlahIstri;
				$scope.pegawai.hapus_foto = false;
				if($scope.pegawai.data_tambahan_pegawai) $scope.pegawai.data_tambahan_pegawai.is_hidup == 1 ? $scope.pegawai.data_tambahan_pegawai.is_hidup = true : $scope.pegawai.data_tambahan_pegawai.is_hidup = false;
				$scope.pegawai.is_aktif == 1 ? $scope.pegawai.is_aktif = true : $scope.pegawai.is_aktif = false;
				$scope.pegawai.is_struktural == 1 ? $scope.pegawai.is_struktural = true : $scope.pegawai.is_struktural = false;
				$scope.pegawai.is_atasan == 1 ? $scope.pegawai.is_atasan = true : $scope.pegawai.is_atasan = false;
				$scope.pegawai.jns_kelamin == 1 ? $scope.pegawai.jns_kelamin = '1' : $scope.pegawai.jns_kelamin = '0';
				$scope.pegawai.namaJenisDokumen = $filter('filter')($scope.jenis_dokumen, {id:$scope.pegawai.jenis_dokumen})[0].nama;
				$scope.pegawai.namaJenisPegawai = $filter('filter')($scope.jenis_pegawai, {id:$scope.pegawai.jenis_pegawai})[0].nama;
				$scope.pegawai.namaStatusPegawai = $filter('filter')($scope.masterStatusKepegawaian, {id:$scope.pegawai.id_status_kepegawaian})[0].nama;
			});
		};
		
		$scope.getDataPegawaiPrint = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/print'
			}).then(function(data){
				console.log("stfu");
				$scope.pegawai = data.data.pegawai;
				$scope.masterJurusan = data.data.masterJurusan;
				$scope.masterPendidikan = data.data.masterPendidikan;
				$scope.jumlahAnak = data.data.jumlahAnak;
				$scope.jumlahIstri = data.data.jumlahIstri;

				$scope.instansiInduk = data.data.instansiInduk;
				$scope.satuanKerjaInduk = data.data.satuanKerjaInduk;
				$scope.unitOrganisasi = data.data.unitOrganisasi;
				$scope.subUnitOrganisasi = data.data.subUnitOrganisasi;

				$scope.pegawai.hapus_foto = false;
				if($scope.pegawai.data_tambahan_pegawai) $scope.pegawai.data_tambahan_pegawai.is_hidup == 1 ? $scope.pegawai.data_tambahan_pegawai.is_hidup = true : $scope.pegawai.data_tambahan_pegawai.is_hidup = false;
				$scope.pegawai.is_aktif == 1 ? $scope.pegawai.is_aktif = true : $scope.pegawai.is_aktif = false;
				$scope.pegawai.is_struktural == 1 ? $scope.pegawai.is_struktural = true : $scope.pegawai.is_struktural = false;
				$scope.pegawai.is_atasan == 1 ? $scope.pegawai.is_atasan = true : $scope.pegawai.is_atasan = false;
				$scope.pegawai.jns_kelamin == 1 ? $scope.pegawai.jns_kelamin = '1' : $scope.pegawai.jns_kelamin = '0';
				$scope.pegawai.namaJenisDokumen = $filter('filter')($scope.jenis_dokumen, {id:$scope.pegawai.jenis_dokumen})[0].nama;
				$scope.pegawai.namaJenisPegawai = $filter('filter')($scope.jenis_pegawai, {id:$scope.pegawai.jenis_pegawai})[0].nama;
				$scope.pegawai.namaStatusPegawai = $filter('filter')($scope.masterStatusKepegawaian, {id:$scope.pegawai.id_status_kepegawaian})[0].nama;
			});
		};

		$scope.savePegawai = function(a){
			var fd = new FormData();
			$scope.inputPegawai.hapus_foto ? $scope.inputPegawai.hapus_foto = '1' : $scope.inputPegawai.hapus_foto = '0';
			if ($scope.inputPegawai.is_aktif) {
				$scope.inputPegawai.is_aktif = '1';
				// $scope.inputPegawai.is_aktif_false_ket = '';
			}
			else $scope.inputPegawai.is_aktif = '0';
			$scope.inputPegawai.is_struktural ? $scope.inputPegawai.is_struktural = '1' : $scope.inputPegawai.is_struktural = '0';
			$scope.inputPegawai.is_atasan ? $scope.inputPegawai.is_atasan = '1' : $scope.inputPegawai.is_atasan = '0';
			$scope.inputPegawai._method = 'PUT';
			for (var key in $scope.inputPegawai) {
				if ($scope.inputPegawai.hasOwnProperty(key)) {
					fd.append(key, $scope.inputPegawai[key]);
				}
			}
			console.log(fd);
			$http({
				method: 'POST',
				url: '/pegawai/'+$scope.idPegawai,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then( function () {
				// $scope.reloadPaginate($scope.pegawai.current_page);
				$scope.getDataPegawai($scope.idPegawai);
				$scope.inputPegawai = {};
				a.$setPristine();
				a.$setUntouched();
				a.$submitted = false;
  			});
		};

		$scope.updateDataTambahan = function(data) {
			if (data.is_hidup) {
				data.akte_meninggal = null;
				data.tanggal_meninggal = null;
			}
			data.id_pegawai = $scope.idPegawai;
			$http({
				url: '/data_tambahan_pegawai/'+$scope.idPegawai,
				method: 'PUT',
				data : data
			}).then( function() {
				$scope.getDataPegawai($scope.idPegawai);
  			});
		};

		//CRUD PEGAWAI
		$scope.updatePegawai = function(data){
			data.id_pegawai = $scope.idPegawai;
			data.tgl_lahir = $filter('date')(data.tgl_lahir, 'yyyy-MM-dd');
			console.log(data);
			$http({
				method: 'PUT',
				url: '/pegawai/'+data.id,
				data: data
			}).then(function(){
				$scope.getDataPegawai($scope.idPegawai);
				$scope.buttonEdit.pkk = false;
			});
		};

		//CRUD RIWAYAT PENDIDIKAN
		$scope.getRiwayatPendidikan = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_pendidikan'
			}).then(function(data){
				console.log(data);
				$scope.pegawai.riwayat_pendidikan = data.data;
			});
		};
		$scope.saveRiwayatPendidikan = function(id){
			$scope.pendidikan.id_pegawai = id;
			console.log($scope.pendidikan);
			$http({
				method: 'POST',
				url: '/riwayat_pendidikan',
				data: $scope.pendidikan
			}).then(function(data){
				console.log(data);
				$scope.getRiwayatPendidikan($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.updateRiwayatPendidikan = function(data){
			// if(data.jenis=='Formal') data.jenis = true;
			// else data.jenis = false;
			console.log(data);
			$http({
				method: 'PUT',
				url: '/riwayat_pendidikan/'+data.id,
				data: data
			}).then(function(){
				$scope.getDataPegawai($window.id);
				// $scope.getRiwayatPendidikan($scope.idPegawai);
			});
		};
		$scope.delRiwayatPendidikan = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_pendidikan/'+id
				}).then(function(){
					$scope.getRiwayatPendidikan($scope.idPegawai);
				});
			});
		};

		$scope.saveAnggotaKeluarga = function(id){
			$scope.newData.id_pegawai = id;
			$http({
				method: 'POST',
				url: '/anggota_keluarga',
				data: $scope.newData
			}).then(function(){
				$scope.getAnggotaKeluarga($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.updateAnggotaKeluarga = function(data){
			$http({
				method: 'PUT',
				url: '/anggota_keluarga/'+data.id,
				data: data
			}).then(function(){
				$scope.getAnggotaKeluarga($scope.idPegawai);
			});
		};
		$scope.delAnggotaKeluarga = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/anggota_keluarga/'+id
				}).then(function(){
					$scope.getAnggotaKeluarga($scope.idPegawai);
				});
			});
		};
		$scope.getAnggotaKeluarga = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/anggota_keluarga'
			}).then(function(data){
				console.log(data.data);
				$scope.pegawai.anggota_keluarga = data.data;
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
			$scope.jabatan = {};
			$scope.pendidikan = {};
		}
		$scope.opened = {};

		$scope.open = function($event, elementOpened) {
			$event.preventDefault();
			$event.stopPropagation();

			$scope.opened[elementOpened] = !$scope.opened[elementOpened];
		};
		var pathArray = window.location.pathname.split('/');
		if(pathArray.length>3) $scope.getDataPegawaiPrint($window.id);
		else $scope.getDataPegawai($window.id);
	}]);
