angular.module('simpegApp.controllers')
.controller('RiwayatAbsensiCtrl', ['$http', '$scope', '$filter', '$confirm', '$window',
	function($http, $scope, $filter, $confirm, $window){
		$scope.selectedData = {};
		$scope.idPegawai = $window.id;
		$scope.tanggalCollection = [];
		for(var i = 1; i < 32; i++){
			$scope.tanggalCollection.push({tanggal:i});
		}
		$scope.bulanCollection = [];
		for(var i = 1; i < 13; i++){
			$scope.bulanCollection.push({bulan:i});
		}
		var currentTime = new Date();
		var year = currentTime.getFullYear();
		$scope.tahunCollection = [];
		for(var i = 2000; i <= year; i++){
			$scope.tahunCollection.push({tahun:i});
		}
		$scope.getRiwayatAbsensi = function(id){
			$http({
				method: 'GET',
				url: '/pegawai/'+id+'/riwayat_absensi'
			}).then(function(data){
				$scope.riwayat_absensi = data.data;
			});
		}
		$scope.getRiwayatAbsensi($scope.idPegawai);
		$scope.saveRiwayatAbsensi = function(){
			$scope.newData.id_pegawai = $scope.idPegawai;
			var fd = new FormData();
			for (var key in $scope.newData) {
				if ($scope.newData.hasOwnProperty(key)) {
					fd.append(key, $scope.newData[key]);
				}
			}
			$http({
				method: 'POST',
				url: '/riwayat_absensi',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(){
				$scope.getRiwayatAbsensi($scope.idPegawai);
				$scope.resetForm();
			});
		};
		$scope.createRiwayatAbsensi = function(){
			$('.create-riwayat-absensi-modal').modal('show');
		}
		$scope.editRiwayatAbsensi = function(a){
			$scope.editData = angular.copy(a);
			$('.edit-riwayat-absensi-modal').modal('show');
		}
		$scope.updateRiwayatAbsensi = function(data){
			data._method = 'PUT';
			var fd = new FormData();
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					fd.append(key, data[key]);
				}
			}
			$http({
				method: 'POST',
				url: '/riwayat_absensi/'+data.id,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then(function(){
				$scope.getRiwayatAbsensi($scope.idPegawai);
			});
		};
		$scope.delRiwayatAbsensi = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/riwayat_absensi/'+id
				}).then(function(){
					$scope.getRiwayatAbsensi($scope.idPegawai);
				});
			});
		};
		$scope.resetForm = function(){
			$scope.newData = {};
		}
		$scope.approveRiwayatAbsensi = function() {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
				var keys = Object.keys($scope.selectedData);
				var filtered = keys.filter(function(key) {
				    if($scope.selectedData[key]){
				    	$http({
				    		url: '/riwayat_absensi/'+key+'/approve',
				    		method: 'GET'
				    	});
				    }
				});
				$scope.getRiwayatAbsensi($scope.idPegawai);
			});
		};
		$scope.unapproveRiwayatAbsensi = function() {
			var keys = Object.keys($scope.selectedData);
			var filtered = keys.filter(function(key) {
			    if($scope.selectedData[key]){
			    	$http({
			    		url: '/riwayat_absensi/'+key+'/unapprove',
			    		method: 'GET'
			    	});
			    }
			});
			$scope.getRiwayatAbsensi($scope.idPegawai);
		};
		$scope.approvedRiwayatAbsensi = function(id) {
			$confirm({text: 'Pastikan data yang anda isi telah valid!', title: 'Approve'})
			.then(function() {
		    	$http({
		    		url: '/riwayat_absensi/'+id+'/approve',
		    		method: 'GET'
		    	}).then(function successCallback(response){
			    	$scope.getRiwayatAbsensi($scope.idPegawai);
		    	});
			});
		};
		$scope.unapprovedRiwayatAbsensi = function(id) {
	    	$http({
	    		url: '/riwayat_absensi/'+id+'/unapprove',
	    		method: 'GET'
	    	}).then(function successCallback(response){
		    	$scope.getRiwayatAbsensi($scope.idPegawai);
	    	});
		};
	}]);