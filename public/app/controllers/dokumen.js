angular.module('simpegApp.controllers')
.controller('DokumenCtrl', ['$http', '$scope','$confirm', 
	function($http, $scope, $confirm){
		$scope.published = false;
		$scope.selected = {};
		$scope.getData = function() {
			$http({
				url: '/master_kategori_artikel/',
				method: 'GET'
			}).then( function (data) {
				$scope.master_kategori_artikel = data.data;
			});
			$http({
				url: '/dokumen/',
				method: 'GET'
			}).then( function (data) {
				console.log(data);
				$scope.data = data.data;
			});
		}
		$scope.getData();
		$scope.reloadPaginate = function(current) {
			$http({
				url: '/dokumen?page='+current,
				method: 'GET'
			}).then( function (data) {
				$scope.data = data.data;
			});
		};
		$scope.saveDokumen = function(){
			var fd = new FormData();
			for (var key in $scope.newData) {
				if ($scope.newData.hasOwnProperty(key)) {
					fd.append(key, $scope.newData[key]);
				}
			}
			$http({
				method: 'POST',
				url: '/dokumen',
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then( function () {
				$scope.reloadPaginate($scope.data.current_page);
				$scope.newData = {};
			});
		};
		$scope.delDokumen = function(id){
			$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
			.then(function() {
				$http({
					method: 'DELETE',
					url: '/dokumen/'+id
				}).then(function(){
					$scope.reloadPaginate($scope.data.current_page);
				});
			});
		};
		$scope.resDokumen = function(id){
			$http({
				method: 'GET',
				url: '/dokumen/'+id+'/restore'
			}).then(function(){
				$scope.reloadPaginate($scope.data.current_page);
			});
		};
		$scope.editModal = function(a){
			$scope.editData = angular.copy(a);
			$scope.editData.hapus_foto = false;
			$('.edit-modal').modal('show');
		};
		$scope.updateDokumen = function(a){
			var fd = new FormData();
			$scope.editData._method = 'PUT';
			for (var key in $scope.editData) {
				if ($scope.editData.hasOwnProperty(key)) {
					fd.append(key, $scope.editData[key]);
				}
			}
			// Display the key/value pairs
			// for (var pair of fd.entries()) {
			// 	console.log(pair[0]+ ', ' + pair[1]); 
			// }
			$http({
				method: 'POST',
				url: '/dokumen/'+a.id,
				data: fd,
				transformRequest: angular.identity,
				headers: {'Content-Type': undefined}
			}).then( function (data) {
				$scope.reloadPaginate($scope.data.current_page);
			});
		};
		$scope.publish = function(id) {
			$http({
				url: '/dokumen/publish/'+id+'/edit',
				method: 'PUT'
			}).then(function successCallback(response){
				$scope.reloadPaginate($scope.data.current_page);
			});
		};
		$scope.unpublish = function(id) {
			$http({
				url: '/dokumen/unpublish/'+id+'/edit',
				method: 'PUT'
			}).then(function successCallback(response){
				$scope.reloadPaginate($scope.data.current_page);
			});
		};

	}]);