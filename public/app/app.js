angular.module('simpegApp', [
    'simpegApp.controllers',
    'ngMessages',
    'ui.mask',
    'xeditable',
    'ui.bootstrap',
    'angular-confirm',
    'angular-loading-bar',
    'ui.tinymce'
    ])

.run(function($http, editableOptions, $confirmModalDefaults){
  $http.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
  editableOptions.theme = 'bs3';
  $confirmModalDefaults.defaultLabels.title = 'Hapus data';
  $confirmModalDefaults.defaultLabels.ok = 'Iya';
  $confirmModalDefaults.defaultLabels.cancel = 'Tidak';
})

.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])
.directive('convertToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(val) {
        return val != null ? parseInt(val, 10) : null;
      });
      ngModel.$formatters.push(function(val) {
        return val != null ? '' + val : null;
      });
    }
  };
});
;