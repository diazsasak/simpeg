(function(){
	appCtrl.controller('AccountCtrl', ['$scope','$filter', '$http', '$confirm',
		function($scope, $filter, $http, $confirm){
			$http.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
			$scope.init = function(){
				$http({
					method: 'GET',
					url: 'account'
				}).success(function(data){
					$scope.data = data;
				});
			};
			$scope.newdata = {
				code: "",
				name: "",
				balance:0
			};
			$scope.add = function(){
				$http({
					method: 'POST',
					url: 'account',
					data: $scope.newdata
				}).success(function(data){
					$scope.data = data;
					$scope.resetForm();
				});
			};
			$scope.update = function(data){
				$http({
					method: 'PUT',
					url: 'account/'+data.id,
					data: data
				}).success(function(data){
					$scope.data = data;
				});
			}
			$scope.resetForm = function(){
				$scope.newdata = {
					code: "",
					name: "",
					balance:0
				};
			}
			$scope.resetBalance = function(){
				$http({
					method: 'GET',
					url: '/account/resetBalance'
				}).success(function(data){
					$scope.data = data;
				});
			};
			$scope.remove = function(id){
				$confirm({text: 'Apakah anda yakin ingin menghapus data ?'})
				.then(function() {
					$http({
						method: 'DELETE',
						url: 'account/'+id
					}).success(function(data){
						$scope.data = data;
					});
				});
			};
			$scope.init();
		}]);
})();