@extends('layout2')
@section('title')
Daftar Akun
@endsection
@section('content')
<div ng-controller="AccountCtrl">
	<h4>Daftar Akun</h4>
	<table class="table-hover">
		<tr class="info">
			<th width="30%">Kode</th>
			<th width="40%">Nama</th>
			<th width="30%">Balance</th>
			<th>&nbsp;</th>
		</tr>
		<tr>
			<td>
				<input type="text" ng-model="newdata.code" class="form-control" placeholder="Kode">
			</td>
			<td>
				<input type="text" ng-model="newdata.name" class="form-control" placeholder="Nama">
			</td>
			<td>
				<div class="input-group">
					<span class="input-group-addon">Rp. </span>
					<input type="number" ng-model="newdata.balance" class="form-control" placeholder="Balance">
				</div>
			</td>
			<td>
				<button title="simpan" class="btn btn-primary" ng-click="add()"><span class="fa fa-save"></span></button>
			</td>
		</tr>
		<tr ng-repeat="a in data">
			<td onaftersave="update(a)" editable-text="a.code">{{ a.code }}</td>
			<td onaftersave="update(a)" editable-text="a.name">{{ a.name }}</td>
			<td onaftersave="update(a)" editable-text="a.balance">{{ a.balance | currency:"Rp. ":"0" }}</td>
			<td>
				<button title="hapus" ng-click="remove(a.id)" class="btn btn-danger">
					<span class="fa fa-trash"></span>
				</button>
			</td>
		</tr>
	</table>
</div>
@endsection
@section('script')
<script src="<% asset("dist/angular/account/controller.js") %>" type="text/javascript"></script>
@endsection