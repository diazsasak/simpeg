@extends('layout.layout')
@section('title')
Master Sub Unit Organisasi - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-folder-o"></i>
		Sub Unit Organisasi
		<small>Manajemen Master Sub Unit Organisasi</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Master</li>
		<li class="active">Master Sub Unit Organisasi</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Tabel Akun</h3> -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-modal">Tambah Sub Unit Organisasi</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr class="info">
							<th>#</th>
							<th>Kode Sub Unit</th>
							<th>Nama Sub Unit</th>
							<th>Jenis unit</th>
							<th>Unit Organisasi</th>
							<th colspan="2">&nbsp;</th>
						</tr>
						@foreach($data as $index=>$a)
						<tr>
							<td>{{$index+1}}</td>
							<td>{{ $a->kode_sub_unit_organisasi }}</td>
							<td>{{ $a->nama }}</td>
							<td>{{ $a->masterJenisUnit->nama }}</td>
							<td>{{ $a->masterUnitSkpd->nama }}</td>
							<td>
								@if($a->deleted_at == null)
								<a href="#" type="button" data-toggle="modal" data-target=".edit{{$a->id}}"><i class="fa fa-pencil fa-lg"></i></a>
								@endif
								<!-- Modal -->
								<div class="modal fade edit{{$a->id}}" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="{{ url('master_sub_unit_organisasi/'.$a->id) }}" method="POST">
												{{ method_field('PUT') }}
												{{ csrf_field() }}
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Edit Sub Unit Organisasi</h4>
												</div>
												<div class="modal-body">
													<div class="row">
														<div class="col-xs-12">
															<div class="form-group">
																<label>Kode Sub Unit</label>
																<input type="text" class="form-control" name="kode_sub_unit_organisasi" value="{{$a->kode_sub_unit_organisasi}}" required>
															</div>
															<div class="form-group">
																<label>Nama Unit</label>
																<input type="text" class="form-control" name="nama" value="{{$a->nama}}" required>
															</div>
															<div class="form-group">
																<label>Jenis Unit</label><br>
																<select class="form-control select2" style="width: 100%" name="id_jenis_unit" required>
																	@foreach($ju as $b)
																	<option value="{{$b->id}}" @if($a->id_jenis_unit==$b->id) selected @endif>{{$b->nama}}</option>
																	@endforeach
																</select>
															</div>
															<div class="form-group">
																<label>Unit Organisasi</label><br>
																<select class="form-control select2" style="width: 100%" name="id_unit_skpd" required>
																	@foreach($us as $b)
																	<option value="{{$b->id}}" @if($a->id_unit_skpd==$b->id) selected @endif>{{$b->nama}}</option>
																	@endforeach
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
													<button type="submit" class="btn btn-primary">Simpan</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</td>
							<td>
								@if($a->deleted_at == null)
								<a href="#" class="text-red pull-right margin-r-5" type="button" data-toggle="modal" data-target=".delete{{$a->id}}"><i class="fa fa-trash fa-lg"></i></a>
								<!-- Modal -->
								<div class="modal fade delete{{$a->id}}" role="dialog">
									<div class="modal-dialog modal-sm">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Hapus</h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda ingin menghapus data ini?</p>
											</div>
											<div class="modal-footer">
												<form method="POST" action="master_sub_unit_organisasi/{{$a->id}}">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<button type="submit" class="btn btn-danger">Hapus</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
												</form>
											</div>
										</div>
									</div>
								</div>
								@else
								<a class="pull-right margin-r-5" href="{{ url('master_sub_unit_organisasi/'.$a->id.'/restore') }}"><i class="fa fa-undo fa-lg"></i></a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
					@if(method_exists($data, 'links'))
                    {{ $data->links() }}
                    @endif
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{ url('master_sub_unit_organisasi') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Sub Unit Organisasi</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label>Kode Sub Unit</label>
								<input type="text" class="form-control" name="kode_sub_unit_organisasi" required>
							</div>
							<div class="form-group">
								<label>Nama Sub Unit</label>
								<input type="text" class="form-control" name="nama" required>
							</div>
							
							<div class="form-group">
								<label>Jenis Unit</label><br>
								<select class="form-control select2" style="width: 100%" name="id_jenis_unit" required>
									@foreach($ju as $b)
									<option value="{{$b->id}}">{{$b->nama}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group">
								<label>Unit Organisasi</label><br>
								<select class="form-control select2" style="width: 100%" name="id_unit_skpd" required>
									@foreach($us as $b)
									<option value="{{$b->id}}">{{$b->nama}}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
