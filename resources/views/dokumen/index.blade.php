@extends('layout.layout')
@section('title')
Dokumen - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<link href="{{ asset('simpeg/fullscreen_imagepreview.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-file"></i>
		Dokumen
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dokumen</li>
	</ol>
</section>
<section class="content" ng-controller="DokumenCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Dokumen</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>#</th>
								<th>Judul</th>
								<th><i class="fa fa-file-text-o" aria-hidden="true"></i> Dokumen</th>
								<th><i class="fa fa-share-alt-square" aria-hidden="true"></i> Kategori</th>
								<th><i class="fa fa-user"></i> Penulis</th>
								<th>Status</th>
								<th><a class="pull-right" data-toggle="modal" data-target=".create-modal" href="#"><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah Dokumen" ng-click="loadAll()"></i></a></th>
								<th>&nbsp;</th>
							</tr>
							<tr ng-repeat="a in data.data">
								<td ng-cloak>
									<!-- <input type="checkbox" name="selected" value="@{{ a.id }}" ng-if="!a.published" ng-model="selected[a.id]" id="@{{a.id}}"> -->
									@{{ $index+1 }}
								</td>
								<td ng-cloak>@{{ a.judul }}</td>
								<!-- <td ng-cloak>@{{ a.deskripsi }}</td> -->
								<td ng-cloak>
									<a href="{{ url('/') }}/@{{ a.document_path }}" target="_blank">File</a>
								</td>
								<td ng-cloak>@{{ a.master_kategori_artikel.nama }}</td>
								<td ng-cloak>@{{ a.user.name }}</td>
								<td>
									<a ng-class="{false: 'label label-success', true: 'label label-danger'}[published]" ng-if="a.published" ng-mouseenter="published=true" ng-mouseleave="published=false" href="#" ng-click="unpublish(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{published ? "Unpublish" : "Published"}}</a>
									<a ng-class="{false: 'label label-warning', true: 'label label-success'}[published]" ng-if="!a.published" ng-mouseenter="published=true" ng-mouseleave="published=false" href="#" ng-click="publish(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{published ? "Publish" : "Unpublished"}}</a>
								</td>
								<td><a href="" class="pull-right" ng-click="editModal(a)" ng-hide="a.deleted_at" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a></td>
								<td>
									<a class="text-red pull-right margin-r-5" ng-click="delDokumen(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
									<a class="text-green pull-right margin-r-5" ng-click="resDokumen(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a>
								</td>
							</tr>
							<tr ng-if="data.data.length!=0" ng-cloak>
								<td colspan="9">
									<!-- <button type="button" class="btn btn-primary" ng-click="approve()">Approve</button> -->
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(1)"  ng-disabled="data.current_page==1" data-toggle="tooltip" data-placement="top" title="First Page"><i class="fa fa-angle-double-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(data.prev_page_url)"  ng-disabled="data.prev_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Previous"><i class="fa fa-angle-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(data.next_page_url)" ng-disabled="data.next_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Next"><i class="fa fa-angle-right fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(data.last_page)"  ng-disabled="data.current_page==data.last_page" data-toggle="tooltip" data-placement="top" title="Last Page"><i class="fa fa-angle-double-right fa-lg"></i></button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah dokumen</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" class="form-control" ng-model="newData.judul">
								</div>
								<div class="form-group">
									<label>Dokumen</label>
									<input type="file" file-model="newData.dokumen">
									<i title="File Word" class="fa fa-file-word-o" aria-hidden="true"></i> <i title="File Pdf" class="fa fa-file-pdf-o" aria-hidden="true"></i> <i title="File Power Point" class="fa fa-file-powerpoint-o" aria-hidden="true"></i> <i title="File Excel" class="fa fa-file-excel-o" aria-hidden="true"></i>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label><i class="fa fa-share-alt-square" aria-hidden="true"></i> Kategori</label><br>
									<select style="width: 100%" class="select2 form-control"  name="id_kabupaten" ng-model="newData.id_kategori_artikel">
										<option ng-repeat="a in master_kategori_artikel" ng-value="a.id">@{{a.nama}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveDokumen(newData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade edit-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit dokumen</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-12">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" class="form-control" ng-model="editData.judul">
								</div>
								<div class="form-group">
									<div class="form-group">
									<label>Dokumen</label>
									<input type="file" file-model="editData.dokumen">
								</div>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label>Kategori</label><br>
									<select style="width: 100%" class="select2 form-control"  name="id_kabupaten" ng-model="editData.id_kategori_artikel">
										<option ng-repeat="a in master_kategori_artikel" ng-value="a.id">@{{a.nama}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateDokumen(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('app/controllers/dokumen.js') }}"></script>
@endsection
