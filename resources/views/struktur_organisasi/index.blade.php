@extends('layout.layout')
@section('title')
Struktur Organisasi - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<style type="text/css">
.sh-organisasi{
	text-align: center;
}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-folder-o"></i>
		Struktur Organisasi
		<small>Manajemen Struktur Organisasi</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Struktur Organisasi</li>
	</ol>
</section>
<section class="content">
	<form method="POST" action="{{ url('struktur_organisasi') }}">
		{{ csrf_field() }}
	<div class="row">
		<div class="col-md-12">
			<h2 style="text-align: center;margin-bottom: 40px;color: orange">Struktur Organisasi</h2>
			<div class="row sh-organisasi">
				<div class="col-md-4 col-md-offset-4">
					@if($data[0]->belongsToPegawai()->exists())
					<img src="{{ asset($data[0]->belongsToPegawai->poto) }}" class="img-rounded" />
					@else
					<img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" />
					@endif
					<div class="form-group">
						<div class="col-sm-12">
							<input type="hidden" name="id[]" value="{{ $data[0]->id }}">
							<input type="text" class="form-control" name="nama_jabatan[]" placeholder="Jabatan" value="{{ $data[0]->nama_jabatan }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="nip[]" placeholder="NIP" 
							@if($data[0]->belongsToPegawai()->exists())
							value="{{ $data[0]->belongsToPegawai->nip }}"
							@endif
							>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row sh-organisasi">
				<div class="col-md-3">
					@if($data[1]->belongsToPegawai()->exists())
					<img src="{{ asset($data[1]->belongsToPegawai->poto) }}" class="img-rounded" />
					@else
					<img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" />
					@endif
					<div class="form-group">
						<div class="col-sm-12">
							<input type="hidden" name="id[]" value="{{ $data[1]->id }}">
							<input type="text" class="form-control" name="nama_jabatan[]" placeholder="Jabatan" value="{{ $data[1]->nama_jabatan }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="nip[]" placeholder="NIP" 
							@if($data[1]->belongsToPegawai()->exists())
							value="{{ $data[1]->belongsToPegawai->nip }}"
							@endif
							>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					@if($data[2]->belongsToPegawai()->exists())
					<img src="{{ asset($data[2]->belongsToPegawai->poto) }}" class="img-rounded" />
					@else
					<img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" />
					@endif
					<div class="form-group">
						<div class="col-sm-12">
							<input type="hidden" name="id[]" value="{{ $data[2]->id }}">
							<input type="text" class="form-control" name="nama_jabatan[]" placeholder="Jabatan" value="{{ $data[2]->nama_jabatan }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="nip[]" placeholder="NIP" 
							@if($data[2]->belongsToPegawai()->exists())
							value="{{ $data[2]->belongsToPegawai->nip }}"
							@endif
							>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					@if($data[3]->belongsToPegawai()->exists())
					<img src="{{ asset($data[3]->belongsToPegawai->poto) }}" class="img-rounded" />
					@else
					<img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" />
					@endif
					<div class="form-group">
						<div class="col-sm-12">
							<input type="hidden" name="id[]" value="{{ $data[3]->id }}">
							<input type="text" class="form-control" name="nama_jabatan[]" placeholder="Jabatan" value="{{ $data[3]->nama_jabatan }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="nip[]" placeholder="NIP" 
							@if($data[3]->belongsToPegawai()->exists())
							value="{{ $data[3]->belongsToPegawai->nip }}"
							@endif
							>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					@if($data[4]->belongsToPegawai()->exists())
					<img src="{{ asset($data[4]->belongsToPegawai->poto) }}" class="img-rounded" />
					@else
					<img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" />
					@endif
					<div class="form-group">
						<div class="col-sm-12">
							<input type="hidden" name="id[]" value="{{ $data[4]->id }}">
							<input type="text" class="form-control" name="nama_jabatan[]" placeholder="Jabatan" value="{{ $data[4]->nama_jabatan }}">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<input type="text" class="form-control" name="nip[]" placeholder="NIP" 
							@if($data[4]->belongsToPegawai()->exists())
							value="{{ $data[4]->belongsToPegawai->nip }}"
							@endif
							>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<br/>
			<input type="submit" class="btn btn-success" value="Simpan">
		</div>
	</div>
	</form>
</section>
@endsection
