@extends('layout.layout')
@section('title')
Artikel - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<link href="{{ asset('simpeg/fullscreen_imagepreview.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-newspaper-o"></i>
		Artikel
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Artikel</li>
	</ol>
</section>
<section class="content" ng-controller="ArtikelCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Artikel</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody>
							<tr>
								<th>#</th>
								<th>Judul</th>
								<!-- <th>Deskripsi</th> -->
								<th><i class="fa fa-camera" aria-hidden="true"></i> Gambar</th>
								<th><i class="fa fa-share-alt-square" aria-hidden="true"></i> Kategori</th>
								<th><i class="fa fa-user"></i> Penulis</th>
								<th>Status</th>
								<th><a class="pull-right" data-toggle="modal" data-target=".create-modal" href="#"><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah artikel" ng-click="loadAll()"></i></a></th>
								<th>&nbsp;</th>
							</tr>
							<tr ng-repeat="a in artikel.data">
								<td ng-cloak>
									<!-- <input type="checkbox" name="selected" value="@{{ a.id }}" ng-if="!a.published" ng-model="selected[a.id]" id="@{{a.id}}"> -->
									@{{ $index+1 }}
								</td>
								<td ng-cloak>@{{ a.judul }}</td>
								<!-- <td ng-cloak>@{{ a.deskripsi }}</td> -->
								<td ng-cloak><img ng-src="/@{{a.gambar_path}}" style="border-radius: 10px; height: auto; width: 100px;"></td>
								<td ng-cloak>@{{ a.master_kategori_artikel.nama }}</td>
								<td ng-cloak>@{{ a.user.name }}</td>
								<td>
									<a ng-class="{false: 'label label-success', true: 'label label-danger'}[published]" ng-if="a.published" ng-mouseenter="published=true" ng-mouseleave="published=false" href="#" ng-click="unpublish(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{published ? "Unpublish" : "Published"}}</a>
									<a ng-class="{false: 'label label-warning', true: 'label label-success'}[published]" ng-if="!a.published" ng-mouseenter="published=true" ng-mouseleave="published=false" href="#" ng-click="publish(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{published ? "Publish" : "Unpublished"}}</a>
								</td>
								<td><a href="" class="pull-right" ng-click="editModal(a)" ng-hide="a.deleted_at" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a></td>
								<td>
									<a class="text-red pull-right margin-r-5" ng-click="delArtikel(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
									<a class="text-green pull-right margin-r-5" ng-click="resArtikel(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a>
								</td>
							</tr>
							<tr ng-if="artikel.data.length!=0" ng-cloak>
								<td colspan="9">
									<!-- <button type="button" class="btn btn-primary" ng-click="approve()">Approve</button> -->
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(1)"  ng-disabled="artikel.current_page==1" data-toggle="tooltip" data-placement="top" title="First Page"><i class="fa fa-angle-double-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(artikel.prev_page_url)"  ng-disabled="artikel.prev_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Previous"><i class="fa fa-angle-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(artikel.next_page_url)" ng-disabled="artikel.next_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Next"><i class="fa fa-angle-right fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(artikel.last_page)"  ng-disabled="artikel.current_page==artikel.last_page" data-toggle="tooltip" data-placement="top" title="Last Page"><i class="fa fa-angle-double-right fa-lg"></i></button>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg modal-lg-habib" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Artikel</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-8">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" class="form-control" ng-model="newData.judul">
								</div>
								<div class="form-group">
									<label>Deskripsi</label>
									<textarea class="form-control" ui-tinymce="tinymceOptions" ng-model="newData.deskripsi"></textarea>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="form-group">
									<label><i class="fa fa-share-alt-square" aria-hidden="true"></i> Kategori</label><br>
									<select style="width: 100%" class="select2 form-control"  name="id_kabupaten" ng-model="newData.id_kategori_artikel">
										<option ng-repeat="a in master_kategori_artikel" ng-value="a.id">@{{a.nama}}</option>
									</select>
								</div>
								<br>
								<div class="form-group">
									<label><i class="fa fa-camera" aria-hidden="true"></i> Upload Gambar</label>
									<div class="input-group image-preview">
										<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
										<span class="input-group-btn">
											<!-- image-preview-clear button -->
											<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
												<span class="glyphicon glyphicon-remove"></span> Clear
											</button>
											<!-- image-preview-input -->
											<div class="btn btn-default image-preview-input">
												<span class="glyphicon glyphicon-folder-open"></span>
												<span class="image-preview-input-title">Browse</span>
												<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="newData.gambar_path"/> <!-- rename it -->
											</div>
										</span>
									</div>
								</div>
								<span>Husus Slider Rekomendasi 1920 px X 667px</span>
							</div>

						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="insertArtikel(newData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal fade edit-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg modal-lg-habib" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Artikel</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-xs-8">
								<div class="form-group">
									<label>Judul</label>
									<input type="text" class="form-control" ng-model="editData.judul">
								</div>
								<div class="form-group">
									<label>Deskripsi</label>
									<textarea class="form-control" ui-tinymce="tinymceOptions" ng-model="editData.deskripsi"></textarea>
								</div>
							</div>
							<div class="col-xs-4">
								<div class="form-group">
									<label>Kategori</label><br>
									<select style="width: 100%" class="select2 form-control"  name="id_kabupaten" ng-model="editData.id_kategori_artikel">
										<option ng-repeat="a in master_kategori_artikel" ng-value="a.id">@{{a.nama}}</option>
									</select>
								</div>
								<br>
								<div class="form-group">
									<label>Upload Gambar</label>
									<div class="input-group image-preview">
										<input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
										<span class="input-group-btn">
											<!-- image-preview-clear button -->
											<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
												<span class="glyphicon glyphicon-remove"></span> Clear
											</button>
											<!-- image-preview-input -->
											<div class="btn btn-default image-preview-input">
												<span class="glyphicon glyphicon-folder-open"></span>
												<span class="image-preview-input-title">Browse</span>
												<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="editData.gambar_path"/> <!-- rename it -->
											</div>
										</span>
									</div>
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" name="hapus_foto" ng-model="editData.hapus_foto"> Hapus Foto ?
									</label>
								</div>
								<span>Husus Slider Rekomendasi 1920 px X 667px</span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateArtikel(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@endsection
@section('up-script')
<script type="text/javascript" src="{{ asset('bower_components/tinymce/tinymce.js') }}"></script>
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('app/controllers/artikel.js') }}"></script>
<script src="{{ asset('simpeg/imagepreview.js') }}" type="text/javascript"></script>
@endsection
