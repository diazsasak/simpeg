@extends('layout.layout')
@section('title')
Diklat Pegawai - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<style type="text/css">
	.table-borderless tbody tr td,
	.table-borderless tbody tr th,
	.table-borderless thead tr th,
	.table-borderless thead tr td,
	.table-borderless tfoot tr th,
	.table-borderless tfoot tr td  {
		border: none;
	}
	.editable-radiolist label {
		display: block;
	}

</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		Diklat Pegawai
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active"><a href="{{ url('diklat_pegawai') }}">Diklat Pegawai</a></li>
	</ol>
</section>
<section class="content" ng-controller="DiklatPegawaiCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Cari Pegawai</h3>

					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="maximize" >
							<i class="fa fa-expand"></i></button>
						<button type="button" class="btn btn-box-tool" data-widget="remove" >
							<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form action="#" method="get" class="sidebar-form" style="margin: 0px;">
							<div class="input-group" style="width: 100%">
								<input type="text" name="q" class="form-control" placeholder="NIP" ng-model="searchQuery" ng-change="globalSearch(searchQuery)" autocomplete="off">
								<span class="input-group-btn">
									<button type="button" name="search" id="search-btn" class="btn btn-flat" ng-click="globalSearch(searchQuery)"><i class="fa fa-search"></i>
									</button>
								</span>
							</div>
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<div class="box box-success" ng-cloak>
					<div class="box-header with-border">
						<h3 class="box-title">Pegawai</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div ng-repeat="a in searchPegawai" ng-hide="!searchPegawai||!searchPegawai.length">
							<a href="#" ng-click="setPegawai(a)">@{{a.nip+' '+a.nama+' '+a.tempat_lahir+' '+a.golongan+' '+a.alamat_sekarang+' '+a.asal_provinsi+' '+a.asal_kabupaten_kota+' '+a.asal_kecamatan+' '+a.asal_alamat_lengkap+' '+a.tahun_pengangkatan}} </a><br>
						</div>
						<div ng-show="!searchPegawai||!searchPegawai.length">
							<form class="form-horizontal">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">Nama</label>
									<div class="col-sm-10">
										<input type="email" class="form-control" ng-model="pegawai.nama" disabled>
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label">NIP</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" ng-model="pegawai.nip" disabled>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Diklat</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<div class="box-body">
						<div class="table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>#</th>
										<th>Nama</th>
										<th>Jenis</th>
										<th>Tempat</th>
										<th>Tanggal pelaksanaan</th>
										<th>Pelaksana</th>
										<th>Status</th>
										<th><a class="pull-right margin-r-5" data-toggle="modal" data-target=".create-diklat-modal" href="#"><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
									</tr>
									<tr ng-repeat="a in riwayat_diklat">
										<td>
											<input type="checkbox" name="selectedRiwayatDiklat" value="@{{ a.id }}" ng-model="selectedRiwayatDiklat[a.id]" id="@{{a.id}}">
										</td>
										<td onaftersave="updateRiwayatDiklat(a)" editable-text="a.nama" ng-cloak>@{{ a.nama }}</td>
										<td onaftersave="updateRiwayatDiklat(a)" editable-select="a.id_diklat" e-ng-options="b.id as b.jenis for b in masterDiklat" ng-cloak>@{{ a.master_diklat.jenis }}</td>
										<td onaftersave="updateRiwayatDiklat(a)" editable-text="a.tempat" ng-cloak>@{{ a.tempat }}</td>
										<td><a href="#" style="border:none" onaftersave="updateRiwayatDiklat(a)" e-show-calendar-button="true" editable-bsdate="a.tanggal_pelaksanaan" e-is-open="opened.$data" e-ng-click="open($event,'$data')" e-datepicker-popup="yyyy-MM-dd" ng-cloak>@{{ a.tanggal_pelaksanaan | date:"yyyy-MM-dd" }}</a></td>
										<td onaftersave="updateRiwayatDiklat(a)" editable-text="a.pelaksana" ng-cloak>@{{ a.pelaksana }}</td>
										<td>
											<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatDiklat(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{is_approved ? "Unapproved" : "Approved"}}</a>
											<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatDiklat(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{is_approved ? "Approve" : "Pending"}}</a>
										</td>
										<td>
											<a class="text-red pull-right margin-r-5" ng-click="delRiwayatDiklat(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
										</td>
									</tr>
									<tr>
										<td colspan="8" align="center">
											<a class="badge bg-green margin-r-5" ng-click="approveRiwayatDiklat()">Approve Selected</a>
											<a class="badge bg-red" ng-click="unapproveRiwayatDiklat()">Unapprove Selected</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>

		<div class="modal fade create-diklat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<form action="#" method="POST" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Tambah Riwayat Diklat</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nama</label>
										<input type="text" class="form-control" ng-model="newData.nama" name="nama" required>
									</div>
									<div class="form-group">
										<label>Jenis Diklat</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="newData.id_diklat" required>
											<option ng-repeat="a in master_diklat" value="@{{ a.id }}" ng-cloak>@{{ a.nama }} ( @{{ a.jenis }} )</option>
										</select>
									</div>
									<div class="form-group">
										<label>Tempat</label>
										<input type="text" class="form-control" ng-model="newData.tempat" name="tempat" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tanggal pelaksanaan</label>
										<input type="text" class="datepicker form-control" ng-model="dateFilter" name="tgl_lahir" required>
									</div>
									<div class="form-group">
										<label>Pelaksana</label>
										<input type="text" class="form-control" ng-model="newData.pelaksana" name="alamat_sekarang" required>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							<a class="btn btn-primary" ng-click="saveRiwayatDiklat()" data-dismiss="modal">Simpan</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>

	@endsection
	@section('script')
	<script src="{{ asset('app/controllers/diklat_pegawai.js') }}" type="text/javascript"></script>
	@endsection
