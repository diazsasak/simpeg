@extends('layout.layout')
@section('title')
Edit Akun - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>Edit Akun</h1>
			<form action="{{ url('akun/'.$akun->id) }}" method="POST">
				{{ method_field('PUT') }}
				{{ csrf_field() }}
				<div class="form-group">
					<label for="exampleInputEmail1">Nama</label>
					<input type="text" class="form-control" value="{{ $akun->name }}" name="name" id="exampleInputEmail1" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Username</label>
					<input type="text" class="form-control" value="{{ $akun->username }}" name="username" id="exampleInputEmail1" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Level User</label>
					<select class="form-control" name="user_level" required>
						@if($akun->user_level == 'super_admin')
						<option value="super_admin" selected>Super Admin</option>
						@else
						<option value="super_admin">Super Admin</option>
						@endif
						@if($akun->user_level == 'kaban')
						<option value="kaban" selected>Kaban</option>
						@else
						<option value="kaban">Kaban</option>
						@endif
						@if($akun->user_level == 'kabid')
						<option value="kabid" selected>Kabid</option>
						@else
						<option value="kabid">Kabid</option>
						@endif
						@if($akun->user_level == 'kasubid')
						<option value="kasubid" selected>Kasubid</option>
						@else
						<option value="kasubid">Kasubid</option>
						@endif
						@if($akun->user_level == 'staff')
						<option value="staff" selected>Staff / Operator</option>
						@else
						<option value="staff">Staff / Operator</option>
						@endif
					</select>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Bidang</label>
					<select class="form-control" name="user_group" required>
						@if($akun->user_group == 'Bidang Data dan Informasi')
						<option value="Bidang Data dan Informasi" selected>Bidang Data dan Informasi</option>
						@else
						<option value="Bidang Data dan Informasi">Bidang Data dan Informasi</option>
						@endif
						@if($akun->user_group == 'Bidang Mutasi')
						<option value="Bidang Mutasi" selected>Bidang Mutasi</option>
						@else
						<option value="Bidang Mutasi">Bidang Mutasi</option>
						@endif
						@if($akun->user_group == 'Bidang Disiplin')
						<option value="Bidang Disiplin" selected>Bidang Disiplin</option>
						@else
						<option value="Bidang Disiplin">Bidang Disiplin</option>
						@endif
						@if($akun->user_group == 'Bidang Pengembangan Sumber Daya Manusia')
						<option value="Bidang Pengembangan Sumber Daya Manusia" selected>Bidang Pengembangan Sumber Daya Manusia</option>
						@else
						<option value="Bidang Pengembangan Sumber Daya Manusia">Bidang Pengembangan Sumber Daya Manusia</option>
						@endif
					</select>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Data Pegawai</label>
					<select class="form-control select2" name="id_pegawai" required>
						<option value="-">-</option>
						@foreach($pegawai as $a)
						<option value="{{$a->id}}" <?php if($a->id == $akun->id_pegawai){ echo "selected"; } ?>>{{$a->nama}}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Password baru</label>
					<input type="password" class="form-control" name="password" id="exampleInputPassword1">
				</div>
				<div class="form-group">
					<label for="exampleInputPassword1">Konfirmasi password baru</label>
					<input type="password" class="form-control" name="password_confirmation" id="exampleInputPassword1">
				</div>
				<button type="submit" class="btn btn-default">Simpan</button>
			</form>
		</div>
	</div>
</div>
@endsection
