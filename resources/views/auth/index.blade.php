@extends('layout.layout')
@section('title')
Daftar Akun - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-user"></i>
		Akun
		<small>Manajemen Akun</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Akun</li>
	</ol>
</section>
<section class="content" ng-controller="AkunCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					@if(Auth::user()->user_level == 'super_admin')
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-modal">Tambah Akun</button>
					@endif
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr class="info">
							<!-- <th>Nama</th> -->
							<th>Username</th>
							<th>Level user</th>
							<th>Bidang</th>
							<th>Pegawai</th>
							<th colspan="2">&nbsp;</th>
						</tr>
						@foreach($akun as $a)
						<tr>
							<td>{{ $a->username }}</td>
							<td>{{ $a->user_level }}</td>
							<td>
								{{ $a->getInstansiName() }}
							</td>
							<td>
								@if($a->id_pegawai)
								{{ $a->pegawai->nama }}
								@else
								Tidak
								@endif
							</td>
							<td>
								<a href="#" type="button" data-toggle="modal" data-target=".edit{{$a->id}}" ng-click="loadSelectedPegawai()"><i class="fa fa-pencil fa-lg"></i></a>
								<input type="hidden" id="id-pegawai" value="{{ $a->id_pegawai }}">
								<!-- Modal -->
								<div class="modal fade edit{{$a->id}}" role="dialog">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<form action="{{ url('akun/'.$a->id) }}" method="POST" name="myForm">
												{{ method_field('PUT') }}
												{{ csrf_field() }}
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Edit Akun</h4>
												</div>
												<div class="modal-body">
													<div class="row">
														@if(Auth::user()->user_level == 'super_admin')
														<div class="col-lg-6">
															<div class="form-group">
																<label>Unit Organisasi</label><br>
																<select style="width: 100%" class="select2 form-control master-satuan-kerja-induk-select" name="user_group">
																	<option value="-" selected disabled>-- Pilih Unit Kerja --</option>
																	@foreach($masterSatuanKerjaInduk as $b)
																	<option value="{{ $b->id }}"> {{ $b->nama }}
																	</option>
																	@endforeach
																</select>
																<span class="help-block">Kosongkan jika tidak akan diubah</span>
															</div>
															<div class="form-group">
																<label>Anda PNS.? Sinkron di sini</label><br>
																<select style="width: 100%" class="select2 form-control" name="id_pegawai">
																	<option ng-repeat="a in pegawai" ng-value="a.id">@{{a.nama}}</option>
																</select>
																<span class="help-block">Kosongkan jika tidak akan diubah</span>
															</div>
															<div class="form-group">
																<label>Level User</label><br>
																<select class="form-control select2" style="width: 100%" name="user_level" required>
																	@if($a->user_level == 'super_admin')
																	<option value="super_admin" selected>Super Admin</option>
																	@else
																	<option value="super_admin">Super Admin</option>
																	@endif
																	@if($a->user_level == 'kaban')
																	<option value="kaban" selected>Kaban</option>
																	@else
																	<option value="kaban">Kaban</option>
																	@endif
																	@if($a->user_level == 'kabid')
																	<option value="kabid" selected>Kabid</option>
																	@else
																	<option value="kabid">Kabid</option>
																	@endif
																	@if($a->user_level == 'kasubbid')
																	<option value="kasubbid" selected>Kasubbid</option>
																	@else
																	<option value="kasubbid">Kasubbid</option>
																	@endif
																	@if($a->user_level == 'staff')
																	<option value="staff" selected>Staff / Operator</option>
																	@else
																	<option value="staff">Staff / Operator</option>
																	@endif
																</select>
															</div>
														</div>
														<div class="col-lg-6">
															@else
															<div class="col-lg-12">
																@endif
																<div class="form-group">
																	<label>Username</label>
																	<input type="text" class="form-control" value="{{ $a->username }}" name="username" required>
																</div>
																<div class="form-group">
																	<label>Password baru</label>
																	<input type="password" class="form-control" name="password">
																</div>
																<div class="form-group">
																	<label>Konfirmasi password baru</label>
																	<input type="password" class="form-control" name="password_confirmation">
																</div>
															</div>
														</div>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary">Simpan</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</td>
								@if(Auth::user()->user_level == 'super_admin')
								<td>
									@if($a->deleted_at == null)
									<a href="#" class="text-red pull-right margin-r-5" type="button" data-toggle="modal" data-target=".delete{{$a->id}}"><i class="fa fa-trash fa-lg"></i></a>
									<!-- Modal -->
									<div class="modal fade delete{{$a->id}}" role="dialog">
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Hapus</h4>
												</div>
												<div class="modal-body">
													<p>Apakah anda ingin menghapus data ini?</p>
												</div>
												<div class="modal-footer">
													<form method="POST" action="akun/{{$a->id}}">
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<button type="submit" class="btn btn-danger">Hapus</button>
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
													</form>
												</div>
											</div>
										</div>
									</div>
									@else
									<a class="pull-right margin-r-5" href="{{ url('akun/'.$a->id.'/restore') }}"><i class="fa fa-undo fa-lg"></i></a>
									@endif
								</td>
								@endif
							</tr>
							@endforeach
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<form action="{{ url('akun') }}" method="POST">
						{{ csrf_field() }}
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Tambah Akun</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Unit Organiasi</label><br>
										<select style="width: 100%" class="select2 form-control master-satuan-kerja-induk-select" name="user_group" required>
                                                                                                                                                                                                                                                                                                                                                                                    <option>-- Pilih Unit Organisasi --</option>
											<option ng-repeat="a in masterSatuanKerjaInduk" value="@{{a.id}}">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Anda PNS.?, Sinkron disini</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_pegawai">
											<option ng-repeat="a in pegawai" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Level User</label><br>
										<select class="form-control select2" style="width: 100%" name="user_level" required>
											<option value="super_admin">Super Admin</option>
											<option value="kaban">Kaban</option>
											<option value="kabid">Kabid</option>
											<option value="kasubbid">Kasubid</option>
											<option value="staff">Staff / Operator</option>
										</select>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Username</label>
										<input type="text" class="form-control" name="username" required>
									</div>
									<div class="form-group">
										<label>Password</label>
										<input type="password" class="form-control" name="password" required>
									</div>
									<div class="form-group">
										<label>Konfirmasi Password</label>
										<input type="password" class="form-control" name="password_confirmation" required>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	@endsection
	@section('script')
	<script src="{{ asset('app/controllers/akun.js') }}" type="text/javascript"></script>
	@endsection