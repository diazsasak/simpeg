@extends('layout.login')
@section('title')
Login - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<div class="row login-bkpsdm">
      <div class="col-md-8">
      </div>
      <div class="col-md-4 box-login">
            <div class="header-logo">
                  <img src="{{asset('images/logo.png')}}" class="logo-custom">
                  <span class="logo-text"><strong>e</strong>Kepegawaian</span>
            </div>
            <p>
                  {{-- pesan error --}}
                  @if(count($errors) > 0)
                  <section class="row">
                        <div class="col-sm-12 col-md-12">
                              <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <ul>
                                          @foreach($errors->all() as $error)
                                          <li>{!! $error !!}</li>
                                          @endforeach
                                    </ul>
                              </div>
                        </div>
                  </section>
                  @endif
                  {{-- pesan sukses --}}
                  @if(Session::has('success'))
                  <section class="row">
                        <div class="col-sm-12 col-md-12">
                              <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {{ Session::pull('success') }}
                              </div>
                        </div>
                  </section>
                  @endif
                  {{-- pesan gagal --}}
                  @if(Session::has('fail'))
                  <section class="row">
                        <div class="col-sm-12 col-md-12">
                              <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    {!! Session::pull('fail') !!}
                              </div>
                        </div>
                  </section>
                  @endif
                  Silahkan Login
            </p>
            <br>
            <form action="{{ url('login') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="username" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                        <label>
                              <input type="checkbox" name="remember_me"> Remember Me
                        </label>
                  </div>
                  <br>
                  <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4">
                              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="ion ion-log-in"></i> Log In</button>
                        </div>
                        <!-- /.col -->
                  </div>
            </form>
            <!-- /.login-box-body -->
            <div class="footer-custom">
                  &COPY; 2017 Pemerintah Kabupaten Lombok Timur
            </div>
      </div>
</div>
@endsection
