@extends('layout.layout')
@section('title')
Detail Pegawai - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<link href="{{ asset('simpeg/fullscreen_imagepreview.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.table-borderless tbody tr td,
.table-borderless tbody tr th,
.table-borderless thead tr th,
.table-borderless thead tr td,
.table-borderless tfoot tr th,
.table-borderless tfoot tr td  {
	border: none;
}
.editable-radiolist label {
	display: block;
}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-user"></i>
		Detail
		<small>Manajemen Pegawai</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href=""><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="{{ url('pegawai') }}">Pegawai</a></li>
		<li class="active">Detail</li>
	</ol>
</section>
<section class="content" ng-controller="DetailPegawaiCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#detail-pegawai" data-toggle="tab">Detail Pegawai</a></li>
					<li><a href="#data-tambahan-pegawai" data-toggle="tab">Data Tambahan Pegawai</a></li>
					<li ng-if="!pegawai.is_approved" class="pull-right"><a href="" class="text-green" ng-click="editPegawai(pegawai)"><i class="fa fa-pencil margin-r-5"></i> Edit Pegawai</a></li>
					<li ng-if="pegawai.is_approved" class="pull-right"><a href="{{ url('pegawai/'.$id.'/print') }}" target="_blank" class="text-green"><i class="fa fa-print margin-r-5"></i> Cetak Pegawai</a></li>
				</ul>
				<div class="tab-content no-padding">
					<div class="tab-pane active" id="detail-pegawai">
						<table class="table table-borderless" ng-cloak>
							<tbody>
								<tr>
									<td>NIP</td>
									<td>@{{ pegawai.nip }}</td>
									<td rowspan="6" align="center" valign="middle" style="vertical-align: middle"><img ng-src="/@{{pegawai.poto}}" style="border-radius: 10px; height: 100%; width: auto;"></td>
								</tr>
								<tr>
									<td>Nama</td>
									<td>
										@{{ pegawai.gelar_depan }}
										@{{ pegawai.nama }}
										@{{ pegawai.gelar_belakang }}
									</td>
								</tr>
								<tr>
									<td>Jenis Kelamin</td>
									<td>@{{ showJk() }}</td>
								</tr>
								<tr>
									<td>Tempat, Tanggal Lahir</td>
									<td>
										@{{ pegawai.tempat_lahir }},&nbsp;
										@{{ pegawai.tgl_lahir | date:"dd-MM-yyyy" }}
									</td>
								</tr>
								<tr ng-if="showPangkat">
									<td>Pangkat</td>
									<td>@{{ showPangkat.master_pangkat.nama_pangkat }}, @{{ showPangkat.master_pangkat.nama_golongan }}</td>
								</tr>
								<tr ng-if="showPangkat">
									<td>TMT Pangkat</td>
									<td>@{{ showPangkat.tmt_pangkat }}</td>
								</tr>
								<tr ng-if="showJabatan">
									<td>Jabatan</td>
									<td>@{{ showJabatan.master_jabatan.nama }}</td>
								</tr>
								<tr ng-if="showJabatan">
									<td>TMT Jabatan</td>
									<td>@{{ showJabatan.tanggal_jabatan }}</td>
								</tr>
								
								
								<tr ng-if="showSubUnitOrganisasi">
									<td>Unit Kerja</td>
									<td>@{{ showSubUnitOrganisasi.nama }}</td>
								</tr>

								<tr ng-if="showUnitOrganisasi">
									<td>Sub Organisasi</td>
									<td>@{{ showUnitOrganisasi.nama }}</td>
								</tr>
								<tr ng-if="showSatuanKerjaInduk">
									<td>Unit Organisasi</td>
									<td>@{{ showSatuanKerjaInduk.nama }}</td>
								</tr>

								<tr ng-if="showInstansiInduk">
									<td>Instansi Induk</td>
									<td>@{{ showInstansiInduk.nama }}</td>
								</tr>
								<tr ng-if="showPendidikan">
									<td>Pendidikan Terakhir</td>
									<td>@{{ showPendidikan.nama }}</td>
								</tr>
								<tr>
									<td>Agama</td>
									<td>@{{ pegawai.master_agama.nama }}</td>
								</tr>
								<tr>
									<td>Status Perkawinan</td>
									<td>@{{ pegawai.status_perkawinan }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Alamat Sekarang</td>
									<td>@{{ pegawai.alamat_sekarang }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Kecamatan, Kabupaten/Kota, Provinsi</td>
									<td>@{{ pegawai.kecamatan.nama+', '+pegawai.kabupaten.nama+', '+pegawai.provinsi.nama}}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Alamat Lengkap Asal</td>
									<td>@{{ pegawai.asal_alamat_lengkap }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>No Telepon</td>
									<td>@{{ pegawai.nomor_telepon }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>@{{pegawai.namaJenisDokumen}}</td>
									<td>@{{ pegawai.no_dokumen }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Jenis Pegawai</td>
									<td>@{{pegawai.namaJenisPegawai}}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Status Kepegawaian</td>
									<td>@{{pegawai.namaStatusPegawai}}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>TMT PNS</td>
									<td>@{{ pegawai.tanggal_pns | date:"dd-MM-yyyy" }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>TMT CPNS</td>
									<td>@{{ pegawai.tanggal_cpns | date:"dd-MM-yyyy" }}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Tahun Pengangkatan / TMT CPNS</td>
									<td>@{{ pegawai.tahun_pengangkatan}}</td>
								</tr>
								<tr ng-show="loadDetail.id">
									<td>Email</td>
									<td>@{{ pegawai.email }}</td>
								</tr>
								<tr>
									<td colspan="3" align="center"><a href="" ng-click="loadMore()">
										<span ng-if="!loadDetail.id"><i class="fa fa-arrow-circle-down"></i></span>
										<span ng-if="loadDetail.id"><i class="fa fa-arrow-circle-up"></i></span></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.tab-pane -->
					<div class="tab-pane" id="data-tambahan-pegawai">
						<div class="row">
							<div class="col-xs-12">
								<br>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Jumlah Istri</label>
										<input type="text" class="form-control" ng-value="jumlahIstri" disabled>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Jumlah Anak</label>
										<input type="text" class="form-control" ng-value="jumlahAnak" disabled>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Nomor Surat Keterangan Sehat Dokter</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_surat_ket_sehat_dokter">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Tanggal Surat Keterangan Sehat Dokter</label>
										<input type="text" class="datepicker form-control" ng-model="pegawai.data_tambahan_pegawai.tanggal_surat_ket_sehat_dokter">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Nomor Surat Keterangan Bebas Narkoba</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_surat_ket_bebas_narkoba">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Tanggal Surat Keterangan Bebas Narkoba</label>
										<input type="text" class="datepicker form-control" ng-model="pegawai.data_tambahan_pegawai.tanggal_surat_ket_bebas_narkoba">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Nomor Surat Keterangan Catatan Polisi</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_surat_ket_catatan_polisi">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Tanggal Surat Keterangan Catatan Polisi</label>
										<input type="text" class="datepicker form-control" ng-model="pegawai.data_tambahan_pegawai.tanggal_surat_ket_catatan_polisi">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Nomor NPWP</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_npwp">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Tanggal NPWP</label>
										<input type="text" class="datepicker form-control" ng-model="pegawai.data_tambahan_pegawai.tanggal_npwp">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Akta Kelahiran</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.akte_kelahiran">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>No TASPEN</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_taspen">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>No BPJS/ASKES</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_bpjs">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>NO KARIS/KARSU</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.no_karis">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="checkbox">
										<label>
											<input type="checkbox" ng-model="pegawai.data_tambahan_pegawai.is_hidup"> Masih Hidup
										</label>
									</div>
								</div>
								<div class="col-xs-6" ng-show="!pegawai.data_tambahan_pegawai.is_hidup">
									<div class="form-group">
										<label>Akte Meninggal</label>
										<input type="text" class="form-control" ng-model="pegawai.data_tambahan_pegawai.akte_meninggal">
									</div>
								</div>
								<div class="col-xs-6" ng-show="!pegawai.data_tambahan_pegawai.is_hidup">
									<div class="form-group">
										<label>Tanggal Meninggal</label>
										<input type="text" class="datepicker form-control" ng-model="pegawai.data_tambahan_pegawai.tanggal_meninggal">
									</div>
								</div>
								<div class="col-xs-12" style="margin-bottom: 10px">
									<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="updateDataTambahan(pegawai.data_tambahan_pegawai)">Simpan</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			</div>
			<!-- nav-tabs-custom -->

			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse" title="minimize">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="maximize" title="full screen">
							<i class="fa fa-expand"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" title="close">
								<i class="fa fa-times"></i></button>
							</div>
						</div>
						<div class="box-body">
							<div class="nav-tabs-custom">
								<!-- Tabs within a box -->
								<ul class="nav nav-tabs">
									<li class="active"><a href="#riwayat-pangkat" data-toggle="tab">Pangkat</a></li>
									<li><a href="#riwayat-jabatan" data-toggle="tab">Jabatan</a></li>
									<li><a href="#pendidikan" data-toggle="tab">Pendidikan</a></li>
									<li><a href="#diklat" data-toggle="tab">Diklat</a></li>
									<li><a href="#anggota-keluarga" data-toggle="tab">Anggota Keluarga</a></li>
									<li><a href="#riwayat-cuti" data-toggle="tab">Cuti</a></li>
									<li><a href="#riwayat-absensi" data-toggle="tab">Absensi</a></li>
									<li><a href="#riwayat-skp" data-toggle="tab">SKP</a></li>
									<li><a href="#riwayat-hukuman" data-toggle="tab">Hukuman</a></li>
									<li><a href="#pensiun" data-toggle="tab">Pensiun</a></li>
									<li><a href="#harta-pejabat" data-toggle="tab">Harta Pejabat</a></li>
								</ul>
								<div class="tab-content no-padding">
									@include('pegawai.detail.riwayat_pangkat')
									@include('pegawai.detail.riwayat_jabatan')
									@include('pegawai.detail.pendidikan')
									@include('pegawai.detail.diklat')
									@include('pegawai.detail.anggota_keluarga')
									@include('pegawai.detail.riwayat_cuti')
									@include('pegawai.detail.riwayat_absensi')
									@include('pegawai.detail.riwayat_skp')
									@include('pegawai.detail.riwayat_hukuman')
									@include('pegawai.detail.pensiun')
									@include('pegawai.detail.harta_pejabat')
								</div>
							</div>
							<!-- nav-tabs-custom -->
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>

			<div class="modal fade edit-pkk-modal" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<form action="#" method="POST" enctype="multipart/form-data">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Edit Pegawai</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-xs-4">
										<div class="form-group">
											<label>Asal Provinsi *</label><br>
											<select style="width: 100%" class="select2 form-control" name="id_provinsi" ng-model="pegawai.id_provinsi" ng-change="loadKabupaten(pegawai.id_provinsi)" required>
												<option ng-repeat="a in provinsi" ng-value="a.id_provinsi">@{{a.nama}}</option>
											</select>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="form-group">
											<label>Asal Kabupaten/Kota *</label><br>
											<select style="width: 100%" class="select2 form-control"  name="id_kabupaten" ng-model="pegawai.id_kabupaten" ng-change="loadKecamatan(pegawai.id_kabupaten)" ng-disabled="!kabupaten" required>
												<option ng-repeat="a in kabupaten" ng-value="a.id_kabupaten">@{{a.nama}}</option>
											</select>
										</div>
									</div>
									<div class="col-xs-4">
										<div class="form-group">
											<label>Asal Kecamatan *</label><br>
											<select style="width: 100%" class="select2 form-control" name="id_kecamatan" ng-model="pegawai.id_kecamatan" ng-change="buttonEdit.pkk=true" ng-disabled="!kecamatan" required>
												<option ng-repeat="a in kecamatan" ng-value="a.id_kecamatan">@{{a.nama}}</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
								<a class="btn btn-primary" ng-click="updatePegawai(pegawai)" ng-disabled="!buttonEdit.pkk" data-dismiss="modal">Simpan</a>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal fade edit-tp-modal" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog modal-sm" role="document">
					<div class="modal-content">
						<form action="#" method="POST" enctype="multipart/form-data">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Edit Pegawai</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
											<label>Tahun Pengangkatan *</label>
											<input type="text" class="yearpicker form-control" name="tahun_pengangkatan" ng-model="pegawai.tahun_pengangkatan" required>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
								<a class="btn btn-primary" ng-click="updatePegawai(pegawai)" data-dismiss="modal">Simpan</a>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="modal fade edit-poto-modal" role="dialog" aria-labelledby="myLargeModalLabel">
				<form action="{{ url('pegawai') }}/<?php echo $id; ?>" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Edit Pegawai</h4>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group">
											<label>Upload Foto Pegawai</label>
											<div class="input-group image-preview">
												<input type="text" class="form-control image-preview-filename" disabled="disabled" ng-model="poto"> <!-- don't give a name === doesn't send on POST/GET -->
												<span class="input-group-btn">
													<!-- image-preview-clear button -->
													<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
														<span class="glyphicon glyphicon-remove"></span> Clear
													</button>
													<!-- image-preview-input -->
													<div class="btn btn-default image-preview-input">
														<span class="glyphicon glyphicon-folder-open"></span>
														<span class="image-preview-input-title">Browse</span>
														<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="inputPegawai.poto"/> <!-- rename it -->
													</div>
												</span>
											</div>
											<span class="help-block"><i class="fa fa-exclamation-circle margin-r-5"></i>Kosongkan apabila ingin menghapus foto.</span>
										</div>
										<center><img ng-src="/@{{pegawai.poto}}" style="border-radius: 10px; height: 100%; width: auto;"></center>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
								<button type="submit" class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div modal class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
				<div modal-dialog class="modal-dialog modal-lg" role="document">
					<div modal-content class="modal-content">
						<form name="xx" ng-submit="savePegawai()" method="POST" enctype="multipart/form-data">
							<div modal-header class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title">Edit Pegawai</h4>
							</div>
							<div modal-body class="modal-body">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group" ng-class="validView(xx.nip)">
											<label>NIP *</label>
											<input type="text" class="form-control" name="nip" ng-model="inputPegawai.nip" ui-mask="99999999 999999 9 999" required>
											<div class="error-container" ng-show="isError(xx.nip)" ng-messages="xx.nip.$error">
												<div ng-messages-include="error-list.html"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-3">
												<div class="form-group">
													<label>Gelar Depan</label>
													<input type="text" class="form-control" maxlength="10" ng-model="inputPegawai.gelar_depan">
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.nama)">
													<label>Nama *</label>
													<input type="text" class="form-control" name="nama" ng-model="inputPegawai.nama" ng-minlength="3" ng-maxlength="50" required>
													<div class="error-container" ng-show="isError(xx.nama)" ng-messages="xx.nama.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-3">
												<div class="form-group">
													<label>Gelar Belakang</label>
													<input type="text" class="form-control" ng-model="inputPegawai.gelar_belakang">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-click="xx.id_agama.$touched=true" ng-class="validView(xx.id_agama)">
													<label>Agama *</label><br>
													<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="inputPegawai.id_agama" required>
														<option ng-repeat="a in masterAgama" ng-value="a.id">@{{a.nama}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.id_agama)" ng-messages="xx.id_agama.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group">
													<label>Jenis Kelamin *</label><br>
													<div class="radio-inline">
														<input type="radio" name="jns_kelamin" ng-model="inputPegawai.jns_kelamin" value="1" checked>
														Laki-laki
													</div>
													<div class="radio-inline">
														<input type="radio" name="jns_kelamin" ng-model="inputPegawai.jns_kelamin" value="0">
														Perempuan
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.tempat_lahir)">
													<label>Tempat Lahir *</label>
													<input type="text" class="form-control" name="tempat_lahir" ng-model="inputPegawai.tempat_lahir" ng-minlength="2" ng-maxlength="50" required>
													<div class="error-container" ng-show="isError(xx.tempat_lahir)" ng-messages="xx.tempat_lahir.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.tgl_lahir)">
													<label>Tanggal Lahir *</label>
													<input type="text" class="datepicker form-control" name="tgl_lahir" ng-model="inputPegawai.tgl_lahir" required>
													<div class="error-container" ng-show="isError(xx.tgl_lahir)" ng-messages="xx.tgl_lahir.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group" ng-class="validView(xx.alamat_sekarang)">
											<label>Alamat Sekarang *</label>
											<input type="text" class="form-control" name="alamat_sekarang" ng-model="inputPegawai.alamat_sekarang" ng-minlength="5" ng-maxlength="100" required>
											<div class="error-container" ng-show="isError(xx.alamat_sekarang)" ng-messages="xx.alamat_sekarang.$error">
												<div ng-messages-include="error-list.html"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<div class="form-group" ng-click="xx.id_provinsi.$touched=true" ng-class="validView(xx.id_provinsi)">
													<label>Asal Provinsi *</label><br>
													<select style="width: 100%" class="select2 form-control" name="id_provinsi" ng-model="inputPegawai.id_provinsi" ng-change="loadKabupaten(inputPegawai.id_provinsi)" required>
														<option ng-repeat="a in provinsi" ng-value="a.id_provinsi">@{{a.nama}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.id_provinsi)" ng-messages="xx.id_provinsi.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="form-group" ng-click="xx.id_kabupaten.$touched=true" ng-class="validView(xx.id_kabupaten)">
													<label>Asal Kabupaten/Kota *</label><br>
													<select style="width: 100%" class="select2 form-control" name="id_kabupaten" ng-model="inputPegawai.id_kabupaten" ng-change="loadKecamatan(inputPegawai.id_kabupaten)" ng-disabled="!kabupaten" required>
														<option ng-repeat="a in kabupaten" ng-value="a.id_kabupaten">@{{a.nama}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.id_kabupaten)" ng-messages="xx.id_kabupaten.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-4">
												<div class="form-group" ng-click="xx.id_kecamatan.$touched=true" ng-class="validView(xx.id_kecamatan)">
													<label>Asal Kecamatan *</label><br>
													<select style="width: 100%" class="select2 form-control" name="id_kecamatan" ng-model="inputPegawai.id_kecamatan" ng-disabled="!kecamatan" required>
														<option ng-repeat="a in kecamatan" ng-value="a.id_kecamatan">@{{a.nama}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.id_kecamatan)" ng-messages="xx.id_kecamatan.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group" ng-class="validView(xx.asal_alamat_lengkap)">
											<label>Alamat Lengkap Asal *</label>
											<input type="text" class="form-control" name="asal_alamat_lengkap" ng-model="inputPegawai.asal_alamat_lengkap" required>
											<div class="error-container" ng-show="isError(xx.asal_alamat_lengkap)" ng-messages="xx.asal_alamat_lengkap.$error">
												<div ng-messages-include="error-list.html"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.nomor_telepon)">
													<label>No Telp *</label>
													<input type="text" class="form-control" name="nomor_telepon" ng-model="inputPegawai.nomor_telepon" ng-pattern="/^[0-9]+$/" ng-minlength="6" ng-maxlength="12" required>
													<div class="error-container" ng-show="isError(xx.nomor_telepon)" ng-messages="xx.nomor_telepon.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.nomor_hp)">
													<label>No HP *</label>
													<input type="text" class="form-control" name="nomor_hp" ng-model="inputPegawai.nomor_hp" ng-pattern="/^[0-9]+$/" ng-minlength="10" ng-maxlength="12" required>
													<div class="error-container" ng-show="isError(xx.nomor_hp)" ng-messages="xx.nomor_hp.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validViewSelect(xx.jenis_dokumen)">
													<label>Jenis Dokumen *</label>
													<select class="form-control" name="jenis_dokumen" ng-model="inputPegawai.jenis_dokumen" required>
														<option ng-value="1">KTP</option>
														<option ng-value="2">SIM</option>
														<option ng-value="3">Passport</option>
													</select>
													<div class="error-container" ng-show="xx.jenis_dokumen.$error && xx.jenis_dokumen.$touched" ng-messages="xx.jenis_dokumen.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.no_dokumen)">
													<label>Nomor Dokumen *</label>
													<input type="text" class="form-control" name="no_dokumen" ng-model="inputPegawai.no_dokumen" ng-maxlength="50" required>
													<div class="error-container" ng-show="isError(xx.no_dokumen.$error)" ng-messages="xx.no_dokumen.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validViewSelect(xx.jenis_pegawai)">
													<label>Jenis Pegawai *</label>
													<select class="form-control" name="jenis_pegawai" ng-model="inputPegawai.jenis_pegawai" required>
														<option ng-value="1">PNS Daerah Kab. Lombok Timur</option>
													</select>
													<div class="error-container" ng-show="xx.jenis_pegawai.$error && xx.jenis_pegawai.$touched" ng-messages="xx.jenis_pegawai.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-click="xx.id_status_kepegawaian.$touched=true" ng-class="validView(xx.id_status_kepegawaian)">
													<label>Status Kepegawaian *</label><br>
													<select style="width: 100%" class="select2 form-control" name="id_status_kepegawaian" ng-model="inputPegawai.id_status_kepegawaian" required>
														<option ng-repeat="a in masterStatusKepegawaian" ng-value="a.id">@{{a.nama}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.id_status_kepegawaian)" ng-messages="xx.id_status_kepegawaian.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.sk_pns)">
													<label>SK PNS *</label>
													<input type="text" class="form-control" name="sk_pns" ng-model="inputPegawai.sk_pns" ng-maxlength="50" required>
													<div class="error-container" ng-show="isError(xx.sk_pns)" ng-messages="xx.sk_pns.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.tanggal_pns)">
													<label>Tanggal PNS *</label>
													<input type="text" class="datepicker form-control" name="tanggal_pns" ng-model="inputPegawai.tanggal_pns" required>
													<div class="error-container" ng-show="isError(xx.tanggal_pns)" ng-messages="xx.tanggal_pns.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.sk_cpns)">
													<label>SK CPNS *</label>
													<input type="text" class="form-control" name="sk_cpns" ng-model="inputPegawai.sk_cpns" ng-maxlength="50" required>
													<div class="error-container" ng-show="isError(xx.sk_cpns)" ng-messages="xx.sk_cpns.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.tanggal_cpns)">
													<label>Tanggal CPNS *</label>
													<input type="text" class="datepicker form-control" name="tanggal_cpns" ng-model="inputPegawai.tanggal_cpns" required>
													<div class="error-container" ng-show="isError(xx.tanggal_cpns)" ng-messages="xx.tanggal_cpns.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group" ng-class="validView(xx.tahun_pengangkatan)">
													<label>Tahun Pengangkatan *</label>
													<input type="text" class="yearpicker form-control" name="tahun_pengangkatan" ng-model="inputPegawai.tahun_pengangkatan" required>
													<div class="error-container" ng-show="isError(xx.tahun_pengangkatan)" ng-messages="xx.tahun_pengangkatan.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group">
													<label>Email *</label>
													<input type="email" class="form-control" name="email" ng-model="inputPegawai.email" ng-minlength="3" ng-maxlength="50">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-xs-6">
												<div class="form-group">
													<label>NUPTK</label>
													<input type="text" class="form-control" name="nuptk" ng-model="inputPegawai.nuptk" ng-maxlength="50">
												</div>
											</div>
											<div class="col-xs-6">
												<div class="form-group" ng-click="xx.status_perkawinan.$touched=true" ng-class="validView(xx.status_perkawinan)">
													<label>Status Perkawinan *</label>
													<select class="form-control" name="status_perkawinan" ng-model="inputPegawai.status_perkawinan" required>
														<option ng-repeat="a in status_perkawinan" ng-value="a">@{{a}}</option>
													</select>
													<div class="error-container" ng-show="isError(xx.status_perkawinan)" ng-messages="xx.status_perkawinan.$error">
														<div ng-messages-include="error-list.html"></div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label>Upload Foto Pegawai</label>
											<div class="input-group image-preview">
												<input type="text" class="form-control image-preview-filename" disabled="disabled" ng-model="poto"> <!-- don't give a name === doesn't send on POST/GET -->
												<span class="input-group-btn">
													<!-- image-preview-clear button -->
													<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
														<span class="glyphicon glyphicon-remove"></span> Clear
													</button>
													<!-- image-preview-input -->
													<div class="btn btn-default image-preview-input">
														<span class="glyphicon glyphicon-folder-open"></span>
														<span class="image-preview-input-title">Browse</span>
														<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="inputPegawai.poto"/> <!-- rename it -->
													</div>
												</span>
											</div>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="hapus_foto" ng-model="inputPegawai.hapus_foto"> Hapus Foto ?
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="is_atasan" ng-model="inputPegawai.is_atasan"> Atasan *
											</label>
										</div>
										* Wajib Diisi
									</div>
								</div>
							</div>
							<div modal-footer class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
								<button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="savePegawai(xx)" ng-disabled="xx.$invalid">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>


		@endsection
		@section('script')
		<script>
			var id = <?php echo $id; ?>;
			var user_level = '{{ Auth::user()->user_level }}';
			var user_group = '{{ Auth::user()->user_group }}';
		</script>
		<script src="{{ asset('app/controllers/detail_pegawai.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/pensiun.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_skp.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_pangkat.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_hukuman.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/diklat.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/anggota_keluarga.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_jabatan.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_cuti.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/riwayat_absensi.js') }}" type="text/javascript"></script>
		<script src="{{ asset('app/controllers/harta_pejabat.js') }}" type="text/javascript"></script>
		<script src="{{ asset('simpeg/imagepreview.js') }}" type="text/javascript"></script>
		<script id="error-list.html" type="text/ng-template">
			<span ng-message="required" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Harus Diisi.</span>
			<span ng-message="minlength" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Terlalu Pendek.</span>
			<span ng-message="maxlength" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Terlalu Panjang.</span>
			<span ng-message="email" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Format Email Tidak Valid.</span>
			<span ng-message="pattern" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Tidak Valid.</span>
		</script>
		@endsection
