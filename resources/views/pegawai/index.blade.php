@extends('layout.layout')
@section('title')
Pegawai - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<link href="{{ asset('simpeg/fullscreen_imagepreview.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
/*filter*/
#filter {
	margin: 0px
}
</style>
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-users"></i>
		Pegawai
		<small>Manajemen Pegawai</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Pegawai</li>
	</ol>
</section>
<section class="content" ng-controller="PegawaiCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					<div class="row">
						<!-- <div class="col-sm-2">
							<h3 class="box-title">Filter</h3>
						</div> -->
						<div class="col-sm-2">
							<div class="form-group" style="margin: 0px">
								<select style="width: 100%" class="select2 form-control" id="filter-pegawai">
									<option selected value="all">Semua Status Pegawai</option>
									<option ng-repeat="a in status_kepegawaian" ng-value="a.id">@{{a.nama}}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group" style="margin: 0px">
								<select style="width: 100%" class="select2 form-control" id="filter-eselon">
									<option selected value="all">Semua Eselon</option>
									<option ng-repeat="a in eselon" ng-value="a.id">@{{a.nama}}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group" style="margin: 0px">
								<select style="width: 100%" class="select2 form-control" id="filter-jk">
									<option selected value="all">Semua Jenis Kelamin</option>
									<option ng-repeat="a in jk" ng-value="a.id">@{{a.nama}}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group" style="margin: 0px">
								<input type="text" class="yearpicker form-control" id="filter-tahun-angkatan" placeholder="Semua Tahun Angkatan" required>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group" style="margin: 0px">
								<select style="width: 100%" class="select2 form-control" id="filter-pendidikan">
									<option selected value="all">Semua Pendidikan</option>
									<option ng-repeat="a in pendidikan" ng-value="a.id">@{{a.nama}}</option>
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<button class="btn btn-success btn-block" ng-click="doFilter()"><i class="fa fa-filter margin-r-5"></i>Filter</button>
						</div>
					</div>
				</div>
				<!-- /.box-header -->
			</div>
			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Tabel Pegawai</h3>
					<div class="box-tools pull-right">
						<a href="#" target="_blank" class="btn btn-box-tool text-green" ng-click="doExport()"><i class="fa fa-file-excel-o margin-r-5"></i> Export Excel</a>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tbody><tr>
							<th>#</th>
							<th>NIP</th>
							<th>Nama</th>
							<th><i class="fa fa-venus-mars" aria-hidden="true"></i> Jenis Kelamin</th>
							<th>Eselon</th>
							<th>Tahun Angkatan</th>
							<th><i class="fa fa-graduation-cap" aria-hidden="true"></i> Pendidikan</th>
							<th>Status</th>
							<th>
								@if(Auth::user()->user_level == 'super_admin')
								<a class="pull-right" data-toggle="modal" data-target=".create-modal" href="#"><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah Pegawai" ng-click="loadAll()"></i></a></th>
								@endif
								<th>&nbsp;</th>
							</tr>
							<tr ng-repeat="a in pegawai.data">
								<td>
									<input type="checkbox" name="selectedPegawai" value="@{{ a.id }}" ng-model="selectedPegawai[a.id]" id="@{{a.id}}">
								</td>
								<td ng-cloak>@{{ a.nip }}</td>
								<td ng-cloak>@{{ a.nama }}</td>
								<td ng-cloak>@{{ a.jns_kelamin ? "Laki-laki" : "Perempuan" }}</td>
								<td ng-cloak>@{{ a.nama_eselon }}</td>
								<td ng-cloak>@{{ a.tahun_pengangkatan }}</td>
								<td ng-cloak>@{{ a.riwayat_pendidikan[0].nama }}</td>
								<td>
									<!-- if user group BKD or superadmin -->
									@if(((Auth::user()->user_group != null) && (Auth::user()->masterUnitSkpd->masterSatuanKerjaInduk->id == 1)) || (Auth::user()->user_level == 'super_admin'))
									<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="#" ng-click="unapproved(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{is_approved ? "Unapproved" : "Approved"}}</a>
									@else
									<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
									@endif
									<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="#" ng-click="approved(a.id)" ng-hide="a.deleted_at" ng-cloak>@{{is_approved ? "Approve" : "Pending"}}</a>
								</td>
								<td>
									<a class="pull-right" href="@{{'/pegawai/'+a.id}}" data-toggle="tooltip" data-placement="bottom" title="Detail" ng-hide="a.deleted_at"><i class="fa fa-id-card-o fa-lg"></i></a>
								</td>
								<td>
									@if(((Auth::user()->user_group != null) && (Auth::user()->masterUnitSkpd->masterSatuanKerjaInduk->id == 1)) || (Auth::user()->user_level == 'super_admin'))
									<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delPegawai(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
									@endif
									<a class="text-green pull-right margin-r-5" ng-click="resPegawai(a.id)" href="#" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a>
								</td>
							</tr>
							<tr>
								<td colspan="10">
									<button ng-if="pegawai.data.length" type="button" class="btn btn-success" ng-click="approve()">Approve</button>
									@if(((Auth::user()->user_group != null) && (Auth::user()->masterUnitSkpd->masterSatuanKerjaInduk->id == 1)) || (Auth::user()->user_level == 'super_admin'))
									<button ng-if="pegawai.data.length" type="button" class="btn btn-danger" ng-click="unapprove()">Unpprove</button>
									@endif
									<div class="btn-group pull-right">
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(1)"  ng-disabled="pegawai.current_page==1" data-toggle="tooltip" data-placement="top" title="First Page"><i class="fa fa-angle-double-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(pegawai.prev_page_url)"  ng-disabled="pegawai.prev_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Previous"><i class="fa fa-angle-left fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="paginate(pegawai.next_page_url)" ng-disabled="pegawai.next_page_url==undefined" data-toggle="tooltip" data-placement="top" title="Next"><i class="fa fa-angle-right fa-lg"></i></button>
										<button type="button" class="btn btn-primary" ng-click="reloadPaginate(pegawai.last_page)"  ng-disabled="pegawai.current_page==pegawai.last_page||pegawai.last_page==0" data-toggle="tooltip" data-placement="top" title="Last Page"><i class="fa fa-angle-double-right fa-lg"></i></button>
									</div>
								</td>
							</tr>
						</tbody></table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
		@include('pegawai.form')
	</section>
	@endsection
	@section('script')
	<script type="text/javascript">
		var baseUrl = '{{ url('/') }}';
	</script>
	<script src="{{ asset('app/controllers/pegawai.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
	//image preview
	$(document).on('click', '#close-preview', function(){
		$('.image-preview').popover('hide');
	    // Hover befor close the preview
	    $('.image-preview').hover(
	    	function () {
	    		$('.image-preview').popover('show');
	    	},
	    	function () {
	    		$('.image-preview').popover('hide');
	    	}
	    	);
	});
	$(function() {
	    // Create the close button
	    var closebtn = $('<button/>', {
	    	type:"button",
	    	text: 'x',
	    	id: 'close-preview',
	    	style: 'font-size: initial;',
	    });
	    closebtn.attr("class","close pull-right");
	    // Set the popover default content
	    $('.image-preview').popover({
	    	trigger:'manual',
	    	html:true,
	    	title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
	    	content: "There's no image",
	    	placement:'bottom'
	    });
	    // Clear event
	    $('.image-preview-clear').click(function(){
	    	$('.image-preview').attr("data-content","").popover('hide');
	    	$('.image-preview-filename').val("");
	    	$('.image-preview-clear').hide();
	    	$('.image-preview-input input:file').val("");
	    	$(".image-preview-input-title").text("Browse");
	    });
	    // Create the preview image
	    $(".image-preview-input input:file").change(function (){
	    	var img = $('<img/>', {
	    		id: 'dynamic',
	    		width:200,
	    		height:null
	    	});
	    	var file = this.files[0];
	    	var reader = new FileReader();
	        // Set preview image into the popover data-content
	        reader.onload = function (e) {
	        	$(".image-preview-input-title").text("Change");
	        	$(".image-preview-clear").show();
	        	$(".image-preview-filename").val(file.name);
	        	img.attr('src', e.target.result);
	        	$(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
	        }
	        reader.readAsDataURL(file);
	    });
	});
</script>
<script id="error-list.html" type="text/ng-template">
	<span ng-message="required" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Harus Diisi.</span>
	<span ng-message="minlength" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Terlalu Pendek.</span>
	<span ng-message="maxlength" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Terlalu Panjang.</span>
	<span ng-message="email" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Format Email Tidak Valid.</span>
	<span ng-message="pattern" class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>Tidak Valid.</span>
</script>
@endsection
