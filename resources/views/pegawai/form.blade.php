<div modal class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel" id="create-modal">
	<div modal-dialog class="modal-dialog modal-lg" role="document">
		<div modal-content class="modal-content">
			<form name="xx" ng-submit="savePegawai(xx)" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div modal-header class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Pegawai</h4>
				</div>
				<div modal-body class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group" ng-class="validView(xx.nip)">
								<label>NIP *</label>
								<input type="text" class="form-control" name="nip" ng-model="inputPegawai.nip" ui-mask="99999999 999999 9 999" required>
								<div class="error-container" ng-show="isError(xx.nip)" ng-messages="xx.nip.$error" ng-cloak>
									<div ng-messages-include="error-list.html"></div>
								</div>
								<div class="error-container" ng-show="nipError" ng-cloak>
									<span class="help-block text-red"><i class="fa fa-exclamation-circle margin-r-5"></i>@{{ nipError }}</span>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-3">
									<div class="form-group">
										<label>Gelar Depan</label>
										<input type="text" class="form-control" maxlength="10" ng-model="inputPegawai.gelar_depan">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.nama)">
										<label>Nama *</label>
										<input type="text" class="form-control" name="nama" ng-model="inputPegawai.nama" ng-minlength="3" ng-maxlength="50" required>
										<div class="error-container" ng-show="isError(xx.nama)" ng-messages="xx.nama.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group">
										<label>Gelar Belakang</label>
										<input type="text" class="form-control" ng-model="inputPegawai.gelar_belakang">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-click="xx.id_agama.$touched=true" ng-class="validView(xx.id_agama)">
										<label>Agama *</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="inputPegawai.id_agama" required>
											<option ng-repeat="a in agama" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.id_agama)" ng-messages="xx.id_agama.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Jenis Kelamin *</label><br>
										<div class="radio-inline">
											<input type="radio" name="jns_kelamin" ng-model="inputPegawai.jns_kelamin" value="1" checked>
											Laki-laki
										</div>
										<div class="radio-inline">
											<input type="radio" name="jns_kelamin" ng-model="inputPegawai.jns_kelamin" value="0">
											Perempuan
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.tempat_lahir)">
										<label>Tempat Lahir *</label>
										<input type="text" class="form-control" name="tempat_lahir" ng-model="inputPegawai.tempat_lahir" ng-minlength="2" ng-maxlength="50" required>
										<div class="error-container" ng-show="isError(xx.tempat_lahir)" ng-messages="xx.tempat_lahir.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.tgl_lahir)">
										<label>Tanggal Lahir *</label>
										<input type="text" class="datepicker form-control" name="tgl_lahir" ng-model="inputPegawai.tgl_lahir" required>
										<div class="error-container" ng-show="isError(xx.tgl_lahir)" ng-messages="xx.tgl_lahir.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" ng-class="validView(xx.alamat_sekarang)">
								<label>Alamat Sekarang *</label>
								<input type="text" class="form-control" name="alamat_sekarang" ng-model="inputPegawai.alamat_sekarang" ng-minlength="5" ng-maxlength="100" required>
								<div class="error-container" ng-show="isError(xx.alamat_sekarang)" ng-messages="xx.alamat_sekarang.$error" ng-cloak>
									<div ng-messages-include="error-list.html"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-4">
									<div class="form-group" ng-click="xx.id_provinsi.$touched=true" ng-class="validView(xx.id_provinsi)">
										<label>Asal Provinsi *</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_provinsi" ng-model="inputPegawai.id_provinsi" ng-change="loadKabupaten(inputPegawai.id_provinsi)" required>
											<option ng-repeat="a in provinsi" ng-value="a.id_provinsi">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.id_provinsi)" ng-messages="xx.id_provinsi.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group" ng-click="xx.id_kabupaten.$touched=true" ng-class="validView(xx.id_kabupaten)">
										<label>Asal Kabupaten/Kota *</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_kabupaten" ng-model="inputPegawai.id_kabupaten" ng-change="loadKecamatan(inputPegawai.id_kabupaten)" ng-disabled="!kabupaten" required>
											<option ng-repeat="a in kabupaten" ng-value="a.id_kabupaten">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.id_kabupaten)" ng-messages="xx.id_kabupaten.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-4">
									<div class="form-group" ng-click="xx.id_kecamatan.$touched=true" ng-class="validView(xx.id_kecamatan)">
										<label>Asal Kecamatan *</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_kecamatan" ng-model="inputPegawai.id_kecamatan" ng-disabled="!kecamatan" required>
											<option ng-repeat="a in kecamatan" ng-value="a.id_kecamatan">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.id_kecamatan)" ng-messages="xx.id_kecamatan.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group" ng-class="validView(xx.asal_alamat_lengkap)">
								<label>Alamat Lengkap Asal *</label>
								<input type="text" class="form-control" name="asal_alamat_lengkap" ng-model="inputPegawai.asal_alamat_lengkap" required>
								<div class="error-container" ng-show="isError(xx.asal_alamat_lengkap)" ng-messages="xx.asal_alamat_lengkap.$error" ng-cloak>
									<div ng-messages-include="error-list.html"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.nomor_hp)">
										<label>No HP *</label>
										<input type="text" class="form-control" name="nomor_hp" ng-model="inputPegawai.nomor_hp" ng-pattern="/^[0-9]+$/" ng-minlength="10" ng-maxlength="12" required>
										<div class="error-container" ng-show="isError(xx.nomor_hp)" ng-messages="xx.nomor_hp.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.nomor_telepon)">
										<label>No Telp *</label>
										<input type="text" class="form-control" name="nomor_telepon" ng-model="inputPegawai.nomor_telepon" ng-pattern="/^[0-9]+$/" ng-minlength="6" ng-maxlength="12" required>
										<div class="error-container" ng-show="isError(xx.nomor_telepon)" ng-messages="xx.nomor_telepon.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
									<span>*Jika tidak ada No.Telp, Copy dari No.Hp</span>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validViewSelect(xx.jenis_dokumen)">
										<label>Jenis Dokumen *</label>
										<select class="form-control" name="jenis_dokumen" ng-model="inputPegawai.jenis_dokumen" required>
											<option ng-value="1">KTP</option>
											<option ng-value="2">SIM</option>
											<option ng-value="3">Passport</option>
										</select>
										<div class="error-container" ng-show="xx.jenis_dokumen.$error && xx.jenis_dokumen.$touched" ng-messages="xx.jenis_dokumen.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.no_dokumen)">
										<label>Nomor Dokumen *</label>
										<input type="text" class="form-control" name="no_dokumen" ng-model="inputPegawai.no_dokumen" ng-maxlength="50" required>
										<div class="error-container" ng-show="isError(xx.no_dokumen.$error)" ng-messages="xx.no_dokumen.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validViewSelect(xx.jenis_pegawai)">
										<label>Jenis Pegawai *</label>
										<select class="form-control" name="jenis_pegawai" ng-model="inputPegawai.jenis_pegawai" required>
											<option ng-value="1">PNS Daerah Kab. Lombok Timur</option>
											<option ng-value="2">CPNS Daerah Kab. Lombok Timur</option>
										</select>
										<div class="error-container" ng-show="xx.jenis_pegawai.$error && xx.jenis_pegawai.$touched" ng-messages="xx.jenis_pegawai.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-click="xx.id_status_kepegawaian.$touched=true" ng-class="validView(xx.id_status_kepegawaian)">
										<label>Status Kepegawaian *</label><br>
										<select style="width: 100%" class="select2 form-control" name="id_status_kepegawaian" ng-model="inputPegawai.id_status_kepegawaian" required>
											<option ng-repeat="a in masterStatusKepegawaian" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.id_status_kepegawaian)" ng-messages="xx.id_status_kepegawaian.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<!--<div class="form-group" ng-class="validView(xx.sk_pns)">-->
                                                                                                                                                                                                                                                                                                                  <div class="form-group">
										<label>SK PNS</label>
										<input type="text" class="form-control" name="sk_pns" ng-model="inputPegawai.sk_pns" ng-maxlength="50">
                                                                                                                                                                                                                                                                                                                                                  <!--
										<div class="error-container" ng-show="isError(xx.sk_pns)" ng-messages="xx.sk_pns.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
                                                                                                                                                                                                                                                                                                                                                  -->
									</div>
								</div>
								<div class="col-xs-6">
									<!--<div class="form-group" ng-class="validView(xx.tanggal_pns)">-->
                                                                                                                                                                                                                                                                                                                  <div class="form-group">
										<label>Tanggal PNS</label>
										<input type="text" class="datepicker form-control" name="tanggal_pns" ng-model="inputPegawai.tanggal_pns">
										<!--<div class="error-container" ng-show="isError(xx.tanggal_pns)" ng-messages="xx.tanggal_pns.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>-->
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.sk_cpns)">
										<label>SK CPNS *</label>
										<input type="text" class="form-control" name="sk_cpns" ng-model="inputPegawai.sk_cpns" ng-maxlength="50">
										<div class="error-container" ng-show="isError(xx.sk_cpns)" ng-messages="xx.sk_cpns.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.tanggal_cpns)">
										<label>Tanggal CPNS *</label>
										<input type="text" class="datepicker form-control" name="tanggal_cpns" ng-model="inputPegawai.tanggal_cpns" required>
										<div class="error-container" ng-show="isError(xx.tanggal_cpns)" ng-messages="xx.tanggal_cpns.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group" ng-class="validView(xx.tahun_pengangkatan)">
										<label>Tahun Pengangkatan/Tahun CPNS *</label>
										<input type="text" class="yearpicker form-control" name="tahun_pengangkatan" ng-model="inputPegawai.tahun_pengangkatan" required>
										<div class="error-container" ng-show="isError(xx.tahun_pengangkatan)" ng-messages="xx.tahun_pengangkatan.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<label>Email</label>
										<input type="email" class="form-control" name="email" ng-model="inputPegawai.email" ng-minlength="3" ng-maxlength="50">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6">
									<div class="form-group">
										<label>NUPTK</label>
										<input type="text" class="form-control" name="nuptk" ng-model="inputPegawai.nuptk" ng-maxlength="50">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group" ng-click="xx.status_perkawinan.$touched=true" ng-class="validView(xx.status_perkawinan)">
										<label>Status Perkawinan *</label>
										<select class="form-control" name="status_perkawinan" ng-model="inputPegawai.status_perkawinan" required>
											<option ng-repeat="a in status_perkawinan" ng-value="a">@{{a}}</option>
										</select>
										<div class="error-container" ng-show="isError(xx.status_perkawinan)" ng-messages="xx.status_perkawinan.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label><i class="fa fa-camera" aria-hidden="true"></i> Upload Foto Pegawai</label>
								<div class="input-group image-preview">
									<input type="text" class="form-control image-preview-filename" disabled="disabled" ng-model="poto"> <!-- don't give a name === doesn't send on POST/GET -->
									<span class="input-group-btn">
										<!-- image-preview-clear button -->
										<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
											<span class="glyphicon glyphicon-remove"></span> Clear
										</button>
										<!-- image-preview-input -->
										<div class="btn btn-default image-preview-input">
											<span class="glyphicon glyphicon-folder-open"></span>
											<span class="image-preview-input-title">Browse</span>
											<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="inputPegawai.poto"/> <!-- rename it -->
										</div>
									</span>
								</div>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" ng-model="inputPegawai.is_atasan"> Atasan
								</label>
							</div>
							Form dengan tanda * (Bintang) Wajib Diisi
						</div>
					</div>
				</div>
				<div modal-footer class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="button" class="btn btn-primary" ng-click="savePegawai(xx)" ng-disabled="xx.$invalid">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
