@extends('layout.print_layout')
@section('title')
Print Pegawai - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<link href="{{ asset('simpeg/fullscreen_imagepreview.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
.table-borderless tbody tr td,
.table-borderless tbody tr th,
.table-borderless thead tr th,
.table-borderless thead tr td,
.table-borderless tfoot tr th,
.table-borderless tfoot tr td  {
	border: none;
}
.editable-radiolist label {
	display: block;
}
</style>
@endsection
@section('content')
<section class="content" ng-controller="DetailPegawaiCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div style="text-align: center; margin: 20px;">
				<h2><u>PROFIL PEGAWAI NEGERI SIPIL</u></h2>
			</div>
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Data Pribadi</h3>
				</div>
				<div class="box-body">
					<table class="table table-borderless" ng-cloak>
						<tr>
							<td>NIP</td>
							<td>@{{ pegawai.nip }}</td>
							<td rowspan="6" align="center" valign="middle" style="vertical-align: middle"><img ng-src="/@{{pegawai.poto}}" style="border-radius: 10px; height: 100%; width: auto;"></td>
						</tr>
						<tr>
							<td>Nama</td>
							<td>
								@{{ pegawai.gelar_depan }}
								@{{ pegawai.nama }}
								@{{ pegawai.gelar_belakang }} 
							</td>
						</tr>

						<tr ng-if="subUnitOrganisasi">
							<td>Unit Kerja</td>
							<td>@{{ subUnitOrganisasi.nama }}</td>
						</tr>

						<tr ng-if="unitOrganisasi">
							<td>Sub Organisasi</td>
							<td>@{{ unitOrganisasi.nama }}</td>
						</tr>
						<tr ng-if="satuanKerjaInduk">
							<td>Unit Organisasi</td>
							<td>@{{ satuanKerjaInduk.nama }}</td>
						</tr>

						<tr ng-if="instansiInduk">
							<td>Instansi Induk</td>
							<td>@{{ instansiInduk.nama }}</td>
						</tr>

						<tr>
							<td>Jenis Kelamin</td>
							<td>@{{ showJk() }}</td>
						</tr>
						<tr>
							<td>Tempat, Tanggal Lahir</td>
							<td>
								@{{ pegawai.tempat_lahir }},&nbsp;
								@{{ pegawai.tgl_lahir | date:"yyyy-MM-dd" }}
							</td>
						</tr>
						<tr>
							<td>Agama</td>
							<td>@{{ pegawai.master_agama.nama }}</td>
						</tr>
						<tr>
							<td>Status Perkawinan</td>
							<td>@{{ pegawai.status_perkawinan }}</td>
						</tr>
						<tr>
							<td>Alamat Sekarang</td>
							<td>@{{ pegawai.alamat_sekarang }}</td>
						</tr>
						<tr>
							<td>Kecamatan, Kabupaten/Kota, Provinsi</td>
							<td>@{{ pegawai.kecamatan.nama+', '+pegawai.kabupaten.nama+', '+pegawai.provinsi.nama}}</td>
						</tr>
						<tr>
							<td>Alamat Lengkap Asal</td>
							<td>@{{ pegawai.asal_alamat_lengkap }}</td>
						</tr>
						<tr>
							<td>No Telepon</td>
							<td>@{{ pegawai.nomor_telepon }}</td>
						</tr>
						<tr>
							<td>@{{pegawai.namaJenisDokumen}}</td>
							<td>@{{ pegawai.no_dokumen }}</td>
						</tr>
						<tr>
							<td>Jenis Pegawai</td>
							<td>@{{pegawai.namaJenisPegawai}}</td>
						</tr>
						<tr>
							<td>Status Kepegawaian</td>
							<td>@{{pegawai.namaStatusPegawai}}</td>
						</tr>
						<tr>
							<td>Tanggal PNS</td>
							<td>@{{ pegawai.tanggal_pns | date:"yyyy-MM-dd" }}</td>
						</tr>
						<tr>
							<td>Tanggal CPNS</td>
							<td>@{{ pegawai.tanggal_cpns | date:"yyyy-MM-dd" }}</td>
						</tr>
						<tr>
							<td>Tahun Pengangkatan</td>
							<td>@{{ pegawai.tahun_pengangkatan}}</td>
						</tr>
						<tr>
							<td>Email</td>
							<td>@{{ pegawai.email }}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Golongan</h3>
				</div>
				<div class="box-body">	
					<table class="table table-hover" ng-cloak>
						<thead>
							<tr>
								<th>Pangkat</th>
								<th>TMT Pangkat</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="a in pegawai.riwayat_pangkat">
								<td>@{{ a.master_pangkat.nama_golongan }} @{{ a.master_pangkat.nama_pangkat }}</td>
								<td>@{{ a.tmt_pangkat }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Jabatan</h3>
				</div>
				<div class="box-body">
					<table class="table table-hover" ng-cloak>
						<thead>
							<tr>
								<th>Jabatan</th>
								<th>Tanggal SK</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="a in pegawai.riwayat_jabatan">
								<td>@{{ a.master_jabatan.nama }}</td>
								<td>@{{ a.tanggal_sk }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Diklat Pimpinan</h3>
				</div>
				<div class="box-body">	
					<table class="table table-hover" ng-cloak>
						<thead>
							<tr>
								<th>Nama</th>
								<th>Tanggal pelaksanaan</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="a in pegawai.riwayat_diklat">
								<td>@{{ a.nama }}</td>
								<td>@{{ a.tanggal_pelaksanaan }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Pendidikan</h3>
				</div>
				<div class="box-body">	
					<table class="table table-hover" ng-cloak>
						<thead>
							<tr>
								<th>Pendidikan</th>
								<th>Institusi</th>
								<th>Tahun Lulus</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="a in pegawai.riwayat_pendidikan">
								<td>@{{ a.nama }}</td>
								<td>@{{ a.pivot.nama_institusi }}</td>
								<td>@{{ a.pivot.tahun_lulus }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-6" ng-controller="AnggotaKeluargaCtrl">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Riwayat Keterangan Keluarga</h3>
				</div>
				<div class="box-body">			
					<table class="table table-hover" ng-cloak>
						<thead>
							<tr>
								<th>Status</th>
								<th>Nama</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="a in anggotaKeluarga">
								<td>
									&nbsp;
								</td>
								<td>@{{ a.master_keluarga.status }}</td>
								<td>@{{ a.nama }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('script')
<script>
	var id = <?php echo $id; ?>;
	var user_level = '{{ Auth::user()->user_level }}';
	var user_group = '{{ Auth::user()->user_group }}';
</script>
<script src="{{ asset('app/controllers/detail_pegawai.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/pensiun.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_skp.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_pangkat.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_hukuman.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/diklat.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/anggota_keluarga.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_jabatan.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_cuti.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/riwayat_absensi.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/harta_pejabat.js') }}" type="text/javascript"></script>
<script src="{{ asset('simpeg/imagepreview.js') }}" type="text/javascript"></script>
@endsection
