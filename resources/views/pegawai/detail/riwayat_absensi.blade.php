<div class="chart tab-pane" id="riwayat-absensi" ng-controller="RiwayatAbsensiCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Hadir</th>
					<th>Bolos</th>
					<th>Izin</th>
					<th>Telat</th>
					<th>Sakit</th>
					<th>Cuti</th>
					<th>Cuti eksternal</th>
					<th>File izin dokter</th>
					<th>Tanggal Rekap</th>
					<th>Bulan Rekap</th>
					<th>Tahun Rekap</th>
					<th>Status</th>
					<th colspan="3"><a class="pull-right margin-r-5" ng-click="createRiwayatAbsensi()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayat_absensi">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.hadir }}</td>
					<td>@{{ a.bolos }}</td>
					<td>@{{ a.izin }}</td>
					<td>@{{ a.telat }}</td>
					<td>@{{ a.sakit }}</td>
					<td>@{{ a.cuti }}</td>
					<td>@{{ a.cuti_eksternal }}</td>
					<td>
						<a href="{{ url('/') }}/@{{ a.file_izin_dokter }}" target="_blank">File</a>
					</td>
					<td>@{{ a.tanggal }}</td>
					<td>@{{ a.bulan }}</td>
					<td>@{{ a.tahun }}</td>
					<td style="width: 50px;">
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatAbsensi(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatAbsensi(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a class="pull-right margin-r-5" ng-click="editRiwayatAbsensi(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatAbsensi(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						<a ng-if="!a.is_approved" class="text-green pull-right margin-r-5" ng-click="resRiwayatAbsensi(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a>
					</td>
				</tr>
				<tr>
					<td colspan="12">
						<a ng-if="riwayat_absensi.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatAbsensi()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayat_absensi.length" class="badge bg-red" ng-click="unapproveRiwayatAbsensi()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-riwayat-absensi-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah riwayat absensi bulanan</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Hadir</label>
									<input type="number" ng-model="newData.hadir" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Bolos</label>
									<input type="number" ng-model="newData.bolos" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Izin</label>
									<input type="number" ng-model="newData.izin" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Telat</label>
									<input type="number" ng-model="newData.telat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Sakit</label>
									<input type="number" ng-model="newData.sakit" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Cuti</label>
									<input type="number" ng-model="newData.cuti" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Cuti eksternal</label>
									<input type="number" ng-model="newData.cuti_eksternal" class="form-control" required>
								</div>
								<div class="form-group">
									<label>File izin dokter</label>
									<input type="file" file-model="newData.file_izin_dokter" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.tanggal" required>
										<option ng-repeat="a in tanggalCollection" ng-value="a.tanggal">@{{a.tanggal}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Bulan Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.bulan" required>
										<option ng-repeat="a in bulanCollection" ng-value="a.bulan">@{{a.bulan}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Tahun Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.tahun" required>
										<option ng-repeat="a in tahunCollection" ng-value="a.tahun">@{{a.tahun}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatAbsensi()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-riwayat-absensi-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit riwayat absensi bulanan</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Hadir</label>
									<input type="number" ng-model="editData.hadir" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Bolos</label>
									<input type="number" ng-model="editData.bolos" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Izin</label>
									<input type="number" ng-model="editData.izin" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Telat</label>
									<input type="number" ng-model="editData.telat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Sakit</label>
									<input type="number" ng-model="editData.sakit" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Cuti</label>
									<input type="number" ng-model="editData.cuti" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Cuti eksternal</label>
									<input type="number" ng-model="editData.cuti_eksternal" class="form-control" required>
								</div>
								<div class="form-group">
									<label>File izin dokter</label>
									<input type="file" file-model="editData.file_izin_dokter" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.tanggal" required>
										<option ng-repeat="a in tanggalCollection" ng-value="a.tanggal" ng-selected="a.tanggal === editData.tanggal">@{{a.tanggal}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Bulan Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.bulan" required>
										<option ng-repeat="a in bulanCollection" ng-value="a.bulan" ng-selected="a.bulan === editData.bulan">@{{a.bulan}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Tahun Rekap</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.tahun" required>
										<option ng-repeat="a in tahunCollection" ng-value="a.tahun" ng-selected="a.tahun === editData.tahun">@{{a.tahun}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatAbsensi(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>