<div class="chart tab-pane" id="pensiun" ng-controller="PensiunCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Jenis</th>
					<th>SK</th>
					<th>Tanggal SK</th>
					<th>TMT Pensiun</th>
					<th>Status</th>
					<th><a class="pull-right margin-r-5" data-toggle="modal" data-target=".create-pensiun-modal" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in pensiun">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.jenis_pensiun }}</td>
					<td>@{{ a.sk_pensiun }}</td>
					<td>@{{ a.tanggal_sk_pensiun | date:"dd-MM-yyyy" }}</td>
					<td>@{{ a.tmt_pensiun | date:"dd-MM-yyyy" }}</td>
					<td>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedPensiun(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedPensiun(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editPensiun(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delPensiun(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						<!-- <a class="text-green pull-right margin-r-5" ng-click="resPensiun(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
					</td>
				</tr>
				<tr>
					<td colspan="11">
						<a ng-if="pensiun.length" class="badge bg-green margin-r-5" ng-click="approvePensiun()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="pensiun.length" class="badge bg-red" ng-click="unapprovePensiun()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-pensiun-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah pensiun</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Jenis pensiun</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.jenis_pensiun" required>
										<option ng-repeat="a in jenisPensiun" ng-value="a.jenis_pensiun">@{{a.jenis_pensiun}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK pensiun</label>
									<input type="text" ng-model="newData.sk_pensiun" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK pensiun</label>
									<input type="text" ng-model="newData.tanggal_sk_pensiun" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>TMT pensiun</label>
									<input type="text" ng-model="newData.tmt_pensiun" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="savePensiun()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-pensiun-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit pensiun</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Jenis pensiun</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.jenis_pensiun" required>
										<option ng-repeat="a in jenisPensiun" ng-value="a.jenis_pensiun" ng-selected="a.jenis_pensiun.valueOf() === editData.jenis_pensiun.valueOf()">@{{a.jenis_pensiun}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK pensiun</label>
									<input type="text" ng-model="editData.sk_pensiun" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK pensiun</label>
									<input type="text" ng-model="editData.tanggal_sk_pensiun" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>TMT pensiun</label>
									<input type="text" ng-model="editData.tmt_pensiun" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updatePensiun(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>