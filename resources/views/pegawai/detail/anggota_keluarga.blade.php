<div class="chart tab-pane" id="anggota-keluarga" ng-controller="AnggotaKeluargaCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Nama</th>
					<th>Jenis Kelamin</th>
					<th>Status</th>
					<th>Tanggal lahir</th>
					<th>Tempat lahir</th>
					<th>Pendidikan</th>
					<th>Status kawin</th>
					<th>Pekerjaan</th>
					<th>
						<a class="pull-right margin-r-5" data-toggle="modal" ng-click="createAnggotaKeluarga()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in anggotaKeluarga">
					<td>
						@{{$index+1}}
					</td>
					<td>@{{ a.nama }}</td>
					<td>@{{ a.jk }}</td>
					<td>@{{ a.master_keluarga.status }}</td>
					<td>@{{ a.tanggal_lahir }}</td>
					<td>@{{ a.tempat_lahir }}</td>
					<td>@{{ a.master_pendidikan.nama }}</td>
					<td>@{{ a.status_perkawinan }}</td>
					<td>@{{ a.master_pekerjaan.nama }}</td>
					<td>
						<a ng-click="editAnggotaKeluarga(a)" class="fa fa-pencil fa-lg"></a>
						@if(Auth::user()->user_level == 'super_admin' || Auth::user()->user_group == 1)
						<a class="text-red pull-right margin-r-5" ng-click="delAnggotaKeluarga(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						@endif
						<a class="text-green pull-right margin-r-5" ng-click="resAnggotaKeluarga(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-anggota-keluarga-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah anggota keluarga</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" ng-model="newData.nama" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Jenis kelamin</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.jk" required>
										<option ng-repeat="a in jk" value="@{{ a.nama }}">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status keluarga</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_status" required>
										<option ng-repeat="a in masterKeluarga" value="@{{ a.id }}">@{{ a.status }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pendidikan</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_pendidikan" required>
										<option ng-repeat="a in masterPendidikan" value="@{{ a.id }}">@{{ a.nama }}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status perkawinan</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.status_perkawinan" required>
										<option ng-repeat="a in statusPerkawinan" value="@{{ a.nama }}">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pekerjaan</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_pekerjaan" required>
										<option ng-repeat="a in masterPekerjaan" value="@{{ a.id }}">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Tempat lahir</label>
									<input type="text" ng-model="newData.tempat_lahir" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal lahir</label>
									<input type="text" ng-model="newData.tanggal_lahir" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveAnggotaKeluarga()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-anggota-keluarga-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Anggota Keluarga</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" ng-model="editData.nama" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Jenis kelamin</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.jk" required>
										<option ng-repeat="a in jk" ng-value="a.nama">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Status keluarga</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.id_status" required>
										<option ng-repeat="a in masterKeluarga" ng-value="a.id">@{{ a.status }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pendidikan</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.id_pendidikan" required>
										<option ng-repeat="a in masterPendidikan" ng-value="a.id">@{{ a.nama }}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status perkawinan</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.status_perkawinan" required>
										<option ng-repeat="a in statusPerkawinan" ng-value="a.nama">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Pekerjaan</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.id_pekerjaan" required>
										<option ng-repeat="a in masterPekerjaan" ng-value="a.id">@{{ a.nama }}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Tempat lahir</label>
									<input type="text" ng-model="editData.tempat_lahir" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal lahir</label>
									<input type="text" ng-model="editData.tanggal_lahir" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateAnggotaKeluarga(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>