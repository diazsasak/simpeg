<div class="chart tab-pane active" id="riwayat-pangkat" ng-controller="RiwayatPangkatCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Pangkat</th>
					<th>TMT Pangkat</th>
					<th>SK Pangkat</th>
					<th>Tanggal SK Pangkat</th>
					<th>Gaji Pokok</th>
					<th>Masa kerja tahun</th>
					<th>Masa kerja bulan</th>
					<th>Status</th>
					<th colspan="2"><a class="pull-right margin-r-5" ng-click="createRiwayatPangkat()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayat_pangkat">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.master_pangkat.nama_pangkat }}</td>
					<td>@{{ a.tmt_pangkat | date:"dd-MM-yyyy" }}</td>
					<td>@{{ a.sk_pangkat }}</td>
					<td>@{{ a.tanggal_sk_pangkat | date:"dd-MM-yyyy" }}</td>
					<td>@{{ a.gaji_pokok | currency : "Rp. " : 0 }}</td>
					<td>@{{ a.masa_kerja_tahun }}</td>
					<td>@{{ a.masa_kerja_bulan }}</td>
					<td style="width: 100px;">
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatPangkat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatPangkat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editRiwayatPangkat(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
					</td>
					<td>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatPangkat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						@endif
						<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatPangkat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
					</td>
				</tr>
				<tr>
					<td colspan="12">
						<a ng-if="riwayat_pangkat.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatPangkat()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayat_pangkat.length" class="badge bg-red" ng-click="unapproveRiwayatPangkat()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-riwayat-pangkat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Riwayat Pangkat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Pangkat</label><br>
									<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="newData.id_pangkat" required>
										<option ng-repeat="a in pangkat" ng-value="a.id">@{{a.nama_golongan}} @{{a.nama_pangkat}}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>TMT Pangkat</label>
									<input type="text" ng-model="newData.tmt_pangkat" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>SK Pangkat</label>
									<input type="text" ng-model="newData.sk_pangkat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK Pangkat</label>
									<input type="text" ng-model="newData.tanggal_sk_pangkat" class="datepicker form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Gaji Pokok</label>
									<input type="number" ng-model="newData.gaji_pokok" class="form-control" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Masa kerja tahun <span style="color: red;">*</span></label>
									<input type="number" ng-model="newData.masa_kerja_tahun" class="form-control" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Masa kerja bulan <span style="color: red;">*</span></label>
									<input type="number" ng-model="newData.masa_kerja_bulan" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatPangkat()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-riwayat-pangkat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Riwayat Pangkat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Pangkat</label><br>
									<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="editData.id_pangkat" required>
										<option ng-repeat="a in pangkat" ng-value="a.id">@{{a.nama_golongan}} @{{a.nama_pangkat}}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>TMT Pangkat</label>
									<input type="text" ng-model="editData.tmt_pangkat" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>SK Pangkat</label>
									<input type="text" ng-model="editData.sk_pangkat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK Pangkat</label>
									<input type="text" ng-model="editData.tanggal_sk_pangkat" class="datepicker form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Gaji Pokok</label>
									<input ng-model="editData.gaji_pokok" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Masa kerja tahun</label>
									<input type="number" ng-model="editData.masa_kerja_tahun" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Masa kerja bulan</label>
									<input type="number" ng-model="editData.masa_kerja_bulan" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatPangkat(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>