<div class="chart tab-pane" id="riwayat-hukuman" ng-controller="RiwayatHukumanCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Jenis hukuman</th>
					<th>SK hukuman</th>
					<th>Tanggal SK hukuman</th>
					<th>Masa hukuman (tahun)</th>
					<th>Masa hukuman (bulan)</th>
					<th>Tanggal hukuman berakhir</th>
					<th>No PP</th>
					<th>Alasan hukuman</th>
					<th>Keterangan</th>
					<th>Status</th>
					<th colspan="3"><a class="pull-right margin-r-5" ng-click="createRiwayatHukuman()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayat_hukuman">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.master_hukuman.jenis_hukuman }}</td>
					<td>@{{ a.sk_hukuman }}</td>
					<td>@{{ a.tanggal_sk_hukuman }}</td>
					<td>@{{ a.masa_hukuman_berlaku_tahun }}</td>
					<td>@{{ a.masa_hukuman_berlaku_bulan }}</td>
					<td>@{{ a.tanggal_hukuman_berakhir }}</td>
					<td>@{{ a.no_pp }}</td>
					<td>@{{ a.alasan_hukuman }}</td>
					<td>@{{ a.keterangan }}</td>
					<td style="width: 50px;">
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatHukuman(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatHukuman(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editRiwayatHukuman(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatHukuman(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatHukuman(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
					</td>
				</tr>
				<tr>
					<td colspan="12">
						<a ng-if="riwayat_hukuman.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatHukuman()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayat_hukuman.length" class="badge bg-red" ng-click="unapproveRiwayatHukuman()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-riwayat-hukuman-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Riwayat Hukuman</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Jenis hukuman</label><br>
									<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="newData.id_jenis_hukuman" required>
										<option ng-repeat="a in masterHukuman" ng-value="a.id">@{{a.jenis_hukuman}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK hukuman</label>
									<input type="text" ng-model="newData.sk_hukuman" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK hukuman</label>
									<input type="text" ng-model="newData.tanggal_sk_hukuman" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Masa hukuman (tahun)</label>
									<input type="number" ng-model="newData.masa_hukuman_berlaku_tahun" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Masa hukuman (bulan)</label>
									<input type="number" ng-model="newData.masa_hukuman_berlaku_bulan" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal hukuman berakhir</label>
									<input type="text" ng-model="newData.tanggal_hukuman_berakhir" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>No PP</label>
									<input type="text" ng-model="newData.no_pp" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Alasan hukuman</label>
									<input type="text" ng-model="newData.alasan_hukuman" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" ng-model="newData.keterangan" >									
									</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatHukuman()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-riwayat-hukuman-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit riwayat hukuman</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Jenis hukuman</label><br>
									<select style="width: 100%" class="select2 form-control" name="id_agama" ng-model="editData.id_jenis_hukuman" required>
										<option ng-repeat="a in masterHukuman" ng-value="a.id">@{{a.jenis_hukuman}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK hukuman</label>
									<input type="text" ng-model="editData.sk_hukuman" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK hukuman</label>
									<input type="text" ng-model="editData.tanggal_sk_hukuman" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Masa hukuman (tahun)</label>
									<input type="number" ng-model="editData.masa_hukuman_berlaku_tahun" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Masa hukuman (bulan)</label>
									<input type="number" ng-model="editData.masa_hukuman_berlaku_bulan" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal hukuman berakhir</label>
									<input type="text" ng-model="editData.tanggal_hukuman_berakhir" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>No PP</label>
									<input type="text" ng-model="editData.no_pp" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Alasan hukuman</label>
									<input type="text" ng-model="editData.alasan_hukuman" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" ng-model="editData.keterangan" >									
									</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatHukuman(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>