<div class="chart tab-pane" id="diklat" ng-controller="DiklatCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Nama</th>
					<th>SK</th>
					<th>Sertifikat</th>
					<th>No sertifikat</th>
					<th>Jenis</th>
					<th>Tempat</th>
					<th>Tanggal pelaksanaan</th>
					<th>Pelaksana</th>
					<th>Status</th>
					<th><a class="pull-right margin-r-5" data-toggle="modal" ng-click="createRiwayatDiklat()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayatDiklat">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.nama }}</td>
					<td>@{{ a.sk_diklat }}</td>
					<td><a href="{{ url('/') }}/@{{ a.sertifikat }}" target="_blank">File</a></td>
					<td>@{{ a.no_sertifikat }}</td>
					<td>@{{ a.master_diklat.nama }} ( @{{ a.master_diklat.jenis }} )</td>
					<td>@{{ a.tempat }}</td>
					<td>@{{ a.tanggal_pelaksanaan }}</td>
					<td>@{{ a.pelaksana }}</td>
					<td>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatDiklat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatDiklat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a ng-if="!a.is_approved" href="#" ng-click="editRiwayatDiklat(a)" class="fa fa-pencil fa-lg"></a>
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatDiklat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatDiklat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
					</td>
				</tr>
				<tr>
					<td colspan="11">
						<a ng-if="riwayatDiklat.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatDiklat()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayatDiklat.length" class="badge bg-red" ng-click="unapproveRiwayatDiklat()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-diklat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Riwayat Diklat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" class="form-control" ng-model="newData.nama" name="nama" required>
								</div>
								<div class="form-group">
									<label>Jenis Diklat</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_diklat" required>
										<option ng-repeat="a in masterDiklat" value="@{{ a.id }}">@{{ a.nama }} ( @{{ a.jenis }} )</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK</label>
									<input type="text" ng-model="newData.sk_diklat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tempat</label>
									<input type="text" class="form-control" ng-model="newData.tempat" name="tempat" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal pelaksanaan</label>
									<input type="text" class="datepicker form-control" ng-model="newData.tanggal_pelaksanaan" required>
								</div>
								<div class="form-group">
									<label>Pelaksana</label>
									<input type="text" class="form-control" ng-model="newData.pelaksana" name="alamat_sekarang" required>
								</div>
								<div class="form-group">
									<label>No sertifikat</label>
									<input type="text" class="form-control" ng-model="newData.no_sertifikat" name="alamat_sekarang" required>
								</div>
								<div class="form-group">
									<label>Upload sertifikat</label>
									<div class="input-group image-preview">
										<input type="text" class="form-control image-preview-filename" disabled="disabled" ng-model="poto"> <!-- don't give a name === doesn't send on POST/GET -->
										<span class="input-group-btn">
											<!-- image-preview-clear button -->
											<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
												<span class="glyphicon glyphicon-remove"></span> Clear
											</button>
											<!-- image-preview-input -->
											<div class="btn btn-default image-preview-input">
												<span class="glyphicon glyphicon-folder-open"></span>
												<span class="image-preview-input-title">Browse</span>
												<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="newData.sertifikat"/> <!-- rename it -->
											</div>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatDiklat()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-diklat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Riwayat Diklat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nama</label>
									<input type="text" class="form-control" ng-model="editData.nama" name="nama" required>
								</div>
								<div class="form-group">
									<label>Jenis Diklat</label><br>
									<select style="width: 100%" class="form-control" ng-model="editData.id_diklat" required>
										<option ng-repeat="a in masterDiklat" ng-value="a.id" ng-selected="editData.id_diklat == a.id">@{{ a.nama }} ( @{{ a.jenis }} )</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK</label>
									<input type="text" ng-model="editData.sk_diklat" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tempat</label>
									<input type="text" class="form-control" ng-model="editData.tempat" name="tempat" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tanggal pelaksanaan</label>
									<input type="text" class="datepicker form-control" ng-model="editData.tanggal_pelaksanaan" required>
								</div>
								<div class="form-group">
									<label>Pelaksana</label>
									<input type="text" class="form-control" ng-model="editData.pelaksana" name="alamat_sekarang" required>
								</div>
								<div class="form-group">
									<label>No sertifikat</label>
									<input type="text" class="form-control" ng-model="editData.no_sertifikat" name="alamat_sekarang" required>
								</div>
								<div class="form-group">
									<label>Upload sertifikat ( kosongkan jika tidak berubah )</label>
									<input type="checkbox" ng-model="editData.hapus_foto"> Hapus file
									<div class="input-group image-preview">
										<input type="text" class="form-control image-preview-filename" disabled="disabled" ng-model="poto"> <!-- don't give a name === doesn't send on POST/GET -->
										<span class="input-group-btn">
											<!-- image-preview-clear button -->
											<button type="button" class="btn btn-default image-preview-clear" style="display:none;">
												<span class="glyphicon glyphicon-remove"></span> Clear
											</button>
											<!-- image-preview-input -->
											<div class="btn btn-default image-preview-input">
												<span class="glyphicon glyphicon-folder-open"></span>
												<span class="image-preview-input-title">Browse</span>
												<input type="file" accept="image/png, image/jpeg, image/gif" name="poto" file-model="editData.sertifikat"/> <!-- rename it -->
											</div>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatDiklat(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>