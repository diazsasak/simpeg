<div class="chart tab-pane" id="riwayat-skp" ng-controller="RiwayatSkpCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Tahun SKP</th>
					<th>Nilai SKP</th>
					<th>Orientasi Pelayanan</th>
					<th>Komitmen</th>
					<th>Kerjasama</th>
					<th>Nilai Perilaku Kerja</th>
					<th>Nilai Prestasi Kerja</th>
					<th>Integritas</th>
					<th>Disiplin</th>
					<th>Pegawai Penilai</th>
					<th>Atasan Pegawai Penilai</th>
					<th colspan="3"><a class="pull-right margin-r-5" ng-click="createRiwayatSkp()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayat_skp">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}" @if(Auth::user()->user_level != 'super_admin' && Auth::user()->user_group != 1) disabled @endif>
					</td>
					<td>@{{ a.tahun_skp }}</td>
					<td>@{{ a.nilai_skp }}</td>
					<td>@{{ a.orientasi_pelayanan }}</td>
					<td>@{{ a.komitmen }}</td>
					<td>@{{ a.kerjasama }}</td>
					<td>@{{ a.nilai_perilaku_kerja }}</td>
					<td>@{{ a.nilai_prestasi_kerja }}</td>
					<td>@{{ a.integritas }}</td>
					<td>@{{ a.disiplin }}</td>
					<td>@{{ a.pegawai_penilai.nama }}</td>
					<td>@{{ a.atasan_pegawai_penilai.nama }}</td>
					<td>
						@if(Auth::user()->user_level == 'super_admin' || Auth::user()->user_group == 1)
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatSkp(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatSkp(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<div>
						<td>
							<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editRiwayatSkp(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
						</td>
						<td>
							@if(Auth::user()->user_level == 'super_admin' || Auth::user()->user_group == 1)
							<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatSkp(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
							@endif
							<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatSkp(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
						</td>
					</div>
				</tr>
				@if(Auth::user()->user_level == 'super_admin' || Auth::user()->user_group == 1)
				<tr>
					<td colspan="13">
						<a ng-if="riwayat_skp.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatSkp()">Approve Selected</a>
						<a ng-if="riwayat_skp.length" class="badge bg-red" ng-click="unapproveRiwayatSkp()">Unapprove Selected</a>
					</td>
				</tr>
				@endif
			</tbody>
		</table>
	</div>

	<div class="modal fade create-riwayat-skp-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Riwayat SKP</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Tahun SKP</label>
									<input type="text" ng-model="newData.tahun_skp" class="yearpicker form-control" required>
								</div>
								<div class="form-group">
									<label>Nilai SKP</label>
									<input type="text" ng-model="newData.nilai_skp" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Orientasi Pelayanan</label>
									<input type="text" ng-model="newData.orientasi_pelayanan" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Komitmen</label>
									<input type="text" ng-model="newData.komitmen" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Kerjasama</label>
									<input type="text" ng-model="newData.kerjasama" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Nilai Perilaku Kerja</label>
									<input type="text" ng-model="newData.nilai_perilaku_kerja" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nilai Prestasi Kerja</label>
									<input type="text" ng-model="newData.nilai_prestasi_kerja" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Integritas</label>
									<input type="text" ng-model="newData.integritas" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Disiplin</label>
									<input type="text" ng-model="newData.disiplin" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Pegawai Penilai</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_pegawai_penilai" required>
										<option ng-repeat="a in pegawaiPenilai" ng-value="a.id">@{{a.nip}} @{{a.nama}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>Atasan Pegawai Penilai</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.id_atasan_pegawai_penilai" required>
										<option ng-repeat="a in pegawaiPenilai" ng-value="a.id">@{{a.nip}} @{{a.nama}}</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatSkp()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-riwayat-skp-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Riwayat SKP</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="col-lg-6">
									<div class="form-group">
										<label>Tahun SKP</label>
										<input type="text" ng-model="editData.tahun_skp" class="yearpicker form-control" required>
									</div>
									<div class="form-group">
										<label>Nilai SKP</label>
										<input type="text" ng-model="editData.nilai_skp" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Orientasi Pelayanan</label>
										<input type="text" ng-model="editData.orientasi_pelayanan" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Komitmen</label>
										<input type="text" ng-model="editData.komitmen" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Kerjasama</label>
										<input type="text" ng-model="editData.kerjasama" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Nilai Perilaku Kerja</label>
										<input type="text" ng-model="editData.nilai_perilaku_kerja" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<label>Nilai Prestasi Kerja</label>
										<input type="text" ng-model="editData.nilai_prestasi_kerja" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Integritas</label>
										<input type="text" ng-model="editData.integritas" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Disiplin</label>
										<input type="text" ng-model="editData.disiplin" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Pegawai Penilai</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="editData.id_pegawai_penilai" required>
											<option ng-repeat="a in pegawaiPenilai" ng-value="a.id">@{{a.nip}} @{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Atasan Pegawai Penilai</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="editData.id_atasan_pegawai_penilai" required>
											<option ng-repeat="a in pegawaiPenilai" ng-value="a.id">@{{a.nip}} @{{a.nama}}</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatSkp(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>