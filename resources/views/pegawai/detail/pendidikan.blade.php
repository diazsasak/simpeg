<div class="chart tab-pane" id="pendidikan">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Pendidikan</th>
					<th>Jurusan</th>
					<th>Nama Institusi</th>
					<th>Tahun Lulus</th>
					<th>No Ijazah</th>
					<th>Gelar Depan</th>
					<th>Gelar Belakang</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>&nbsp;</td>
					<td>
						<select class="form-control" ng-model="pendidikan.id_pendidikan" required>
							<option ng-repeat="a in masterPendidikan" ng-value="a.id">@{{a.nama}}</option>
						</select>
					</td>
					<td>
						<!-- <input type="text" ng-model="pendidikan.jurusan" class="form-control" required> -->
						<select class="form-control" ng-model="pendidikan.id_jurusan">
							<option ng-repeat="a in masterJurusan" ng-value="a.id" ng-cloak>@{{a.nama}}</option>
						</select>
					</td>
					<td>
						<input type="text" ng-model="pendidikan.nama_institusi" class="form-control" required>
					</td>
					<td>
						<input type="text" ng-model="pendidikan.tahun_lulus" class="yearpicker form-control">
					</td>
					<td>
						<input type="text" ng-model="pendidikan.no_ijazah" class="form-control">
					</td>
					<td>
						<input type="text" ng-model="pendidikan.gelar_depan" class="form-control">
					</td>
					<td>
						<input type="text" ng-model="pendidikan.gelar_belakang" class="form-control">
					</td>
					<td>
						<button title="simpan" class="btn btn-primary" ng-click="saveRiwayatPendidikan(idPegawai)"><span class="fa fa-save fa-lg"></span></button>
					</td>
				</tr>
				<tr ng-repeat="a in pegawai.riwayat_pendidikan">
					<td>
						@{{$index+1}}
					</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-select="a.pivot.id_pendidikan" e-ng-options="b.id as b.nama for b in masterPendidikan">@{{ a.nama }}</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-select="a.pivot.id_jurusan" e-ng-options="b.id as b.nama for b in masterJurusan">@{{ pegawai.has_many_riwayat_pendidikan[$index].master_jurusan.nama }}</td>
					<!-- <td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.jurusan">@{{ pegawai.has_many_riwayat_pendidikan[$index].master_jurusan.nama }}</td> -->
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.nama_institusi">@{{ a.pivot.nama_institusi }}</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.tahun_lulus">@{{ a.pivot.tahun_lulus }}</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.no_ijazah">@{{ a.pivot.no_ijazah }}</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.gelar_depan">@{{ a.pivot.gelar_depan }}</td>
					<td style="border:none" onaftersave="updateRiwayatPendidikan(a.pivot)" editable-text="a.pivot.gelar_belakang">@{{ a.pivot.gelar_belakang }}</td>
					<td>
						<a ng-if="!a.is_approved" href="#myModal@{{a.pivot.id}}" data-target="#myModal@{{a.pivot.id}}" data-toggle="modal"><i class="fa fa-pencil fa-lg"></i></a>
						<div id="myModal@{{a.pivot.id}}" class="modal fade edit-pensiun-modal" role="dialog" aria-labelledby="myLargeModalLabel">
							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<form action="#" method="POST" enctype="multipart/form-data">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title">Edit Riwayat Pendidikan</h4>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group">
														<label>Pendidikan</label><br>
														<select style="width: 100%" class="select2 form-control" ng-model="a.pivot.id_pendidikan" required>
															<option ng-repeat="b in masterPendidikan" ng-value="b.id" ng-selected="b.id.valueOf() === a.pivot.id_pendidikan.valueOf()">@{{b.nama}}</option>
														</select>
													</div>
													<div class="form-group">
														<label>Jurusan</label><br>
														<select style="width: 100%" class="select2 form-control" ng-model="a.pivot.id_jurusan" required>
															<option ng-repeat="b in masterJurusan" ng-value="b.id" ng-selected="b.id.valueOf() === a.pivot.id_jurusan.valueOf()">@{{b.nama}}</option>
														</select>
													</div>
													<div class="form-group">
														<label>Nama Institusi</label>
														<input type="text" ng-model="a.pivot.nama_institusi" class="form-control" required>
													</div>
													<div class="form-group">
														<label>Tahun Lulus</label>
														<input type="text" ng-model="a.pivot.tahun_lulus" class="yearpicker form-control" required>
													</div>
													<div class="form-group">
														<label>No Ijazah</label>
														<input type="text" ng-model="a.pivot.no_ijazah" class="form-control" required>
													</div>
													<div class="form-group">
														<label>Gelar Depan</label>
														<input type="text" ng-model="a.pivot.gelar_depan" class="form-control" required>
													</div>
													<div class="form-group">
														<label>Gelar Belakang</label>
														<input type="text" ng-model="a.pivot.gelar_belakang" class="form-control" required>
													</div>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
											<a class="btn btn-primary" ng-click="updateRiwayatPendidikan(a.pivot)" data-dismiss="modal">Simpan</a>
										</div>
									</form>
								</div>
							</div>
						</div>
						@if((Auth::user()->user_group == 1) || (Auth::user()->user_level == 'super_admin'))
						<a class="text-red pull-right margin-r-5" ng-click="delRiwayatPendidikan(a.pivot.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.pivot.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="modal fade create-pendidikan-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah pendidikan</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Jenis pendidikan</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.jenis_pendidikan" required>
										<option ng-repeat="a in jenispendidikan" ng-value="a.jenis_pendidikan">@{{a.jenis_pendidikan}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK pendidikan</label>
									<input type="text" ng-model="newData.sk_pendidikan" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK pendidikan</label>
									<input type="text" ng-model="newData.tanggal_sk_pendidikan" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>TMT pendidikan</label>
									<input type="text" ng-model="newData.tmt_pendidikan" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="savependidikan()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-pendidikan-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit pendidikan</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Jenis pendidikan</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.jenis_pendidikan" required>
										<option ng-repeat="a in jenispendidikan" ng-value="a.jenis_pendidikan" ng-selected="a.jenis_pendidikan.valueOf() === editData.jenis_pendidikan.valueOf()">@{{a.jenis_pendidikan}}</option>
									</select>
								</div>
								<div class="form-group">
									<label>SK pendidikan</label>
									<input type="text" ng-model="editData.sk_pendidikan" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK pendidikan</label>
									<input type="text" ng-model="editData.tanggal_sk_pendidikan" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>TMT pendidikan</label>
									<input type="text" ng-model="editData.tmt_pendidikan" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updatependidikan(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>