<div class="chart tab-pane" id="riwayat-cuti" ng-controller="RiwayatCutiCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>SK cuti</th>
					<th>Tanggal SK cuti</th>
					<th>Jenis cuti</th>
					<th>Alasan cuti</th>
					<th>Tanggal mulai cuti</th>
					<th>Tanggal selesai cuti</th>
					<th>Status</th>
					<th colspan="3"><a class="pull-right margin-r-5" ng-click="createRiwayatCuti()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in riwayat_cuti">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.sk_cuti }}</td>
					<td>@{{ a.tanggal_sk_cuti }}</td>
					<td>@{{ a.jenis_cuti }}</td>
					<td>@{{ a.alasan_cuti }}</td>
					<td>@{{ a.tanggal_mulai_cuti }}</td>
					<td>@{{ a.tanggal_selesai_cuti }}</td>
					<td style="width: 50px;">
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatCuti(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatCuti(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<div>
						<td>
							<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editRiwayatCuti(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
						</td>
						<td>
							<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatCuti(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
							<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatCuti(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
						</td>
					</div>
				</tr>
				<tr>
					<td colspan="12">
						<a ng-if="riwayat_cuti.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatCuti()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayat_cuti.length" class="badge bg-red" ng-click="unapproveRiwayatCuti()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-riwayat-cuti-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah riwayat cuti</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>SK cuti</label>
									<input type="text" ng-model="newData.sk_cuti" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK cuti</label>
									<input type="text" ng-model="newData.tanggal_sk_cuti" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Jenis cuti</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="newData.jenis_cuti" required>
										<option ng-repeat="a in jenisCuti" ng-value="a.jenis_cuti">@{{a.jenis_cuti}}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Alasan cuti</label>
									<textarea class="form-control" ng-model="newData.alasan_cuti"></textarea>
								</div>
								<div class="form-group">
									<label>Tanggal mulai cuti</label>
									<input type="text" ng-model="newData.tanggal_mulai_cuti" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal selesai cuti</label>
									<input type="text" ng-model="newData.tanggal_selesai_cuti" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveRiwayatCuti()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-riwayat-cuti-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit riwayat cuti</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>SK cuti</label>
									<input type="text" ng-model="editData.sk_cuti" class="form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal SK cuti</label>
									<input type="text" ng-model="editData.tanggal_sk_cuti" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Jenis cuti</label><br>
									<select style="width: 100%" class="select2 form-control" ng-model="editData.jenis_cuti" required>
										<option ng-repeat="a in jenisCuti" ng-value="a.jenis_cuti" ng-selected="a.jenis_cuti.valueOf() === editData.jenis_cuti.valueOf()">@{{a.jenis_cuti}}</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Alasan cuti</label>
									<textarea class="form-control" ng-model="editData.alasan_cuti"></textarea>
								</div>
								<div class="form-group">
									<label>Tanggal mulai cuti</label>
									<input type="text" ng-model="editData.tanggal_mulai_cuti" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Tanggal selesai cuti</label>
									<input type="text" ng-model="editData.tanggal_selesai_cuti" class="datepicker form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateRiwayatCuti(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>