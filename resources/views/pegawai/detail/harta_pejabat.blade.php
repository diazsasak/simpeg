<div class="chart tab-pane" id="harta-pejabat" ng-controller="HartaPejabatCtrl">
	<div class="table-responsive">
		<table class="table table-hover" ng-cloak>
			<thead>
				<tr>
					<th>#</th>
					<th>Tanggal Laporan</th>
					<th>Keterangan</th>
					<th>Dokumen</th>
					<th>Status</th>
					<th colspan="3"><a class="pull-right margin-r-5" ng-click="createHartaPejabat()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="a in harta_pejabat">
					<td>
						<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
					</td>
					<td>@{{ a.tanggal_laporan }}</td>
					<td>@{{ a.keterangan }}</td>
					<td><a href="{{ url('/') }}/@{{ a.dokumen }}" target="_blank">Dokumen Harta</a></td>
					<td style="width: 50px;">
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedHartaPejabat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
						@else
						<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
						@endif
						<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedHartaPejabat(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="pull-right margin-r-5" ng-click="editHartaPejabat(a)" href="" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil fa-lg"></i></a>
					</td>
					<td>
						<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delHartaPejabat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
						<!-- <a class="text-green pull-right margin-r-5" ng-click="resHartaPejabat(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
					</td>
				</tr>
				<tr>
					<td colspan="8">
						<a ng-if="riwayat_hukuman.length" class="badge bg-green margin-r-5" ng-click="approveHartaPejabat()">Approve Selected</a>
						@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
						<a ng-if="riwayat_hukuman.length" class="badge bg-red" ng-click="unapproveHartaPejabat()">Unapprove Selected</a>
						@endif
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="modal fade create-harta-pejabat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Tambah Harta Pejabat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Tanggal Laporan</label>
									<input type="text" ng-model="newData.tanggal_laporan" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" ng-model="newData.keterangan"></textarea>
								</div>
								<div class="form-group">
									<label>Dokumen</label>
									<input type="file" file-model="newData.dokumen" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="saveHartaPejabat()" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade edit-harta-pejabat-modal" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<form action="#" method="POST" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Harta Pejabat</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Tanggal Laporan</label>
									<input type="text" ng-model="editData.tanggal_laporan" class="datepicker form-control" required>
								</div>
								<div class="form-group">
									<label>Keterangan</label>
									<textarea class="form-control" ng-model="editData.keterangan"></textarea>
								</div>
								<div class="form-group">
									<label>Dokumen</label>
									<input type="file" file-model="editData.dokumen" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
						<a class="btn btn-primary" ng-click="updateHartaPejabat(editData)" data-dismiss="modal">Simpan</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>