	<div class="chart tab-pane" id="riwayat-jabatan" ng-controller="RiwayatJabatanCtrl">
		<div class="table-responsive">
			<table class="table table-hover" ng-cloak>
				<thead>
					<tr>
						<th>
							<a class="pull-right margin-r-5" data-toggle="modal" ng-click="createRiwayatJabatan()" href=""><i class="fa fa-plus fa-lg" data-toggle="tooltip" data-placement="bottom" title="Tambah"></i></a>
						</th>
						<th>Instansi</th>
						<th>Jabatan</th>
						<th>Eselon</th>
						<th>TMT Jabatan</th>
						<th>Golongan awal</th>
						<th>Golongan terbaru</th>
						<th>Bobot jabatan</th>
						<th>SK Jabatan</th>
						<th>Tanggal SK</th>
						<th>Atasan</th>
						<th>Keterangan</th>
						<th>Keluar daerah</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="a in riwayatJabatan">
						<td>
							<input type="checkbox" name="selectedData" value="@{{ a.id }}" ng-model="selectedData[a.id]" id="@{{a.id}}">
						</td>
						<td>@{{ a.struktur_instansi }}</td>
						<td>@{{ a.master_jabatan.nama }}</td>
						<td>@{{ a.master_eselon.nama }}</td>
						<td>@{{ a.tanggal_jabatan | date:"dd-MM-yyyy" }}</td>
						<td>@{{ a.golongan_awal.nama_golongan }}</td>
						<td>@{{ a.golongan_terbaru.nama_golongan }}</td>
						<td>@{{ a.bobot_jabatan }}</td>
						<td>@{{ a.sk_jabatan }}</td>
						<td>@{{ a.tanggal_sk | date:"dd-MM-yyyy" }}</td>
						<td>@{{ a.atasan.nama }}</td>
						<td>@{{ a.keterangan }}</td>
						<td>
							<div ng-if="a.is_keluar_daerah == 1">Ya</div>
							<div ng-if="a.is_keluar_daerah == 0">Tidak</div>
						</td>
						<td>
							@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
							<a ng-class="{false: 'label label-success', true: 'label label-danger'}[is_approved]" ng-if="a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="unapprovedRiwayatJabatan(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Unapproved" : "Approved"}}</a>
							@else
							<span ng-if="a.is_approved">@{{is_approved ? "Unapproved" : "Approved"}}</span>
							@endif
							<a ng-class="{false: 'label label-warning', true: 'label label-success'}[is_approved]" ng-if="!a.is_approved" ng-mouseenter="is_approved=true" ng-mouseleave="is_approved=false" href="" ng-click="approvedRiwayatJabatan(a.id)" ng-hide="a.deleted_at">@{{is_approved ? "Approve" : "Pending"}}</a>
						</td>
						<td>
							<a ng-if="!a.is_approved" href="#" ng-click="editRiwayatJabatan(a)" class="fa fa-pencil fa-lg"></a>
							<a ng-if="!a.is_approved" class="text-red pull-right margin-r-5" ng-click="delRiwayatJabatan(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Hapus" ng-if="!a.deleted_at"><i class="fa fa-trash fa-lg"></i></a>
							<!-- <a class="text-green pull-right margin-r-5" ng-click="resRiwayatJabatan(a.id)" href="" data-toggle="tooltip" data-placement="bottom" title="Restore" ng-if="a.deleted_at"><i class="fa fa-undo fa-lg"></i></a> -->
						</td>
					</tr>
					
					<tr>
						<td colspan="11">
							<a ng-if="riwayatJabatan.length" class="badge bg-green margin-r-5" ng-click="approveRiwayatJabatan()">Approve Selected</a>
							@if(Auth::user()->isBKD() || (Auth::user()->user_level == 'super_admin'))
							<a ng-if="riwayatJabatan.length" class="badge bg-red" ng-click="unapproveRiwayatJabatan()">Unapprove Selected</a>
							@endif
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="modal fade create-riwayat-jabatan-modal" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<form action="#" name="myForm" method="POST" enctype="multipart/form-data">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Tambah Riwayat Jabatan</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Instansi Induk <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterSatuanKerjaInduk(selectedInstansi.id_instansi_induk)" ng-model="selectedInstansi.id_instansi_induk" name="id_instansi_induk" required>
											<option ng-repeat="a in masterInstansiInduk" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(myForm.id_instansi_induk)" ng-messages="myForm.id_instansi_induk.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
									<div class="form-group">
										<label>Unit Organisasi</label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterUnitSkpd(selectedInstansi.id_satuan_kerja_induk)" ng-model="selectedInstansi.id_satuan_kerja_induk" name="id_satuan_kerja_induk">
											<option ng-repeat="a in masterSatuanKerjaInduk" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(myForm.id_satuan_kerja_induk)" ng-messages="myForm.id_satuan_kerja_induk.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
									<div class="form-group">
										<label>Sub Organisasi</label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterSubUnitOrganisasi(selectedInstansi.id_master_unit_skpd)" ng-model="selectedInstansi.id_master_unit_skpd" name="id_master_unit_skpd">
											<option ng-repeat="a in masterUnitSkpd" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<div class="error-container" ng-show="isError(myForm.id_master_unit_skpd)" ng-messages="myForm.id_master_unit_skpd.$error" ng-cloak>
											<div ng-messages-include="error-list.html"></div>
										</div>
									</div>
									<div class="form-group">
										<label>Unit Kerja</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="selectedInstansi.id_sub_unit_organisasi">
											<option ng-repeat="a in masterSubUnitOrganisasi" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Golongan awal</label><br>
										<input type="text" ng-model="golonganAwal.master_pangkat.nama_golongan" class="form-control" disabled>
									</div>
									<div class="form-group">
										<label>Golongan terbaru</label><br>
										<input type="text" ng-model="golonganTerbaru.master_pangkat.nama_golongan" class="form-control" disabled>
									</div>
									<div class="form-group">
										<label>SK jabatan <span style="color: red;">*</span></label>
										<input type="text" ng-model="newData.sk_jabatan" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Kategori jabatan <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterJabatan(newData.id_kategori_jabatan);" ng-model="newData.id_kategori_jabatan" required>
											<option ng-repeat="a in masterKategoriJabatan" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Eselon <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="newData.id_eselon" ng-required="newData.id_kategori_jabatan == 2"  ng-disabled="newData.id_kategori_jabatan != 2">
											<option></option>
											<option ng-repeat="a in masterEselon" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Jabatan <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="newData.id_jabatan" ng-disabled="!newData.id_kategori_jabatan" required>
											<option ng-repeat="a in masterJabatan" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Atasan</label><br>
										<div class="row">
											<div class="col-xs-9">
												<select style="width: 100%" class="select2 form-control" ng-model="newData.id_pegawai_atasan">
													<option></option>
													<option ng-repeat="a in atasan" ng-value="a.id">@{{a.nama}}</option>
												</select>
											</div>
											<div class="col-xs-3">
												<button class="btn btn-primary" type="button" ng-click="getAtasan()"><span class="fa fa-refresh"></span></button>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Tanggal SK <span style="color: red;">*</span></label>
										<input type="text" ng-model="newData.tanggal_sk" class="datepicker form-control" required>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Bobot jabatan</label>
										<input type="number" ng-model="newData.bobot_jabatan" class="form-control">
									</div>
									<div class="form-group">
										<label>Keterangan</label>
										<input type="text" ng-model="newData.keterangan" class="form-control">
									</div>
									<div class="form-group">
										<label>TMT jabatan <span style="color: red;">*</span></label>
										<input type="text" ng-model="newData.tanggal_jabatan" class="datepicker form-control" required>
									</div>									
									<div class="checkbox">
										<label>
											<input type="checkbox" ng-model="newData.is_keluar_daerah"> Mutasi keluar daerah
										</label>
									</div>
									<!-- <div class="form-group">
										<label>Jabatan sekarang ? <span style="color: red;">*</span></label><br>
										<div class="radio">
											<label>
												<input type="radio" ng-model="newData.is_jabatan_sekarang" name="is_jabatan_sekarang" value="1" required>
												Iya
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" ng-model="newData.is_jabatan_sekarang" name="is_jabatan_sekarang" value="0" required>
												Tidak
											</label>
										</div>
									</div> -->
									<div class="form-group">
										<span style="color: red;">* Wajib diisi</span>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							<button class="btn btn-primary" type="button" ng-click="saveRiwayatJabatan()" ng-disabled="myForm.$invalid" data-dismiss="modal">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="modal fade edit-riwayat-jabatan-modal" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<form action="#" method="POST" enctype="multipart/form-data" name="editForm">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title">Edit riwayat jabatan</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label>Instansi Induk <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterSatuanKerjaInduk(selectedInstansi.id_instansi_induk)" ng-model="selectedInstansi.id_instansi_induk" name="id_instansi_induk">
											<option ng-repeat="a in masterInstansiInduk" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<span ng-if="!selectedInstansi.id_instansi_induk" class="help-block">Kosongkan jika tidak akan diubah</span>
									</div>
									<div class="form-group">
										<label>Unit Organisasi</label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterUnitSkpd(selectedInstansi.id_satuan_kerja_induk)" ng-model="selectedInstansi.id_satuan_kerja_induk" name="id_satuan_kerja_induk">
											<option ng-repeat="a in masterSatuanKerjaInduk" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<span ng-if="!selectedInstansi.id_satuan_kerja_induk" class="help-block">Kosongkan jika tidak akan diubah</span>
									</div>
									<div class="form-group">
										<label>Sub Organisasi</label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterSubUnitOrganisasi(selectedInstansi.id_master_unit_skpd)" ng-model="selectedInstansi.id_master_unit_skpd" name="id_master_unit_skpd">
											<option ng-repeat="a in masterUnitSkpd" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<span ng-if="!selectedInstansi.id_master_unit_skpd" class="help-block">Kosongkan jika tidak akan diubah</span>
									</div>
									<div class="form-group">
										<label>Unit Kerja</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="selectedInstansi.id_sub_unit_organisasi">
											<option ng-repeat="a in masterSubUnitOrganisasi" ng-value="a.id">@{{a.nama}}</option>
										</select>
										<span ng-if="!selectedInstansi.id_sub_unit_organisasi" class="help-block">Kosongkan jika tidak akan diubah</span>
									</div>
									<div class="form-group">
										<label>Golongan awal</label><br>
										<input type="text" ng-model="golonganAwal.master_pangkat.nama_golongan" class="form-control" disabled>
									</div>
									<div class="form-group">
										<label>Golongan terbaru</label><br>
										<input type="text" ng-model="golonganTerbaru.master_pangkat.nama_golongan" class="form-control" disabled>
									</div>
									<div class="form-group">
										<label>SK jabatan <span style="color: red;">*</span></label>
										<input type="text" ng-model="editData.sk_jabatan" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Kategori jabatan <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-change="getMasterJabatan(editData.id_kategori_jabatan);" ng-model="editData.id_kategori_jabatan" required>
											<option ng-repeat="a in masterKategoriJabatan" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Eselon <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="editData.id_eselon" ng-required="editData.id_kategori_jabatan == 2"  ng-disabled="editData.id_kategori_jabatan != 2">
											<option></option>
											<option ng-repeat="a in masterEselon" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Jabatan <span style="color: red;">*</span></label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="editData.id_jabatan" ng-disabled="!editData.id_kategori_jabatan" required>
											<option ng-repeat="a in masterJabatan" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Atasan</label><br>
										<select style="width: 100%" class="select2 form-control" ng-model="editData.id_pegawai_atasan">
											<option></option>
											<option ng-repeat="a in pegawai" ng-value="a.id">@{{a.nama}}</option>
										</select>
									</div>
									<div class="form-group">
										<label>Tanggal SK <span style="color: red;">*</span></label>
										<input type="text" ng-model="editData.tanggal_sk" class="datepicker form-control" required>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label>Bobot jabatan</label>
										<input type="number" ng-model="editData.bobot_jabatan" class="form-control">
									</div>
									<div class="form-group">
										<label>Keterangan</label>
										<input type="text" ng-model="editData.keterangan" class="form-control">
									</div>
									<div class="form-group">
										<label>Tanggal jabatan <span style="color: red;">*</span></label>
										<input type="text" ng-model="editData.tanggal_jabatan" class="datepicker form-control" required>
									</div>									
									<div class="checkbox">
										<label>
											<input type="checkbox" ng-model="editData.is_keluar_daerah" ng-checked="editData.is_keluar_daerah == 1" ng-true-value="1" ng-false-value="0"> Mutasi keluar daerah
										</label>
									</div>
									<!-- <div class="form-group">
										<label>Jabatan sekarang ?<span style="color: red;">*</span></label><br>
										<div class="radio">
											<label>
												<input type="radio" ng-model="editData.is_jabatan_sekarang" name="is_jabatan_sekarang" ng-value="1" ng-checked="editData.is_jabatan_sekarang == 1" required>
												Iya
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" ng-model="editData.is_jabatan_sekarang" name="is_jabatan_sekarang" ng-value="0" ng-checked="editData.is_jabatan_sekarang == 0" required>
												Tidak
											</label>
										</div>
									</div> -->
									<div class="form-group">
										<span style="color: red;">* Wajib diisi</span>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
							<button class="btn btn-primary" type="button" ng-click="updateRiwayatJabatan(editData)" ng-disabled="editForm.$invalid" data-dismiss="modal">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>