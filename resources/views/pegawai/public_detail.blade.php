@extends('layout.beranda')
@section('title')
Detail Pegawai
@endsection
@section('content')

<div class="container sh-center-main-box">
	<!-- <h1>@yield('title')</h1> -->
</div>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-lg-6 col-lg-offset-3">
			<div class="box box-success box-solid">
				<div class="box-header with-border">
					<h3 class="box-title">Detail Pegawai</h3>
				</div>
				<div class="box-body">	
					<table class="table table-hover">
						<tr>
							<td colspan="2" align="center">
								<img src="{{ asset($pegawai->poto) }}" class="img-responsive img-rounded">
							</td>
						</tr>
						<tr>
							<td>NIP</td>
							<td>{{ $pegawai->nip }}</td>
						</tr>
						<tr>
							<td>Nama</td>
							<td>{{ $pegawai->gelar_depan }} {{ $pegawai->nama }} {{ $pegawai->gelar_belakang }}</td>
						</tr>
						<tr>
							<td>Tempat / Tanggal Lahir</td>
							<td>{{ $pegawai->tempat_lahir }}, {{ date('d-m-Y', strtotime($pegawai->tgl_lahir)) }}</td>
						</tr>
						<tr>
							<td>Agama</td>
							<td>{{ $pegawai->masterAgama->nama }}</td>
						</tr>
						<tr>
							<td>Jenis Kelamin</td>
							<td>
								@if($pegawai->jns_kelamin == 1)
								Laki - Laki
								@else 
								Perempuan
								@endif
							</td>
						</tr>
						<tr>
							<td>Status Pegawai</td>
							<td>{{ $pegawai->belongsToStatusKepegawaian->nama }}</td>
						</tr>
						@if($pangkat) 
						<tr>
							<td>Pangkat</td>
							<td> {{ $pangkat->masterPangkat->nama_pangkat }}, {{ $pangkat->masterPangkat->nama_golongan }}</td>
						</tr>
						<tr>
							<td>TMT Pangkat</td>
							<td>{{ date('d-m-Y', strtotime($pangkat->tmt_pangkat)) }}</td>
						</tr>
						@endif
						@if($jabatan) 
						<tr>
							<td>Jabatan</td>
							<td>{{ $jabatan->masterJabatan->nama }}</td>

							
						</tr>
						<tr>
							<td>TMT Jabatan</td><td>@if($jabatan) {{ date('d-m-Y', strtotime($jabatan->tanggal_jabatan)) }} @endif</td>
							
						</tr>
						@endif
						
						@if(isset($subUnitOrganisasi->nama))
						<tr>
							<td>Unit Kerja</td>
							<td>{{ $subUnitOrganisasi->nama }}</td>
						</tr>
						@endif

						@if(isset($unitOrganisasi->nama))
						<tr>
							<td>Sub Organisasi</td>
							<td>{{ $unitOrganisasi->nama }}</td>
						</tr>
						@endif
						@if(isset($satuanKerjaInduk->nama))
						<tr>
							<td>Unit Organisasi</td>
							<td>{{ $satuanKerjaInduk->nama }}</td>
						</tr>
						@endif
						@if(isset($instansiInduk->nama))
						<tr>
							<td>Instansi Induk</td>
							<td>{{ $instansiInduk->nama }}</td>
						</tr>
						@endif
						@if(isset($pendidikan->masterPendidikan->nama_pangkat))
						<tr>
							<td>Pendidikan Terakhir</td>
							<td>{{ $pendidikan->masterPendidikan->nama }}</td>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<footer class="main-footer hidden-print">
	<!-- To the right -->
	<center>
		<!-- Default to the left -->
		<strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
	</center>
</footer>
@endsection
@section('script')
@endsection
