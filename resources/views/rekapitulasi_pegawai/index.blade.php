@extends('layout.beranda')
@section('title')
Rekapitulasi Pegawai
@endsection
@section('content')


<div class="container-fluid">
	<section class="content" ng-controller="RekapitulasiCtrl">
		<div class="row">
			<div class="col-xs-12">
				<center>
					<h1>REKAPITULASI PEGAWAI</h1>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Agama</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-6">
							<canvas id="agamaChart" width="100" height="50"></canvas>
						</div>
						<div class="col-md-6">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Agama</th>
										<th>Jumlah</th>
									</tr>
									<!-- get from db -->
									<?php $total = 0 ?>
									@foreach($agama as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jml']}}</td>
									</tr>
									<?php $total += $a['jml'] ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Pendidikan</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-6">
							<canvas id="pendidikanChart" width="100" height="50"></canvas>
						</div>
						<div class="col-md-6">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Pendidikan</th>
										<th>Jumlah</th>
									</tr>
									<!-- get from db -->
									<?php $total = 0 ?>
									@foreach($pendidikan as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jml']}}</td>
									</tr>
									<?php $total += $a['jml'] ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Jenis Kelamin</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-6">
							<canvas id="jkChart" width="100" height="50"></canvas>
						</div>
						<div class="col-md-6">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Jenis Kelamin</th>
										<th>Jumlah</th>
									</tr>
									<!-- get from db -->
									<?php $total = 0 ?>
									@foreach($jenisKelamin as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jml']}}</td>
									</tr>
									<?php $total += $a['jml'] ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Fungsional Tertentu - Kategori Jabatan</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-12">
							<canvas id="fungsionalTertentuChart" width="100" height="50"></canvas>
						</div>
						<div class="col-md-12">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Jabatan</th>
										<th>Jumlah</th>
									</tr>
									<!-- get from db -->
									<?php $total = 0 ?>
									@foreach($fungsionalTertentu as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jml']}}</td>
									</tr>
									<?php $total += $a['jml'] ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Fungsional Umum/Staf - Kategori Jabatan</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-12">
							<canvas id="fungsionalUmumChart" width="100" height="50"></canvas>
						</div>
						<div class="col-md-12">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Jenis Kelamin</th>
										<th>Jumlah</th>
									</tr>
									<!-- get from db -->
									<?php $total = 0 ?>
									@foreach($fungsionalUmum as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jml']}}</td>
									</tr>
									<?php $total += $a['jml'] ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Pangkat dan Gol. Ruang</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-6">
							<canvas id="pangkatChart" width="100" height="200"></canvas>
						</div>
						<div class="col-md-6">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Gol. Ruang</th>
										<th>Laki-laki</th>
										<th>Perempuan</th>
										<th>Jumlah</th>
									</tr>
									<?php $totalL = 0 ?>
									<?php $totalP = 0 ?>
									<?php $total = 0 ?>
									@foreach($pangkat as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama_golongan']}}</td>
										<td>{{$a['jmlL']}}</td>
										<td>{{$a['jmlP']}}</td>
										<td>{{$a['jmlL']+$a['jmlP']}}</td>
									</tr>
									<?php $totalL += $a['jmlL'] ?>
									<?php $totalP += $a['jmlP'] ?>
									<?php $total += ($a['jmlL']+$a['jmlP']) ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$totalL}}</td>
										<td>{{$totalP}}</td>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="box box-success box-solid">
					<div class="box-header">
						<h3 class="box-title">Struktural - Kategori Jabatan</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<div class="col-md-6">
							<canvas id="eselonChart" width="100" height="100"></canvas>
						</div>
						<div class="col-md-6">
							<table class="table table-hover">
								<tbody>
									<tr>
										<th>No.</th>
										<th>Eselon</th>
										<th>Laki-laki</th>
										<th>Perempuan</th>
										<th>Jumlah</th>
									</tr>
									<?php $totalL = 0 ?>
									<?php $totalP = 0 ?>
									<?php $total = 0 ?>
									@foreach($eselon as $key => $a)
									<tr>
										<td>{{$key+1}}</td>
										<td>{{$a['nama']}}</td>
										<td>{{$a['jmlL']}}</td>
										<td>{{$a['jmlP']}}</td>
										<td>{{$a['jmlL']+$a['jmlP']}}</td>
									</tr>
									<?php $totalL += $a['jmlL'] ?>
									<?php $totalP += $a['jmlP'] ?>
									<?php $total += ($a['jmlL']+$a['jmlP']) ?>
									@endforeach
									<tr>
										<th colspan="2">Total</th>
										<td>{{$totalL}}</td>
										<td>{{$totalP}}</td>
										<td>{{$total}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</section>
</div>
<footer class="main-footer hidden-print"  style="background: url({{asset('images/bg_head.jpg')}}) center no-repeat; min-height: 100px;">
      <div class="container">
      <div class="col-md-7">
            <!-- Default to the left -->
        <strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
    
      </div>
            <div class="col-md-5" style="text-align: right;">
                  Alamat Kantor: Jl. MT. Haryono No.15 Selong
                  <br>
                  Email: bkdlotim@gmail.com
                  <br>
                  Telp: (0376)-21081/21091
            </div>
      </div>
</footer>
<!-- <div class="container">
      <hr>
      
      <footer>
            <p>&copy; {{ date('Y') }} Pemerintah Kabupaten Lombok Timur</p>
      </footer>
  </div> -->
  @endsection
  @section('script')
  <script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.min.js') }}"></script>
  <!-- AngularJS -->
  <script src="{{ asset('bower_components/angular/angular.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-ui-mask/dist/mask.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-confirm-modal/angular-confirm.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-loading-bar/build/loading-bar.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ asset('bower_components/angular-ui-tinymce/src/tinymce.js') }}"></script>
  <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('bower_components/angular-xeditable/dist/js/xeditable.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('app/app.js') }}" type="text/javascript"></script>
  <script src="{{ asset('app/controllers.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ asset('app/controllers/rekapitulasi.js') }}"></script>
  @endsection