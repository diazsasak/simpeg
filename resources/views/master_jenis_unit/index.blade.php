@extends('layout.layout')
@section('title')
Master Jenis Unit - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-folder-o"></i>
		Jenis Unit
		<small>Manajemen Master Jenis Unit</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li>Master</li>
		<li class="active">Master Jenis Unit</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<!-- <h3 class="box-title">Tabel Akun</h3> -->
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".create-modal">Tambah Jenis Unit</button>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover">
						<tr class="info">
							<th>#</th>
							<th>Nama Jenis Unit</th>
							<th colspan="2">&nbsp;</th>
						</tr>
						@foreach($data as $index=>$a)
						<tr>
						<td>{{$index+1}}</td>
							<td>{{ $a->nama }}</td>
							<td>
								@if($a->deleted_at == null)
								<a href="#" type="button" data-toggle="modal" data-target=".edit{{$a->id}}"><i class="fa fa-pencil fa-lg"></i></a>
								@endif
								<!-- Modal -->
								<div class="modal fade edit{{$a->id}}" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="{{ url('master_jenis_unit/'.$a->id) }}" method="POST">
												{{ method_field('PUT') }}
												{{ csrf_field() }}
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">&times;</button>
													<h4 class="modal-title">Edit Jenis Unit</h4>
												</div>
												<div class="modal-body">
													<div class="form-group">
														<label>Nama</label>
														<input type="text" class="form-control" value="{{ $a->nama }}" name="nama" required>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
													<button type="submit" class="btn btn-primary">Simpan</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</td>
							<td>
								@if($a->deleted_at == null)
								<a href="#" class="text-red pull-right margin-r-5" type="button" data-toggle="modal" data-target=".delete{{$a->id}}"><i class="fa fa-trash fa-lg"></i></a>
								<!-- Modal -->
								<div class="modal fade delete{{$a->id}}" role="dialog">
									<div class="modal-dialog modal-sm">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Hapus</h4>
											</div>
											<div class="modal-body">
												<p>Apakah anda ingin menghapus data ini?</p>
											</div>
											<div class="modal-footer">
												<form method="POST" action="master_jenis_unit/{{$a->id}}">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<button type="submit" class="btn btn-danger">Hapus</button>
													<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
												</form>
											</div>
										</div>
									</div>
								</div>
								@else
								<a class="pull-right margin-r-5" href="{{ url('master_jenis_unit/'.$a->id.'/restore') }}"><i class="fa fa-undo fa-lg"></i></a>
								@endif
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade create-modal" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="{{ url('master_jenis_unit') }}" method="POST">
				{{ csrf_field() }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Jenis Unit</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" name="nama" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
