@extends('layout.layout')
@section('title')
Dashboard - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('head')
<!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/chart.css') }}"> -->
@endsection
@section('content')
<section class="content-header">
	<h1>
  	<i class="fa fa-home"></i>
		Dashboard
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>
<section class="content" ng-controller="DashboardCtrl">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-success">
				<div class="box-header">
					<h3 class="box-title">Dashboard</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body table-responsive no-padding">
				<canvas id="myChart" width="100" height="20"></canvas>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('script')
<script src="{{ asset('bower_components/chart.js/dist/Chart.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('app/controllers/dashboard.js') }}"></script>
@endsection
