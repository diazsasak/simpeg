<!DOCTYPE html>
<html lang="id-ID">
      <head>
            @yield('meta')
            <meta charset="UTF-8">
            <title>@yield('title')</title>
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />

            <!-- Bootstrap 3.3.4 -->
            <link href="{{ asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
            <!-- Font Awesome Icons -->
            <link href="{{ asset('outsource/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('outsource/css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
            <!-- Theme style -->
            <link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('css/bkpsdm-v1.css')}}" rel="stylesheet" type="text/css" />
            
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css">
            <!-- Our Css -->
            @yield('head')
            <style type="text/css">
                  [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                        display: none !important;
                  }
                  .datepicker {
                        z-index:1151 !important;
                  }
            </style>
      </head>
      <body class="bg-body">


            @yield('content')
            <!-- jQuery-->
            <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
            <script src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.js')}}" type="text/javascript"></script>
            <script src="{{ asset('bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js')}}" type="text/javascript"></script>
            <!-- AdminLTE App -->
            <script src="{{ asset('bower_components/AdminLTE/dist/js/app.min.js')}}" type="text/javascript"></script>
            <script src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>

            @yield('up-script')

            @yield('script')
      </body>
</html>
