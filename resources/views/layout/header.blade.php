<!-- Main Header -->
<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->

        <span class="logo-mini"><b>S</b>P</span>
        <!-- logo for regular state and mobile devices -->
        <div class="logo-lg">
          <img src="{{asset('images/logo.png')}}" class="logo-head">
          <strong>e</strong>Kepegawaian
        </div>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        @if(Auth::check())
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        @endif

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <div id="online-user" class="label label-primary"></div>
                </li>
                @if(Auth::check())
                <li class="user user-menu">
                    @if(Auth::user()->id_pegawai!=NULL)
                    <a href="#">
                        <span>{{ Auth::user()->pegawai->nama }}</span>
                    </a>
                    @else
                    <a href="#">
                        <span>{{ Auth::user()->name }}</span>
                    </a>
                    @endif
                </li>
                @else
                <li>
                    <a href="<% URL::to('login') %>">&nbsp; SI Pegawai BKD LOTIM</a>
                </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
