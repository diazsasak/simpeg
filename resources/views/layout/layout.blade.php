<!DOCTYPE html>
<html ng-app="simpegApp" lang="id-ID">
<head>
    @yield('meta')
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/AdminLTE/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->

    <link href="{{ asset('outsource/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('outsource/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/AdminLTE/dist/css/skins/skin-green-light.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css" rel="stylesheet" type="text/css">
    <link href="{{ asset('bower_components/angular-xeditable/dist/css/xeditable.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('bower_components/angular-loading-bar/src/loading-bar.css') }}" rel="stylesheet" type="text/css" >
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <link href="{{ asset('css/bkpsdm-v1.css') }}" rel="stylesheet" type="text/css" />
    <!-- Our Css -->
    @yield('head')
    <style type="text/css">
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
        .datepicker {
            z-index:1151 !important;
        }
        /*.popover-wrapper
        { position: relative; z-index: 99 !important; }*/
        .maximized {
            z-index: 9999;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
        }
    </style>
</head>
@if(Auth::check())
<body class="skin-green-light fixed">
    @else
    <body class="skin-green-light fixed sidebar-collapse">
        @endif
        <!-- <div class="se-pre-con"></div> -->
        <div class="wrapper">
            @include('layout.header')
            @include('layout.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                {{-- pesan error --}}
                @if(count($errors) > 0)
                <section class="content-header">
                    <div class="col-sm-12 col-md-6 col-md-offset-3">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{!! $error !!}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </section>
                @endif
                {{-- pesan sukses --}}
                @if(Session::has('success'))
                <section class="content-header">
                    <div class="col-sm-12 col-md-6 col-md-offset-3">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ Session::pull('success') }}
                        </div>
                    </div>
                </section>
                @endif
                {{-- pesan gagal --}}
                @if(Session::has('fail'))
                <section class="content-header">
                    <div class="col-sm-12 col-md-6 col-md-offset-3">
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {!! Session::pull('fail') !!}
                        </div>
                    </div>
                </section>
                @endif
                @if(Auth::check() && isset($menu))
                @if($menu!='master' && $menu!='akun' && $menu!='log' && $menu!='artikel' && $menu!='dokumen' && $menu!='dashboard' && $menu!='print' && $menu!='struktur_organisasi')
                <section class="content-header" ng-controller="SearchCtrl">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="#" method="get" class="sidebar-form" style="margin: 0px;">
                                <div class="input-group" style="width: 100%">
                                      <span class="input-group-btn">
                                            <button style="background: #fff !important;" type="button" name="search" id="search-btn" class="btn btn-flat" ng-click="globalSearch(searchQuery)"><i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                    <input type="text" name="q" class="form-control" placeholder="Search..." ng-model="searchQuery" ng-change="globalSearch(searchQuery)" autocomplete="off">
                                    
                                </div>
                            </form>
                            <div class="box" ng-hide="!searchPegawai||!searchPegawai.length" ng-cloak>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <h4>Pegawai</h4>
                                    <div ng-repeat="a in searchPegawai">
                                        <a href="{{ url('pegawai') }}/@{{a.id}}">@{{a.nip+' '+a.nama+' '+a.tempat_lahir+' '+a.alamat_sekarang+' '+a.asal_kabupaten_kota+' '+a.asal_kecamatan+' '+a.asal_alamat_lengkap}} </a><br><br>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                </section>
                @endif
                @endif
                <!-- <div style="min-height:500px"> -->
                @yield('content')
                <!-- </div> -->
            </div>
            @include('layout.footer')
        </div>
        <!-- REQUIRED JS SCRIPTS -->
        <!-- jQuery-->
        <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bower_components/AdminLTE/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
        <!-- <script src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimScroll.min.js') }}" type="text/javascript"></script> -->
        <!-- <script src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script> -->
        <!-- AdminLTE App -->
        <!-- <script src="{{ asset('bower_components/AdminLTE/dist/js/app.min.js') }}" type="text/javascript"></script> -->
        <script src="{{ asset('bower_components/AdminLTE/dist/js/app.js') }}" type="text/javascript"></script>
        <script src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <!-- AngularJS -->
        <script src="{{ asset('bower_components/angular/angular.min.js') }}" type="text/javascript"></script>
    <!-- script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-messages/1.6.2/angular-messages.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-mask/1.8.7/mask.min.js"></script> -->
    @yield('up-script')
    <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-ui-mask/dist/mask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-confirm-modal/angular-confirm.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-loading-bar/build/loading-bar.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-ui-tinymce/src/tinymce.js') }}"></script>
    <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-xeditable/dist/js/xeditable.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/controllers.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/controllers/search.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script> -->
    <!-- <script src="{{ asset('bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script> -->
    <!-- <script src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.js') }}" type="text/javascript"></script> -->
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
            $('.datepicker').datepicker({
                autoclose: true,
                locale: 'no',
                format: 'yyyy-mm-dd'
            });
            $(".yearpicker").datepicker( {
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('[data-toggle="tooltip"]').on('click', function () {
                $('*').tooltip('hide')
            })
            $("[data-mask]").inputmask();
        });
    </script>
    @yield('script')
</body>
</html>
