<!-- Main Footer -->
<footer class="main-footer hidden-print">
      <!-- To the right -->
      <center>
            <!-- Default to the left -->
            <strong><font class="hidden-xs">Copyright</font> &copy; <font class="hidden-xs">{{ date('Y') }}</font> <a href="{{URL::to('about') }}">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
      </center>
</footer>
