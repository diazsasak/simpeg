<!DOCTYPE html>
<html lang="id-ID" ng-app="simpegApp">
      <head>
            @yield('meta')
            <meta charset="UTF-8">
            <title>@yield('title') - BKPSDM Pemerintah Kabupaten Lombok Timur</title>
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" />

            <!-- Bootstrap 3.3.4 -->
            <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css')}}" />
            <!-- Font Awesome Icons -->
            <link rel="stylesheet" type="text/css" href="{{ asset('outsource/css/font-awesome.min.css')}}" />
            <link rel="stylesheet" type="text/css" href="{{ asset('outsource/css/ionicons.min.css')}}" />
            <!-- Theme style -->
            <link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
            <link href="{{ asset('bower_components/AdminLTE/dist/css/skins/skin-green-light.min.css') }}" rel="stylesheet" type="text/css" />
            <link rel="stylesheet" type="text/css" href="{{ asset('css/beranda-bkpsdm-v1.css')}}"  />
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css" />
            <!-- Our Css -->
            @yield('head')

      </head>
      <body class="skin-blue layout-top-nav">
            <div class="sh-head-custom">
                  <div class="container">
                        <div class="row">
                              <div class="col-md-8">
                                    <a href="{{url('/')}}">
                                          <img src="{{asset('images/logo.png')}}" class="logo-custom">
                                          <span class="logo-text">bkpsdm lotim</span>
                                    </a>
                              </div>
                              <div class="col-md-4 sh-head-custom-right">
                                    <a title="Beranda" href="{{url('/')}}"><i class="fa fa-home" aria-hidden="true"></i></a>
                                    <a title="Berita" href="{{url('public-artikel')}}"><i class="fa fa-rss-square" aria-hidden="true"></i></a>
                                    <a title="Contact Info" href="{{url('kontak')}}"><i class="fa fa-address-book-o" aria-hidden="true"></i></a>
                                    <a title="Login" href="{{url('pegawai')}}" target="_blank"><i class="fa fa-sign-in" aria-hidden="true"></i></a>
                              </div>
                        </div>
                  </div>

            </div>

            <nav class="navbar navbar-inverse navbar-static-top-custom navbar-static-top">
                  <div class="container">
                        <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                              </button>
                        </div>
                        <div id="navbar" class="navbar">
                              <ul class="nav navbar-nav navbar">
                                    <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #fff;">LAYANAN KEPEGAWAIAN <span class="caret"></span></a>
                                          <ul class="dropdown-menu">
                                                <li><a href="{{ url('layanan/layanan-sekretariat-bkpsdm') }}">SEKRETARIAT BKPSDM</a></li>
                                                <li><a href="{{ url('layanan/layanan-bidang-mutasi') }}">BIDANG MUTASI</a></li>
                                                <li><a href="{{ url('layanan/layanan-bidang-disiplin') }}">BIDANG DISIPLIN</a></li>
                                                <li><a href="{{ url('layanan/layanan-bidang-data-dan-formasi') }}">BIDANG DATA DAN FORMASI</a></li>
                                                <li><a href="{{ url('layanan/layanan-bidang-pengembangan-sdm') }}">BIDANG PENGEMBANGAN SDM</a></li>
                                                
                                          </ul>
                                    </li>
                                    <li><a href="{{url('rekapitulasi-pegawai')}}" style="color: #fff;">REKAPITULASI PNSD</a></li>
                              </ul>
                        </div>
                  </div>
            </nav>
            @yield('content')



            <!-- jQuery-->
            <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
            <!-- Bootstrap 3.3.2 JS -->
            <script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}" ></script>
            <script type="text/javascript" src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.js')}}"></script>
            <script type="text/javascript" src="{{ asset('bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js')}}" ></script>
            <!-- AdminLTE App -->
            <script type="text/javascript" src="{{ asset('bower_components/AdminLTE/dist/js/app.min.js')}}" ></script>
            <script type="text/javascript" src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js')}}" ></script>

            @yield('up-script')

            @yield('script')

      </body>
</html>
