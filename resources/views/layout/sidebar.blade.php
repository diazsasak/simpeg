<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar hidden-print">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <!-- <div class="user-panel">
        </div> -->
        @if(Auth::check())
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li @if(isset($menu)) @if($menu=='dashboard') class="active" @endif @endif><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>

        @if(Auth::user()->user_level == 'super_admin')
        <li class="treeview @if(isset($menu)) @if($menu=='master') active @endif @endif">
            <a href="#">
                <i class="fa fa-database"></i> <span>Data Master</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li @if(isset($submenu)) @if($submenu=='pendidikan') class="active" @endif @endif><a href="{{ url('master_pendidikan') }}"><i class="fa fa-folder"></i> Master Pendidikan</a></li>
                    <li @if(isset($submenu)) @if($submenu=='jurusan') class="active" @endif @endif><a href="{{ url('master_jurusan') }}"><i class="fa fa-folder"></i> Master Jurusan</a></li>
                    <li @if(isset($submenu)) @if($submenu=='diklat') class="active" @endif @endif><a href="{{ url('master_diklat') }}"><i class="fa fa-folder"></i> Master Diklat</a></li>
                    <li @if(isset($submenu)) @if($submenu=='keluarga') class="active" @endif @endif><a href="{{ url('master_keluarga') }}"><i class="fa fa-folder"></i> Master Keluarga</a></li>
                    <li @if(isset($submenu)) @if($submenu=='jenis_unit') class="active" @endif @endif><a href="{{ url('master_jenis_unit') }}"><i class="fa fa-folder"></i> Master Jenis Unit</a></li>
                    <li @if(isset($submenu)) @if($submenu=='instansi_induk') class="active" @endif @endif><a href="{{ url('master_instansi_induk') }}"><i class="fa fa-folder"></i> Master Instansi Induk</a></li>
                    <li @if(isset($submenu)) @if($submenu=='satuan_kerja_induk') class="active" @endif @endif><a href="{{ url('master_satuan_kerja_induk') }}"><i class="fa fa-folder"></i> Master Organisasi</a></li>
                    <!--<li @if(isset($submenu)) @if($submenu=='kantor_regional_induk') class="active" @endif @endif><a href="{{ url('master_kantor_regional_induk') }}"><i class="fa fa-folder"></i> Master Kantor Regional Induk</a></li> -->
                    <li @if(isset($submenu)) @if($submenu=='unit_skpd') class="active" @endif @endif><a href="{{ url('master_unit_skpd') }}"><i class="fa fa-folder"></i> Master Sub Organisasi</a></li>
                    <li @if(isset($submenu)) @if($submenu=='sub_unit_organisasi') class="active" @endif @endif><a href="{{ url('master_sub_unit_organisasi') }}"><i class="fa fa-folder"></i> Master Unit Kerja</a></li>
                    <li @if(isset($submenu)) @if($submenu=='agama') class="active" @endif @endif><a href="{{ url('master_agama') }}"><i class="fa fa-folder"></i> Master Agama</a></li>
                    <li @if(isset($submenu)) @if($submenu=='eselon') class="active" @endif @endif><a href="{{ url('master_eselon') }}"><i class="fa fa-folder"></i> Master Eselon</a></li>
                    <li @if(isset($submenu)) @if($submenu=='kategori_jabatan') class="active" @endif @endif><a href="{{ url('master_kategori_jabatan') }}"><i class="fa fa-folder"></i> Master Kategori Jabatan</a></li>
                    <li @if(isset($submenu)) @if($submenu=='jabatan') class="active" @endif @endif><a href="{{ url('master_jabatan') }}"><i class="fa fa-folder"></i> Master Jabatan</a></li>
                    <li @if(isset($submenu)) @if($submenu=='pangkat') class="active" @endif @endif><a href="{{ url('master_pangkat') }}"><i class="fa fa-folder"></i> Master Pangkat</a></li>
                    <li @if(isset($submenu)) @if($submenu=='pekerjaan') class="active" @endif @endif><a href="{{ url('master_pekerjaan') }}"><i class="fa fa-folder"></i> Master Pekerjaan</a></li>
                    <li @if(isset($submenu)) @if($submenu=='kategori_artikel') class="active" @endif @endif><a href="{{ url('master_kategori_artikel') }}"><i class="fa fa-folder"></i> Master Kategori Artikel</a></li>
                    <li @if(isset($submenu)) @if($submenu=='hukuman') class="active" @endif @endif><a href="{{ url('master_hukuman') }}"><i class="fa fa-folder"></i> Master Hukuman</a>
                    </li>
                    <li @if(isset($submenu)) @if($submenu=='status_kepegawaian') class="active" @endif @endif><a href="{{ url('master_status_kepegawaian') }}"><i class="fa fa-folder"></i> Master Status Kepegawaian</a></li>
                </ul>
            </li>
            <li @if(isset($menu)) @if($menu=='artikel') class="active" @endif @endif><a href="{{ url('artikel') }}"><i class="fa fa-newspaper-o"></i> Artikel</a></li>
            <li @if(isset($menu)) @if($menu=='dokumen') class="active" @endif @endif><a href="{{ url('dokumen') }}"><i class="fa fa-file"></i> Dokumen</a></li>
            <li @if(isset($menu)) @if($menu=='log') class="active" @endif @endif><a href="{{ url('log_aktifitas') }}"><i class="fa fa-clock-o"></i> Log Aktifitas</a></li>
            <li @if(isset($menu)) @if($menu=='struktur_organisasi') class="active" @endif @endif><a href="{{ url('struktur_organisasi') }}"><i class="fa fa-university"></i> Struktur Organisasi</a></li>
            @endif
            <li @if(isset($menu)) @if($menu=='akun') class="active" @endif @endif><a href="{{ url('akun') }}"><i class="fa fa-user"></i> Akun</a></li>
            <li @if(isset($menu)) @if($menu=='pegawai') class="active" @endif @endif><a href="{{ url('pegawai') }}"><i class="fa fa-users"></i> Pegawai</a></li>
            <li>
                <a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Log Out</a>
            </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
