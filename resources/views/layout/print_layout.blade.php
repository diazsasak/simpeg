<!DOCTYPE html>
<html ng-app="simpegApp" lang="id-ID">
<head>
    @yield('meta')
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>

<!-- Bootstrap 3.3.4 -->
<link href="{{ asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/AdminLTE/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/AdminLTE/plugins/datepicker/datepicker3.css') }}" rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->

<link href="{{ asset('outsource/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('outsource/css/ionicons.min.css') }}" rel="stylesheet" type="text/css" />

<!-- Theme style -->
<link href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('bower_components/AdminLTE/dist/css/skins/skin-green-light.min.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('bower_components/angular-xeditable/dist/css/xeditable.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('bower_components/angular-loading-bar/src/loading-bar.css') }}" rel="stylesheet" type="text/css" >
<link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
<link href="{{ asset('css/bkpsdm-v1.css') }}" rel="stylesheet" type="text/css" />
<!-- Our Css -->
@yield('head')
<style type="text/css">
[ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
    display: none !important;
}
.datepicker {
    z-index:1151 !important;
}
        /*.popover-wrapper
        { position: relative; z-index: 99 !important; }*/

        .maximized {
            z-index: 9999;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
        }
    </style>
</head>
<body class="skin-green-light">
    <div class="wrapper">
        @yield('content')
    </div>
    <!-- REQUIRED JS SCRIPTS -->
    <!-- jQuery-->
    <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/plugins/fastclick/fastclick.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/plugins/input-mask/jquery.inputmask.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/dist/js/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <!-- AngularJS -->
    <script src="{{ asset('bower_components/angular/angular.min.js') }}" type="text/javascript"></script>
    @yield('up-script')
    <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-ui-mask/dist/mask.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-confirm-modal/angular-confirm.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-loading-bar/build/loading-bar.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('bower_components/angular-ui-tinymce/src/tinymce.js') }}"></script>
    <script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/angular-xeditable/dist/js/xeditable.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/controllers.js') }}" type="text/javascript"></script>
    <script src="{{ asset('app/controllers/search.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2();
            $('.datepicker').datepicker({
                autoclose: true,
                locale: 'no',
                format: 'yyyy-mm-dd'
            });
            $(".yearpicker").datepicker( {
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('[data-toggle="tooltip"]').on('click', function () {
                $('*').tooltip('hide')
            })
            $("[data-mask]").inputmask();
        });
    </script>
    @yield('script')
</body>
</html>
