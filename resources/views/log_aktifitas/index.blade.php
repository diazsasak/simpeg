@extends('layout.layout')
@section('title')
Log Aktifitas - eKepegawaian Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<section class="content-header">
	<h1>
		<i class="fa fa-clock-o"></i>
		Log Aktifitas
		<small>Manajemen Log Aktifitas</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Log Aktifitas</li>
	</ol>
</section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body table-responsive no-padding">
					<table class="table table-hover table-striped">
						<tr class="info">
							<th>Tipe Data</th>
							<th>Atribut</th>
							<th>Deskripsi</th>
							<th>Pengguna</th>
							<th>Tanggal</th>
						</tr>
						@foreach($data as $a)
						<tr>
							<td>{{ substr($a->subject_type, 4, strlen($a->subject_type)) }}</td>
							<td>
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$a->id}}">Detail</a>
							</td>
							<td>{{ $a->description }}</td>
							<td>{{ $a->causer->username }}</td>
							<td>{{ $a->created_at }}</td>
						</tr>
						<tr id="collapseOne{{$a->id}}" class="panel-collapse collapse">
							<td colspan="5">
								@foreach($a->properties as $b => $c)
								<b>{{$b}}</b> :  <br/>
								@foreach($c as $d => $e)
								{{$d}} : {{$e}} <br/>
								@endforeach
								@endforeach
							</td>
						</tr>
						@endforeach
					</table>
					<center>{{ $data->links() }}</center>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
