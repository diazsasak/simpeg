@extends('layout.beranda')
@section('title')
Beranda - bkpsdm Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')
<div style="background-color: black; color: white; padding-top: 5px">
    <marquee onmouseover="this.stop();" onmouseout="this.start();">
        @if(!count($running_text))
        Tidak ada Info.
        @else
        @foreach($running_text as $a)
        <a href="{{url('public-artikel-detail/'.$a->url)}}" style="color: white">{{$a->judul}}</a> &nbsp; ~  &nbsp; 
        @endforeach
        @endif
    </marquee>
</div>
<div class="jumbotron sh-top-jumbotron">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
            
        @foreach($slider as $index => $v)

        <div class="item @if($index==0) active @endif">
          <img src="{{asset($v->gambar_path)}}" alt="{{$v->judul}}" style="width:100%;">
        </div>
      @endforeach
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>

<div class="container sh-center-main-box">
    <div class="row">
        <div class="col-md-5">
            <h4 class="sh-title-one"><i class="fa fa-rss-square" aria-hidden="true"></i> Berita</h4>

            <div class="col-md-12 sh-box-one">
                @if(!count($berita))
                Tidak ada berita.
                @else
                @foreach($berita as $a)
                <p><i class="fa fa-rss-square" aria-hidden="true"></i> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                @endforeach
                @endif
            </div>

        </div>
        <div class="col-md-4">
            <h4 class="sh-title-one"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download</h4>
            <div class="col-md-12 sh-box-one">
                @if(!count($dokumen))
                Tidak ada dokumen.
                @else
                @foreach($dokumen as $a)
                <p><i class="fa fa-cloud-download" aria-hidden="true"></i> <a href="{{url($a->document_path)}}">{{$a->judul}}</a></p>
                @endforeach
                @endif
            </div>

        </div>
        <div class="col-md-3">
            <h4 class="sh-title-one"><i class="fa fa-volume-up" aria-hidden="true"></i> Pengumuman</h4>
            <div class="col-md-12 sh-box-one">
                @if(!count($pengumuman))
                Tidak ada pengumuman.
                @else
                @foreach($pengumuman as $a)
                <p><i class="fa fa-volume-up" aria-hidden="true"></i> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
<div class="jumbotron sh-search">
    <div class="container">
        <h2 class="sh-title-three">Cari Pegawai</h2>
        <section class="content-header" ng-controller="SearchCtrl">
            <div class="row">
                <div class="col-xs-12">
                    <form action="#" method="get" class="sidebar-form" style="margin: 0px;">
                        <div class="input-group" style="width: 100%">
                            <input type="text" name="q" class="form-control" placeholder="Cari berdasarkan nip atau nama pegawai..." ng-model="searchQuery" ng-change="globalSearch(searchQuery)" autocomplete="off" style="border-radius: 5px">
                        </div>
                    </form>
                    <div class="box" ng-hide="!searchPegawai||!searchPegawai.length" style="background: white; border-radius: 5px; padding: 10px" ng-cloak>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <h4>Pegawai</h4>
                            <div ng-repeat="a in searchPegawai">
                                        <a href="{{ url('pegawai/public-detail/') }}/@{{a.id}}" target="_blank">@{{a.nip+' '+a.nama+' '+a.tempat_lahir+' '+a.alamat_sekarang+' '+a.asal_kabupaten_kota+' '+a.asal_kecamatan+' '+a.asal_alamat_lengkap}} </a><br><br>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>
</div>
<section style="background: url({{asset('images/bg_depan.jpg')}});">
<div class="container" style="text-align: center;padding-bottom: 30px;">
    <h2 style="text-align: center;margin-bottom: 40px;color: orange">Visi</h2>
    The standard Lorem Ipsum passage, used since the 1500s

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    <br>
    
    <h2 style="text-align: center;margin-bottom: 40px;color: orange">Misi</h2>
    Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC

"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

</div>
</section>
<div class="container">
    <h2 style="text-align: center;margin-bottom: 40px;color: orange">Struktur Organisasi</h2>
    <div class="row sh-organisasi">
        <div class="col-md-4 col-md-offset-4">
            @if($so[0]->belongsToPegawai()->exists())
            <img src="{{ asset($so[0]->belongsToPegawai->poto) }}" class="img-rounded" height="90" />
            @else
            <img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" height="90" />
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>{{ $so[0]->nama_jabatan }}</h4>
                    @if($so[0]->belongsToPegawai()->exists())
                    <h5>{{ $so[0]->belongsToPegawai->nama }}</h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row sh-organisasi">
          <div class="col-md-2">
                
          </div>
        <div class="col-md-2">
            @if($so[1]->belongsToPegawai()->exists())
            <img src="{{ asset($so[1]->belongsToPegawai->poto) }}" class="img-rounded" height="90" />
            @else
            <img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" height="90" />
            @endif
            <div class="form-group">
                <h4>{{ $so[1]->nama_jabatan }}</h4>
                @if($so[1]->belongsToPegawai()->exists())
                <h5>{{ $so[1]->belongsToPegawai->nama }}</h5>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            @if($so[2]->belongsToPegawai()->exists())
            <img src="{{ asset($so[2]->belongsToPegawai->poto) }}" class="img-rounded" height="90" />
            @else
            <img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" height="90" />
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>{{ $so[2]->nama_jabatan }}</h4>
                    @if($so[2]->belongsToPegawai()->exists())
                    <h5>{{ $so[2]->belongsToPegawai->nama }}</h5>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2">
            @if($so[3]->belongsToPegawai()->exists())
            <img src="{{ asset($so[3]->belongsToPegawai->poto) }}" class="img-rounded" height="90" />
            @else
            <img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" height="90" />
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>{{ $so[3]->nama_jabatan }}</h4>
                    @if($so[3]->belongsToPegawai()->exists())
                    <h5>{{ $so[3]->belongsToPegawai->nama }}</h5>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-2">
            @if($so[4]->belongsToPegawai()->exists())
            <img src="{{ asset($so[4]->belongsToPegawai->poto) }}" class="img-rounded" height="90" />
            @else
            <img src="{{ asset('images/default_poto_pegawai.png') }}" class="img-rounded" height="90" />
            @endif
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>{{ $so[4]->nama_jabatan }}</h4>
                    @if($so[4]->belongsToPegawai()->exists())
                    <h5>{{ $so[4]->belongsToPegawai->nama }}</h5>
                    @endif
                </div>
            </div>
        </div>
          <div class="col-md-2">
                
          </div>
    </div>

</div>

<footer class="main-footer hidden-print"  style="background: url({{asset('images/bg_head.jpg')}}) center no-repeat; min-height: 100px;">
      <div class="container">
      <div class="col-md-7">
            <!-- Default to the left -->
        <strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
    
      </div>
            <div class="col-md-5" style="text-align: right;">
                  Alamat Kantor: Jl. MT. Haryono No.15 Selong
                  <br>
                  Email: bkdlotim@gmail.com
                  <br>
                  Telp: (0376)-21081/21091
            </div>
      </div>
</footer>
@endsection
@section('script')
<!-- AngularJS -->
<script src="{{ asset('bower_components/angular/angular.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-ui-mask/dist/mask.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-confirm-modal/angular-confirm.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-loading-bar/build/loading-bar.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('bower_components/angular-ui-tinymce/src/tinymce.js') }}"></script>
<script src="{{ asset('bower_components/angular-messages/angular-messages.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/angular-xeditable/dist/js/xeditable.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers.js') }}" type="text/javascript"></script>
<script src="{{ asset('app/controllers/search.js') }}" type="text/javascript"></script>
@endsection
