@extends('layout.beranda')
@section('title')
{{$detail_artikel->judul}} - bkpsdm Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')


<div class="container sh-center-main-box" style="min-height: 400px;">
      <div class="row">
            <div class="col-md-9" style="word-wrap: break-word;">
                  <h3 class="sh-title-two"> {{$detail_artikel->judul}}</h3>
                  <span class="label label-success"><i class="fa fa-user margin-r-5"></i>@if($detail_artikel->instansi_on_created=='-')  Superadmin  @else Penulis: {{ $detail_artikel->user->pegawai->nama }}, {{ $detail_artikel->instansi_on_created }} @endif</span>
                  <span class="label label-warning"><i class="fa fa-clock-o margin-r-5"></i>{{$detail_artikel->updated_at}}</span>
                  <br><br>
                  {!!$detail_artikel->deskripsi !!}
            </div>

            <div class="col-md-3">
                  <div class="row">
                        <div class="col-md-12">
                              <h4 class="sh-title-one"><i class="fa fa-volume-up" aria-hidden="true"></i> Pengumuman</h4>
                              @foreach($pengumuman as $a)
                              <p><i class="fa fa-volume-up" aria-hidden="true"></i> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                              @endforeach
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-12">
                              <h4 class="sh-title-one"><i class="fa fa-rss-square" aria-hidden="true"></i> Berita</h4>
                              @foreach($berita as $a)
                              <p><i class="fa fa-rss-square" aria-hidden="true"></i> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                              @endforeach
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-12">
                              <h4 class="sh-title-one"><i class="fa fa-cloud-download" aria-hidden="true"></i> Download</h4>
                              @foreach($dokumen as $a)
                              <p><i class="fa fa-cloud-download" aria-hidden="true"></i> <a href="{{url($a->document_path)}}">{{$a->judul}}</a></p>
                              @endforeach
                        </div>
                  </div>
            </div>
      </div>
</div>

<footer class="main-footer hidden-print"  style="background: url({{asset('images/bg_head.jpg')}}) center no-repeat; min-height: 100px;">
      <div class="container">
      <div class="col-md-7">
            <!-- Default to the left -->
        <strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
    
      </div>
            <div class="col-md-5" style="text-align: right;">
                  Alamat Kantor: Jl. MT. Haryono No.15 Selong
                  <br>
                  Email: bkdlotim@gmail.com
                  <br>
                  Telp: (0376)-21081/21091
            </div>
      </div>
</footer>
@endsection
@section('script')
@endsection
