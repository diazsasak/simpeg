@extends('layout.beranda')
@section('title')
Semua Berita, Pengumuman, Dan Download - bkpsdm Pemerintah Kabupaten Lombok Timur
@endsection
@section('content')


<div class="container sh-center-main-box">
      <div class="row">
            <div class="col-md-12" style="text-align: center;">
                  <h3 class="sh-title-two"> Semua Pengumuman</h3>

            </div>
            @foreach($pengumuman as $a)
            <div class="col-md-3">
                  <div class="col-md-12 sh-box-detail">
                        <img src="{{ asset($a->gambar_path) }}" class="img-responsive">
                        <p> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                  </div>
            </div>
            @endforeach
            <div class="col-md-12" style="text-align: center;">
                  {{ $pengumuman->render() }}
            </div>
            <hr>
      </div>

      <div class="row">
            <div class="col-md-12" style="text-align: center;">
                  <h3 class="sh-title-two"> Semua Berita</h3>

            </div>
            @foreach($berita as $a)
            <div class="col-md-3">
                  <div class="col-md-12 sh-box-detail">
                  <img src="{{$a->gambar_path}}" class="img-responsive">
                  <p> <a href="{{url('public-artikel-detail/'.$a->url)}}">{{$a->judul}}</a></p>
                  </div>
            </div>
            @endforeach
            <div class="col-md-12" style="text-align: center;">
                  {{ $berita->render() }}
            </div>
      </div>

      <div class="row">
            <div class="col-md-12" style="text-align: center;">
                  <h3 class="sh-title-two"> Semua Dokumen Download</h3>

            </div>
            @foreach($dokumen as $a)
            <div class="col-md-3">
                  <div class="col-md-12 sh-box-detail">
                        <p><i class="fa fa-cloud-download" aria-hidden="true"></i> <a href="{{url($a->document_path)}}">{{$a->judul}}</a></p>
                  </div>
            </div>
            @endforeach
            <div class="col-md-12" style="text-align: center;">
                  {{ $dokumen->render() }}
            </div>
      </div>
</div>

<footer class="main-footer hidden-print"  style="background: url({{asset('images/bg_head.jpg')}}) center no-repeat; min-height: 100px;">
      <div class="container">
      <div class="col-md-7">
            <!-- Default to the left -->
        <strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
    
      </div>
            <div class="col-md-5" style="text-align: right;">
                  Alamat Kantor: Jl. MT. Haryono No.15 Selong
                  <br>
                  Email: bkdlotim@gmail.com
                  <br>
                  Telp: (0376)-21081/21091
            </div>
      </div>
</footer>
@endsection
@section('script')
@endsection
