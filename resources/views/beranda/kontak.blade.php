@extends('layout.beranda')
@section('title')
Kontak Kami
@endsection
@section('content')
<div class="container sh-center-main-box">
    <div class="row">
        <div class="col-xs-12" style="margin: 20px">
            <!-- <center>
            	<h4>Badan Kepegawaian dan Pengembangan Sumber Daya Manusia (BKPSDM)</h4>
        	</center> -->
        </div>
		<div class="col-md-12 col-lg-6 col-lg-offset-3">
        	<div class="box box-success box-solid">
				<div class="box-header">
					<h3 class="box-title">Badan Kepegawaian dan Pengembangan Sumber Daya Manusia (BKPSDM)</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<table class="table table-hover">
						<tr>
							<td><i class="fa fa-map"></i></td>
							<td>Alamat</td>
							<td>Jl. MT. Haryono No.15 Selong</td>
						</tr>
						<tr>
							<td><i class="fa fa-envelope"></i></td>
							<td>Email</td>
							<td>bkdlotim@gmail.com</td>
						</tr>
						<tr>
							<td><i class="fa fa-phone"></i></td>
							<td>Telepon</td>
							<td>(0376)-21081/21091</td>
						</tr>
						<tr>
							<td colspan="3">
								<iframe
								width="100%"
								height="450"
								frameborder="0" style="border:0"
								src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDv9903nSabLwkqayi5EvyXdEFuaqzAInc
								&q=BKPSDM+LOMBOK+TIMUR" allowfullscreen>
							</iframe>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
    </div>
</div>

<footer class="main-footer hidden-print"  style="background: url({{asset('images/bg_head.jpg')}}) center no-repeat; min-height: 100px;">
      <div class="container">
      <div class="col-md-7">
            <!-- Default to the left -->
        <strong><font class="hidden-xs">Copyright</font> © <font class="hidden-xs">2017</font> <a href="http://127.0.0.1:8000/about">eKepegawaian Pemerintah Kabupaten Lombok Timur</a></strong>
    
      </div>
            <div class="col-md-5" style="text-align: right;">
                  Alamat Kantor: Jl. MT. Haryono No.15 Selong
                  <br>
                  Email: bkdlotim@gmail.com
                  <br>
                  Telp: (0376)-21081/21091
            </div>
      </div>
</footer>
@endsection
@section('script')
@endsection
